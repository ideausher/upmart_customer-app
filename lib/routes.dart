import 'package:flutter/material.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/coupon_listing/coupon_routes.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/page/categories_shop_page.dart';
import 'package:user/modules/favourite_shop/page/favourite_shop_page.dart';
import 'package:user/modules/stripe/customer/stripe_customer_routes.dart';

import 'modules/auth/auth_routes.dart';

import 'modules/dashboard/page/dashboard_screen.dart';
import 'modules/dashboard/sub_modules/cart/page/cart_page.dart';
import 'modules/dashboard/sub_modules/category_shop/category_shop_routes.dart';
import 'modules/dashboard/sub_modules/profile/user_profile_routes.dart';
import 'modules/faq/page/faq_page.dart';

import 'modules/help_and_support/queries/screens/query_listing_page.dart';
import 'modules/help_and_support/start_new_query/screens/send_query_page.dart';
import 'modules/invite_friends/page/invite_friends_page.dart';
import 'modules/notification/screens/notification/page/notification_page.dart';
import 'modules/orders/order_routes.dart';

import 'modules/payment_succesful/page/payment_successful_page.dart';
import 'modules/stripe/customer/page/stripe_add_card_page.dart';
import 'modules/stripe/customer/page/stripe_customer_card_listing.dart';

class Routes {
  static const String DASH_BOARD = '/DASH_BOARD';
  static const String FEEDBACK = '/FEEDDBACK';
  static const String NOTIFICATION = '/NOTIFICATION';
  static const String START_SCREENS = '/START_SCREENS';
  static const String CARD_LISTING = '/card_listing';
  static const String ADD_CARD = '/add_card';
  static const String CART_PAGE = '/cart_page';
  static const String PAYMENT_SUCCESSFUL = '/payment_successful';
  static const String CATEGORIES_SHOP_PAGE = '/categories_shop_page';
  static const String FAQ_PAGE = '/FAQ_PAGE';
  static const String QUERY_LISTING_PAGE = '/QUERY_LISTING_PAGE';
  static const String SEND_QUERY_PAGE = '/SEND_QUERY_PAGE';
  static const String FAV_SHOP_LIST = '/FAV_SHOP_LIST';
  static const String REFERRAL_PAGE = '/REFERRAL_PAGE';

  static Map<String, WidgetBuilder> routes() {
    Map<String, WidgetBuilder> route = {
      // When we navigate to the "/" route, build the FirstScreen Widget
      DASH_BOARD: (context) => DashboardScreen(context),
      //START_SCREENS: (context) => StartScreenPage(context),
      CARD_LISTING: (context) => StripeCustomerCardListingPage(context),
      ADD_CARD: (context) => StripeAddCartPage(context),
      CART_PAGE: (context) => CartPage(context: context),
      PAYMENT_SUCCESSFUL: (context) => PaymentSuccessFullPage(context:context,),
      CATEGORIES_SHOP_PAGE: (context) => CategoriesShopPage(context),
      FAQ_PAGE: (context) => FaqPage(context: context,),
      QUERY_LISTING_PAGE: (context) => QueriesListingPage(context),
      SEND_QUERY_PAGE: (context) => SendQueryPage(context),
      NOTIFICATION: (context) => NotificationPage(context),
      FAV_SHOP_LIST: (context) => FavShopListing(context),
      REFERRAL_PAGE: (context) => InviteFriendsPage(context)
      /*FEEDBACK: (context) => FeedBackPage(),
    ,*/
    };
    route.addAll(AuthRoutes.routes());

    route.addAll(UserProfileRoutes.routes());
    route.addAll(CategoryShopRoutes.routes());
    route.addAll(AddressRoutes.routes());
    route.addAll(CouponRoutes.routes());
    route.addAll(OrderRoutes.routes());
    route.addAll(StripeCustomerRoutes.routes());

    return route;
  }
}

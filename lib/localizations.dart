import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_sheet_localization/flutter_sheet_localization.dart';
part 'localizations.g.dart';

//@SheetLocalization("1SOKzFFdEcm7nZ_qoMgIW3cB_qCVDQzQDH0uYuyq7tI8","0") // <- See 1. to get DOCID and SHEETID
@SheetLocalization("1KLVz2SENm6zcb-ROj88xfivJ_Ddbg_OdfBYzBTTxnt0", "0") // <- See 1. to get DOCID and SHEETID
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => AppLocalizations.languages.containsKey(locale);

  @override
  Future<AppLocalizations> load(Locale locale) => SynchronousFuture<AppLocalizations>(AppLocalizations(locale));

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;

}

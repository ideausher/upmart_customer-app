import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/auth/constants/image_constant.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/routes.dart';

class PaymentSuccessFullPage extends StatefulWidget {
  BuildContext context;


  PaymentSuccessFullPage({this.context}) {

  }

  @override
  _PaymentSuccessFullPageState createState() => _PaymentSuccessFullPageState();
}

class _PaymentSuccessFullPageState extends State<PaymentSuccessFullPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), () async {
      var _currentLocation = await FetchPrefsUtils.fetchPrefsUtilsInstance.getCurrentLocationModel();
      NavigatorUtils.navigatorUtilsInstance
          .navigatorClearStack(
          context, Routes.DASH_BOARD, dataToBeSend: _currentLocation);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height:
      CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: context, percentage: SIZE_100, ofWidth: false),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(LOGIN_SUCCESSFUL_LOGO,
              width: CommonUtils.commonUtilsInstance
                  .getPercentageSize(
                  context: context, percentage: SIZE_20, ofWidth: true)),
          Text(
           AppLocalizations.of(context).paymentsuccesspage.title.paymentSuccessfull,
            style: AppConfig
                .of(context)
                .themeData
                .textTheme
                .headline6,
          )
        ],
      ),
    );
  }
}

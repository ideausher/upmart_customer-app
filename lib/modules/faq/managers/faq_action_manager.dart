import 'package:flutter/material.dart';
import 'package:user/modules/faq/bloc/faq_bloc.dart';
import 'package:user/modules/faq/bloc/faq_event.dart';
import 'package:user/modules/faq/bloc/faq_state.dart';

class FaqActionManagers {
  BuildContext context;
  FaqBloc faqBloc = new FaqBloc();
  FaqState faqState;

  //action to update
  actionOnInItState() {
    faqBloc.emitEvent(ShowFaqDataEvent(
        context: context,
        faqResponseModel: faqState?.faqResponseModel,
        selectedIndex: faqState?.selectedIndex,
        isLoading: false));
  }

  //action to update selected index
  actionOnTabSelection({int selectedIndex}) {
    faqBloc.emitEvent(OnTabSelectedEvent(
        context: context,
        faqResponseModel: faqState?.faqResponseModel,
        selectedIndex: selectedIndex ?? faqState?.selectedIndex,
        isLoading: false));
  }
}

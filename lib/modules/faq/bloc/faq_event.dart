import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/faq/api/model/faq_response_model.dart';

abstract class FaqEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int selectedIndex;
  final FaqResponseModel faqResponseModel;
  FaqEvent({this.isLoading: false, this.context, this.selectedIndex, this.faqResponseModel});
}

// for showing data
class ShowFaqDataEvent extends FaqEvent {
  ShowFaqDataEvent({BuildContext context, bool isLoading, int selectedIndex, FaqResponseModel faqResponseModel})
      : super(context: context, isLoading: isLoading, selectedIndex: selectedIndex, faqResponseModel: faqResponseModel);
}

// for tab change event
class OnTabSelectedEvent extends FaqEvent {
  OnTabSelectedEvent({BuildContext context, bool isLoading, int selectedIndex, FaqResponseModel faqResponseModel})
      : super(context: context, isLoading: isLoading, selectedIndex: selectedIndex, faqResponseModel: faqResponseModel);
}

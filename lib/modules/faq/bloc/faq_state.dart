import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/faq/api/model/faq_response_model.dart';

class FaqState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final int selectedIndex;
  final FaqResponseModel faqResponseModel;

  FaqState({
    this.isLoading,
    this.context,
    this.selectedIndex,
    this.faqResponseModel
  }) : super(isLoading);

  // not authenticated
  factory FaqState.initiating({bool isLoading}) {
    return FaqState(
      isLoading: isLoading,
    );
  }

  factory FaqState.updateUi({
    bool isLoading,
    BuildContext context,
    int selectedIndex,
    FaqResponseModel faqResponseModel
  }) {
    return FaqState(
      isLoading: isLoading,
      context: context,
      selectedIndex: selectedIndex,
      faqResponseModel: faqResponseModel
    );
  }
}

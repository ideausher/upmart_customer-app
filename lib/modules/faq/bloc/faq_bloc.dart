import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/faq/api/model/faq_response_model.dart';
import 'package:user/modules/faq/api/provider/faq_api_provider.dart';
import 'package:user/modules/faq/bloc/faq_event.dart';
import 'package:user/modules/faq/bloc/faq_state.dart';

class FaqBloc extends BlocEventStateBase<FaqEvent, FaqState> {
  FaqBloc({bool isLoading = false}) : super(initialState: FaqState.initiating(isLoading: isLoading));

  @override
  Stream<FaqState> eventHandler(FaqEvent event, FaqState currentState) async* {
    // used for the select tab
    if (event is ShowFaqDataEvent) {
      String _message = "";
      FaqResponseModel _faqResponseModel;

      yield FaqState.updateUi(
          isLoading: true,
          context: event.context,
          selectedIndex: event.selectedIndex,
          faqResponseModel: event?.faqResponseModel);

      var result = await FaqApiProvider().getFaqData(
        context: event.context,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          _faqResponseModel = FaqResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield FaqState.updateUi(
          isLoading: false,
          context: event.context,
          selectedIndex: event.selectedIndex,
          faqResponseModel: _faqResponseModel);
    }

    // used for the faq tab
    if (event is OnTabSelectedEvent) {
      yield FaqState.updateUi(
          isLoading: false,
          context: event?.context,
          selectedIndex: event?.selectedIndex,
          faqResponseModel: event?.faqResponseModel);
    }
  }


}

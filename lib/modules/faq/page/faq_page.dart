import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/faq/api/model/faq_response_model.dart';
import 'package:user/modules/faq/bloc/faq_state.dart';
import 'package:user/modules/faq/managers/faq_action_manager.dart';
import 'package:intl/intl.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';

class FaqPage extends StatefulWidget {
  BuildContext context;

  FaqPage({this.context}) {
    this.context = context;
  }

  @override
  _FaqPageState createState() => _FaqPageState();
}

class _FaqPageState extends State<FaqPage> {
  FaqActionManagers _faqActionManagers = new FaqActionManagers();

  @override
  void initState() {
    super.initState();
    _faqActionManagers?.context = widget?.context;
    _faqActionManagers?.actionOnInItState();
  }

  @override
  void dispose() {
    super.dispose();
    _faqActionManagers?.faqBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _faqActionManagers?.context = widget?.context;
    return BlocEventStateBuilder<FaqState>(
      bloc: _faqActionManagers?.faqBloc,
      builder: (BuildContext buildContext, FaqState faqState) {
        if (_faqActionManagers?.faqState != faqState) {
          _faqActionManagers.context = context;
          _faqActionManagers.faqState = faqState;
        }
        // main ui started
        return ModalProgressHUD(
          inAsyncCall: _faqActionManagers?.faqState?.isLoading ?? false,
          child: Scaffold(
            appBar: _showAppBar(),
            body: Container(
              child: Padding(
                padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
                child: (_faqActionManagers
                            ?.faqState?.faqResponseModel?.data?.isNotEmpty ==
                        true)
                    ? _showTabsView()
                    : (_faqActionManagers?.faqState?.isLoading == false)
                        ? Center(
                            child: CommonImageWithTextWidget(
                              context: _faqActionManagers?.context,
                              localImagePath: EMPTY_FAQ_LOGO,
                              imageHeight: SIZE_5,
                              imageWidth: SIZE_5,
                              textToShow: AppLocalizations.of(_faqActionManagers?.context).common.text.noFaq,
                            ),
                          )
                        : const SizedBox(),
              ),
            ),
          ),
        );
      },
    );
  }

  //method to show dynamic tabs on the basis of api response
  Widget _showDynamicTabs(List<Question> questions) {
    return (questions?.isNotEmpty == true)
        ? Card(
          color: COLOR_LIGHT_RED,
          margin: EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SIZE_15),
          ),
          child: Padding(
            padding: const EdgeInsets.all(SIZE_10),
            child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) =>
                    Divider(
                      color: COLOR_BORDER_GREY,
                      thickness: SIZE_1_5,
                    ),
                itemCount: questions?.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  Question _question = questions[index];
                  return Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      childrenPadding: EdgeInsets.zero,
                      leading: Text(
                        AppLocalizations.of(context).faqpage.text.question(
                            value:
                                "${index + 1}.${_question?.question ?? ""}")
                        /* "Q${index + 1}. ${_question?.question ?? ""}"*/,
                        style: AppConfig.of(_faqActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline6,
                        textAlign: TextAlign.start,
                      ),
                      trailing: Icon(
                        (_question?.isExpanded == true)
                            ? Icons.expand_less
                            : Icons.expand_more,
                        size: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: _faqActionManagers?.context,
                                percentage: SIZE_3,
                                ofWidth: false),
                        color: COLOR_PRIMARY,
                      ),
                      title: const SizedBox(),
                      onExpansionChanged: (expanded) {
                        _question?.isExpanded = expanded;
                        _faqActionManagers?.actionOnTabSelection();
                      },
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              AppLocalizations.of(context)
                                  .faqpage
                                  .text
                                  .answer(
                                      value:
                                          ".${_question?.answer ?? ""}")
                              /* "Ans${index + 1}. ${_question?.answer ?? ""}"*/,
                              textAlign: TextAlign.justify,
                            )),
                        SizedBox(
                          height: SIZE_10,
                        )
                      ],
                    ),
                  );
                }),
          ),
        )
        : const SizedBox();
  }

  //show tab bar view
  Widget _showTabsView() {
    return DefaultTabController(
      length: _faqActionManagers?.faqState?.faqResponseModel?.data?.length,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TabBar(
            indicatorColor: COLOR_PRIMARY,
            isScrollable: true,
            indicatorSize: TabBarIndicatorSize.label,
            labelStyle: textStyleSize16WithGreenColor,
            unselectedLabelStyle: textStyleSize16WithGreyColor,
            labelColor: COLOR_PRIMARY,
            unselectedLabelColor: COLOR_BORDER_GREY,
            onTap: (index) {
              _faqActionManagers?.actionOnTabSelection(selectedIndex: index);
            },
            tabs: _faqActionManagers?.faqState?.faqResponseModel?.data
                ?.map((Datum faqData) => Tab(
                    text: toBeginningOfSentenceCase(faqData?.categoryName ??
                        AppLocalizations.of(context).common.text.notAllowed)))
                .toList(),
          ),
          Expanded(
            child: TabBarView(
              children: _faqActionManagers?.faqState?.faqResponseModel?.data
                  ?.map((Datum categoryData) {
                return _showDynamicTabs(categoryData?.questions);
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }

  //method to show app bar widget
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _faqActionManagers?.context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(context).profilepage.text.fAQs,
        appBarTitleStyle: AppConfig.of(_faqActionManagers?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        defaultLeadingIcon: Icons.arrow_back,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_16),
            child: NotificationReadCountWidget(
              context: _faqActionManagers?.context,
            ),
          )
        ],
        defaultLeadingIconColor: COLOR_BLACK,
        backGroundColor: Colors.transparent);
  }
}

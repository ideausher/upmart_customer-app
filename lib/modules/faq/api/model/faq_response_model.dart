// To parse this JSON data, do
//
//     final faqResponseModel = faqResponseModelFromJson(jsonString);

import 'dart:convert';

FaqResponseModel faqResponseModelFromJson(String str) => FaqResponseModel.fromJson(json.decode(str));

String faqResponseModelToJson(FaqResponseModel data) => json.encode(data.toJson());

class FaqResponseModel {
  FaqResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<Datum> data;
  String message;
  int statusCode;

  factory FaqResponseModel.fromJson(Map<String, dynamic> json) => FaqResponseModel(
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Datum {
  Datum({
    this.categoryId,
    this.categoryName,
    this.questions,
  });

  int categoryId;
  String categoryName;
  List<Question> questions;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    categoryId: json["category_id"] == null ? null : json["category_id"],
    categoryName: json["category_name"] == null ? null : json["category_name"],
    questions: json["questions"] == null ? null : List<Question>.from(json["questions"].map((x) => Question.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "category_id": categoryId == null ? null : categoryId,
    "category_name": categoryName == null ? null : categoryName,
    "questions": questions == null ? null : List<dynamic>.from(questions.map((x) => x.toJson())),
  };
}

class Question {
  Question({
    this.id,
    this.question,
    this.answer,
    this.isExpanded
  });

  int id;
  String question;
  String answer;
  bool isExpanded;

  factory Question.fromJson(Map<String, dynamic> json) => Question(
    id: json["id"] == null ? null : json["id"],
    question: json["question"] == null ? null : json["question"],
    answer: json["answer"] == null ? null : json["answer"],
    isExpanded: json["isExpanded"] == null ? null : json["isExpanded"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "question": question == null ? null : question,
    "answer": answer == null ? null : answer,
    "isExpanded": isExpanded == null ? null : isExpanded,
  };
}

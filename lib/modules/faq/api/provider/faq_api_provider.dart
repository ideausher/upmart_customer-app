import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';

class FaqApiProvider
{

  //get faq data api call
  Future<dynamic> getFaqData({BuildContext context}) async {
    var faq = "faqs";
    var result = await AppConfig.of(context).baseApi.getRequest(faq, context);
    return result;
  }


}
import 'package:flutter/material.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import '../../../modules/common/utils/common_utils.dart';
import '../../../modules/common/constants/color_constants.dart';
import '../../../modules/coupon_listing/manager/coupon_list_manager.dart';
import '../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_bloc.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_state.dart';
import '../../../localizations.dart';
import '../../../modules/common/app_config/app_config.dart';
import '../../../modules/common/constants/dimens_constants.dart';

class CouponListingPage extends StatefulWidget {
  BuildContext context;
  Function clickCallback;

  CouponListingPage({
    this.context,
    this.clickCallback,
  });

  @override
  _CouponListingPageState createState() => _CouponListingPageState();
}

class _CouponListingPageState extends State<CouponListingPage> {
  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  BuildContext _context;
  TextEditingController _promoCodeController;

  var _bloc = CouponListingBloc();
  CouponListingState _state;

  // initialise controllers
  ScrollController _scrollController;

  int _page = 1;
  int _limit = 10;

  CouponListManager _couponListManager = CouponListManager();

  @override
  void dispose() {
    _bloc?.dispose();
    _scrollController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _promoCodeController = TextEditingController();
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _couponListManager.actionToGetCouponList(
        context: widget.context,
        page: _page,
        scaffoldState: _scaffoldKey?.currentState,
        couponListingBloc: _bloc,
        couponList: new CouponListModel());
    super.initState();
  }

  // usd for the pagination
  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange &&
        (_state?.couponListModel?.data?.length == (_page * _limit))) {
      print("reach the bottom");
      _couponListManager.actionToGetCouponList(
          context: _context ?? widget.context,
          page: (_page != null) ? _page + 1 : 1,
          scaffoldState: _scaffoldKey?.currentState,
          couponListingBloc: _bloc,
          couponList: _state?.couponListModel);
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the top");
    }
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      key: _scaffoldKey,
      appBar: _showAppBar(),
      body: BlocEventStateBuilder<CouponListingState>(
        bloc: _bloc,
        builder: (BuildContext context, CouponListingState state) {
          if (state != null && _state != state) {
            _state = state;
            _page = _state?.page;
            _couponListManager.actionOnChatListScreenStateChange(
              context: context,
              scaffoldState: _scaffoldKey?.currentState,
              couponListingState: _state,
            );
          }
          return ModalProgressHUD(
              inAsyncCall: _state.isLoading,
              child: (_state?.couponListModel?.data?.isNotEmpty == true)
                  ? WillPopScope(
                      onWillPop: () {
                        return NavigatorUtils.navigatorUtilsInstance
                            .navigatorPopScreen(context, dataToBeSend: null);
                      },
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: SIZE_12, right: SIZE_12, bottom: SIZE_12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Divider(
                              color: COLOR_GREY,
                              thickness: SIZE_1,
                            ),
                            SizedBox(
                              height: SIZE_15,
                            ),
                            // _showAvailablePromoCodeText(),
                            SizedBox(
                              height: SIZE_10,
                            ),
                            _showCouponList(),
                          ],
                        ),
                      ),
                    )
                  : Visibility(
                      visible: _state?.isLoading == false,
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context).common.text.noDataFound,
                          style: AppConfig.of(context)
                              .themeData
                              .primaryTextTheme
                              .headline2,
                        ),
                      ),
                    ));
        },
      ),
    );
  }

//method to return a list of coupons
  Widget _showCouponList() {
    return Expanded(
      child: ListView.separated(
          controller: _scrollController,
          key: PageStorageKey("coupon_listing_key"),
          shrinkWrap: true,
          separatorBuilder: (BuildContext context, int index) => Divider(
                color: COLOR_BORDER_GREY,
              ),
          itemCount: _state?.couponListModel?.data?.length,
          itemBuilder: (BuildContext context, int index) {
            CouponData couponData = _state?.couponListModel?.data[index];
            return Padding(
              padding: const EdgeInsets.only(
                  left: SIZE_5, right: SIZE_5, top: SIZE_2, bottom: SIZE_2),
              child: Column(
                children: [
                  _showCouponImage(couponData),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            _showImageAndTitle(couponData),
                            SizedBox(
                              height: SIZE_5,
                            ),
                            _couponDescriptionText(couponData),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: SIZE_10),
                          child: InkWell(
                            onTap: () {
                              _couponListManager?.actionOnApplyCouponTap(
                                  context: _context,
                                  state: _state,
                                  couponData: couponData,
                                  couponListingBloc: _bloc);
                              // DialogSnackBarUtils?.dialogSnackBarUtilsInstance?.showAlert(context, datum);
                            },
                            child: Text(
                              AppLocalizations.of(context).couponpage.text.use.toUpperCase(),
                              style: textStyleSize16WithGreenColor,
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }

//method to return a row as image and title
  Widget _showImageAndTitle(CouponData couponList) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          couponList?.name?.toUpperCase() ?? "",
          style: AppConfig.of(_context).themeData.primaryTextTheme.headline6,
          textAlign: (couponList?.name?.isNotEmpty == true)
              ? TextAlign.end
              : TextAlign.start,
        ),
      ],
    );
  }

//method to return a text of coupon description
  Widget _couponDescriptionText(CouponData couponList) {
    return Visibility(
      visible: couponList?.description?.isNotEmpty == true,
      child: Text(
        couponList?.description ?? "",
        style: textStyleSize14WithGREY,
        maxLines: 3,
      ),
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: false,
        appBarTitle:AppLocalizations.of(context).couponpage.appbartitle.promosAvailable,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(context, dataToBeSend: null);
        },
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all( SIZE_16),
            child: NotificationReadCountWidget(
              context: _context,
            ),
          )
        ],
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
    ;
  }

  Widget _showCouponImage(CouponData datum) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
        context: context,
        showProgressBarInPlaceHolder: true,
        radius: SIZE_0,
        shape: BoxShape.rectangle,
        url: datum?.couponImage?.fileName ?? "",
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, percentage: SIZE_10, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, percentage: SIZE_100, ofWidth: true));
  }
}

import 'package:flutter/material.dart';
import '../../modules/coupon_listing/page/coupon_listing_page.dart';


class CouponRoutes {
  static const String COUPON_LISTING_SCREEN = '/coupon_listing_screen';


  static Map<String, WidgetBuilder> routes() {
    return {
      // When we navigate to the "/" route, build the FirstScreen Widget
      COUPON_LISTING_SCREEN: (context) => CouponListingPage(context: context,),

    };
  }
}

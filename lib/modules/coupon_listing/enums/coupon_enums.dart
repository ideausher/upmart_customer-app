enum CouponType { Percent, Fixed }


extension CouponTypeExtension on CouponType {
  int get value {
    switch (this) {
      case CouponType.Fixed:
        return 1;
      case CouponType.Percent:
        return 0;
      default:
        return null;
    }
  }
}
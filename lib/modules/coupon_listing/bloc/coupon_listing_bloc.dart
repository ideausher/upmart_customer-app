import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_request_model.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_response_model.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import '../../../localizations.dart';
import '../../../modules/common/enum/enums.dart';
import '../../../modules/coupon_listing/api/coupon_list/provider/coupon_list_provider.dart';
import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_event.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_state.dart';

class CouponListingBloc extends BlocEventStateBase<CouponListingEvent, CouponListingState> {
  CouponListingBloc({bool initializing = true}) : super(initialState: CouponListingState.initiating());

  @override
  Stream<CouponListingState> eventHandler(CouponListingEvent event, CouponListingState currentState) async* {
    if (event is GetCouponListEvent) {
      yield CouponListingState.getCouponList(
        isLoading: true,
        message: "",
        page: event.page,
        couponListModel: event.couponList,
      );
      CouponListModel _responseCouponList = event?.couponList;
      String _message = '';
      int page = event?.page;
      // call api
      var result = await CouponListProvider().getCouponListApiCall(context: event.context, page: page);

      // check result calue
      if (result != null) {
        // create temp couponmodel list
        List<CouponData> _newCouponList = List();
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse result to list
          _responseCouponList = CouponListModel.fromJson(result);
          // check if the page is first and event.coupon list is null or empty
          if (event?.couponList == null || event?.couponList?.data?.isEmpty == true || event.page == 1) {
            // pass response list t temp value
            _newCouponList = _responseCouponList?.data;
          } else {
            // manage list here
            for (var _coupon in _responseCouponList?.data) {
              bool _couponContains = false;

              for (var _oldCoupon in event?.couponList?.data) {
                if (_oldCoupon.id == _coupon.id) {
                  _couponContains = true;
                }
              }

              if (_couponContains == false) {
                _newCouponList.add(_coupon);
              }
            }
          }

          List<CouponData> _oldCouponList = List();

          if (event.couponList?.data?.isNotEmpty == true && event.page != 1) {
            _oldCouponList = event.couponList?.data;
          }
          _oldCouponList?.addAll(_newCouponList);

          _responseCouponList?.data = _oldCouponList;

          // _message= result[ApiStatusParams.Message.value];
          page = event.page;


        } else {
          //  _message= result[ApiStatusParams.Message.value];
          page = (event.page > 1) ? event.page - 1 : event.page;

        }
      } else {
        _message = AppLocalizations.of(event.context).common.error.somethingWentWrong;
        page = (event.page > 1) ? event.page - 1 : event.page;


      }

      yield CouponListingState.getCouponList(
        isLoading: false,
        message: _message,
        page: page,
        couponListModel: _responseCouponList,
      );
    }

    //apply coupon api call
    if (event is ApplyCouponEvent) {
      String _message = "";
      ApplyCouponResponseModel _applyCouponResponseModel;
      ApplyCouponRequestModel _applyCouponRequestModel;
      yield CouponListingState.getCouponList(
        isLoading: true,
        message: "",
        page: event.page,
        couponData: event?.couponData,
        applyCouponResponseModel: event?.applyCouponResponseModel,
        applyCouponRequestModel: event?.applyCouponRequestModel,
        couponListModel: event.couponList,
      );

      _applyCouponRequestModel = new ApplyCouponRequestModel(
          couponCode: event?.couponData?.code,
          shopId: CartUtilsManager?.cartUtilsInstance?.shopDetailsModel?.id?.toString(),
          amount: CartUtilsManager?.cartUtilsInstance.cartAmount());

      var result = await CouponListProvider().applyCouponApiCall(
        context: event.context,
        applyCouponRequestModel: _applyCouponRequestModel,
      );
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse result to list
          _applyCouponResponseModel = ApplyCouponResponseModel.fromJson(result);
          print("coupon ${_applyCouponResponseModel?.data}");
        } else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield CouponListingState.getCouponList(
          isLoading: false,
          message: _message,
          page: event?.page,
          couponData: event?.couponData,
          couponListModel: event?.couponList,
          applyCouponRequestModel: event?.applyCouponRequestModel,
          applyCouponResponseModel: _applyCouponResponseModel);
    }
  }
}

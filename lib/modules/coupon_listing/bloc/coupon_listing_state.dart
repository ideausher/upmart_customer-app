import 'package:flutter/material.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_request_model.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_response_model.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';

import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

class CouponListingState extends BlocState {
  final bool isLoading;
  final BuildContext context;
  final String message;
  final CouponListModel couponListModel;
  final CouponData couponData;
  final int page;
  final ApplyCouponRequestModel applyCouponRequestModel;
  final ApplyCouponResponseModel applyCouponResponseModel;

  CouponListingState({
    this.isLoading,
    this.message,
    this.context,
    this.couponData,
    this.couponListModel,
    this.applyCouponRequestModel,
    this.applyCouponResponseModel,
    this.page,
  }) : super(isLoading);

  factory CouponListingState.getCouponList(
      {bool isLoading,
      String message,
      CouponListModel couponListModel,
      int page,
      ApplyCouponRequestModel applyCouponRequestModel,
      CouponData couponData,
      ApplyCouponResponseModel applyCouponResponseModel}) {
    return CouponListingState(
        isLoading: isLoading,
        message: message,
        couponListModel: couponListModel,
        page: page,
        couponData: couponData,
        applyCouponRequestModel: applyCouponRequestModel,
        applyCouponResponseModel: applyCouponResponseModel);
  }

  factory CouponListingState.initiating() {
    return CouponListingState(
      isLoading: false,
    );
  }
}

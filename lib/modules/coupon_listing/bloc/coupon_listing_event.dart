import 'package:flutter/cupertino.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_request_model.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_response_model.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';

import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class CouponListingEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int page;
  final CouponListModel couponList;
  final ApplyCouponRequestModel applyCouponRequestModel;
  final ApplyCouponResponseModel applyCouponResponseModel;
  final CouponData couponData;

  CouponListingEvent(
      {this.isLoading,
      this.context,
      this.page,
      this.couponList,
      this.couponData,
      this.applyCouponRequestModel,
      this.applyCouponResponseModel});
}

class GetCouponListEvent extends CouponListingEvent {
  GetCouponListEvent(
      {bool isLoading,
      BuildContext context,
      int page,
      CouponListModel couponList,
      CouponData couponData,
      ApplyCouponResponseModel applyCouponResponseModel,
      ApplyCouponRequestModel applyCouponRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            page: page,
            couponList: couponList,
            couponData: couponData,
            applyCouponResponseModel: applyCouponResponseModel,
            applyCouponRequestModel: applyCouponRequestModel);
}

class ApplyCouponEvent extends CouponListingEvent {
  ApplyCouponEvent(
      {bool isLoading,
      BuildContext context,
      int page,
      CouponListModel couponList,
      CouponData couponData,
      ApplyCouponResponseModel applyCouponResponseModel,
      ApplyCouponRequestModel applyCouponRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            page: page,
            couponList: couponList,
            couponData: couponData,
            applyCouponResponseModel: applyCouponResponseModel,
            applyCouponRequestModel: applyCouponRequestModel);
}

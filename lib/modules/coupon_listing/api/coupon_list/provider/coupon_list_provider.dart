import 'package:flutter/material.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/apply_coupon_request_model/apply_coupon_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import '../../../../common/app_config/app_config.dart';

class CouponListProvider {
  Future<dynamic> getCouponListApiCall({BuildContext context, int page}) async {
    print('The page is $page');
    var couponList = "getcoupon";
    var data = {
      "limit": 10,
      "page": page,
      "shop_id": CartUtilsManager.cartUtilsInstance.shopDetailsModel?.id?.toString(),
      "is_global": 1,
      "only_primary": 0
    };

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(couponList, context, queryParameters: data);

    return result;
  }

  //method to call apply coupon api
  Future<dynamic> applyCouponApiCall(
      {BuildContext context,
      ApplyCouponRequestModel applyCouponRequestModel}) async {
    var couponList = "coupon/apply";
    var result = await AppConfig.of(context).baseApi.getRequest(
        couponList, context,
        queryParameters: applyCouponRequestModel.toJson());

    return result;
  }
}

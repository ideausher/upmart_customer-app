// To parse this JSON data, do
//
//     final couponListModel = couponListModelFromJson(jsonString);

import 'dart:convert';

CouponListModel couponListModelFromJson(String str) => CouponListModel.fromJson(json.decode(str));

String couponListModelToJson(CouponListModel data) => json.encode(data.toJson());

class CouponListModel {
  CouponListModel({
    this.data,
  });

  List<CouponData> data;

  factory CouponListModel.fromJson(Map<String, dynamic> json) => CouponListModel(
    data: json["data"] == null ? null : List<CouponData>.from(json["data"].map((x) => CouponData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class CouponData {
  CouponData({
    this.id,
    this.name,
    this.code,
    this.couponImage,
    this.isPrimary,
    this.isGlobal,
    this.type,
    this.discount,
    this.description,
    this.minAmount,
    this.maxAmount,
    this.maximumCustomerUse,
    this.totalUse,
    this.startDate,
    this.endDate,
  });

  int id;
  String name;
  String code;
  CouponImage couponImage;
  int isPrimary;
  int isGlobal;
  int type;
  int discount;
  String description;
  int minAmount;
  int maxAmount;
  int maximumCustomerUse;
  int totalUse;
  DateTime startDate;
  DateTime endDate;

  factory CouponData.fromJson(Map<String, dynamic> json) => CouponData(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    code: json["code"] == null ? null : json["code"],
    couponImage: json["coupon_image"] == null ? null : CouponImage.fromJson(json["coupon_image"]),
    isPrimary: json["is_primary"] == null ? null : json["is_primary"],
    isGlobal: json["is_global"] == null ? null : json["is_global"],
    type: json["type"] == null ? null : json["type"],
    discount: json["discount"] == null ? null : json["discount"],
    description: json["description"] == null ? null : json["description"],
    minAmount: json["min_amount"] == null ? null : json["min_amount"],
    maxAmount: json["max_amount"] == null ? null : json["max_amount"],
    maximumCustomerUse: json["maximum_customer_use"] == null ? null : json["maximum_customer_use"],
    totalUse: json["total_use"] == null ? null : json["total_use"],
    startDate: json["start_date"] == null ? null : DateTime.parse(json["start_date"]),
    endDate: json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "code": code == null ? null : code,
    "coupon_image": couponImage == null ? null : couponImage.toJson(),
    "is_primary": isPrimary == null ? null : isPrimary,
    "is_global": isGlobal == null ? null : isGlobal,
    "type": type == null ? null : type,
    "discount": discount == null ? null : discount,
    "description": description == null ? null : description,
    "min_amount": minAmount == null ? null : minAmount,
    "max_amount": maxAmount == null ? null : maxAmount,
    "maximum_customer_use": maximumCustomerUse == null ? null : maximumCustomerUse,
    "total_use": totalUse == null ? null : totalUse,
    "start_date": startDate == null ? null : startDate.toIso8601String(),
    "end_date": endDate == null ? null : endDate.toIso8601String(),
  };
}

class CouponImage {
  CouponImage({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory CouponImage.fromJson(Map<String, dynamic> json) => CouponImage(
    id: json["id"] == null ? null : json["id"],
    modelId: json["model_id"] == null ? null : json["model_id"],
    modelType: json["model_type"] == null ? null : json["model_type"],
    fileName: json["file_name"] == null ? null : json["file_name"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "model_id": modelId == null ? null : modelId,
    "model_type": modelType == null ? null : modelType,
    "file_name": fileName == null ? null : fileName,
    "name": name == null ? null : name,
  };
}

// To parse this JSON data, do
//
//     final applyCouponRequestModel = applyCouponRequestModelFromJson(jsonString);

import 'dart:convert';

ApplyCouponRequestModel applyCouponRequestModelFromJson(String str) => ApplyCouponRequestModel.fromJson(json.decode(str));

String applyCouponRequestModelToJson(ApplyCouponRequestModel data) => json.encode(data.toJson());

class ApplyCouponRequestModel {
  ApplyCouponRequestModel({
    this.couponCode,
    this.amount,
    this.shopId,
  });

  String couponCode;
  num amount;
  String shopId;

  factory ApplyCouponRequestModel.fromJson(Map<String, dynamic> json) => ApplyCouponRequestModel(
    couponCode: json["coupon_code"] == null ? null : json["coupon_code"],
    amount: json["amount"] == null ? null : json["amount"],
    shopId: json["shop_id"] == null ? null : json["shop_id"],
  );

  Map<String, dynamic> toJson() => {
    "coupon_code": couponCode == null ? null : couponCode,
    "amount": amount == null ? null : amount,
    "shop_id": shopId == null ? null : shopId,
  };
}

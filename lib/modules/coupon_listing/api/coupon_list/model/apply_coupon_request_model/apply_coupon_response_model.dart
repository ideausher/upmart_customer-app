// To parse this JSON data, do
//
//     final applyCouponResponseModel = applyCouponResponseModelFromJson(jsonString);

import 'dart:convert';

import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';

ApplyCouponResponseModel applyCouponResponseModelFromJson(String str) =>
    ApplyCouponResponseModel.fromJson(json.decode(str));

String applyCouponResponseModelToJson(ApplyCouponResponseModel data) => json.encode(data.toJson());

class ApplyCouponResponseModel {
  ApplyCouponResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  CouponData data;
  String message;
  int statusCode;

  factory ApplyCouponResponseModel.fromJson(Map<String, dynamic> json) => ApplyCouponResponseModel(
        data: json["data"] == null ? null : CouponData.fromJson(json["data"]),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}


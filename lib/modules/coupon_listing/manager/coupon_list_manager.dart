import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';
import '../../../modules/common/utils/dialog_snackbar_utils.dart';
import '../../../modules/common/utils/network_connectivity_utils.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_bloc.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_event.dart';
import '../../../modules/coupon_listing/bloc/coupon_listing_state.dart';

class CouponListManager {
  //for get coupon list
  actionToGetCouponList(
      {BuildContext context,
      ScaffoldState scaffoldState,
      int page,
      CouponListingBloc couponListingBloc,
      CouponListModel couponList}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        couponListingBloc.emitEvent(
          GetCouponListEvent(context: context, isLoading: true, page: page, couponList: couponList),
        );
      }
    });
  }

  // for state change in coupon list screen
  actionOnChatListScreenStateChange({
    BuildContext context,
    ScaffoldState scaffoldState,
    CouponListingState couponListingState,
  }) {
    if ( couponListingState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (couponListingState?.applyCouponResponseModel?.statusCode == ApiStatus?.Success?.value) {
          if (couponListingState?.applyCouponResponseModel != null) {
            DialogSnackBarUtils?.dialogSnackBarUtilsInstance
                ?.showCouponAlert(context, couponListingState?.applyCouponResponseModel?.data);
          }
        } else if (couponListingState?.message?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: couponListingState?.message?.toString()?.trim(),
          );
        }
      });
    }
  }

  //this method will be called when user applies coupon from coupon listing
  actionOnApplyCouponTap({
    BuildContext context,
    CouponListingState state,
    CouponData couponData,
    ScaffoldState scaffoldState,
    CouponListingBloc couponListingBloc,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        couponListingBloc.emitEvent(
          ApplyCouponEvent(
              context: context,
              isLoading: true,
              page: state?.page,
              couponData: couponData,
              couponList: state?.couponListModel,
              applyCouponResponseModel: state?.applyCouponResponseModel,
              applyCouponRequestModel: state?.applyCouponRequestModel),
        );
      }
    });
  }
}

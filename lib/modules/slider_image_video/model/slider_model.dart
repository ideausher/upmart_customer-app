// To parse this JSON data, do
//
//     final sliderModel = sliderModelFromJson(jsonString);

import 'package:flutter/material.dart';

import '../../../modules/slider_image_video/enums/slider_image_video_enum.dart';

class SliderImageVideoModel {
  bool autoPlay = false;
  bool enlargeCenterPage = false;
  double aspectRation = 2;
  Axis scrollDirection;
  bool comingFromHome;

  List<SliderModel> sliderModelList = List<SliderModel>();

  SliderImageVideoModel(
      {this.autoPlay = true,
      this.enlargeCenterPage = true,
        this.comingFromHome=false,
      this.aspectRation = 2.0,
      this.scrollDirection = Axis.horizontal,
      this.sliderModelList});
}

class SliderModel {
  MediaType mediaType;
  String url;
  String videoThumbnail;
  String placeHolderImage;
  String titleText;
  String subTitleText;

  SliderModel(
      {this.mediaType, this.url, this.videoThumbnail, this.placeHolderImage, this.titleText, this.subTitleText});
}

// To parse this JSON data, do
//
//     final inviteFriendsResponseModel = inviteFriendsResponseModelFromJson(jsonString);

import 'dart:convert';

InviteFriendsResponseModel inviteFriendsResponseModelFromJson(String str) => InviteFriendsResponseModel.fromMap(json.decode(str));

String inviteFriendsResponseModelToJson(InviteFriendsResponseModel data) => json.encode(data.toJson());

class InviteFriendsResponseModel {
  InviteFriendsResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory InviteFriendsResponseModel.fromMap(Map<String, dynamic> json) => InviteFriendsResponseModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.referralCode,
  });

  String referralCode;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    referralCode: json["referral_code"] == null ? null : json["referral_code"],
  );

  Map<String, dynamic> toJson() => {
    "referral_code": referralCode == null ? null : referralCode,
  };
}

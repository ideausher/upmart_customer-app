import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';

class GetReferalAmountProvider {
  Future<dynamic> getReferalAmountApi(
      {BuildContext context,}) async {
    var confirmOtp = "code/referral";

    var result = await AppConfig.of(context).baseApi.getRequest(
          confirmOtp,
          context,
        );

    return result;
  }
}

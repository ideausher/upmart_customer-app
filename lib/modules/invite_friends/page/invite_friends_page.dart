import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share/share.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/invite_friends/bloc/invite_friends_bloc.dart';
import 'package:user/modules/invite_friends/bloc/invite_friends_event.dart';
import 'package:user/modules/invite_friends/bloc/invite_friends_state.dart';
import 'package:user/modules/invite_friends/constants/image_constant.dart';

import '../../../localizations.dart';

class InviteFriendsPage extends StatefulWidget {
  BuildContext context;

  InviteFriendsPage(this.context);

  @override
  _InviteFriendsPageState createState() => _InviteFriendsPageState();
}

class _InviteFriendsPageState extends State<InviteFriendsPage> {
  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  BuildContext _context;
  AuthState _authState = AuthState();
  InviteFriendsState _inviteFriendsState = InviteFriendsState();
  RenderBox box;

  var _authBloc;
  var _inviteFriendsBloc;

  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _inviteFriendsBloc = InviteFriendsBloc();
    box = widget?.context?.findRenderObject() as RenderBox;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _context = context;
        if (authState != null && _authState != authState) {
          _authState = authState;
          _context = context;
          if (_authState?.isLoading == false) {
            // connection check
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(
              context,
              showNetworkDialog: true,
            )
                .then((onValue) {
              if (onValue) {
                _inviteFriendsBloc.emitEvent(GetReferalAmount(
                  context: _context,
                  isLoading: true,
                ));
              }
            });
          }
        }
        return ModalProgressHUD(
            inAsyncCall: _authState?.isLoading ?? false,
            child: BlocEventStateBuilder<InviteFriendsState>(
              bloc: _inviteFriendsBloc,
              builder: (BuildContext context,
                  InviteFriendsState inviteFriendsState) {
                _context = context;

                if (inviteFriendsState != null &&
                    _inviteFriendsState != inviteFriendsState) {
                  _inviteFriendsState = inviteFriendsState;
                  _context = context;
                }
                return ModalProgressHUD(
                  inAsyncCall: _inviteFriendsState?.isLoading ?? false,
                  child: Scaffold(
                    key: _scaffoldKey,
                    appBar: CommonUtils.commonUtilsInstance.getAppBar(
                        context: _context,
                        elevation: SIZE_0,
                        backGroundColor: Colors.white,
                        popScreenOnTapOfLeadingIcon: false,
                        defaultLeadingIconColor: Colors.black,
                        appBarTitleStyle: AppConfig.of(_context)
                            .themeData
                            .primaryTextTheme
                            .headline3,
                        appBarTitle: AppLocalizations.of(_context)
                            .invitefriendspage
                            .text
                            .appBarTitle,
                        defaultLeadIconPressed: () {
                          NavigatorUtils.navigatorUtilsInstance
                              .navigatorPopScreen(_context);
                        }),
                    body: Container(
                      height: MediaQuery.of(context).size.height,
                      color: COLOR_LIGHT_RED,
                      child: SingleChildScrollView(
                        padding: EdgeInsets.all(SIZE_20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            //_showInviteFriendsCode(),
                            SizedBox(
                              height: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                      context: _context, percentage: SIZE_10),
                            ),
                            Container(
                              height: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                      context: _context, percentage: SIZE_60),
                              width: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                      context: _context,
                                      percentage: SIZE_60,
                                      ofWidth: true),
                              child: Image.asset(
                                INVITE_FRIENDS_CENTER_IMAGE,
                                fit: BoxFit.cover,
                              ),
                            ),
                            //titleText(),
                            SizedBox(
                              height: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                      context: _context, percentage: SIZE_20),
                            ),
                            Card(
                              color: Colors.white,
                              elevation: ELEVATION_05,
                              child: Padding(
                                padding: const EdgeInsets.all(SIZE_20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      AppLocalizations.of(_context)
                                          .invitefriendspage
                                          .text
                                          .refferalCode,
                                      style: AppConfig.of(context)
                                          .themeData
                                          .textTheme
                                          .headline3,
                                    ),
                                    const SizedBox(
                                      height: SIZE_10,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Row(
                                            children: <Widget>[
                                              _getImageView(context: _context),
                                              SizedBox(
                                                width: SIZE_20,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  (_inviteFriendsState
                                                              ?.inviteFriendsResponseModel
                                                              ?.data
                                                              ?.referralCode
                                                              ?.isNotEmpty ==
                                                          true)
                                                      ? _inviteFriendsState
                                                          ?.inviteFriendsResponseModel
                                                          ?.data
                                                          ?.referralCode
                                                      : AppLocalizations.of(
                                                              _context)
                                                          .invitefriendspage
                                                          .text
                                                          .code,
                                                  style: AppConfig.of(_context)
                                                      .themeData
                                                      .textTheme
                                                      .bodyText1,
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  if (_inviteFriendsState
                                                          ?.inviteFriendsResponseModel
                                                          ?.data
                                                          ?.referralCode
                                                          ?.isNotEmpty ==
                                                      true) {
                                                    Clipboard.setData(new ClipboardData(
                                                        text: _inviteFriendsState
                                                            ?.inviteFriendsResponseModel
                                                            ?.data
                                                            ?.referralCode));
                                                    DialogSnackBarUtils
                                                        .dialogSnackBarUtilsInstance
                                                        .showSnackbar(
                                                            context: _context,
                                                            scaffoldState:
                                                                _scaffoldKey
                                                                    ?.currentState,
                                                            message: AppLocalizations
                                                                    .of(_context)
                                                                .invitefriendspage
                                                                .text
                                                                .codeCopied);
                                                  }
                                                },
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: SIZE_20),
                                                  child: Image.asset(
                                                    COPY_CODE_IMAGE,
                                                    height: SIZE_20,
                                                    width: SIZE_20,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () async {
                                            if (_inviteFriendsState
                                                    ?.inviteFriendsResponseModel
                                                    ?.data
                                                    ?.referralCode
                                                    ?.isNotEmpty ==
                                                true) {
                                              await Share.share(
                                                  ' Your Referral Code:${_inviteFriendsState?.inviteFriendsResponseModel?.data?.referralCode}\n https://play.google.com/store/apps/details?id=com.customer.upmart',
                                                  subject: AppLocalizations.of(
                                                          _context)
                                                      .invitefriendspage
                                                      .text
                                                      .refferalCode);
                                            }
                                          },
                                          child: Text(
                                            AppLocalizations.of(_context)
                                                .invitefriendspage
                                                .text
                                                .share,
                                            style: AppConfig.of(context)
                                                .themeData
                                                .textTheme
                                                .headline3,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ));
      },
    );
  }

  // used for the share image view
  Widget _getImageView({
    BuildContext context,
  }) {
    return Container(
      height: SIZE_30,
      width: SIZE_30,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(SIZE_10),
        child: ImageUtils.imageUtilsInstance.showCacheNetworkImage(
            url: "", context: context, placeHolderImage: SHARE_CODE_IMAGE),
      ),
    );
  }
}

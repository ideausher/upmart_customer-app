import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/invite_friends/api/model/invite_friends_response_model.dart';

class InviteFriendsState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final InviteFriendsResponseModel inviteFriendsResponseModel;

  InviteFriendsState({
    this.isLoading,
    this.context,
    this.inviteFriendsResponseModel,
  }) : super(isLoading);

  // not authenticated
  factory InviteFriendsState.initiating({bool isLoading}) {
    return InviteFriendsState(
      isLoading: isLoading,
    );
  }

  factory InviteFriendsState.updateUi({
    bool isLoading,
    BuildContext context,
    InviteFriendsResponseModel inviteFriendsResponseModel,
  }) {
    return InviteFriendsState(
      isLoading: isLoading,
      context: context,
      inviteFriendsResponseModel: inviteFriendsResponseModel,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class InviteFriendsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;

  InviteFriendsEvent({
    this.isLoading: false,
    this.context,
  });
}


// for get referal amount
class GetReferalAmount extends InviteFriendsEvent {
  GetReferalAmount({
    BuildContext context,
    bool isLoading,
  }) : super(
    context: context,
    isLoading: isLoading,
  );
}
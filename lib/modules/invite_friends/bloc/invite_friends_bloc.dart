import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/invite_friends/api/model/invite_friends_response_model.dart';
import 'package:user/modules/invite_friends/api/provider/get_referal_amount_provider.dart';

import 'invite_friends_event.dart';
import 'invite_friends_state.dart';

class InviteFriendsBloc
    extends BlocEventStateBase<InviteFriendsEvent, InviteFriendsState> {
  InviteFriendsBloc({bool isLoading = false})
      : super(
            initialState: InviteFriendsState.initiating(isLoading: isLoading));

  @override
  Stream<InviteFriendsState> eventHandler(
      InviteFriendsEvent event, InviteFriendsState currentState) async* {
    // used for the get referal amount
    if (event is GetReferalAmount) {
      yield InviteFriendsState.updateUi(
        isLoading: true,
        context: event.context,
      );

      var _result = await GetReferalAmountProvider()
          .getReferalAmountApi(context: event.context);

      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          InviteFriendsResponseModel _inviteFriendsResponseModel =
              InviteFriendsResponseModel.fromMap(_result);

          // update UI
          yield InviteFriendsState.updateUi(
            isLoading: false,
            context: event.context,
            inviteFriendsResponseModel: _inviteFriendsResponseModel,
          );
        } else {
          // update UI
          yield InviteFriendsState.updateUi(
            isLoading: false,
            context: event.context,
          );
        }
      } else {
        // update UI
        yield InviteFriendsState.updateUi(
          isLoading: false,
          context: event.context,
        );
      }
    }
  }
}

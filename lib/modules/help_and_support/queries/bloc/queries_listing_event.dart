import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/help_and_support/queries/api/model/query_list_response_model.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

abstract class QueriesListingEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final String selectedLocation;
  final List<Query> listQuery;
  final int page;
  final Order order;

  QueriesListingEvent(
      {this.isLoading: false,
      this.context,
      this.order,
      this.selectedLocation,
      this.listQuery,
      this.page});
}

//Event to set the selected location of the user
class GetUserQueriesBookingList extends QueriesListingEvent {
  GetUserQueriesBookingList(
      {bool isLoading,
      BuildContext context,
      List<Query> listQuery,
      int page,
      Order order})
      : super(
            isLoading: isLoading,
            context: context,
            order: order,
            listQuery: listQuery,
            page: page);
}

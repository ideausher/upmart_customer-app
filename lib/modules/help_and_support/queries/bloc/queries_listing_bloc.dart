import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/help_and_support/queries/api/model/query_list_response_model.dart';
import 'package:user/modules/help_and_support/queries/api/provider/queries_provider.dart';
import 'package:user/modules/help_and_support/queries/bloc/queries_listing_event.dart';
import 'package:user/modules/help_and_support/queries/bloc/queries_listing_state.dart';
import '../../../../localizations.dart';

class QueriesListingBloc extends BlocEventStateBase<QueriesListingEvent, QueriesListingState> {
  QueriesListingBloc({bool initializing = true}) : super(initialState: QueriesListingState.initiating());

  @override
  Stream<QueriesListingState> eventHandler(QueriesListingEvent event, QueriesListingState currentState) async* {
    // update user Location
    if (event is GetUserQueriesBookingList) {
      yield QueriesListingState.updateUi(
          isLoading: true, context: event?.context, listQuery: event?.listQuery, page: event.page, order: event?.order);

      // api calling
      var result = await QueriesProvider().getQueriesList(
        context: event.context,
        page: event.page,
        order: event?.order
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<Query> list;
          List<Query> resultData =
              (result[ApiStatusParams.Data.value] as List).map((itemWord) => Query.fromJson(itemWord)).toList();


          // other pages
          if (event.listQuery?.isNotEmpty == true && event?.page != 1) {
            list = event.listQuery;

            if (resultData?.isNotEmpty == true) {
              // parse value
              list.addAll(resultData);
            }
          } else {
            // first page
            // parse value
            list = (result[ApiStatusParams.Data.value] as List).map((itemWord) => Query.fromJson(itemWord)).toList();
          }

          yield QueriesListingState.updateUi(
            isLoading: false,
            context: event?.context,
            listQuery: list,
            page: event.page,
          );
        }
        // failure case
        else {
          yield QueriesListingState.updateUi(
            isLoading: false,
            context: event?.context,
            message: (event?.page == 1) ? result[ApiStatusParams.Message.value] : "",
            listQuery: event?.listQuery,
            page: (event?.page > 1) ? event.page - 1 : event.page,
          );
        }
      } else {
        yield QueriesListingState.updateUi(
          isLoading: false,
          context: event?.context,
          message: (event?.page == 1) ? AppLocalizations.of(event.context).common.error.somethingWentWrong : "",
          listQuery: event?.listQuery,
          page: (event?.page > 1) ? event.page - 1 : event.page,
        );
      }
    }
  }
}

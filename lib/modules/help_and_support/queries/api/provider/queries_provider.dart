import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

class QueriesProvider {
  Future<dynamic> getQueriesList(
      {BuildContext context, int page, Order order}) async {
    var queryList = "support/detail";
    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(queryList, context, queryParameters: {
      "limit": 10,
      "page": page,
      "booking_id": order?.id,

    });

    return result;
  }
}

// To parse this JSON data, do
//
//     final queryListResponseModel = queryListResponseModelFromJson(jsonString);

import 'dart:convert';

import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

QueryListResponseModel queryListResponseModelFromJson(String str) =>
    QueryListResponseModel.fromMap(json.decode(str));

String queryListResponseModelToJson(QueryListResponseModel data) =>
    json.encode(data.toJson());

class QueryListResponseModel {
  QueryListResponseModel({
    this.message,
    this.statusCode,
    this.data,
  });

  String message;
  String statusCode;
  List<Query> data;

  factory QueryListResponseModel.fromMap(Map<String, dynamic> json) =>
      QueryListResponseModel(
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
        data: json["data"] == null
            ? null
            : List<Query>.from(json["data"].map((x) => Query.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Query {
  Query({
    this.id,
    this.queryStatus,
    this.feedbackMessage,
    this.createdAt,
    this.updatedAt,
    this.adminReply,
    this.order,
    this.images,
    this.ticketId
  });

  int id;
  String ticketId;
  String feedbackMessage;
  String adminReply;
  int queryStatus;
  String createdAt;
  String updatedAt;
  List<String> images;
  Order order;

  factory Query.fromJson(Map<String, dynamic> json) => Query(
        id: json["id"] == null ? null : json["id"],
        ticketId: json["ticket_id"] == null ? null : json["ticket_id"],
        order: json["booking_detail"] == null ? null : Order.fromMap(json["booking_detail"]),
    queryStatus: json["query_status"] == null ? null : json["query_status"],

    feedbackMessage: json["feedback_description"] == null ? null : json["feedback_description"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],

    adminReply: json["admin_reply"] == null
            ? null
            : json["admin_reply"],
    images: json["image"] == null ? null : List<String>.from(json["image"].map((x) => x)),

  );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "ticketId": ticketId == null ? null : ticketId,
        "query_status": queryStatus == null ? null : queryStatus,
        "feedback_description": feedbackMessage == null ? null : feedbackMessage,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "admin_reply": adminReply == null ? null : adminReply,
         "booking_detail": order == null ? null : order?.toMap(),
        "image": images == null ? null : List<dynamic>.from(images.map((x) => x)),

  };
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/help_and_support/queries/bloc/queries_listing_bloc.dart';
import 'package:user/modules/help_and_support/queries/bloc/queries_listing_event.dart';
import 'package:user/modules/help_and_support/queries/bloc/queries_listing_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/order_routes.dart';
import '../../../../routes.dart';

class QueriesListingManager {
  BuildContext context;
  QueriesListingBloc userAllQueriesBloc = new QueriesListingBloc();
  QueriesListingState userAllQueriesState;

  //method to call on home page State change method
  actionTocallOnHelpAndSupportStateChange({
    ScaffoldState currentState,
  }) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) {
        if (userAllQueriesState?.isLoading == false) {
          if (userAllQueriesState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: currentState,
                message: userAllQueriesState?.message);
            print("login==> ${userAllQueriesState?.message}");
          }
        }
      },
    );
  }

  // action to raise Query
  actionToRaiseQuery({Order order}) async {
    if (order != null) {
      // raise a new query
      await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
          context, Routes.SEND_QUERY_PAGE,
          dataToBeSend: order);
    } else {
      await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(context, OrderRoutes.MY_ORDERS,dataToBeSend: true);
    }
    actionTogetUserQueriesList(order: order, page: 1);
  }

  // used to perform the action on init
  void actionOnInit({Order order}) {
    actionTogetUserQueriesList(order: order, page: 1);
  }

  // action to open order page
  actionToOpenDetailPage({Order order}) {
    NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
        context, OrderRoutes.ORDER_DETAILS,
        dataToBeSend: order);
  }

  // used for the pagination
  void actionTogetUserQueriesList({Order order, int page}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        userAllQueriesBloc.emitEvent(GetUserQueriesBookingList(
          context: context,
          page: page,
          order: order,
          isLoading: true,
          listQuery: userAllQueriesState?.listQuery,
        ));
      }
    });
  }
}

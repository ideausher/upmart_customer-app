import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:user/modules/common/app_config/app_config.dart';

class UploadImageProvider
{

  // upload profile pic
  Future<dynamic> uploadDocumentImage({BuildContext context, File file}) async {
    FormData formData = FormData.fromMap({
      "image": await MultipartFile.fromFile(file.path)
    });
    var path = "imageUpload";
    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(path, context, data: formData);
    return result;
  }
  
}
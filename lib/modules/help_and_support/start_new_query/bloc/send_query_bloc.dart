import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/help_and_support/start_new_query/api/model/send_query_request_model.dart';
import 'package:user/modules/help_and_support/start_new_query/api/model/upload_image_response_model.dart';
import 'package:user/modules/help_and_support/start_new_query/api/provider/delete_image_provider.dart';
import 'package:user/modules/help_and_support/start_new_query/api/provider/sned_new_query_provider.dart';
import 'package:user/modules/help_and_support/start_new_query/api/provider/upload_image_provider.dart';
import 'package:user/modules/help_and_support/start_new_query/bloc/send_query_event.dart';
import 'package:user/modules/help_and_support/start_new_query/bloc/send_query_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import '../../../../localizations.dart';

class SendQueryBloc extends BlocEventStateBase<SendQueryEvent, SendQueryState> {
  SendQueryBloc({bool initializing = true,Order order}) : super(initialState: SendQueryState.initiating(orderModel: order));

  @override
  Stream<SendQueryState> eventHandler(SendQueryEvent event, SendQueryState currentState) async* {
    // update user Location
    if (event is SubmitQueryEvent) {
      yield SendQueryState.updateQueryState(
        isLoading: true,
        context: event?.context,
        orderModel: event.orderModel,
      );

      var _result = await SendNewQueryProvider().sendNewQueryApiCall(
          context: event.context,
          sendQueryRequestModel: SendQueryRequestModel(
              bookingId: event.orderModel?.id?.toString(),
              feedbackDescription: event.message,
              feedbackTitle: event.orderModel?.bookingCode?.toString(),
              images: event.images ?? []));

      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          yield SendQueryState.updateQueryState(
              isLoading: false,
              context: event?.context,
              message: _result[ApiStatusParams.Message.value],
              orderModel: event.orderModel,
              images: event?.images,
              apiStatus: ApiStatus.Success);
        } else {
          yield SendQueryState.updateQueryState(
              isLoading: false,
              context: event?.context,
              message: _result[ApiStatusParams.Message.value],
              orderModel: event.orderModel,
              images: event?.images);
        }
      } else {
        yield SendQueryState.updateQueryState(
            isLoading: false,
            context: event?.context,
            message: AppLocalizations.of(event.context).common.error.somethingWentWrong,
            orderModel: event.orderModel,
            images: event?.images);
      }
    }

    //Upload Image
    if (event is UploadImageEvent) {
      String message = "";

      yield SendQueryState.updateQueryState(
          isLoading: event?.isLoading,
          context: event?.context,
          message: message,
          images: event.images,
          imageFile: event.imageFile,
          orderModel: event.orderModel);

      var result = await UploadImageProvider().uploadDocumentImage(context: event?.context, file: event.imageFile);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          var _uploadImageResponseModel = UploadImageResponseModel.fromMap(result);

          event.images.add(_uploadImageResponseModel?.data?.imagePath);
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message = AppLocalizations.of(event.context).common.error.somethingWentWrong;
      }

      yield SendQueryState.updateQueryState(
          isLoading: false,
          context: event?.context,
          message: message,
          images: event.images,
          orderModel: event.orderModel);
    }

    //Delete Image
    if (event is DeleteImageEvent) {
      String message = "";

      yield SendQueryState.updateQueryState(
          isLoading: event?.isLoading,
          context: event?.context,
          message: message,
          images: event.images,
          imageFile: event.imageFile,
          index: event.index,
          orderModel: event.orderModel);

      var result = await DeleteImageProvider().deleteImage(
        context: event?.context,
        imageUrl: event?.images[event.index],
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          event.images.removeAt(event.index);
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message = AppLocalizations.of(event.context).common.error.somethingWentWrong;
      }

      yield SendQueryState.updateQueryState(
          isLoading: false,
          context: event?.context,
          message: message,
          images: event.images,
          orderModel: event.orderModel);
    }
  }
}

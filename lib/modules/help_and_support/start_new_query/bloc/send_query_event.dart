import 'dart:io';

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

abstract class SendQueryEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final String message;
  final Order orderModel;
  final File imageFile;
  final List<String> images;
  final int index;
  final String desc;

  SendQueryEvent({
    this.isLoading: false,
    this.context,
    this.message,
    this.orderModel,
    this.imageFile,
    this.images,
    this.index,
    this.desc,
  });
}

//Event to set the selected location of the user
class SubmitQueryEvent extends SendQueryEvent {
  SubmitQueryEvent({
    bool isLoading,
    BuildContext context,
    String message,
    Order orderModel,
    File imageFile,
    List<String> images,
    int index,
    String desc,
  }) : super(
            isLoading: isLoading,
            context: context,
            message: message,
            orderModel: orderModel,
            desc: desc,
            images: images,
            imageFile: imageFile,
            index: index);
}

//use to upload image
class UploadImageEvent extends SendQueryEvent {
  UploadImageEvent({
    bool isLoading,
    BuildContext context,
    String message,
    Order orderModel,
    File imageFile,
    List<String> images,
    int index,
  }) : super(
            isLoading: isLoading,
            context: context,
            message: message,
            orderModel: orderModel,
            images: images,
            imageFile: imageFile,
            index: index);
}

//use to upload image
class DeleteImageEvent extends SendQueryEvent {
  DeleteImageEvent({
    bool isLoading,
    BuildContext context,
    String message,
    Order orderModel,
    File imageFile,
    List<String> images,
    int index,
  }) : super(
            isLoading: isLoading,
            context: context,
            message: message,
            orderModel: orderModel,
            images: images,
            imageFile: imageFile,
            index: index);
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';

import 'package:user/modules/coupon_listing/constants/image_constants.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';

import '../../../modules/common/app_config/app_config.dart';
import '../../../modules/common/constants/dimens_constants.dart';

class DialogSnackBarUtils {
  static DialogSnackBarUtils _dialogSnackBarUtils = DialogSnackBarUtils();

  static DialogSnackBarUtils get dialogSnackBarUtilsInstance =>
      _dialogSnackBarUtils;

  // show snackbar
  void showSnackbar(
      {BuildContext context,
      ScaffoldState scaffoldState,
      String message,
      Color backgroundColor}) {
    scaffoldState.showSnackBar(new SnackBar(
        backgroundColor:
            backgroundColor ?? AppConfig.of(context).themeData.accentColor,
        content: new Text(
          message,
          style: textStyleSize14WithWhiteColor,
        )));
  }

  // used to show alert
  void showAlertDialog({
    @required BuildContext context,
    @required String title,
    String subTitle,
    String centerImage,
    String positiveButton,
    TextStyle titleTextStyle,
    TextStyle subTitleTextStyle,
    TextStyle buttonTextStyle,
    String negativeButton,
    bool dismissOnHardwareButtonClick: false,
    bool barrierDismissible = false,
    Function onPositiveButtonTab,
    Function onNegativeButtonTab,
  }) {
    var actionWidgets = <Widget>[];
    if (positiveButton?.isNotEmpty == true) {
      actionWidgets.add(WillPopScope(
        onWillPop: () {
          if (!dismissOnHardwareButtonClick) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          }
        },
        child: CupertinoDialogAction(
          child: Text(
            positiveButton ?? "",
            style: buttonTextStyle ??
                TextStyle(
                    color: AppConfig.of(context).themeData.primaryColor,
                    fontSize: AppConfig.of(context)
                        .themeData
                        .textTheme
                        .bodyText2
                        .fontSize),
          ),
          onPressed: onPositiveButtonTab,
        ),
      ));
    }
    if (negativeButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(
          negativeButton ?? "",
          style: buttonTextStyle ??
              TextStyle(
                  color: AppConfig.of(context).themeData.primaryColor,
                  fontSize: AppConfig.of(context)
                      .themeData
                      .textTheme
                      .bodyText2
                      .fontSize),
        ),
        onPressed: onNegativeButtonTab,
      ));
    }

// show the dialog
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Padding(
                padding: const EdgeInsets.only(bottom: SIZE_15),
                child: new Text(
                  title,
                  style: titleTextStyle ?? textStyleSize22WithGreenColor,
                ),
              ),
              content: Visibility(
                  visible: subTitle?.isNotEmpty,
                  child: new Text(
                    subTitle ?? "",
                    style: subTitleTextStyle ??
                        AppConfig.of(context).themeData.textTheme.bodyText2,
                  )),
              actions: actionWidgets,
            ));
  }

  void showCancelAlertDialog({
    @required BuildContext context,
    @required String title,
    String subTitle,
    String centerImage,
    String positiveButton,
    TextStyle titleTextStyle,
    TextStyle subTitleTextStyle,
    TextEditingController cancelBookingController,
    TextStyle positiveButtonTextStyle,
    TextStyle buttonTextStyle,
    String negativeButton,
    bool showWidget = false,
    bool barrierDismissible = false,
    Function onPositiveButtonTab,
    Function onNegativeButtonTab,
  }) {
    var actionWidgets = <Widget>[];
    if (positiveButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(positiveButton ?? "",
            style: positiveButtonTextStyle ??
                TextStyle(
                    color: AppConfig.of(context).themeData.primaryColor,
                    fontSize: AppConfig.of(context)
                        .themeData
                        .textTheme
                        .bodyText2
                        .fontSize)),
        onPressed: onPositiveButtonTab,
      ));
    }
    if (negativeButton?.isNotEmpty == true) {
      actionWidgets.add(CupertinoDialogAction(
        child: Text(negativeButton ?? "",
            style: buttonTextStyle ??
                TextStyle(
                    color: AppConfig.of(context).themeData.primaryColor,
                    fontSize: AppConfig.of(context)
                        .themeData
                        .textTheme
                        .bodyText2
                        .fontSize)),
        onPressed: onNegativeButtonTab,
      ));
    }

// show the dialog
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (BuildContext context) => Theme(
              data: ThemeData(
                  primaryColor: Colors.white,
                  accentColor: Colors.white,
                  primaryColorDark: Colors.white,
                  dialogBackgroundColor: Colors.white,
                  dialogTheme: DialogTheme(backgroundColor: Colors.white)),
              child: CupertinoAlertDialog(
                title: Padding(
                  padding: const EdgeInsets.only(bottom: SIZE_10),
                  child: new Text(
                    title,
                    style: titleTextStyle ??
                        AppConfig.of(context).themeData.textTheme.bodyText2,
                  ),
                ),
                content: (showWidget)
                    ? _textFieldForm(
                        context: context,
                        hint: 'Reason(Minimum 30 Characters)',
                        controller: cancelBookingController)
                    : Visibility(
                        visible: subTitle?.isNotEmpty,
                        child: new Text(
                          subTitle ?? "",
                          style: subTitleTextStyle ??
                              AppConfig.of(context)
                                  .themeData
                                  .textTheme
                                  .bodyText2,
                        )),
                actions: actionWidgets,
              ),
            ));
  }

  Widget showCouponAlert(BuildContext context, CouponData datum) {
    CartUtilsManager.cartUtilsInstance.couponData = datum;

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(SIZE_28),
            titlePadding: EdgeInsets.all(SIZE_0),
            content: SizedBox(
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, ofWidth: true, percentage: SIZE_90),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    DISCOUNT_ICON,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context, ofWidth: true, percentage: SIZE_50),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: SIZE_10),
                    child: Text(
                      AppLocalizations.of(context).couponalert.title.hurray,
                      style: AppConfig?.of(context)
                          ?.themeData
                          ?.primaryTextTheme
                          ?.caption,
                    ),
                  ),
                  Text(
                    AppLocalizations.of(context)
                        .couponalert
                        .subtitle
                        .discountApplied,
                    style: textStyleSizeBlackColor,
                  ),
                  CupertinoDialogAction(
                    child: Text(
                      AppLocalizations.of(context).common.text.ok,
                      style: TextStyle(
                          color: AppConfig.of(context).themeData.primaryColor,
                          fontSize: AppConfig.of(context)
                              .themeData
                              .textTheme
                              .bodyText2
                              .fontSize),
                    ),
                    onPressed: () {
                      NavigatorUtils.navigatorUtilsInstance
                          .navigatorPopScreen(context, dataToBeSend: datum);
                    },
                  )
                ],
              ),
            ),
          );
        }).then((val) {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorPopScreen(context, dataToBeSend: datum);
    });
    ;
  }

  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      bool enabled,
      Widget icon,
      BuildContext context,
      double elevation,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_10),
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        maxLines: 5,
        controller: controller,
        textCapitalization: TextCapitalization.words,
        enabled: enabled,
        keyboardType: keyboardType,
        style: AppConfig.of(context).themeData.textTheme.headline2,
        decoration: InputDecoration(
          hintText: hint,
          contentPadding: EdgeInsets.all(SIZE_10),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(SIZE_10)),
              borderSide: BorderSide(
                color: COLOR_PRIMARY,
              )),
        ),
      ),
    );
  }
}

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'dialog_snackbar_utils.dart';
import 'navigator_utils.dart';

class NetworkConnectionUtils {
  static NetworkConnectionUtils _networkConnectionUtils = NetworkConnectionUtils();

  static NetworkConnectionUtils get networkConnectionUtilsInstance => _networkConnectionUtils;

  final Connectivity _connectivity = Connectivity();

  // used to check connectivity status
  Future<bool> getConnectivityStatus(BuildContext context,
      {bool showNetworkDialog = true}) async {
    var connectivityResult = await (_connectivity.checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
// I am connected to a mobile network.
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
// I am connected to a wifi network.
      return true;
    } else {
      if (showNetworkDialog) {
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
            context: context,
            title: "",
            positiveButton: AppLocalizations.of(context).common.text.ok,
            subTitle: AppLocalizations.of(context).common.text.checkInternetConnection,
            onPositiveButtonTab: () {
              NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            });
//        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
//            context: context,
//            scaffoldState: scaffoldState,
//            message: AppLocalizations.of(context).common.error.pleaseCheckInternetConnection);
      }
      return false;
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';

class DatePickerUtils {
  static DatePickerUtils _datePickerUtils = DatePickerUtils();

  static DatePickerUtils get datePickerUtilsInstance => _datePickerUtils;

  //method to show date picker
  Future<String> getDatePicker(
      {BuildContext context, String selectedDate}) async {
    var date;
    DateTime dateTime = DateTime.now();
    selectedDate.isNotEmpty == true
        ? dateTime = DateUtils.dateUtilsInstance
            .convertStringToDate(dateTime: selectedDate)
        : dateTime;

    final picked = await showDatePicker(
        context: context,
        initialDate: dateTime,
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 7)),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              primaryColor: COLOR_PRIMARY,
              accentColor: COLOR_PRIMARY,
              colorScheme: ColorScheme.light(primary: COLOR_PRIMARY),
              dialogBackgroundColor: Colors.white,
            ),
            child: child,
          );
        });
    if (picked != null && picked != DateTime.now()) {
      date = DateFormat('yyyy-MM-dd').format(picked);
    }

    if (date == null) {
      date = "";
      if (selectedDate.isNotEmpty) {
        date = DateFormat('yyyy-MM-dd').format(dateTime);
      }
    }
    return date.toString();
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom_image_last/pinch_zoom_image_last.dart';
import 'package:user/modules/auth/constants/image_constant.dart';

import '../../../modules/common/constants/color_constants.dart';
import '../../../modules/common/constants/dimens_constants.dart';
import 'navigator_utils.dart';

class ImageUtils {
  static ImageUtils _imageUtils = ImageUtils();

  static ImageUtils get imageUtilsInstance => _imageUtils;

  Widget showCacheNetworkImage(
      {BuildContext context,
      String url,
      String placeHolderImage = "",
      IconData icon,
        double radius,

        BoxShape shape,
      double height = double.maxFinite,
      double width = double.maxFinite,
      BoxFit fit = BoxFit.cover,
      bool showProgressBarInPlaceHolder = false,
      bool showDefaultErrorWidget = false}) {
    return CachedNetworkImage(
        height: height,
        width: width,
        imageUrl: url ?? "",
        imageBuilder: (context, imageProvider) => Container(
              width: width,
              height: height,
              decoration: BoxDecoration(
                shape: shape??BoxShape.circle,
                color: Colors.transparent,

                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              ),
            ),
        fit: fit,
        placeholder: (context, url) {
          return (showProgressBarInPlaceHolder == true)
              ? Center(
                  child: Image.asset(
                  LOADING_ICON,
                  height: SIZE_60,
                  width: SIZE_60,
                ))
              : (icon != null)
                  ? Icon(
                      icon,
                    )
                  : Center(
                      child: Image.asset(
                        placeHolderImage,
                        height: height,
                        width: width,
                        fit: fit,
                      ),
                    );
        },
        errorWidget: (context, url, error) {
          return (showDefaultErrorWidget == true)
              ? Icon(
                  Icons.error,
                )
              : (icon != null)
                  ? Icon(icon, size: height)
                  : Center(
                      child: Image.asset(
                        placeHolderImage,
                        height: height,
                        width: width,
                        fit: fit,
                      ),
                    );
        });
  }

  // used to show the full size image
  void showFullScreenImage(
      {@required BuildContext context,
      @required String image,
      String placeHolder = "",

        BoxShape shape,
      IconData placeHolderIcon,
      Color backgroundColor = Colors.grey,
      BoxFit fit = BoxFit.none}) {
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return Material(
          color: backgroundColor,
          child: Stack(
            children: <Widget>[
              PinchZoomImage(
                image: Center(
                  child: showCacheNetworkImage(
                      context: context,
                      url: image,
                      shape: shape ?? BoxShape.circle,
                      placeHolderImage: placeHolder,
                      icon: placeHolderIcon,
                      height: double.maxFinite,
                      width: double.maxFinite,
                      fit: fit),
                ),
              ),
              Positioned(
                right: 0,
                top: 0,
                child: IconButton(
                  icon: Icon(
                    Icons.cancel,
                    color: COLOR_PRIMARY,
                    size: SIZE_30,
                  ),
                  onPressed: () {
                    NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

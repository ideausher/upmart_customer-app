import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NavigatorUtils {
// used navigator to push new screen with route name whose route is defined in
  static NavigatorUtils _navigatorUtils = NavigatorUtils();

  static NavigatorUtils get navigatorUtilsInstance => _navigatorUtils;

  // used navigator to push if you want enter animation
  Future<dynamic> navigatorPushedName(BuildContext context, String name, {dataToBeSend: dynamic}) async {
    return await Navigator.of(context).pushNamed(name, arguments: dataToBeSend);
  }

  // used navigator to push and replace screen if you want enter animation
  Future<dynamic> navigatorPushReplacementNamed(BuildContext context, String name, {dataToBeSend: dynamic}) async {
    return await Navigator.of(context).pushReplacementNamed(name, arguments: dataToBeSend);
  }

  // used navigator to push and pop screen with exit animation
  Future<dynamic> navigatorPopAndPushNamed(BuildContext context, String name, {dataToBeSend: dynamic}) async {
    return await Navigator.popAndPushNamed(context, name, arguments: dataToBeSend);
  }

  // used to clear previous all stack and pushed new screen
  //(Route<dynamic> route) => false will make sure that all routes before the
  // pushed route be removed.
  Future<dynamic> navigatorClearStack(BuildContext context, String name, {dataToBeSend: dynamic}) async {
    return await Navigator.of(context)
        .pushNamedAndRemoveUntil(name, (Route<dynamic> route) => false, arguments: dataToBeSend);
  }

// used navigator to push to next screen and on pop that pushed screen get data on previous screen
  Future<dynamic> navigatorPushedNameResult(BuildContext context, String name, {dataToBeSend: dynamic}) async {
    return await Navigator.of(context).pushNamed(name, arguments: dataToBeSend);
  }

  // used to push widget
  navigatorPushDataWithWidget(BuildContext context, Widget widget) {
    Route route = new MaterialPageRoute(builder: (BuildContext context) => widget);
    Navigator.push(context, route);
  }

  // used to clear stack with limit screen
  // if we have screen 1, screen 2 screen 3, screen 4 and i pushed screen 5 and remove until screen2 then stack will be
  // screen 1, screen 2, screen 5 -> it will remove screen 3 and 4
  Future<dynamic> navigatorClearStackWithLimit(
      BuildContext context, String pushedScreenName, String ClearRouteUntilScreenName,
      {dataToBeSend: dynamic}) async {
    return await Navigator.of(context).pushNamedAndRemoveUntil(
        pushedScreenName,
        ModalRoute.withName(
          ClearRouteUntilScreenName,
        ),
        arguments: dataToBeSend);
  }

  // used navigator to pop screen if than screen can pop
  navigatorPopScreen(BuildContext context, {dataToBeSend: Object}) {
    if (Navigator.of(context).canPop()) {
      if (dataToBeSend != null)
        Navigator.of(context).pop(dataToBeSend);
      else {
        Navigator.of(context).pop();
      }
    } else {
      SystemNavigator.pop();
    }
  }

  // used navigator to pop screen with may be default function
  navigatorMayBePopScreen(BuildContext context, {dataToBeSend: Object}) {
    if (dataToBeSend != null) {
      Navigator.of(context).maybePop(dataToBeSend);
    } else {
      Navigator.of(context).maybePop();
    }
  }

  // used to pop screen until pop screen
  navigatorPopUntil(BuildContext context, String popScreenTill) {
    Navigator.popUntil(context, ModalRoute.withName(popScreenTill));
  }
}

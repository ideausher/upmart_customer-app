import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NumberFormatUtils {
  static NumberFormatUtils _numberFormatUtils = NumberFormatUtils();

  static NumberFormatUtils get numberFormatUtilsInstance => _numberFormatUtils;

  var _locale = Locale("en", "ca");
  var _stringLocale = "en-ca";

  // update locale
  void updateLocale({String languageCode, String countryCode}) {
    _locale = Locale(languageCode, countryCode);
    _stringLocale = "${languageCode}_${countryCode}";
  }

  // get currency symbol
  String getCurrencySymbol() {
    var _format = NumberFormat.simpleCurrency(locale: _locale.toString());
    return _format.currencySymbol;
  }

  // get currency name
  String getCurrencyName() {
    var _format = NumberFormat.simpleCurrency(locale: _locale.toString());
    return _format.currencyName;
  }

  // format price with symbol
  String formatPriceWithSymbol({num price}) {

    String _value = price?.toStringAsFixed(2);

    return NumberFormat.simpleCurrency(locale: _stringLocale).format(num.parse(_value));
  }

  //format price with currency name
  String formatPriceWithName({num price}) {
    String _value = price?.toStringAsFixed(2);
    return NumberFormat.currency(locale: _stringLocale).format(_value);
  }
}
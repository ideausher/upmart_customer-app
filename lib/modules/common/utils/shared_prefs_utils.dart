import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefUtils {
  static SharedPrefUtils _sharedPrefUtils = SharedPrefUtils();

  static SharedPrefUtils get sharedPrefUtilsInstance => _sharedPrefUtils;

  Future<bool> setString(String value, String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(key, value);
  }

  Future<String> getString(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(key);
  }

  Future<bool> setBooleon(bool value, String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(key, value);
  }

  Future<bool> getBooleon(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(key);
  }

  Future<bool> setInt(int value, String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setInt(key, value);
  }

  Future<int> getInt(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt(key);
  }

  Future<bool> setDouble(double value, String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setDouble(key, value);
  }

  Future<double> getDouble(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getDouble(key);
  }

  Future<bool> saveObject(dynamic objectData, String key) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String modelData = json.encode(objectData.toMap());
    return sharedPreferences.setString(key, modelData);
  }

  Future<dynamic> getObject(String key) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String objectData = sharedPreferences.getString(key);
    if (objectData != null && objectData.isNotEmpty) {
      return jsonDecode(objectData);
    }
    return null;
  }

  //Save Current location
  Future<bool> saveLocationObject(dynamic objectData, String key) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String modelData = json.encode(objectData.toMap());
    return sharedPreferences.setString(key, modelData);
  }

  //get Current Location object
  Future<dynamic> getLocationObject(String key) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String objectData = sharedPreferences.getString(key);
    if (objectData != null && objectData.isNotEmpty) {
      return jsonDecode(objectData);
    }
    return null;
  }

  Future<bool> removeValue(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(key);
  }

  Future<void> clearPref() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
//    var token = prefs.getString(AUTH_MODEL_TAG);
//    token.
    return prefs.clear();
    //return prefs.setString(UserFCMToken, token);
  }
}


import 'package:user/modules/common/model/user_current_location_model.dart';

import '../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../modules/common/enum/enums.dart';
import '../../../modules/common/utils/shared_prefs_utils.dart';

class FetchPrefsUtils {
  static FetchPrefsUtils _fetchPrefsUtils = FetchPrefsUtils();

  static FetchPrefsUtils get fetchPrefsUtilsInstance => _fetchPrefsUtils;

  Future<AuthResponseModel> getAuthResponseModel() async {
    var data = await SharedPrefUtils.sharedPrefUtilsInstance.getObject(PrefsEnum.UserProfileData.value);
    if (data != null) {
      return AuthResponseModel.fromMap(data);
    }
    return null;
  }

  Future<CurrentLocation> getCurrentLocationModel() async {
    var data = await SharedPrefUtils.sharedPrefUtilsInstance.getLocationObject(PrefsEnum.UserLocationData.value);
    if (data != null) {
      CurrentLocation currentLocation = CurrentLocation.fromMap(data);
      return currentLocation;
    }
    return null;
  }
}

import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';

import '../../../modules/common/app_config/app_config.dart';
import '../../../modules/common/constants/dimens_constants.dart';
import '../../../modules/common/utils/navigator_utils.dart';

class CommonUtils {
  static CommonUtils _commonUtils = CommonUtils();

  static CommonUtils get commonUtilsInstance => _commonUtils;

// hide keyboard
  hideKeyboard({BuildContext context}) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  double getPercentageSize(
      {BuildContext context, double percentage, bool ofWidth = true}) {
    if (ofWidth) {
      return (MediaQuery
          .of(context)
          .size
          .width * percentage) / 100;
    } else {
      return (MediaQuery
          .of(context)
          .size
          .height * percentage) / 100;
    }
  }

//for getting device id
  Future<String> getDeviceID() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) { // import 'dart:io'
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  // default app bar
  // default app bar
  AppBar getAppBar({
    @required BuildContext context,
    String appBarTitle = "", // app bar title
    TextStyle appBarTitleStyle, // app bar title text style
    Function defaultLeadIconPressed, // if default icon set then customize tap
    Widget leadingWidget, // customize leading widget
    IconData defaultLeadingIcon, // change leading icon from back pressed to other
    Color defaultLeadingIconColor, // change default leading icon color from white to other
    double elevation = ELEVATION_05,
    Color backGroundColor, // app bar background color
    bool centerTitle = false,
    List<Widget> actionWidgets,
    bool popScreenOnTapOfLeadingIcon = true, // send it false if you want tap
    Widget appBarTitleWidget,
  }) {
    return AppBar(
      backgroundColor: backGroundColor ?? AppConfig
          .of(context)
          .themeData
          .appBarTheme
          .color,
      elevation: elevation,
      centerTitle: centerTitle,
      title: appBarTitleWidget ??
          Text(
            appBarTitle,
            style: appBarTitleStyle ??
                TextStyle(
                  fontSize: AppConfig
                      .of(context)
                      .themeData
                      .textTheme
                      .headline1
                      .fontSize,
                  fontWeight: FontWeight.w600,
                ),
          ),
      leading: leadingWidget ??
          IconButton(
            iconSize: 16.0,
            onPressed: () {
              if (popScreenOnTapOfLeadingIcon == true) {
                NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
                    context);
              }
              if (defaultLeadIconPressed != null) defaultLeadIconPressed();
            },
            icon: Icon(
              defaultLeadingIcon ?? Icons.arrow_back,
              color: defaultLeadingIconColor ?? Colors.white,
              size: SIZE_20,
            ),
          ),
      actions: actionWidgets,
    );
  }


  //show image with text

  Widget textFieldForm({TextEditingController controller,
    TextInputType keyboardType,
    IconData icon,
    String hint,
    double elevation,
    BuildContext context,
    bool enabled,
    int maxLines,
    bool showTickIcon = false,
    FormFieldValidator<String> validator}) {
    return Material(
      color: Colors.white,
      borderRadius: BorderRadius.circular(SIZE_5),
      child: Container(
        padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
        margin: EdgeInsets.only(top: SIZE_10),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_10)),
        child: TextFormField(
          validator: validator,
          style: textStyleSize12WithBlackColor,
          controller: controller,
          enabled: enabled ?? true,
          maxLines: maxLines,
          keyboardType: keyboardType,

          decoration: InputDecoration(
            hintText: hint,
            hintStyle: textStyleSize14GreyColorFont500,
            contentPadding: EdgeInsets.all(SIZE_5),
            border: OutlineInputBorder(borderSide: BorderSide.none),
          ),
        ),
      ),
    );
  }


  //get images of shops and show it as a slider list
  List<String> getShopImages(
      {BuildContext context, ShopDetailsModel shopDetailsModel}) {
    List<String> shopImages = new List();
    if (shopDetailsModel?.media?.isNotEmpty == true) {
      shopDetailsModel?.media?.forEach((element) {
        shopImages.add(element.fileName);
      });
    }
    if (shopDetailsModel?.profileImage?.fileName?.isNotEmpty == true){
      shopImages?.add(shopDetailsModel?.profileImage?.fileName ?? "");}

    if (shopDetailsModel?.banners?.isNotEmpty == true) {
      shopDetailsModel?.banners?.forEach((element) {
        if (element?.link?.isNotEmpty == true) {
          shopImages.add(element.link);
        }
      });
    }

    return shopImages;
  }


}

import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:user/modules/common/constants/color_constants.dart';

class UpdateUiDataModel {
  String phoneNumber;
  bool isTermsAccepted;
  Country selectedCountry;
  int dropDownValue;
  Color resendButton;
  int otpLength;

  UpdateUiDataModel({
    this.phoneNumber: "",
    this.isTermsAccepted: false,
    this.selectedCountry: Country.CA,
    this.dropDownValue: 0,
    this.otpLength: 0,
    this.resendButton: COLOR_BORDER_GREY,
  });
}

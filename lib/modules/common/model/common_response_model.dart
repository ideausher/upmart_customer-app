// To parse this JSON data, do
//
//     final commonResponseModel = commonResponseModelFromJson(jsonString);

import 'dart:convert';

CommonResponseModel commonResponseModelFromJson(String str) => CommonResponseModel.fromJson(json.decode(str));

String commonResponseModelToJson(CommonResponseModel data) => json.encode(data.toJson());

class CommonResponseModel {
  CommonResponseModel({
    this.data,
    this.message,
    this.status,
  });

  String data;
  String message;
  int status;

  factory CommonResponseModel.fromJson(Map<String, dynamic> json) => CommonResponseModel(
        //data: json["data"] == null ? null : json["data"],
        message: json["message"] == null ? null : json["message"],
        status: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        //"data": data == null ? null : data,
        "message": message == null ? null : message,
        "status_code": status == null ? null : status,
      };
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/constants/font_constant.dart';

import '../../../modules/common/constants/color_constants.dart';
import '../../../modules/common/constants/dimens_constants.dart';

// host App Theme
final ThemeData appTheme = new ThemeData(
    primaryColor: COLOR_PRIMARY,
    primaryColorDark: COLOR_PRIMARY_DARK,
    backgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    accentColor: COLOR_ACCENT,
    //   primarySwatch: Colors.blue,
    appBarTheme: AppBarTheme(
      color: COLOR_PRIMARY,
      textTheme: TextTheme(
          headline6: TextStyle(color: Colors.white, fontSize: SIZE_15)),
    ),
    buttonTheme: ButtonThemeData(
      disabledColor: COLOR_PRIMARY.withOpacity(0.7),
      buttonColor: COLOR_PRIMARY,
      //padding: EdgeInsets.all(SIZE_20),
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(SIZE_5)),
    ),
    textTheme: TextTheme(
      // black
      headline1: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_22,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),
      headline2: TextStyle(
          color: Colors.black,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w300,
          fontFamily: FONT_FAMILY_INTER),
      headline3: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),
      // white
      headline4: TextStyle(
          color: COLOR_RED,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_INTER),
      headline5: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_24,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_INTER),
      headline6: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_24,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),

      // color accent
      subtitle1: TextStyle(
          color: Colors.black,
          fontSize: SIZE_14,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      subtitle2: TextStyle(
          color: Colors.black,
          fontSize: SIZE_28,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      caption: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_28,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      bodyText1: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_20,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      bodyText2: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
    ),
    primaryTextTheme: TextTheme(
      // primary color
      headline1: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_14,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      headline2: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_14,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_INTER),
      headline3: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_18,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),
      // black 54
      headline4: TextStyle(
          color: COLOR_LIGHT_GREY,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      headline5: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_20,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),
      headline6: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_16,
          fontFamily: FONT_FAMILY_INTER,
          fontWeight: FontWeight.w600),
      // color white
      subtitle1: TextStyle(
          color: COLOR_PRIMARY,
          fontSize: SIZE_42,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_INTER),
      subtitle2: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_16,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),
      caption: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_30,
          fontWeight: FontWeight.w600,
          fontFamily: FONT_FAMILY_INTER),
      bodyText1: TextStyle(
          color: COLOR_BLACK,
          fontSize: SIZE_30,
          fontWeight: FontWeight.w400,
          fontFamily: FONT_FAMILY_INTER),
      bodyText2: TextStyle(
          color: Colors.white,
          fontSize: SIZE_48,
          fontWeight: FontWeight.w500,
          fontFamily: FONT_FAMILY_INTER),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: COLOR_PRIMARY,
    ));

TextStyle textStyleSize22WithGreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_20,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize16WithGreyColor = TextStyle(
    color: COLOR_BORDER_GREY,
    fontSize: SIZE_16,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14WithGreyColor = TextStyle(
    color: COLOR_RATING,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSizeBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_20,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize15BlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_15,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize28WithPrimaryColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_28,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER,
    height: 1.2);
TextStyle textStyleSize28WithBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_28,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER,
    height: 1.2);
TextStyle textStyleSize22WithWhiteColor = TextStyle(
    fontSize: SIZE_22,
    color: Colors.white,
    fontFamily: FONT_FAMILY_INTER,
    fontWeight: FontWeight.w600);
TextStyle textStyleSize16WithWhiteColor = TextStyle(
    fontSize: SIZE_16,
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize30WithWhiteColor = TextStyle(
    fontSize: SIZE_30,
    fontWeight: FontWeight.w500,
    color: Colors.white,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize14WithGREY = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_DARK_GREY,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12WithBLACK = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_GREY_LIGHT,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize12WithGrey = TextStyle(
    fontSize: SIZE_14,
    color: COLOR_BORDER_GREY,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12WithRed = TextStyle(
    fontSize: SIZE_12,
    color: COLOR_RED,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12WithPrimary = TextStyle(
    fontSize: SIZE_12,
    color: COLOR_PRIMARY,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12WithWhite = TextStyle(
    fontSize: SIZE_12,
    color: Colors.white,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize18WithBlack = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_18,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize26GreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_26,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize15With600BlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_15,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize22WithBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_20,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize16WithGreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_16,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize15WithGreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_15,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize18WithGreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_18,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14WithGreenColor = TextStyle(
    color: COLOR_PRIMARY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14WithRedColor = TextStyle(
    color: COLOR_RED,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14GreyColor = TextStyle(
    color: COLOR_BORDER_GREY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14BlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12GREY = TextStyle(
    fontSize: SIZE_12,
    color: COLOR_DARK_GREY,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12WithWhiteColor = TextStyle(
    fontSize: SIZE_12,
    color: Colors.white,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14GreyColorFont500 = TextStyle(
    color: COLOR_BORDER_GREY,
    fontSize: SIZE_12,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize13WithBlackColor = TextStyle(
    fontSize: SIZE_13,
    color: COLOR_BLACK,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14TabGreyColor = TextStyle(
    color: COLOR_LIGHT_GREY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14TabGreenColor = TextStyle(
    color: COLOR_DARK_PRIMARY,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize12WithBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_12,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14WithBlueColor = TextStyle(
    color: COLOR_DARK_BLUE,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);
TextStyle textStyleSize14WithWhiteColor = TextStyle(
    color: Colors.white,
    fontSize: SIZE_14,
    fontWeight: FontWeight.w500,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize15WithBlackColor = TextStyle(
    fontSize: SIZE_15,
    fontWeight: FontWeight.w200,
    color: COLOR_BLACK,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize15WithREDColor = TextStyle(
  fontSize: SIZE_15,
  fontWeight: FontWeight.w300,
  color: COLOR_PRIMARY,
  fontFamily: FONT_FAMILY_INTER,
);

TextStyle textStyleSize15WithLightGreyColor = TextStyle(
    fontSize: SIZE_15,
    fontWeight: FontWeight.w400,
    color: COLOR_BORDER_GREY,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize16WithLightGreenColor = TextStyle(
    color: COLOR_GREEN,
    fontSize: SIZE_16,
    fontWeight: FontWeight.w400,
    fontFamily: FONT_FAMILY_INTER);

TextStyle textStyleSize16WithBlackColor = TextStyle(
    color: COLOR_BLACK,
    fontSize: SIZE_15,
    fontWeight: FontWeight.w600,
    fontFamily: FONT_FAMILY_INTER);

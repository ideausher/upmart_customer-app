import 'package:flutter/cupertino.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:user/modules/common/search_places/model/search_place_address_model.dart';
const API_KEY = "AIzaSyDPGt3Bs4hqaoAotLGP4wwcENqu8xQVppU";
//const API_KEY = "AIzaSyAtWs90Ka7blaL9njeat8a7wRW6dn36Tzc";

class SearchPlacesUtils {
  static SearchPlacesUtils _searchPlacesUtils = SearchPlacesUtils();

  static SearchPlacesUtils get searchPlacesUtilsInstance => _searchPlacesUtils;
  var count = 0;

  Future<dynamic> showSearchPlacesDialog(
      {BuildContext context,
      String googleApiKey = API_KEY,
      String language = "en",
      List<Component> components}) async {
    try {
      Prediction _prediction = await PlacesAutocomplete.show(
          context: context,
          apiKey: googleApiKey,
          mode: Mode.overlay,
          language: language,);
      print("searchPlacesDialog: get pridiction");

      return await displayPrediction(prediction: _prediction, googleApiKey: googleApiKey);
    } catch (e) {
      print("searchPlacesDialog: get pridiction exception ${e}");
      return null;
    }
  }

  // display prediction regarding the location search
  Future<dynamic> displayPrediction({
    Prediction prediction,
    String googleApiKey,
  }) async {
    print("displayPrediction: prediction is ${prediction}");
    if (prediction != null) {
      GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: googleApiKey);
      var detail = await _places.getDetailsByPlaceId(prediction.placeId);

      //checking if we are getting the value from the google places api or not
      if (detail != null) {
        // get coordinates
        final coordinates = new Coordinates(
          detail.result.geometry.location.lat,
          detail.result.geometry.location.lng,
        );

        // get address
        var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

        return SearchPlaceAddressModel(
            country: addresses?.first?.countryName ?? "",
            latitude: detail.result.geometry.location.lat,
            longitude: detail.result.geometry.location.lng,
            name: detail?.result?.name ?? "",
            formattedAddress: detail?.result?.formattedAddress ?? "",
            city: addresses?.first?.locality,
            placeId: detail?.result?.placeId ?? "");
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}

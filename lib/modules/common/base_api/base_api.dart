import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:package_info/package_info.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/page/send_otp/page/send_otp_page.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';

import '../../../modules/common/model/common_response_model.dart';

class BaseApi {
  Dio _dio;
  GlobalKey<NavigatorState> navigatorKey;

  BaseApi(this.navigatorKey) {}

  setUpOptions({String baseUrl, AppConfiguration configuration}) async {
    BaseOptions baseOptions = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 23000,
      receiveTimeout: 23000,
    );
    _dio = Dio(baseOptions);
    _dio.interceptors.add(
        LogInterceptor(requestBody: true, responseBody: true, error: true));

    var _interceptors = HeaderInterceptor(navigatorKey);
    _dio.interceptors.add(_interceptors);
  }

  Future<dynamic> getRequest(
    String path,
    BuildContext context, {
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await _dio.get(path,
          queryParameters: queryParameters,
          cancelToken: cancelToken,
          onReceiveProgress: onReceiveProgress,
          options: options);

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        //TODo do action by using context and status code

        return CommonResponseModel.fromJson(e.response.data).toJson();
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message).toJson();
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "").toJson();
      }
    }
  }

  Future<dynamic> postRequest(
    String path,
    BuildContext context, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await _dio.post(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        // TODo do action by using context and status code

        return CommonResponseModel.fromJson(e.response.data).toJson();
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message).toJson();
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "").toJson();
      }
    }
  }

  //patch request api
  Future<dynamic> patchRequest(
    String path,
    BuildContext context, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await _dio.patch(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        // TODo do action by using context and status code

        return CommonResponseModel.fromJson(e.response.data).toJson();
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message).toJson();
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "").toJson();
      }
    }
  }

  Future<dynamic> multiPartRequest(
    String path,
    BuildContext context, {
    data,
    String filePath,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      print("request data=${data}");

      FormData _formData = FormData.fromMap(
        {
          "document": filePath,
        },
      );

      var result = await _dio.post(
        path,
        data: _formData,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        // TODo do action by using context and status code

        return CommonResponseModel.fromJson(e.response.data).toJson();
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message).toJson();
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "").toJson();
      }
    }
  }

  Future<dynamic> putRequest(
    String path,
    BuildContext context, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await _dio.put(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        // TODo do action by using context and status code

        return CommonResponseModel.fromJson(e.response.data).toJson();
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message).toJson();
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "").toJson();
      }
    }
  }

  Future<dynamic> deleteRequest(
    String path,
    BuildContext context, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
  }) async {
    try {
      print("request data=${data}");
      print("request data=${data}");
      var result = await _dio.delete(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        // TODo do action by using context and status code

        return CommonResponseModel.fromJson(e.response.data).toJson();
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message).toJson();
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "").toJson();
      }
    }
  }
}

class HeaderInterceptor extends Interceptor {
  var PLATFORM = "";
  var DEVICE_ID = "";
  var AUTHORIZATION = "Authorization";
  GlobalKey<NavigatorState> navigatorKey;

  HeaderInterceptor(this.navigatorKey);

  @override
  onRequest(RequestOptions options) async {
    if (Platform.isAndroid) {
      // Android-specific code
      PLATFORM = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      PLATFORM = "1";
    }
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    var _timeZone = await FlutterNativeTimezone.getLocalTimezone();
    DEVICE_ID = await CommonUtils.commonUtilsInstance.getDeviceID();
    // used to get the token from prefs
    AuthResponseModel authResponseModel =
        await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();
    var fcmToken =
        await FirebaseMessagingUtils.firebaseMessagingUtils.getFcmToken();
    print("fcmtoken ---> ${fcmToken}");
    if (authResponseModel?.userData?.accessToken?.isNotEmpty == true) {
      options.headers = {
        AUTHORIZATION: "Bearer ${authResponseModel?.userData?.accessToken}",
        "Accept": "application/json",
        "Build-Mode": "0",
        "FCM-Token": "${fcmToken}",
        "Device-Id": DEVICE_ID,
        "Build-Version": "${packageInfo.version}",
        "Build-VersionCode": "${packageInfo.buildNumber}",
        "Platform": PLATFORM,
        "TimeZone": _timeZone,
      };
    } else {
      options.headers = {
        "Accept": "application/json",
        "Build-Mode": "0",
        "FCM-Token": "${fcmToken}",
        "Device-Id": DEVICE_ID,
        "Build-Version": "${packageInfo.version}",
        "Build-VersionCode": "${packageInfo.buildNumber}",
        "Platform": PLATFORM,
        "TimeZone": _timeZone,
      };
    }
    options.contentType = "application/json";

    return super.onRequest(options);
  }

  @override
  Future onError(DioError err) async {
    if (err?.response?.statusCode == ApiStatus?.Unauthorise?.value) {
      await SharedPrefUtils.sharedPrefUtilsInstance.clearPref();
      navigatorKey.currentState.pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => SendOtpPage(context: context,isGuestUser:false,)
          ),
          (Route<dynamic> route) => false);
    }
    return super.onError(err);
  }

  @override
  Future onResponse(Response response) {
    log("----Request Base Url ---->${response?.request?.baseUrl}");
    log("----Request Path ---->${response?.request?.path}");
    log("----Response---->${response?.data}");
    return super.onResponse(response);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';

class CommonBottomAppBar extends StatefulWidget {
  OnBottomTabTap onTab;
  BottomBarEnum bottomEnumType;

  CommonBottomAppBar({
    this.onTab,
    this.bottomEnumType,
  });

  @override
  _CommonBottomAppBarState createState() => _CommonBottomAppBarState();
}

class _CommonBottomAppBarState extends State<CommonBottomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: SIZE_10, right: SIZE_10, bottom: SIZE_10, top: SIZE_10),
      child: Container(
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_34)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 3,
              blurRadius: 6,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_34)),
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.only(left: SIZE_10, right: SIZE_10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: _bottomAppbarItem(
                          image: TAB_HOME, barEnum: BottomBarEnum.Home)),
                  Expanded(
                      child: _bottomAppbarItem(
                          image: SEARCH_ICON, barEnum: BottomBarEnum.Cart)),
                  Expanded(
                    child: _bottomAppbarItem(
                        image: PROFILE_ICON, barEnum: BottomBarEnum.Profile),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

// used for the bottom app bar item
  Widget _bottomAppbarItem({String image, BottomBarEnum barEnum}) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        widget.onTab(barEnum);
      },
      child: IntrinsicHeight(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            (barEnum == BottomBarEnum.Home)
                ? Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      (widget.bottomEnumType == barEnum)
                          ? Image.asset(
                              TAB_BG,
                              height: 40,
                              width: 100,
                            )
                          : SizedBox(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset(
                              (widget.bottomEnumType == barEnum)
                                  ? image
                                  : TAB_HOME,
                              color: (widget.bottomEnumType == barEnum)
                                  ? COLOR_PRIMARY
                                  : Colors.grey,
                              height: SIZE_18,
                              width: SIZE_18),
                          (widget.bottomEnumType == barEnum)
                              ? Text(
                                  getTitle(barEnum, context),
                                  style: textStyleSize14WithGreenColor,
                                )
                              : SizedBox()
                        ],
                      )
                    ],
                  )
                : Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      (widget.bottomEnumType == barEnum)
                          ? Image.asset(
                              TAB_BG,
                              height: 40,
                              width: 100,
                            )
                          : SizedBox(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset(
                            image,
                            height: SIZE_20,
                            width: SIZE_20,
                            color: (widget.bottomEnumType == barEnum)
                                ? COLOR_PRIMARY
                                : Colors.grey,
                          ),
                          (widget.bottomEnumType == barEnum)
                              ? Text(
                                  getTitle(barEnum, context),
                                  style: textStyleSize14WithGreenColor,
                                )
                              : SizedBox()
                        ],
                      )
                    ],
                  ),
          ],
        ),
      ),
    );
  }

  String getTitle(BottomBarEnum bottomBarEnum, BuildContext context) {
    if (bottomBarEnum == BottomBarEnum.Home) {
      return AppLocalizations.of(context).bottomappbar.text.home;
    } else if (bottomBarEnum == BottomBarEnum.Cart) {
      return AppLocalizations.of(context).bottomappbar.text.cart;
    } else {
      return AppLocalizations.of(context).bottomappbar.text.profile;
    }
  }
}

typedef void OnBottomTabTap(BottomBarEnum bottomBarEnum);

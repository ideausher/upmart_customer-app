import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/auth/constants/image_constant.dart';
import 'package:user/modules/auth/enums/auth_enums.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/constants/font_constant.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/model/update_ui_data_model.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';

class VerifyOTPWidget extends StatefulWidget {
  String title;
  String subtitle;
  String userPhoneEmail;
  BuildContext context;
  AuthManager authManager;
  AuthState authState;
  AuthBloc authBloc;
  Function onResendOtpButtonPressed;
  Function onSendOtpButtonPressed;
  Function onChangeNumberPressed;
  bool isForEmail;
  bool isFromEmailEdit;
  bool isComingFromEditEmailPhone;
  TextEditingController phoneNumberController;
  TextEditingController otpController;

  VerifyOTPWidget(
      {this.context,
      this.title,
      this.subtitle,
      this.authManager,
      this.authState,
      this.authBloc,
      this.userPhoneEmail,
      this.isFromEmailEdit: false,
      this.onSendOtpButtonPressed,
      this.onResendOtpButtonPressed,
      this.isForEmail: false,
      this.isComingFromEditEmailPhone: false,
      this.phoneNumberController,
      this.otpController,
      this.onChangeNumberPressed});

  @override
  _VerifyOTPWidgetState createState() => _VerifyOTPWidgetState();
}

class _VerifyOTPWidgetState extends State<VerifyOTPWidget> {
  BuildContext _context;
  UpdateUiDataModel _updateUiDataModel;

  //Controllers for OTP
  StreamController<ErrorAnimationType> _errorController =
      StreamController<ErrorAnimationType>();
  int _endTime = DateTime.now()
      .add(Duration(minutes: Timer?.sec?.value))
      .millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
    _updateUiDataModel = new UpdateUiDataModel();
  }

  @override
  void dispose() {
    super.dispose();
    _errorController.close();
    widget?.phoneNumberController?.dispose();
    widget?.otpController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = widget?.context;
    _updateUiDataModel =
        widget?.authState?.updateUiDataModel ?? _updateUiDataModel;
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Center(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Image.asset(UPMART_BACKGROUND,
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      _showLogInLogo(),
                      SizedBox(
                        height: SIZE_50,
                      ),

                      Padding(
                        padding: const EdgeInsets.only(bottom: SIZE_20),
                        child: Container(
                          height: CommonUtils.commonUtilsInstance
                              .getPercentageSize(
                                  context: _context,
                                  percentage: SIZE_60,
                                  ofWidth: false),
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(SIZE_10))),
                            margin: EdgeInsets.all(SIZE_20),
                            child: Padding(
                              padding: const EdgeInsets.all(SIZE_16),
                              child: SingleChildScrollView(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: SIZE_10,
                                    ),

                                    _showTitleText(),
                                    // _enterOtpView(),
                                    _showPhoneNumberTextField(),
                                    _enterOtpView(),

                                    // _showChangeNumberView(),
                                    //_showOTPTimerView(),
                                    _continueButton(),
                                    SizedBox(
                                      height: CommonUtils?.commonUtilsInstance
                                          ?.getPercentageSize(
                                              context: _context,
                                              ofWidth: false,
                                              percentage: SIZE_12),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                      /*    _enterOtpView(),
              _showChangeNumberView(),
              _showOTPTimerView(),
              _continueButton(),*/
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
                child: (widget?.isComingFromEditEmailPhone == true)
                    ? _showAppBar()
                    : const SizedBox())
          ],
        ),
      ),
    );
  }

  //Method to show subtitle text of verify otp screen
  Widget _verifyOtpLogo() {
    return Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(bottom: SIZE_40),
            width: MediaQuery.of(_context).size.width,
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: _context, ofWidth: false, percentage: SIZE_25),
            child: Image.asset(
              VERIFY_OTP_LOGO,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, ofWidth: false, percentage: SIZE_15),
            )),
      ],
    );
  }

  //This method will return a view to fill OTP
  Widget _enterOtpView() {
    return Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: _textFieldForm(
          keyboardType: TextInputType.number,
          hint: 'Enter OTP',
          elevation: ELEVATION_0,
          icon: null,
          inputFormatterLength: 4,
          controller: widget?.otpController,
        ));
  }

  //Method to show continue Raised Button.
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_30),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_7, ofWidth: false),
      //width: CommonUtils.commonUtilsInstance.getPercentageSize(context: _context, percentage: SIZE_50, ofWidth: true),
      child: RaisedButton(
        onPressed: () {
          _callOTPVerification(
            otp: widget?.otpController?.text?.toString(),
          );
          //_validateOtp();
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SIZE_80)),
        padding: EdgeInsets.all(SIZE_0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: <Color>[COLOR_DARK_PRIMARY, COLOR_PRIMARY],
                /* colors: [
                */ /*  (widget?.authState?.updateUiDataModel?.otpLength ==
                          MaxLength.OTP.value)
                      ? COLOR_PRIMARY
                      : COLOR_BORDER_GREY,
                  (widget?.authState?.updateUiDataModel?.otpLength ==
                          MaxLength.OTP.value)
                      ? COLOR_PRIMARY
                      : COLOR_BORDER_GREY*/ /*
                ],*/
              ),
              borderRadius: BorderRadius.circular(SIZE_30)),
          child: Container(
            alignment: Alignment.center,
            child: Text(
             AppLocalizations.of(_context).common.button.continueText,
              textAlign: TextAlign.center,
              style: textStyleSize14WithWhiteColor,
            ),
          ),
        ),
      ),
    );
  }

  //method to show verify mobile number title view
  Widget _verifyMobileNumberTitleView() {
    return Text(
      widget.title,
      style: AppConfig.of(_context).themeData.textTheme.headline1,
    );
  }

  //method to show verify mobile number subtitle view
  Widget _verifyMobileNumberSubtitleView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: widget.subtitle,
                    style: AppConfig.of(_context)
                        .themeData
                        .primaryTextTheme
                        .bodyText1),
                TextSpan(
                    text: widget.userPhoneEmail ?? "",
                    style: AppConfig.of(_context)
                        .themeData
                        .primaryTextTheme
                        .caption),
              ],
            ),
            textScaleFactor: 0.5,
          ),
        )
      ],
    );
  }

  //method to show change number view
  Widget _showChangeNumberView() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_12, right: SIZE_12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            onTap: () {
              _onChangeNumberPressed();
            },
            child: Text(
              (widget?.isFromEmailEdit == true)
                  ? ""
                  : AppLocalizations.of(_context)
                      .verifyotppage
                      .text
                      .changeNumber,
              style: AppConfig.of(_context).themeData.textTheme.headline3,
            ),
          ),
          Wrap(
            direction: Axis.vertical,
            children: <Widget>[
              InkWell(
                onTap: () {
                  if (widget?.authState?.updateUiDataModel?.resendButton ==
                      COLOR_PRIMARY) {
                    _callOnResendOtp();
                  }
                },
                child: Text(
                    AppLocalizations.of(_context).verifyotppage.text.resend,
                    style: TextStyle(
                        color: widget
                                ?.authState?.updateUiDataModel?.resendButton ??
                            COLOR_BORDER_GREY,
                        fontSize: SIZE_16,
                        fontWeight: FontWeight.w600,
                        fontFamily: FONT_FAMILY_INTER)),
              ),
            ],
          )
        ],
      ),
    );
  }

  //this method will call the validate otp method
  void _callOTPVerification({String otp}) {
    widget?.onSendOtpButtonPressed(otp);
  }

  //this method is used to call resend otp method
  void _callOnResendOtp() {
    _endTime = DateTime.now()
        .add(Duration(minutes: Timer?.sec?.value))
        .millisecondsSinceEpoch;
    _updateUiDataModel.resendButton = COLOR_BORDER_GREY;
    widget?.authManager?.updateUI(
        context: _context,
        authBloc: widget?.authBloc,
        authState: widget?.authState,
        updateUiDataModel: _updateUiDataModel);
    widget?.onResendOtpButtonPressed();
  }

  void _onChangeNumberPressed() {
    widget?.onChangeNumberPressed();
  }

  //this method is used to change color when user fills otp
  void _callChangeButtonColorEvent() {
    widget?.authManager?.updateUI(
        context: _context,
        authBloc: widget?.authBloc,
        authState: widget?.authState,
        updateUiDataModel: _updateUiDataModel);
  }

  //method to show otp timer widget view
  Widget _showOTPTimerView() {
    return CountdownTimer(
      endTime: _endTime,
      widgetBuilder: (_, CurrentRemainingTime time) {
        return (time != null)
            ? Container(
                padding: const EdgeInsets.only(right: SIZE_12),
                alignment: Alignment.topRight,
                width: MediaQuery.of(context).size.width,
                child: Text(
                  '0${time?.min ?? "0"} : ${(time?.sec?.toString()?.length == Timer?.sec?.value) ? time?.sec : '0${time?.sec}' ?? "00:00"}',
                  style: AppConfig.of(_context).themeData.textTheme.headline4,
                ),
              )
            : Container(
                padding: const EdgeInsets.only(right: SIZE_12),
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.topRight,
                child: Text(
                  "00:00",
                  style: AppConfig.of(_context).themeData.textTheme.headline4,
                  textAlign: TextAlign.end,
                ),
              );
      },
      emptyWidget: Text(
        "00:00",
        style: AppConfig.of(_context).themeData.textTheme.headline4,
        textAlign: TextAlign.center,
      ),
      onEnd: () {
        UpdateUiDataModel updateUiDataModel = _updateUiDataModel;
        updateUiDataModel.resendButton = COLOR_PRIMARY;
        widget?.authManager?.updateUI(
            context: _context,
            authBloc: widget?.authBloc,
            authState: widget?.authState,
            updateUiDataModel: updateUiDataModel);
      },
      textStyle: AppConfig.of(_context).themeData.textTheme.headline4,
    );
  }

  // used to create a top text view
  Widget _showLogInLogo() {
    return Container(
        margin: EdgeInsets.only(top: SIZE_20),
        width: MediaQuery.of(_context).size.width,
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, ofWidth: false, percentage: SIZE_20),
        child: Image.asset(
          SIGN_UP_LOGO,
        ));
  }

  //this method is used to show title text
  Widget _showTitleText() {
    return Text(
        (widget?.isComingFromEditEmailPhone == true)
            ? 'Verify OTP'
            : AppLocalizations.of(_context).sendotppage.text.signIn,
        textAlign: TextAlign.center,
        style: AppConfig.of(_context).themeData.primaryTextTheme.headline5);
  }

  Widget _showPhoneNumberTextField() {
    widget.phoneNumberController.text = widget?.userPhoneEmail ?? "";
    return Container(
      margin: EdgeInsets.only(top: SIZE_16),
      padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_GREY_PICKER, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: _textFieldForm(
        keyboardType: TextInputType.number,
        enabled: false,
        elevation: ELEVATION_0,
        icon: null,
        prefixIcon: Padding(
          padding: const EdgeInsets.only(top: SIZE_2),
          child: Image.asset(
            USER_PHONE_ICON,
            height: SIZE_10,
            width: SIZE_10,
          ),
        ),
        textStyle: textStyleSize14BlackColor,
        controller: widget?.phoneNumberController,
      ),
    );
  }

  //method to return textform field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      IconData icon,
      String hint,
      double elevation,
      bool enabled,
      Widget prefixIcon,
      TextStyle textStyle,
      int inputFormatterLength: 20,
      FormFieldValidator<String> validator}) {
    return TextFormField(
      validator: validator,
      textCapitalization: TextCapitalization.words,
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
        new LengthLimitingTextInputFormatter(inputFormatterLength),
      ],
      onChanged: (name) {},
      style: AppConfig.of(_context).themeData.textTheme.subtitle1,
      controller: controller,
      enabled: enabled ?? true,
      keyboardType: keyboardType,
      decoration: InputDecoration(
          hintStyle: AppConfig.of(_context).themeData.textTheme.subtitle1,
          hintText: hint,
          prefixIcon: prefixIcon ?? null,
          border: InputBorder.none),
    );
  }

  //method to show app bar on in case of edit phone and email
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: false,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(_context);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: Colors.white,
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }
}

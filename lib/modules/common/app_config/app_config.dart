import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:user/modules/common/base_api/base_api.dart';

import '../../../modules/common/theme/app_themes.dart';

class AppConfig extends InheritedWidget {
  AppConfig({
    @required this.appName,
    @required this.flavorName,
    @required this.apiBaseUrl,
    @required this.termsUrl,
    @required Widget child,
    @required this.baseApi,
  }) : super(child: child) {
    themeData = appTheme;
    baseApi.setUpOptions(baseUrl: apiBaseUrl);
  }

  final String appName;
  final String flavorName;
  final String apiBaseUrl;
  final String termsUrl;
  ThemeData themeData;
  final BaseApi baseApi;

  static AppConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }
}

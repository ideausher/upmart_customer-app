import 'package:flutter/material.dart';
import 'package:user/modules/stripe/customer/page/stripe_add_card_page.dart';
import 'package:user/modules/stripe/customer/page/stripe_customer_card_listing.dart';



class StripeCustomerRoutes {

  static const String STRIPE_CUSTOMER_ORDER_LISTING_SCREEN = '/stripe_customer_order_listing_screen';
  static const String STRIPE_CUSTOMER_ADD_CARD_SCREEN = '/stripe_customer_add_card_screen';

  static Map<String, WidgetBuilder> routes() {
    return {
      // When we navigate to the "/" route, build the FirstScreen Widget
      STRIPE_CUSTOMER_ORDER_LISTING_SCREEN: (context) => StripeCustomerCardListingPage(context),
      STRIPE_CUSTOMER_ADD_CARD_SCREEN: (context) => StripeAddCartPage(context)
    };
  }
}
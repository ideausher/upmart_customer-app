import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_bloc.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_event.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_state.dart';
import 'package:user/modules/stripe/customer/manager/stripe_customer_manager.dart';
import '../../../../localizations.dart';

class StripeCustomerCardListingPage extends StatefulWidget {
  BuildContext context;

  StripeCustomerCardListingPage(this.context) {
    this.context = context;
  }

  @override
  _StripeCustomerCardListingPageState createState() =>
      _StripeCustomerCardListingPageState(context);
}

class _StripeCustomerCardListingPageState
    extends State<StripeCustomerCardListingPage> {
  StripeBloc _stripeBloc; // stripe bloc
  StripeState _state; // stripe state
  var _scaffoldKey = new GlobalKey<ScaffoldState>(); // scaffold key
  StripeCustomerManager _stripeCustomerManager = StripeCustomerManager();
  BuildContext widgetContext;

  _StripeCustomerCardListingPageState(this.widgetContext);

  bool isNativePaySupported = false;

  @override
  void initState() {
    super.initState();
    isNativePaySupported = ModalRoute.of(widget.context).settings.arguments;
    print('The value of isnative  is $isNativePaySupported');

    _stripeBloc = StripeBloc();
    _stripeCustomerManager?.scaffoldState = _scaffoldKey?.currentState;
  /*  StripePayment.setOptions(StripeOptions(
        publishableKey: publishableStripeKey,
        merchantId: 'merchant.com.limitlesscustomer',
        androidPayMode: 'test'));*/

    // used to get the customer id
    _stripeBloc.emitEvent(
      GetCustomerIdEvent(
        isLoading: true,
        context: widget.context,
      ),
    );
  }

  @override
  void dispose() {
    _stripeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    widgetContext = context;
    return WillPopScope(
      onWillPop: () {
        return NavigatorUtils.navigatorUtilsInstance
            .navigatorPopScreen(context);
        //_stripeCustomerManager?.actionBackPressOnCardListing(context: context, stripeState: _state,stripeSelectedCardId:Managers.stripeCardId);
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CommonUtils.commonUtilsInstance.getAppBar(
            context: context,
            elevation: 0,
            backGroundColor: Colors.white,
            popScreenOnTapOfLeadingIcon: true,
            actionWidgets: [
              Padding(
                padding: const EdgeInsets.all(SIZE_10),
                child: NotificationReadCountWidget(
                  context: context,
                ),
              )
            ],
            appBarTitle: AppLocalizations.of(widget?.context)
                .cardlistpage
                .appbartitle
                .paymentMode,
            centerTitle: false,
            appBarTitleStyle: AppConfig.of(widget.context)
                .themeData
                .primaryTextTheme
                .headline3,
            leadingWidget: IconButton(
              icon: Icon(
                Icons.arrow_back,
                size: SIZE_20,
                color: Colors.black,
              ),
              onPressed: () {
                NavigatorUtils.navigatorUtilsInstance
                    .navigatorPopScreen(context);
                // _stripeCustomerManager?.actionBackPressOnCardListing(context: context, stripeState: _state,stripeSelectedCardId:Managers.stripeCardId);
              },
            )),
        body: BlocEventStateBuilder<StripeState>(
          bloc: _stripeBloc,
          builder: (BuildContext context, StripeState state) {
            widgetContext = context;
            if (_state != state) {
              _state = state;
              print("state called ${_state.customerId}");
              _stripeCustomerManager.actionOnCardListingStateChanged(
                  context: context,
                  stripeState: state,
                  stripeBloc: _stripeBloc,
                  scaffoldState: _scaffoldKey.currentState);
            }
            return ModalProgressHUD(
              inAsyncCall: state?.isLoading ?? true,
              child: Padding(
                padding: const EdgeInsets.only(left: SIZE_10, right: SIZE_10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // _showPaymentMethodTitle(),
                    SizedBox(height: SIZE_10),
                    _showSubTitleText(),
                    SizedBox(height: SIZE_10),
                    //_infoText(context: context),
                    _cardList(context: context),
                    //_showApplePayOption(),
                    _showAddCardButtonView(context: context),
                    /*       Padding(
                      padding: const EdgeInsets.only(bottom: SIZE_20),
                      child: _showGoogleOrApplePayOption(),
                    )*/
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _getCardItem({int index, BuildContext context}) {
    var _cardDetails = _state?.cardListResponseModel?.data[index];
    return Slidable(
      actionExtentRatio: 0.20,
      actionPane: SlidableDrawerActionPane(),
      child: InkWell(
        onTap: () {
          _stripeCustomerManager?.selectCardToPay(
              _state?.cardListResponseModel,
              _stripeBloc,
              context,
              _state?.customerId,
              _cardDetails?.id,
              _state?.bookingResponseModel);
        },
        child: Card(
          shape: Border.all(
              color: _stripeCustomerManager?.getSelectedCardColor(
                  stripeState: _state, cardDetails: _cardDetails),
              width: SIZE_1),
          elevation: 2.0,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(SIZE_10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 20,
                      child: getCardTypeIcon(
                          fromCardNumber: false, value: _cardDetails.brand)),
                  SizedBox(
                    width: SIZE_20,
                  ),
                  Expanded(
                      flex: 90,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          // used for the card type
                          Row(
                            children: [
                              Text(
                                '**** **** **** ${_cardDetails.last4}',
                                textAlign: TextAlign.end,
                                style: AppConfig.of(context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline3,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: SIZE_10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              // used for the card number
                              Text(
                                AppLocalizations.of(widget?.context)
                                    .cardlistpage
                                    .text
                                    .expires,
                                style: textStyleSize14GreyColor
                                /*'**** **** **** ${_cardDetails.last4}'*/,
                              ),
                              SizedBox(
                                width: SIZE_5,
                              ),
                              // used for the expiry date
                              Text(
                                "${_cardDetails.expMonth}/${_cardDetails.expYear}",
                                style: textStyleSize14BlackColor,
                              ),
                            ],
                          )
                        ],
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
      secondaryActions: <Widget>[
        SlideAction(
          /* caption: 'Delete',
          iconWidget: Icon(
            Icons.delete,
            color: AppConfig.of(context).themeData.primaryColor,
          )*/
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.delete,
                color: AppConfig.of(context).themeData.primaryColor,
              ),
              Text(
                AppLocalizations.of(widget?.context).cardlistpage.text.delete,
              )
            ],
          ),
          onTap: () {
            _stripeCustomerManager?.actionOnDeleteCard(
                stripeBloc: _stripeBloc,
                context: context,
                stripeState: _state,
                scaffoldState: _scaffoldKey.currentState,
                cardDetails: _cardDetails);
          },
        ),
      ],
    );
  }

  //this method will return Sign Up Button View
  Widget _showAddCardButtonView({BuildContext context}) {
    return Expanded(
      flex: 1,
      child: Align(
        alignment: Alignment.center,
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: SIZE_30,
          spacing: SIZE_30,
          direction: Axis.vertical,
          crossAxisAlignment: WrapCrossAlignment.center,
          runAlignment: WrapAlignment.center,
          children: [
            InkWell(
              onTap: () {
                _stripeCustomerManager.actionOnClickOfAddToCard(
                    scaffoldState: _scaffoldKey.currentState,
                    stripeState: _state,
                    context: context,
                    stripeBloc: _stripeBloc);
              },
              child: Row(
                children: [
                  Icon(
                    Icons.add,
                    size: SIZE_20,
                    color: COLOR_PRIMARY,
                  ),
                  Text(
                    AppLocalizations.of(widget?.context)
                        .cardlistpage
                        .text
                        .addMoreCards,
                    style: textStyleSize14WithGreenColor,
                  ),
                ],
              ),
            ),
            Container(
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: widgetContext, percentage: SIZE_7, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: widgetContext, percentage: SIZE_80, ofWidth: true),
              child: RaisedButton(
                onPressed: () {
                  if (_state?.stripeCardId?.isNotEmpty == true) {
                    _stripeCustomerManager?.actionDoPayment(
                        context: context,
                        scaffoldState: _scaffoldKey?.currentState,
                        creditCardModel: _state?.creditCardModel,
                        stripeBloc: _stripeBloc,
                        stripeId: _state?.stripeCardId,
                        stripeState: _state);
                  } else
                  //in this case show a snackbar to user to select a card before booking
                  {
                    _showSnackBar(context: context);
                  }
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(SIZE_80)),
                padding: EdgeInsets.all(SIZE_0),
                child: Ink(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [COLOR_LIGHT_GREEN, COLOR_DARK_PRIMARY],
                      ),
                      borderRadius: BorderRadius.circular(SIZE_30)),
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      AppLocalizations.of(context).common.button.continueText,
                      textAlign: TextAlign.center,
                      style: textStyleSize14WithWhiteColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // card list
  Widget _cardList({BuildContext context}) {
    return Expanded(
      flex: 2,
      child: (_state?.cardListResponseModel?.data?.isNotEmpty == true)
          ? ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(
                color: COLOR_BORDER_GREY,
              ),
              itemCount: _state?.cardListResponseModel?.data?.length,
              itemBuilder: (BuildContext context, int index) {
                return _getCardItem(index: index, context: context);
              },
            )
          : Visibility(
              visible: !_state?.isLoading,
              child: Center(
                child: Text(
                    AppLocalizations.of(context).cardlistpage.text.noCardAdded),
              ),
            ),
    );
  }

  @override
  void onButtonTapped(
      String tag, BuildContext widgetContext, BuildContext widgetParentContext,
      {GlobalKey<FormState> stateFulWidgetKey}) {
    _stripeCustomerManager.actionOnClickOfAddToCard(
        scaffoldState: _scaffoldKey.currentState,
        stripeState: _state,
        context: context,
        stripeBloc: _stripeBloc);
  }

  Widget _showSubTitleText() {
    return Text(
      AppLocalizations.of(widget?.context).cardlistpage.title.paymentPreference,
      style: textStyleSize18WithBlack,
    );
  }

//to snack bar to select a card for payment
  void _showSnackBar({BuildContext context}) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
        context: context,
        scaffoldState: _scaffoldKey?.currentState,
        message:
            AppLocalizations.of(context).cardlistpage.error.pleaseSelectCard);
  }

  //method to show google pay or apple pay option
  Widget _showGoogleOrApplePayOption() {
    if (Platform.isAndroid) {
      return InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          //calling for google pay
          _stripeCustomerManager?.callGoogleOrApplePayEvent(
              stripeBloc: _stripeBloc,
              stripeId: _state?.stripeCardId,
              scaffoldState: _scaffoldKey?.currentState,
              context: widgetContext,
              stripeState: _state,
              creditCardModel: _state?.creditCardModel);
        },
        child: Align(
            alignment: Alignment.center,
            child: Text(
              'Pay with Google Pay',
              style: textStyleSize14WithGreenColor,
              textAlign: TextAlign.center,
            )),
      );
    } else {
      return SizedBox();
    }
  }

  //this method will show apple pay option
  _showApplePayOption() {
    if (isNativePaySupported) {
      if (Platform.isIOS) {
        return InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () {
            createPaymentMethodNative();
          },
          child: Align(
            alignment: Alignment.center,
            child: Image.asset(
              APPLE_LOGO,
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_50, ofWidth: true),
            ),
          ),
        );
      } else {
        return SizedBox();
      }
    } else {
      return SizedBox();
    }
  }

  createPaymentMethodNative() {
    _stripeCustomerManager?.callApplePay(
        stripeBloc: _stripeBloc,
        stripeId: _state?.stripeCardId,
        scaffoldState: _scaffoldKey?.currentState,
        context: widgetContext,
        stripeState: _state,
        creditCardModel: _state?.creditCardModel);
  }
}

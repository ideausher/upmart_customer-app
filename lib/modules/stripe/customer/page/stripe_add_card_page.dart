
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:flutter_svg/svg.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_bloc.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_state.dart';
import 'package:user/modules/stripe/customer/constants/stripe_customer_constants.dart';
import 'package:user/modules/stripe/customer/manager/stripe_customer_manager.dart';
import 'package:user/modules/stripe/customer/validator/stripe_customer_validator.dart';


class StripeAddCartPage extends StatefulWidget {
  BuildContext context;

  StripeAddCartPage(this.context);

  @override
  _StripeAddCartPageState createState() => new _StripeAddCartPageState();
}

class _StripeAddCartPageState extends State<StripeAddCartPage>  {
  var _scaffoldKey = new GlobalKey<ScaffoldState>(); // scaffold key
  String _cardNumber = ''; // card no
  String _expiryDate = ''; // expiry date
  String _cardHolderName = ''; // card hold name
  String _cvvCode = ''; // cvv code
  bool _isCvvFocused = false;

  StripeBloc _stripeBloc = StripeBloc(); // bloc
  StripeState _stripeState; // state

  StripeCustomerManager _stripeCustomerManager = StripeCustomerManager();

  @override
  void initState() {
    // set up stripe payment option
    StripePayment.setOptions(
        StripeOptions(publishableKey: publishableStripeKey, merchantId: "Test", androidPayMode: 'test'));

    super.initState();
  }

  @override
  void dispose() {
    _stripeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return _stripeCustomerManager?.actionBackPressOnCardListing(context: context,
            stripeState: _stripeState,stripeSelectedCardId: _stripeState?.customerId);
      },
      child: new Scaffold(
          key: _scaffoldKey,
          appBar:CommonUtils.commonUtilsInstance.getAppBar(
              context: context,
              elevation: 0,
              backGroundColor: Colors.white,
              popScreenOnTapOfLeadingIcon:true,
              actionWidgets: [
                Padding(
                  padding: const EdgeInsets.only(bottom: SIZE_10, right: SIZE_10),
                  child: SvgPicture.asset(
                    NOTIFICATION_ICON,
                    height: SIZE_16,
                    width: SIZE_16,
                  ),
                )
              ],
              appBarTitle: AppLocalizations.of(widget?.context).addcardpage.appbartitle.cardListing,
              centerTitle: true,
              leadingWidget: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  size: SIZE_20,
                ),
                onPressed: () {
                   _stripeCustomerManager?.actionBackPressOnCardListing(context: context, stripeState: _stripeState,stripeSelectedCardId:  _stripeState?.customerId);
                },
              )),
          body: BlocEventStateBuilder<StripeState>(
            bloc: _stripeBloc,
            builder: (BuildContext context, StripeState state) {
              if (_stripeState != state) {
                _stripeState = state;
                _stripeCustomerManager?.actionOnAddCardStateChanged(expiryDate: _expiryDate,
                    cvvCode: _cvvCode,
                    cardNumber: _cardNumber,
                    cardHolderName: _cardHolderName,
                    scaffoldState: _scaffoldKey.currentState,
                    context: context,
                    stripeState: _stripeState);
              }
              return ModalProgressHUD(
                inAsyncCall: state?.isLoading ?? false,
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(SIZE_10),
                  child: Wrap(
                    crossAxisAlignment:WrapCrossAlignment.center,
                    runSpacing: SIZE_20,
                    children: <Widget>[
                      _showTitleWidget(),
                      CreditCardWidget(
                        cardNumber: _cardNumber,
                        expiryDate: _expiryDate,
                        cardHolderName: _cardHolderName,
                        cvvCode: _cvvCode,
                        cardBgColor:COLOR_PRIMARY,
                        showBackView: _isCvvFocused,
                      ),
                      CreditCardForm(
                        onCreditCardModelChange: onCreditCardModelChange,
                      ),
                      _showSaveButtonButton(context)
                    ],
                  ),
                ),
              );
            },
          )),
    );
  }

  void _validateInputs(BuildContext context) {
    var result = StripeCustomerValidator.validateCard(
        context: context,
        scaffoldState: _scaffoldKey.currentState,
        cardHolderName: _cardHolderName,
        cardNumber: _cardNumber,
        cvvCode: _cvvCode,
        expiryDate: _expiryDate);

    if (result == null) {
      _stripeCustomerManager.addCard(context: context,
          scaffoldState: _scaffoldKey.currentState,
          cardHolderName: _cardHolderName,
          cardNumber: _cardNumber,
          cvvCode: _cvvCode,
          expiryDate: _expiryDate,
          stripeBloc: _stripeBloc);
    }
  }

  //method used to show save button text
  Widget _showSaveButtonButton(BuildContext context) {
    return  Align(
      alignment: Alignment.center,
      child: Container(
        height: CommonUtils.commonUtilsInstance
            .getPercentageSize(context: context, percentage: SIZE_6, ofWidth: false),
        width: CommonUtils.commonUtilsInstance
            .getPercentageSize(context: context, percentage: SIZE_50, ofWidth: true),
        child: RaisedButton(
          onPressed: () {
           _validateInputs(context);
          },
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(SIZE_80)),
          padding: EdgeInsets.all(SIZE_0),
          child: Ink(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [COLOR_LIGHT_GREEN, COLOR_DARK_PRIMARY],
                ),
                borderRadius: BorderRadius.circular(SIZE_30)),
            child: Container(
              alignment: Alignment.center,
              child: Text(
                AppLocalizations.of(context).common.button.continueText,
                textAlign: TextAlign.center,
                style: textStyleSize14WithWhiteColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      _cardNumber = creditCardModel.cardNumber.trim();
      _expiryDate = creditCardModel.expiryDate.trim();
      _cardHolderName = creditCardModel.cardHolderName.trim();
      _cvvCode = creditCardModel.cvvCode.trim();
      _isCvvFocused = creditCardModel.isCvvFocused;
    });
  }

  @override
  void onButtonTapped(String tag, BuildContext widgetContext, BuildContext widgetParentContext, {GlobalKey<FormState> stateFulWidgetKey}) {
    _validateInputs(widgetContext);
  }

  Widget _showTitleWidget() {
    return Text(AppLocalizations.of(widget?.context).addcardpage.title.addCard,
      style:textStyleSize22WithBlackColor,
    );

  }
}

import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_card_list_response_model.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_bloc.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_event.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_state.dart';
import 'package:user/modules/stripe/customer/model/booking_out_of_stock_response_model.dart';
import 'package:user/modules/stripe/customer/model/booking_response_model.dart';
import 'package:user/routes.dart';

// check card number patterns
Map<CardType, Set<List<String>>> cardNumPatterns =
    <CardType, Set<List<String>>>{
  CardType.visa: <List<String>>{
    <String>['4'],
  },
  CardType.americanExpress: <List<String>>{
    <String>['34'],
    <String>['37'],
  },
  CardType.discover: <List<String>>{
    <String>['6011'],
    <String>['622126', '622925'],
    <String>['644', '649'],
    <String>['65']
  },
  CardType.mastercard: <List<String>>{
    <String>['51', '55'],
    <String>['2221', '2229'],
    <String>['223', '229'],
    <String>['23', '26'],
    <String>['270', '271'],
    <String>['2720'],
  },
};

// check card type
CardType detectCCType({String cardNumber}) {
  //Default card type is other
  CardType cardType = CardType.otherBrand;

  if (cardNumber.isEmpty) {
    return cardType;
  }

  cardNumPatterns.forEach(
    (CardType type, Set<List<String>> patterns) {
      for (List<String> patternRange in patterns) {
        // Remove any spaces
        String ccPatternStr = cardNumber.replaceAll(RegExp(r'\s+\b|\b\s'), '');
        final int rangeLen = patternRange[0].length;
        // Trim the Credit Card number string to match the pattern prefix length
        if (rangeLen < cardNumber.length) {
          ccPatternStr = ccPatternStr.substring(0, rangeLen);
        }

        if (patternRange.length > 1) {
          // Convert the prefix range into numbers then make sure the
          // Credit Card num is in the pattern range.
          // Because Strings don't have '>=' type operators
          final int ccPrefixAsInt = int.parse(ccPatternStr);
          final int startPatternPrefixAsInt = int.parse(patternRange[0]);
          final int endPatternPrefixAsInt = int.parse(patternRange[1]);
          if (ccPrefixAsInt >= startPatternPrefixAsInt &&
              ccPrefixAsInt <= endPatternPrefixAsInt) {
            // Found a match
            cardType = type;
            break;
          }
        } else {
          // Just compare the single pattern prefix with the Credit Card prefix
          if (ccPatternStr == patternRange[0]) {
            // Found a match
            cardType = type;
            break;
          }
        }
      }
    },
  );

  return cardType;
}

// check card type
CardType detectCCTypeFromBrandName({String brandName}) {
  //Default card type is other
  CardType cardType = CardType.otherBrand;

  if (brandName != null && brandName.isNotEmpty) {
    if (brandName.toLowerCase().contains("visa")) {
      cardType = CardType.visa;
    } else if (brandName.toLowerCase().contains("master")) {
      cardType = CardType.mastercard;
    } else if (brandName.toLowerCase().contains("american")) {
      cardType = CardType.americanExpress;
    } else if (brandName.toLowerCase().contains("discover")) {
      cardType = CardType.discover;
    }
  }
  return cardType;
}

// used to get expiry date
String getExpiryDate({CardDetails cardDetails}) {
  var _month;
  var _year;
  _month = (cardDetails?.expMonth?.toString()?.isNotEmpty == true)
      ? cardDetails?.expMonth?.toString()
      : "00";
  _year = (cardDetails?.expYear?.toString()?.isNotEmpty == true)
      ? cardDetails?.expYear?.toString()
      : "0000";

  return "$_month/$_year";
}

//method to get icon card number wise
Widget getCardTypeIcon({String value, bool fromCardNumber}) {
  Widget icon;
  switch ((fromCardNumber)
      ? detectCCType(cardNumber: value)
      : detectCCTypeFromBrandName(brandName: value)) {
    case CardType.visa:
      icon = Image.asset(
        'icons/visa.png',
        height: 48,
        width: 48,
        package: 'flutter_credit_card',
      );
      break;

    case CardType.americanExpress:
      icon = Image.asset(
        'icons/amex.png',
        height: 48,
        width: 48,
        package: 'flutter_credit_card',
      );

      break;

    case CardType.mastercard:
      icon = Image.asset(
        'icons/mastercard.png',
        height: 48,
        width: 48,
        package: 'flutter_credit_card',
      );

      break;

    case CardType.discover:
      icon = Image.asset(
        'icons/discover.png',
        height: 48,
        width: 48,
        package: 'flutter_credit_card',
      );

      break;

    default:
      icon = Container(
        height: 48,
        width: 48,
      );

      break;
  }

  return icon;
}

class StripeCustomerManager {
  ScaffoldState scaffoldState;
  bool isCardSame = false;
  String cardId = "";

  // used on action on state change
  actionOnAddCardStateChanged(
      {BuildContext context,
      StripeState stripeState,
      String cvvCode,
      String cardNumber,
      String expiryDate,
      String cardHolderName,
      ScaffoldState scaffoldState}) {
    if (stripeState.isLoading == false) {
      if (stripeState.message != null) {
        cardNumber = '';
        expiryDate = '';
        cardHolderName = '';
        cvvCode = '';

        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (stripeState.message ==
              AppLocalizations.of(context).common.text.error) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                scaffoldState: scaffoldState,
                context: context,
                message: AppLocalizations.of(context)
                    .common
                    .error
                    .somethingWentWrong);
          } else if(stripeState.message ==
              AppLocalizations.of(context).common.text.success) {
            print('card created successfully and the is is ${stripeState.customerId}');
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context,
                dataToBeSend: stripeState.customerId);
          }
        });
      }
    }
  }

  // used to add to card
  addCard(
      {BuildContext context,
      StripeBloc stripeBloc,
      String cvvCode,
      String cardNumber,
      String expiryDate,
      String cardHolderName,
      ScaffoldState scaffoldState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context)
        .then((value) {
      if (value) {
        stripeBloc.emitEvent(AddCardEvent(
            isLoading: true,
            creditCardModel: CreditCardModel(
                cardNumber, expiryDate, cardHolderName, cvvCode, false),
            context: context));
      } else {
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            scaffoldState: scaffoldState,
            context: context,
            message: AppLocalizations.of(context)
                .common
                .text
                .checkInternetConnection);
      }
    });
  }

  //method to select card to pay
  selectCardToPay(
      StripeCardListResponseModel cardListResponseModel,
      StripeBloc stripeBloc,
      BuildContext context,
      String customerId,
      String stripeCardId,
      BookingResponseModel bookingResponseModel) {
    if (cardListResponseModel != null) {
      stripeBloc.emitEvent(SelectCardEvent(
          isLoading: false,
          context: context,
          cardListModel: cardListResponseModel,
          customerId: customerId,
          bookingResponseModel: bookingResponseModel,
          bookingOutOfStockResponseModel: new BookingOutOfStockResponseModel(),
          stripeCardId: stripeCardId));
    }
  }

  // back press on card listing Screen
  actionBackPressOnCardListing({
    BuildContext context,
    StripeState stripeState,
    String stripeSelectedCardId,
  }) {
    if (stripeState?.isLoading == true) {
    } else {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorPopScreen(context, dataToBeSend: stripeSelectedCardId);
    }
  }

  // action on cart listing screen
  actionOnCardListingStateChanged(
      {BuildContext context,
      StripeState stripeState,
      StripeBloc stripeBloc,
      ScaffoldState scaffoldState}) {
    if (stripeState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (stripeState?.bookingResponseModel?.statusCode ==
            ApiStatus?.Success?.value) {
          CartUtilsManager.cartUtilsInstance.clearData();
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPushedNameResult(context, Routes.PAYMENT_SUCCESSFUL);
        } else if (stripeState?.bookingOutOfStockResponseModel?.statusCode ==
            ApiStatus?.OutOfStock?.value) {
          _showAlertDialog(context);
        } else if (stripeState?.message ==
            AppLocalizations.of(context).common.text.error.toLowerCase()) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              scaffoldState: scaffoldState,
              message:
                  AppLocalizations.of(context).common.error.somethingWentWrong,
              context: context);
        } else if (stripeState?.message?.isNotEmpty == true) {
          print('Calling this method');
          if (stripeState?.googleOrApplePayNewlyCardAdded == true) {
            stripeBloc.emitEvent(DoPaymentEvent(
                stripeCardId: stripeState?.stripeCardId,
                customerId: stripeState?.customerId,
                cardListModel: stripeState?.cardListResponseModel,
                isLoading: true,
                context: context,
                creditCardModel: stripeState?.creditCardModel,
                bookingResponseModel: stripeState?.bookingResponseModel,
                bookingOutOfStockResponseModel:
                    stripeState?.bookingOutOfStockResponseModel));
          } else {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                scaffoldState: scaffoldState,
                message: stripeState?.message,
                context: context);
          }
        }
      });
    }
  }

  // navigate to add to card
  actionOnClickOfAddToCard({
    BuildContext context,
    StripeState stripeState,
    ScaffoldState scaffoldState,
    StripeBloc stripeBloc,
  }) async {
    var result = await NavigatorUtils.navigatorUtilsInstance
        .navigatorPushedNameResult(context, Routes.ADD_CARD);
    print('hello this is card listing*****');
    if (result != null) {
      print('hello this is card listing!!!');
      stripeBloc.emitEvent(
        GetCardListEvent(
            isLoading: true,
            stripeCardId: stripeState?.stripeCardId,
            context: context,
            bookingResponseModel:
                stripeState?.bookingResponseModel ?? new BookingResponseModel(),
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel ??
                    new BookingOutOfStockResponseModel(),
            customerId: result.toString(),
            cardListModel: stripeState?.cardListResponseModel),
      );
    }
  }

  // delete card
  actionOnDeleteCard(
      {BuildContext context,
      StripeState stripeState,
      ScaffoldState scaffoldState,
      StripeBloc stripeBloc,
      CardDetails cardDetails}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) {
      if (value) {
        stripeBloc.emitEvent(DeleteCardEvent(
            stripeCardId: cardDetails.id,
            customerId: stripeState?.customerId,
            cardListModel: stripeState?.cardListResponseModel,
            bookingResponseModel:
                stripeState?.bookingResponseModel ?? new BookingResponseModel(),
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel ??
                    new BookingOutOfStockResponseModel(),
            isLoading: true,
            context: context));
      }
    });
  }

  // do payment
  actionDoPayment(
      {BuildContext context,
      StripeState stripeState,
      ScaffoldState scaffoldState,
      StripeBloc stripeBloc,
      String stripeId,
      CreditCardModel creditCardModel}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) {
      if (value) {
        stripeBloc.emitEvent(DoPaymentEvent(
            stripeCardId: stripeId,
            customerId: stripeState?.customerId,
            cardListModel: stripeState?.cardListResponseModel,
            isLoading: true,
            context: context,
            creditCardModel: stripeState?.creditCardModel,
            bookingResponseModel: stripeState?.bookingResponseModel,
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel));
      }
    });
  }

  //this method is used to show alert dialog
  void _showAlertDialog(BuildContext context) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: AppLocalizations.of(context).bookingalert.tittle.oops,
        positiveButton: AppLocalizations.of(context).common.text.ok,
        titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        buttonTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        subTitle:
            AppLocalizations.of(context).bookingalert.subtitle.pleaseCheckCart,
        onPositiveButtonTab: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        });
  }

  //this method is used to open google pay or apple pay window
  callGoogleOrApplePayEvent(
      {String priceToPay,
      BuildContext context,
      StripeState stripeState,
      ScaffoldState scaffoldState,
      StripeBloc stripeBloc,
      String stripeId,
      CreditCardModel creditCardModel}) {
    StripePayment.paymentRequestWithNativePay(
      androidPayOptions: AndroidPayPaymentRequest(
        totalPrice: CartUtilsManager.cartUtilsInstance
            .getTotalAmountNeedToPayByCustomer()
            .toString(),
        currencyCode: "CAD",
      ),
      applePayOptions: ApplePayPaymentOptions(
        countryCode: 'CAN',
        currencyCode: 'CAD',
        items: [
          ApplePayItem(
            label: 'Test',
            amount:CartUtilsManager.cartUtilsInstance
                .getTotalAmountNeedToPayByCustomer()
                .toString())
        ],
      ),
    ).then((token) {
      print('the card details is ${token?.card?.cardId}');
      //here traverse the list to check whether the card  is same
      if (stripeState?.cardListResponseModel?.data != null &&
          stripeState?.cardListResponseModel?.data?.isNotEmpty == true) {
        for (var card in stripeState.cardListResponseModel.data) {
          if (card?.last4 == token?.card?.last4) {
            isCardSame = true;
            cardId = card?.id;
          }
        }
      }
      if (isCardSame) {
        stripeBloc.emitEvent(DoPaymentEvent(
            stripeCardId: cardId ?? stripeState?.stripeCardId,
            customerId: stripeState?.customerId,
            cardListModel: stripeState?.cardListResponseModel,
            isLoading: true,
            context: context,
            creditCardModel: stripeState?.creditCardModel,
            bookingResponseModel: stripeState?.bookingResponseModel,
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel));
      } else {
        //here call add card event api if card
        print('Call to add card');
        stripeBloc.emitEvent(GoogleOrApplePayEvent(
            stripeCardId: stripeState?.stripeCardId,
            customerId: stripeState?.customerId,
            cardListModel: stripeState?.cardListResponseModel,
            isLoading: true,
            context: context,
            creditCardModel: stripeState?.creditCardModel,
            bookingResponseModel: stripeState?.bookingResponseModel,
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel,
            cardToken: token));
      }
    }).catchError(setError);
  }

  Future<void> callApplePay(
      {String priceToPay,
      BuildContext context,
      StripeState stripeState,
      ScaffoldState scaffoldState,
      StripeBloc stripeBloc,
      String stripeId,
      CreditCardModel creditCardModel}) async {
    StripePayment.paymentRequestWithNativePay(
      applePayOptions: ApplePayPaymentOptions(
        countryCode: 'CAN',
        currencyCode: 'CAD',
        items: [
          ApplePayItem(
            label:'Products',
            amount: CartUtilsManager.cartUtilsInstance
                .getTotalAmountNeedToPayByCustomer()
                .toString(),
          )
        ],
      ),
    ).then((token) async {
      await StripePayment.completeNativePayRequest();
      print('the card details is ${token?.card?.cardId}');
      //here traverse the list to check whether the card  is same
      if (stripeState?.cardListResponseModel?.data != null &&
          stripeState?.cardListResponseModel?.data?.isNotEmpty == true) {
        for (var card in stripeState.cardListResponseModel.data) {
          if (card?.last4 == token?.card?.last4) {
            isCardSame = true;
            cardId = card?.id;
          }
        }
      }
      if (isCardSame) {
        stripeBloc.emitEvent(DoPaymentEvent(
            stripeCardId: cardId ?? stripeState?.stripeCardId,
            customerId: stripeState?.customerId,
            cardListModel: stripeState?.cardListResponseModel,
            isLoading: true,
            context: context,
            creditCardModel: stripeState?.creditCardModel,
            bookingResponseModel: stripeState?.bookingResponseModel,
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel));
      } else {
        //here call add card event api if card
        print('Call to add card');
        stripeBloc.emitEvent(GoogleOrApplePayEvent(
            stripeCardId: stripeState?.stripeCardId,
            customerId: stripeState?.customerId,
            cardListModel: stripeState?.cardListResponseModel,
            isLoading: true,
            context: context,
            creditCardModel: stripeState?.creditCardModel,
            bookingResponseModel: stripeState?.bookingResponseModel,
            bookingOutOfStockResponseModel:
                stripeState?.bookingOutOfStockResponseModel,
            cardToken: token));
      }
    }).catchError(setError);
  }

  void setError(
    dynamic error,
  ) {
    String _error;
    /*scaffoldState.showSnackBar(SnackBar(content: Text(error.toString())));*/
    _error = error.toString();
    print('in error!!! $_error');
  }

  Color getSelectedCardColor(
      {StripeState stripeState, CardDetails cardDetails}) {
    if (stripeState?.stripeCardId?.isNotEmpty == true) {
      if (cardDetails.id == stripeState?.stripeCardId) {
        return COLOR_PRIMARY;
      } else {
        return Colors.white;
      }
    } else {
      return Colors.white;
    }
  }
}

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/stripe/customer/constants/stripe_customer_constants.dart';




class StripeBaseApi {
  static final StripeBaseApi _StripeBaseApi = StripeBaseApi._internal();

  static StripeBaseApi get stripeBaseApiInstance => _StripeBaseApi;

  StripeBaseApi._internal() {
    BaseOptions baseOptions = BaseOptions(
      baseUrl: "https://api.stripe.com/",
      connectTimeout: 35000,
      receiveTimeout: 35000,
    );
    stripeDio = Dio(baseOptions);

    stripeDio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true, error: true));
    stripeDio.interceptors.add(HeaderInterceptor());
  }

  Dio stripeDio;

  Future<dynamic> getRequest(
    String path,
    BuildContext context, {
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await stripeDio.get(path,
          queryParameters: queryParameters,
          cancelToken: cancelToken,
          onReceiveProgress: onReceiveProgress,
          options: options);

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response.statusCode == 401) {
          //todo call logout
        }

        return CommonResponseModel.fromJson(e.response.data);
      } else if (e.message != null) {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: e.message);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return CommonResponseModel(status: 0, message: "");
      }
    }
  }

  Future<dynamic> postRequest(
    String path,
    BuildContext context, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await stripeDio.post(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response.statusCode == 401) {
          //todo call logout
          print("auth failure called");
        }
        return e.response.data;
      } else {
        return e.response;
      }
    }
  }
  Future<dynamic> deleteRequest(
      String path,
      BuildContext context, {
        data,
        Map<String, dynamic> queryParameters,
        Options options,
        CancelToken cancelToken,
        ProgressCallback onSendProgress,
        ProgressCallback onReceiveProgress,
      }) async {
    try {
      var result = await stripeDio.delete(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response.statusCode == 401) {
          //todo call logout
          print("auth failure called");
        }
        return e.response.data;
      } else {
        return e.response;
      }
    }
  }
  Future<dynamic> putRequest(
    String path,
    BuildContext context, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var result = await stripeDio.put(
        path,
        data: data,
        options: options,
        queryParameters: queryParameters,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      return result.data;
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response.statusCode == 401) {
          //todo call logout
          print("auth failure called");
        }
        return e.response.data;
      } else {
        return e.response;
      }
    }
  }
}

class HeaderInterceptor extends Interceptor {
  var AUTHORIZATION = "Authorization";

  @override
  onRequest(RequestOptions options) async {
    options.headers = {
      AUTHORIZATION: "Bearer ${secretStripeKey}",
    };
    options.contentType = "application/x-www-form-urlencoded";

    print("header ${options.headers.toString()}");
    return super.onRequest(options);
  }
}

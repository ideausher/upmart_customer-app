// To parse this JSON data, do
//
//     final orderRequestModel = orderRequestModelFromJson(jsonString);

import 'dart:convert';


StripeOrderRequestModel orderRequestModelFromJson(String str) => StripeOrderRequestModel.fromMap(json.decode(str));

String orderRequestModelToJson(StripeOrderRequestModel data) => json.encode(data.toMap());

class StripeOrderRequestModel {

  String currency_code;
  String card_token;
  PaymentDetails payment_details;

  StripeOrderRequestModel({

    this.currency_code,
    this.card_token,
    this.payment_details,
  });

  factory StripeOrderRequestModel.fromMap(Map<String, dynamic> json) => StripeOrderRequestModel(
    currency_code: json["currency_code"] == null ? null : json["currency_code"],
    card_token: json["card_token"] == null ? null : json["card_token"],
    payment_details: json["payment_details"] == null ? null : PaymentDetails.fromMap(json["payment_details"]),
  );

  Map<String, dynamic> toMap() => {
    "currency_code": currency_code == null ? null : currency_code,
    "card_token": card_token == null ? null : card_token,
    "payment_details": payment_details == null ? null : payment_details.toMap(),
  };
}

class PaymentDetails {
  num total_amount_to_be_charged;
  num total_distance;
  num distance_charges_per_km;
  num percentage_amount_of_total;
  num percentage_value_of_total;
  num max_budget;
  num flat_fee;

  PaymentDetails({
    this.total_amount_to_be_charged,
    this.total_distance,
    this.distance_charges_per_km,
    this.percentage_amount_of_total,
    this.percentage_value_of_total,
    this.max_budget,
    this.flat_fee,
  });

  factory PaymentDetails.fromMap(Map<String, dynamic> json) => PaymentDetails(
    total_amount_to_be_charged: json["total_amount_to_be_charged"] == null ? 0 as num : json["total_amount_to_be_charged"] as num,
    total_distance: json["total_distance"] == null ? 0 as num  : json["total_distance"] as num,
    distance_charges_per_km: json["distance_charges_per_km"] == null ? 0 as num  : json["distance_charges_per_km"] as num,
    percentage_amount_of_total: json["percentage_amount_of_total"] == null ? 0 as num  : json["percentage_amount_of_total"] as num,
    percentage_value_of_total: json["percentage_value_of_total"] == null ? 0 as num  : json["percentage_value_of_total"] as num,
    max_budget: json["max_budget"] == null ? 0 as num  : json["max_budget"] as num,
    flat_fee: json["flat_fee"] == null ? 0 as num  : json["flat_fee"] as num,
  );

  Map<String, dynamic> toMap() => {
    "total_amount_to_be_charged": total_amount_to_be_charged == null ? 0 as num  : total_amount_to_be_charged as num,
    "total_distance": total_distance == null ? 0 as num  : total_distance as num,
    "distance_charges_per_km": distance_charges_per_km == null ? 0 as num  : distance_charges_per_km as num,
    "percentage_amount_of_total": percentage_amount_of_total == null ? 0 as num  : percentage_amount_of_total as num,
    "percentage_value_of_total": percentage_value_of_total == null ? 0 as num  : percentage_value_of_total as num,
    "max_budget": max_budget == null ? 0 as num  : max_budget as num,
    "flat_fee": flat_fee == null ? 0 as num  : flat_fee as num,
  };
}
class Item {
  String name;
  int quantity;
  String alternative;

  Item({
    this.name,
    this.quantity,
    this.alternative,
  });

  factory Item.fromMap(Map<String, dynamic> json) => Item(
    name: json["name"] == null ? null : json["name"],
    quantity: json["quantity"] == null ? null : json["quantity"],
    alternative: json["alternative"] == null ? null : json["alternative"],
  );

  Map<String, dynamic> toMap() => {
    "name": name == null ? null : name,
    "quantity": quantity == null ? null : quantity,
    "alternative": alternative == null ? null : alternative,
  };
}
// To parse this JSON data, do
//
//     final stripeDeleteCardResponseModel = stripeDeleteCardResponseModelFromJson(jsonString);

import 'dart:convert';

StripeDeleteCardResponseModel stripeDeleteCardResponseModelFromJson(String str) => StripeDeleteCardResponseModel.fromJson(json.decode(str));

String stripeDeleteCardResponseModelToJson(StripeDeleteCardResponseModel data) => json.encode(data.toJson());

class StripeDeleteCardResponseModel {
  String id;
  String object;
  bool deleted;

  StripeDeleteCardResponseModel({
    this.id,
    this.object,
    this.deleted,
  });

  factory StripeDeleteCardResponseModel.fromJson(Map<String, dynamic> json) => StripeDeleteCardResponseModel(
    id: json["id"] == null ? null : json["id"],
    object: json["object"] == null ? null : json["object"],
    deleted: json["deleted"] == null ? null : json["deleted"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "object": object == null ? null : object,
    "deleted": deleted == null ? null : deleted,
  };
}

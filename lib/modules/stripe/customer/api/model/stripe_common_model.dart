// To parse this JSON data, do
//
//     final stripeCommonModel = stripeCommonModelFromJson(jsonString);

import 'dart:convert';

StripeCommonModel stripeCommonModelFromJson(String str) => StripeCommonModel.fromJson(json.decode(str));

String stripeCommonModelToJson(StripeCommonModel data) => json.encode(data.toJson());

class StripeCommonModel {
  StripeCommonModel({
    this.status,
    this.message,
    this.data,
  });

  int status;
  String message;
  Data data;

  factory StripeCommonModel.fromJson(Map<String, dynamic> json) => StripeCommonModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.customerId,
  });

  String customerId;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    customerId: json["customerId"] == null ? null : json["customerId"],
  );

  Map<String, dynamic> toJson() => {
    "customerId": customerId == null ? null : customerId,
  };
}

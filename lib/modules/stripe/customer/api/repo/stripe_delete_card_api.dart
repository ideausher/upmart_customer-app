import 'package:flutter/material.dart';
import 'package:user/modules/stripe/customer/api/base_api/stripe_base_api.dart';

// used to delete card directly from stripe
class StripeDeleteCardApi {
  // add card
  Future<dynamic> deleteCardApiCall({BuildContext context, String customerId, String cardId}) async {
    var path = "v1/customers/${customerId}/sources/${cardId}";

    var result = await StripeBaseApi.stripeBaseApiInstance.deleteRequest(
      path,
      context,
    );

    return result;
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/stripe/customer/model/booking_request_model.dart';

// used to do payment from our server
class CallBookingApi {
  // add card
  Future<dynamic> doPaymentApi({BuildContext context, BookingRequestModel bookingRequestModel}) async {
    print('the booking request model is ${bookingRequestModel?.toJson()}');
    var path = "booking";
    var result = await AppConfig.of(context).baseApi.postRequest(path, context, data: bookingRequestModel);

    return result;
  }
}

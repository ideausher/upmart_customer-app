import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';

// used to add card in stripe via server
class StripeAddCardApi {
  // add card
  Future<dynamic> addCardApiCall({BuildContext context, String token}) async {
    var path = "v1/createCustomer";

    var result = await AppConfig.of(context).baseApi.postRequest(path, context, data: {"token_card": token});

    return result;
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/stripe/customer/api/base_api/stripe_base_api.dart';

// used to get card list by using customer id from stripe
class StripeGetCardListApi {
  // get card list
  Future<dynamic> getCardListApiCall({
    BuildContext context,
    String customerId
  }) async {
    var path = "v1/customers/${customerId}/sources";

    var result = await StripeBaseApi.stripeBaseApiInstance.getRequest(
      path,
      context,
    );

    return result;
  }
}

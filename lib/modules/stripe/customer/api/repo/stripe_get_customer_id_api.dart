import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';


// used to get customer id from db
class StripeGetCustomerIdApi {
  // get customer id
  Future<dynamic> getCustomerIdApiCall({
    BuildContext context,
  }) async {
    var path = "v1/customerExists";

    var result = await AppConfig.of(context).baseApi.getRequest(
          path,
          context,
        );

    return result;
  }
}

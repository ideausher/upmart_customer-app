import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/stripe/customer/utils/stripe_card_utils.dart';

class StripeCustomerValidator {
  static String validateCVV(String value, BuildContext context) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).addcardpage.error.requiredField;
    }
    if (value.length < 3 || value.length > 4) {
      return  AppLocalizations.of(context).addcardpage.error.invalidCvv;
    }
    return null;
  }

  static String validateDate(String value, BuildContext context) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).addcardpage.error.requiredField;
    }
    int year;
    int month;
    // The value contains a forward slash if the month and year has been
    // entered.
    if (value.contains(new RegExp(r'(\/)'))) {
      var split = value.split(new RegExp(r'(\/)'));
      // The value before the slash is the month while the value to right of
      // it is the year.
      month = int.parse(split[0]);
      year = int.parse(split[1]);
    } else {
      // Only the month was entered
      month = int.parse(value.substring(0, (value.length)));
      year = -1; // Lets use an invalid year intentionally
    }

    if ((month < 1) || (month > 12)) {
      // A valid month is between 1 (January) and 12 (December)
      return AppLocalizations.of(context).addcardpage.error.invalidMonth;
    }

    var fourDigitsYear = StripeCardUtils.convertYearTo4Digits(year);
    if ((fourDigitsYear < 1) || (fourDigitsYear > 2099)) {
      // We are assuming a valid should be between 1 and 2099.
      // Note that, it's valid doesn't mean that it has not expired.
      return AppLocalizations.of(context).addcardpage.error.expiredYear;
    }

    if (!StripeCardUtils.hasDateExpired(month, year)) {
      return AppLocalizations.of(context).addcardpage.error.cardExpired;
    }
    return null;
  }

  /// With the card number with Luhn Algorithm
  /// https://en.wikipedia.org/wiki/Luhn_algorithm
  static String validateCardNum(String input, BuildContext context) {
    if (input.isEmpty) {
      return AppLocalizations.of(context).addcardpage.error.requiredField;
    }

    input = StripeCardUtils.getCleanedNumber(input);

    if (input.length < 8) {
      return AppLocalizations.of(context).addcardpage.error.invalidCard;
    }

    int sum = 0;
    int length = input.length;
    for (var i = 0; i < length; i++) {
      // get digits in reverse order
      int digit = int.parse(input[length - i - 1]);

      // every 2nd number multiply with 2
      if (i % 2 == 1) {
        digit *= 2;
      }
      sum += digit > 9 ? (digit - 9) : digit;
    }

    if (sum % 10 == 0) {
      return null;
    }

    return  AppLocalizations.of(context).addcardpage.error.invalidCard;
  }

  // validate card
  static String validateCard(
      {BuildContext context,
      String cvvCode,
      String cardNumber,
      String expiryDate,
      String cardHolderName,
      ScaffoldState scaffoldState}) {
    var _result = validateCVV(cvvCode, context);
    if (_result == null) _result = validateCardNum(cardNumber, context);
    if (_result == null) _result = validateDate(expiryDate, context);
    if (cardHolderName.isEmpty) {
      _result = AppLocalizations.of(context).addcardpage.error.enterCardDetails;
    }
    if (_result != null) {

      DialogSnackBarUtils.dialogSnackBarUtilsInstance
          .showSnackbar(scaffoldState: scaffoldState, context: context, message: _result);
    }

    return _result;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_card_list_response_model.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_order_request_model.dart';
import 'package:user/modules/stripe/customer/model/booking_out_of_stock_response_model.dart';
import 'package:user/modules/stripe/customer/model/booking_request_model.dart';
import 'package:user/modules/stripe/customer/model/booking_response_model.dart';

class StripeState extends BlocState {
  StripeState(
      {this.isLoading: false,
      this.message,
      this.context,
      this.customerId,
      this.bookingOutOfStockResponseModel,
      this.cardListResponseModel,
      this.creditCardModel,
      this.stripeCardId,
      this.googleOrApplePayNewlyCardAdded: false,
      this.bookingResponseModel})
      : super(isLoading);

  final bool isLoading;
  final BuildContext context;
  final String message;
  final String customerId;
  final String stripeCardId;
  final StripeCardListResponseModel cardListResponseModel;
  final CreditCardModel creditCardModel;
  final BookingResponseModel bookingResponseModel;
  final BookingOutOfStockResponseModel bookingOutOfStockResponseModel;
  final bool googleOrApplePayNewlyCardAdded;

  factory StripeState.initiating() {
    return StripeState(
      isLoading: false,
    );
  }

  // used for the get customer id api call
  factory StripeState.getCustomerId(
      {bool isLoading,
      String message,
      String customerId,
      String stripeCardId,
      BuildContext context,
      BookingResponseModel bookingResponseModel,
      bool googleOrApplePayNewlyCardAdded,
      BookingOutOfStockResponseModel bookingOutOfStockResponseModel}) {
    return StripeState(
        isLoading: isLoading,
        message: message,
        customerId: customerId,
        context: context,
        stripeCardId: stripeCardId,
        googleOrApplePayNewlyCardAdded: googleOrApplePayNewlyCardAdded,
        bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
        bookingResponseModel: bookingResponseModel);
  }

  // used for the get card list api call
  factory StripeState.getCardList(
      {bool isLoading,
      String message,
      String customerId,
      BuildContext context,
      String stripeCardId,
      bool googleOrApplePayNewlyCardAdded,
      BookingResponseModel bookingResponseModel,
      StripeCardListResponseModel cardListResponseModel,
      BookingOutOfStockResponseModel bookingOutOfStockResponseModel,
      BookingRequestModel bookingRequestModel}) {
    return StripeState(
        isLoading: isLoading,
        message: message,
        customerId: customerId,
        stripeCardId: stripeCardId,
        context: context,
        googleOrApplePayNewlyCardAdded: googleOrApplePayNewlyCardAdded,
        bookingResponseModel: bookingResponseModel,
        bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
        cardListResponseModel: cardListResponseModel);
  }

  // used for the add card api call
  factory StripeState.addCard(
      {bool isLoading,
      String message,
      String customerId,
      String stripeCardId,
      CreditCardModel creditCardModel,
      BookingResponseModel bookingResponseModel,
      bool googleOrApplePayNewlyCardAdded,
      BookingOutOfStockResponseModel bookingOutOfStockResponseModel,
      BookingRequestModel bookingRequestModel}) {
    return StripeState(
        isLoading: isLoading,
        message: message,
        customerId: customerId,
        stripeCardId: stripeCardId,
        creditCardModel: creditCardModel,
        googleOrApplePayNewlyCardAdded: googleOrApplePayNewlyCardAdded,
        bookingResponseModel: bookingResponseModel,
        bookingOutOfStockResponseModel: bookingOutOfStockResponseModel);
  }

  // used for the delete card api call
  factory StripeState.deleteCard(
      {bool isLoading,
      String message,
      String customerId,
      BuildContext context,
      String stripeCardId,
      StripeCardListResponseModel cardListResponseModel,
      BookingResponseModel bookingResponseModel,
      bool googleOrApplePayNewlyCardAdded,
      BookingOutOfStockResponseModel bookingOutOfStockResponseModel,
      BookingRequestModel bookingRequestModel}) {
    return StripeState(
        isLoading: isLoading,
        message: message,
        customerId: customerId,
        stripeCardId: stripeCardId,
        bookingResponseModel: bookingResponseModel,
        googleOrApplePayNewlyCardAdded: googleOrApplePayNewlyCardAdded,
        bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
        context: context,
        cardListResponseModel: cardListResponseModel);
  }
}

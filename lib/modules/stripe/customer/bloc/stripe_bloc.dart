import 'package:stripe_payment/stripe_payment.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_card_list_response_model.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_common_model.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_delete_card_response_model.dart';
import 'package:user/modules/stripe/customer/api/repo/stripe_add_card_api.dart';
import 'package:user/modules/stripe/customer/api/repo/stripe_delete_card_api.dart';
import 'package:user/modules/stripe/customer/api/repo/stripe_do_payment_api.dart';
import 'package:user/modules/stripe/customer/api/repo/stripe_get_card_list_api.dart';
import 'package:user/modules/stripe/customer/api/repo/stripe_get_customer_id_api.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_event.dart';
import 'package:user/modules/stripe/customer/bloc/stripe_state.dart';
import 'package:user/modules/stripe/customer/model/booking_out_of_stock_response_model.dart';
import 'package:user/modules/stripe/customer/model/booking_request_model.dart';
import 'package:user/modules/stripe/customer/model/booking_response_model.dart';

class StripeBloc extends BlocEventStateBase<StripeEvent, StripeState> {
  StripeBloc({bool initializing = false})
      : super(initialState: StripeState.initiating());

  @override
  Stream<StripeState> eventHandler(
      StripeEvent event, StripeState currentState) async* {
    //to select card
    if (event is SelectCardEvent) {
      yield StripeState(
          context: event.context,
          isLoading: false,
          message: "",
          googleOrApplePayNewlyCardAdded: false,
          customerId: event.customerId,
          cardListResponseModel: event.cardListModel,
          stripeCardId: event?.stripeCardId,
          bookingResponseModel: event?.bookingResponseModel);
    }

    // used for the get customer id
    if (event is GetCustomerIdEvent) {
      yield StripeState(
        context: event.context,
        isLoading: true,
        googleOrApplePayNewlyCardAdded: false,
        message: "",
        customerId: "",
      );

      // check whether customer id exist or not
      var _customerIdResult = await StripeGetCustomerIdApi()
          .getCustomerIdApiCall(context: event.context);

      if (_customerIdResult != null &&
          !(_customerIdResult is CommonResponseModel)) {
        var _stripeCommonResponseModel =
            StripeCommonModel.fromJson(_customerIdResult);

        if (_stripeCommonResponseModel != null &&
            _stripeCommonResponseModel?.data?.customerId != null &&
            _stripeCommonResponseModel?.data?.customerId?.isNotEmpty == true) {
          // if exist then call api to fetch list of cards
          String _customerId = _stripeCommonResponseModel?.data?.customerId;
          var result = await StripeGetCardListApi().getCardListApiCall(
              context: event.context, customerId: _customerId);

          if (result != null && result['data'] != null) {
            var cardResponseModel = StripeCardListResponseModel.fromMap(result);
            if (cardResponseModel != null &&
                cardResponseModel.data?.isNotEmpty == true) {
              yield StripeState.getCardList(
                  isLoading: false,
                  message: "",
                  customerId: _customerId,
                  googleOrApplePayNewlyCardAdded: false,
                  context: event.context,
                  cardListResponseModel:
                      StripeCardListResponseModel.fromMap(result));
            } else {
              yield StripeState.getCardList(
                isLoading: false,
                message: "",
                customerId: _customerId,
                googleOrApplePayNewlyCardAdded: false,
                context: event.context,
              );
            }
          } else {
            yield StripeState.getCardList(
              isLoading: false,
              message: "",
              googleOrApplePayNewlyCardAdded: false,
              customerId: _customerId,
              context: event.context,
            );
          }
        } else {
          yield StripeState(
            context: event.context,
            googleOrApplePayNewlyCardAdded: false,
            isLoading: false,
            message: "",
            customerId: "",
          );
        }
      } else {
        yield StripeState(
          context: event.context,
          isLoading: false,
          googleOrApplePayNewlyCardAdded: false,
          message: "",
          customerId: "",
        );
      }
    }
    // used for the get customer id
    if (event is DoPaymentEvent) {
      String _message = "";
      BookingResponseModel bookingResponseModel;
      BookingOutOfStockResponseModel bookingOutOfStockResponseModel;
      yield StripeState(
          context: event.context,
          isLoading: true,
          googleOrApplePayNewlyCardAdded: false,
          message: "",
          creditCardModel: event?.creditCardModel,
          stripeCardId: event?.stripeCardId,
          bookingResponseModel: event?.bookingResponseModel,
          bookingOutOfStockResponseModel: event?.bookingOutOfStockResponseModel,
          customerId: event.customerId,
          cardListResponseModel: event.cardListModel);

      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();
      // call api
      var result = await CallBookingApi().doPaymentApi(
          context: event.context,
          bookingRequestModel: BookingRequestModel(
              addressId: num.parse(_currentLocation?.id),
              bookingType: CartUtilsManager.cartUtilsInstance?.shopType ==
                      ShopType.delivery
                  ? ShopType.delivery.value
                  : ShopType.pickUp.value,
              shopId: CartUtilsManager.cartUtilsInstance?.shopDetailsModel?.id,
              cardId: event?.stripeCardId,
              couponId: CartUtilsManager.cartUtilsInstance?.couponData?.id
                  ?.toString(),
              instruction: CartUtilsManager.cartUtilsInstance?.notes,
              scheduledTime: CartUtilsManager
                          .cartUtilsInstance?.scheduleDateTime !=
                      null
                  ? '${DateUtils.dateUtilsInstance.convertDateTimeToString
                (dateTime: CartUtilsManager.cartUtilsInstance?.
              scheduleDateTime)} '
                  '${CartUtilsManager.cartUtilsInstance.slotsData.slotTo}'
                  : null,
              tipToDeliveryBoy: CartUtilsManager.cartUtilsInstance?.tipOfDriver,
              products: CartUtilsManager.cartUtilsInstance
                  .convertShopToChargeModel()));
      print(result);
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          bookingResponseModel = BookingResponseModel.fromJson(result);
        }
        //product out of stock case and quantity decrease case
        else if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] ==
                ApiStatus.OutOfStock.value) {
          bookingOutOfStockResponseModel =
              BookingOutOfStockResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context)
            ?.common
            ?.error
            ?.somethingWentWrong;
      }

      yield StripeState(
          context: event.context,
          isLoading: false,
          googleOrApplePayNewlyCardAdded: false,
          message: _message,
          customerId: event.customerId,
          bookingResponseModel: bookingResponseModel,
          stripeCardId: event?.stripeCardId,
          bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
          creditCardModel: event?.creditCardModel,
          cardListResponseModel: event?.cardListModel);
    }
    // used for the get card list
    if (event is GetCardListEvent) {
      yield StripeState.getCardList(
          isLoading: true,
          googleOrApplePayNewlyCardAdded: false,
          message: "",
          customerId: event.customerId,
          stripeCardId: event?.stripeCardId,
          context: event.context,
          cardListResponseModel: event.cardListModel);

      String _customerId = event.customerId;
      var result = await StripeGetCardListApi()
          .getCardListApiCall(context: event.context, customerId: _customerId);

      if (result != null && result['data'] != null) {
        var cardResponseModel = StripeCardListResponseModel.fromMap(result);

        if (cardResponseModel != null &&
            cardResponseModel.data?.isNotEmpty == true) {
          yield StripeState.getCardList(
              isLoading: false,
              message: "",
              googleOrApplePayNewlyCardAdded: false,
              customerId: _customerId,
              context: event.context,
              cardListResponseModel:
                  StripeCardListResponseModel.fromMap(result));
        } else {
          yield StripeState.getCardList(
            isLoading: false,
            message: "",
            googleOrApplePayNewlyCardAdded: false,
            customerId: _customerId,
            stripeCardId: event?.stripeCardId,
            context: event.context,
          );
        }
      } else {
        yield StripeState.getCardList(
          isLoading: false,
          message: "",
          googleOrApplePayNewlyCardAdded: false,
          customerId: _customerId,
          context: event.context,
        );
      }
    }

    // used for the add card
    if (event is AddCardEvent) {
      yield StripeState.addCard(
        isLoading: true,
        message: "",
        creditCardModel: event.creditCardModel,
        googleOrApplePayNewlyCardAdded: false,
      );

      try {
        var card = CreditCard(
            name: event.creditCardModel.cardHolderName,
            cvc: event.creditCardModel.cvvCode,
            expMonth: int.parse(event.creditCardModel.expiryDate.split("/")[0]),
            expYear: int.parse(event.creditCardModel.expiryDate.split("/")[1]),
            number: event.creditCardModel.cardNumber);
        // create token
        Token _token = await StripePayment.createTokenWithCard(card);
        if (_token != null && _token.tokenId != null) {
          print('the token id is ${_token.tokenId}');
          // call api
          var result = await StripeAddCardApi()
              .addCardApiCall(context: event.context, token: _token.tokenId);
          print(result);
          if (result != null && !(result is CommonResponseModel)) {
            var _stripeCommonResponseModel = StripeCommonModel.fromJson(result);

            if (_stripeCommonResponseModel != null &&
                _stripeCommonResponseModel?.data?.customerId != null &&
                _stripeCommonResponseModel?.data?.customerId?.isNotEmpty) {
              yield StripeState.addCard(
                  isLoading: false,
                  googleOrApplePayNewlyCardAdded: false,
                  message:
                      AppLocalizations.of(event?.context).common.text.success,
                  creditCardModel: event.creditCardModel,
                  customerId: _stripeCommonResponseModel?.data?.customerId);
            } else {
              yield StripeState.addCard(
                  isLoading: false,
                  googleOrApplePayNewlyCardAdded: false,
                  message:
                      AppLocalizations.of(event?.context).common.text.error,
                  creditCardModel: event.creditCardModel,
                  customerId: "");
            }
          } else {
            yield StripeState.addCard(
                isLoading: false,
                googleOrApplePayNewlyCardAdded: false,
                message: AppLocalizations.of(event?.context).common.text.error,
                creditCardModel: event.creditCardModel,
                customerId: "");
          }
        } else {
          yield StripeState.addCard(
              isLoading: false,
              googleOrApplePayNewlyCardAdded: false,
              message: AppLocalizations.of(event?.context).common.text.error,
              creditCardModel: event.creditCardModel,
              customerId: "");
        }
      } catch (e) {
        print(e);
        yield StripeState.addCard(
            isLoading: false,
            googleOrApplePayNewlyCardAdded: false,
            message: AppLocalizations.of(event?.context).common.text.error,
            creditCardModel: event.creditCardModel,
            customerId: "");
      }
    }

    // used for the delete card
    if (event is DeleteCardEvent) {
      yield StripeState.deleteCard(
          isLoading: true,
          googleOrApplePayNewlyCardAdded: false,
          message: "",
          customerId: event.customerId,
          context: event.context,
          cardListResponseModel: event.cardListModel);

      var result = await StripeDeleteCardApi().deleteCardApiCall(
          context: event.context,
          customerId: event.customerId,
          cardId: event.stripeCardId);

      if (result != null && !(result is CommonResponseModel)) {
        var _stripeModel = StripeDeleteCardResponseModel.fromJson(result);
        if (_stripeModel != null &&
            _stripeModel.deleted != null &&
            _stripeModel.deleted == true) {
          for (var cardData in event.cardListModel.data) {
            if (cardData.id == event.stripeCardId) {
              /*//when the selected card is deleted
              if(cardData.id==Managers.stripeCardId) {
                Managers.stripeCardId="";
              }*/
              event.cardListModel.data.remove(cardData);
              break;
            }
          }

          yield StripeState.deleteCard(
              isLoading: false,
              googleOrApplePayNewlyCardAdded: false,
              message: "",
              customerId: event.customerId,
              context: event.context,
              cardListResponseModel: event.cardListModel);
        } else {
          yield StripeState.deleteCard(
              isLoading: false,
              message: AppLocalizations.of(event?.context).common.text.error,
              customerId: event.customerId,
              context: event.context,
              googleOrApplePayNewlyCardAdded: false,
              cardListResponseModel: event.cardListModel);
        }
      } else {
        yield StripeState.deleteCard(
            isLoading: false,
            googleOrApplePayNewlyCardAdded: false,
            message: "",
            customerId: event.customerId,
            context: event.context,
            cardListResponseModel: event.cardListModel);
      }
    }

    if (event is GoogleOrApplePayEvent) {
      yield StripeState(
          context: event.context,
          isLoading: true,
          googleOrApplePayNewlyCardAdded: false,
          message: "",
          creditCardModel: event?.creditCardModel,
          customerId: event.customerId,
          cardListResponseModel: event.cardListModel,
          stripeCardId: event?.stripeCardId,
          bookingResponseModel: event?.bookingResponseModel);

      try {
        // create token
        if (event?.cardToken != null) {
          print('the token id is ${event?.cardToken}');
          // call api
          var result = await StripeAddCardApi().addCardApiCall(
              context: event.context, token: event?.cardToken?.tokenId);
          print(result);
          if (result != null && !(result is CommonResponseModel)) {
            var _stripeCommonResponseModel = StripeCommonModel.fromJson(result);

            if (_stripeCommonResponseModel != null &&
                _stripeCommonResponseModel?.data?.customerId != null &&
                _stripeCommonResponseModel?.data?.customerId?.isNotEmpty ==
                    true) {
              yield StripeState(
                  isLoading: false,
                  message:
                      AppLocalizations.of(event?.context).common.text.success,
                  googleOrApplePayNewlyCardAdded: true,
                  creditCardModel: event.creditCardModel,
                  customerId: event.customerId,
                  cardListResponseModel: event.cardListModel,
                  stripeCardId: event?.cardToken?.card?.cardId,
                  bookingResponseModel: event?.bookingResponseModel);
            } else {
              yield StripeState(
                  isLoading: false,
                  creditCardModel: event.creditCardModel,
                  cardListResponseModel: event.cardListModel,
                  googleOrApplePayNewlyCardAdded: false,
                  stripeCardId: event?.stripeCardId,
                  bookingResponseModel: event?.bookingResponseModel,
                  message:
                      AppLocalizations.of(event?.context).common.text.error,
                  customerId: "");
            }
          } else {
            yield StripeState(
                isLoading: false,
                message: AppLocalizations.of(event?.context).common.text.error,
                creditCardModel: event.creditCardModel,
                googleOrApplePayNewlyCardAdded: false,
                customerId: "",
                cardListResponseModel: event.cardListModel,
                stripeCardId: event?.stripeCardId,
                bookingResponseModel: event?.bookingResponseModel);
          }
        } else {
          yield StripeState(
              isLoading: false,
              message: AppLocalizations.of(event?.context).common.text.error,
              creditCardModel: event.creditCardModel,
              customerId: "",
              googleOrApplePayNewlyCardAdded: false,
              cardListResponseModel: event.cardListModel,
              stripeCardId: event?.stripeCardId,
              bookingResponseModel: event?.bookingResponseModel);
        }
      } catch (e) {
        print(e);
        yield StripeState(
            isLoading: false,
            message: AppLocalizations.of(event?.context).common.text.error,
            creditCardModel: event.creditCardModel,
            cardListResponseModel: event.cardListModel,
            googleOrApplePayNewlyCardAdded: false,
            stripeCardId: event?.stripeCardId,
            bookingResponseModel: event?.bookingResponseModel,
            customerId: "");
      }
    }
  }
}

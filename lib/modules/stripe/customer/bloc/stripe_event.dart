import 'package:flutter/cupertino.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:stripe_payment/stripe_payment.dart';

import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_card_list_response_model.dart';
import 'package:user/modules/stripe/customer/api/model/stripe_order_request_model.dart';
import 'package:user/modules/stripe/customer/model/booking_out_of_stock_response_model.dart';
import 'package:user/modules/stripe/customer/model/booking_response_model.dart';

abstract class StripeEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final String customerId;
  final Token cardToken;
  final StripeCardListResponseModel cardListModel;
  final CreditCardModel creditCardModel;
  final String stripeCardId;
  final BookingResponseModel bookingResponseModel;
  final BookingOutOfStockResponseModel bookingOutOfStockResponseModel;

  StripeEvent({this.isLoading,
    this.context,
    this.customerId,
    this.cardListModel,
    this.creditCardModel,
    this.stripeCardId,
    this.cardToken,
    this.bookingResponseModel,
    this.bookingOutOfStockResponseModel});
}

// get customer id api call event
class GetCustomerIdEvent extends StripeEvent {
  GetCustomerIdEvent({bool isLoading,
    BuildContext context,
    String customerId,
    StripeCardListResponseModel cardListModel,
    String stripeCardId,
    BookingResponseModel bookingResponseModel,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel})
      : super(
      isLoading: isLoading,
      context: context,
      customerId: customerId,
      cardListModel: cardListModel,
      stripeCardId: stripeCardId,
      bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
      bookingResponseModel: bookingResponseModel);
}

// get customer id api call event
class DoPaymentEvent extends StripeEvent {
  DoPaymentEvent({bool isLoading,
    BuildContext context,
    String customerId,
    StripeCardListResponseModel cardListModel,
    BookingResponseModel bookingResponseModel,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel,
    CreditCardModel creditCardModel,
    String stripeCardId})
      : super(
    isLoading: isLoading,
    context: context,
    customerId: customerId,
    cardListModel: cardListModel,
    bookingResponseModel: bookingResponseModel,
    bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
    creditCardModel: creditCardModel,
    stripeCardId: stripeCardId,
  );
}

// get card list api call event
class GetCardListEvent extends StripeEvent {
  GetCardListEvent({bool isLoading,
    BuildContext context,
    String customerId,
    String stripeCardId,
    StripeCardListResponseModel cardListModel,
    BookingResponseModel bookingResponseModel,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel})
      : super(
      isLoading: isLoading,
      context: context,
      customerId: customerId,
      cardListModel: cardListModel,
      stripeCardId: stripeCardId,
      bookingResponseModel: bookingResponseModel,
      bookingOutOfStockResponseModel: bookingOutOfStockResponseModel);
}

// add card api call event
class AddCardEvent extends StripeEvent {
  AddCardEvent({bool isLoading,
    BuildContext context,
    String stripeCardId,
    CreditCardModel creditCardModel,
    BookingResponseModel bookingResponseModel,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel})
      : super(
      isLoading: isLoading,
      context: context,
      stripeCardId: stripeCardId,
      creditCardModel: creditCardModel,
      bookingResponseModel: bookingResponseModel,
      bookingOutOfStockResponseModel: bookingOutOfStockResponseModel);
}

// select card  api call event
class SelectCardEvent extends StripeEvent {
  SelectCardEvent({bool isLoading,
    BuildContext context,
    String stripeCardId,
    StripeCardListResponseModel cardListModel,
    String customerId,
    BookingResponseModel bookingResponseModel,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel})
      : super(
      isLoading: isLoading,
      context: context,
      cardListModel: cardListModel,
      customerId: customerId,
      stripeCardId: stripeCardId,
      bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
      bookingResponseModel: bookingResponseModel);
}

// delete card api call event
class DeleteCardEvent extends StripeEvent {
  DeleteCardEvent({bool isLoading,
    BuildContext context,
    String customerId,
    String stripeCardId,
    StripeCardListResponseModel cardListModel,
    BookingResponseModel bookingResponseModel,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel})
      : super(
    isLoading: isLoading,
    context: context,
    customerId: customerId,
    cardListModel: cardListModel,
    stripeCardId: stripeCardId,
    bookingResponseModel: bookingResponseModel,
    bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
  );
}


//Google pay or Apple pay event with stripe integration
class GoogleOrApplePayEvent extends StripeEvent {
  GoogleOrApplePayEvent({bool isLoading,
    BuildContext context,
    String customerId,
    String stripeCardId,
    CreditCardModel creditCardModel,
    StripeCardListResponseModel cardListModel,
    BookingResponseModel bookingResponseModel,
    Token cardToken,
    BookingOutOfStockResponseModel bookingOutOfStockResponseModel})
      : super(
    isLoading: isLoading,
    context: context,
    customerId: customerId,
    cardListModel: cardListModel,
    stripeCardId: stripeCardId,
    cardToken: cardToken,
    creditCardModel: creditCardModel,
    bookingResponseModel: bookingResponseModel,
    bookingOutOfStockResponseModel: bookingOutOfStockResponseModel,
  );
}
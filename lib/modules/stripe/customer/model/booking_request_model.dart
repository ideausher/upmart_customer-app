// To parse this JSON data, do
//
//     final bookingRequestModel = bookingRequestModelFromJson(jsonString);

import 'dart:convert';

import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_request_model.dart';

BookingRequestModel bookingRequestModelFromJson(String str) => BookingRequestModel.fromJson(json.decode(str));

String bookingRequestModelToJson(BookingRequestModel data) => json.encode(data.toJson());

class BookingRequestModel {
  BookingRequestModel({
    this.addressId,
    this.couponId,
    this.shopId,
    this.cardId,
    this.products,
    this.bookingType,
    this.scheduledTime,
    this.tipToDeliveryBoy,
    this.instruction,
  });

  int addressId;
  String couponId;
  int shopId;
  String cardId;
  List<Product> products;
  int bookingType;
  String scheduledTime;
  num tipToDeliveryBoy;
  String instruction;

  factory BookingRequestModel.fromJson(Map<String, dynamic> json) => BookingRequestModel(
    addressId: json["addressId"] == null ? null : json["addressId"],
    couponId: json["couponId"] == null ? null : json["couponId"],
    shopId: json["shopId"] == null ? null : json["shopId"],
    cardId: json["card_id"] == null ? null : json["card_id"],
    products: json["products"] == null ? null : List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    bookingType: json["booking_type"] == null ? null : json["booking_type"],
    scheduledTime: json["scheduled_time"] == null ? null : json["scheduled_time"],
    tipToDeliveryBoy: json["tip_to_delivery_boy"],
    instruction: json["instruction"] == null ? null : json["instruction"],
  );

  Map<String, dynamic> toJson() => {
    "addressId": addressId == null ? null : addressId,
    "couponId": couponId == null ? null : couponId,
    "shopId": shopId == null ? null : shopId,
    "card_id": cardId == null ? null : cardId,
    "products": products == null ? null : List<dynamic>.from(products.map((x) => x.toJson())),
    "booking_type": bookingType == null ? null : bookingType,
    "scheduled_time": scheduledTime == null ? null : scheduledTime,
    "tip_to_delivery_boy": tipToDeliveryBoy,
    "instruction": instruction == null ? null : instruction,
  };
}



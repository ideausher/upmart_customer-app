// To parse this JSON data, do
//
//     final bookingResponseModel = bookingResponseModelFromJson(jsonString);

import 'dart:convert';

BookingResponseModel bookingResponseModelFromJson(String str) => BookingResponseModel.fromJson(json.decode(str));

String bookingResponseModelToJson(BookingResponseModel data) => json.encode(data.toJson());

class BookingResponseModel {
  BookingResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  num statusCode;

  factory BookingResponseModel.fromJson(Map<String, dynamic> json) => BookingResponseModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.id,
    this.bookingCode,
    this.deliveryBoyId,
    this.bookingType,
    this.bookingDateTime,
    this.amountBeforeDiscount,
    this.discountAmount,
    this.amountAfterDiscount,
    this.deliveryChargeToDeliveryBoy,
    this.deliveryChargeForCustomer,
    this.commissionCharge,
    this.platformCharge,
    this.afterChargeAmount,
    this.stripeCharges,
    this.amountAfterStripeCharges,
    this.tipToDeliveryBoy,
    this.instruction,
    this.status,
    this.orderDetails,
    this.userDetails,
    this.shopDetails,
    this.couponDetails,
    this.deliveryAddress,
    this.deliveryBoyDetails,
    this.bookingStatus,
    this.createdAt,
  });

  num id;
  String bookingCode;
  num deliveryBoyId;
  num bookingType;
  String bookingDateTime;
  num amountBeforeDiscount;
  num discountAmount;
  num amountAfterDiscount;
  num deliveryChargeToDeliveryBoy;
  num deliveryChargeForCustomer;
  num commissionCharge;
  num platformCharge;
  num afterChargeAmount;
  num stripeCharges;
  num amountAfterStripeCharges;
  num tipToDeliveryBoy;
  String instruction;
  num status;
  List<OrderDetail> orderDetails;
  UserDetails userDetails;
  ShopDetails shopDetails;
  CouponDetails couponDetails;
  DeliveryAddress deliveryAddress;
  DeliveryBoyDetails deliveryBoyDetails;
  BookingStatus bookingStatus;
  String createdAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    bookingCode: json["booking_code"] == null ? null : json["booking_code"],
    deliveryBoyId: json["delivery_boy_id"] == null ? null : json["delivery_boy_id"],
    bookingType: json["booking_type"] == null ? null : json["booking_type"],
    bookingDateTime: json["booking_date_time"] == null ? null :json["booking_date_time"],
    amountBeforeDiscount: json["amount_before_discount"] == null ? null : json["amount_before_discount"],
    discountAmount: json["discount_amount"] == null ? null : json["discount_amount"].toDouble(),
    amountAfterDiscount: json["amount_after_discount"] == null ? null : json["amount_after_discount"].toDouble(),
    deliveryChargeToDeliveryBoy: json["delivery_charge_to_delivery_boy"] == null ? null : json["delivery_charge_to_delivery_boy"].toDouble(),
    deliveryChargeForCustomer: json["delivery_charge_for_customer"] == null ? null : json["delivery_charge_for_customer"].toDouble(),
    commissionCharge: json["commission_charge"] == null ? null : json["commission_charge"].toDouble(),
    platformCharge: json["platform_charge"] == null ? null : json["platform_charge"],
    afterChargeAmount: json["after_charge_amount"] == null ? null : json["after_charge_amount"].toDouble(),
    stripeCharges: json["stripe_charges"] == null ? null : json["stripe_charges"].toDouble(),
    amountAfterStripeCharges: json["amount_after_stripe_charges"] == null ? null : json["amount_after_stripe_charges"].toDouble(),
    tipToDeliveryBoy: json["tip_to_delivery_boy"] == null ? null : json["tip_to_delivery_boy"],
    instruction: json["instruction"] == null ? null : json["instruction"],
    status: json["status"] == null ? null : json["status"],
    orderDetails: json["order_details"] == null ? null : List<OrderDetail>.from(json["order_details"].map((x) => OrderDetail.fromJson(x))),
    userDetails: json["user_details"] == null ? null : UserDetails.fromJson(json["user_details"]),
    shopDetails: json["shop_details"] == null ? null : ShopDetails.fromJson(json["shop_details"]),
    couponDetails: json["coupon_details"] == null ? null : CouponDetails.fromJson(json["coupon_details"]),
    deliveryAddress: json["delivery_address"] == null ? null : DeliveryAddress.fromJson(json["delivery_address"]),
    deliveryBoyDetails: json["delivery_boy_details"] == null ? null : DeliveryBoyDetails.fromJson(json["delivery_boy_details"]),
    bookingStatus: json["booking_status"] == null ? null : BookingStatus.fromJson(json["booking_status"]),
    createdAt: json["created_at"] == null ? null : json["created_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "booking_code": bookingCode == null ? null : bookingCode,
    "delivery_boy_id": deliveryBoyId == null ? null : deliveryBoyId,
    "booking_type": bookingType == null ? null : bookingType,
    "booking_date_time": bookingDateTime == null ? null : bookingDateTime,
    "amount_before_discount": amountBeforeDiscount == null ? null : amountBeforeDiscount,
    "discount_amount": discountAmount == null ? null : discountAmount,
    "amount_after_discount": amountAfterDiscount == null ? null : amountAfterDiscount,
    "delivery_charge_to_delivery_boy": deliveryChargeToDeliveryBoy == null ? null : deliveryChargeToDeliveryBoy,
    "delivery_charge_for_customer": deliveryChargeForCustomer == null ? null : deliveryChargeForCustomer,
    "commission_charge": commissionCharge == null ? null : commissionCharge,
    "platform_charge": platformCharge == null ? null : platformCharge,
    "after_charge_amount": afterChargeAmount == null ? null : afterChargeAmount,
    "stripe_charges": stripeCharges == null ? null : stripeCharges,
    "amount_after_stripe_charges": amountAfterStripeCharges == null ? null : amountAfterStripeCharges,
    "tip_to_delivery_boy": tipToDeliveryBoy == null ? null : tipToDeliveryBoy,
    "instruction": instruction == null ? null : instruction,
    "status": status == null ? null : status,
    "order_details": orderDetails == null ? null : List<dynamic>.from(orderDetails.map((x) => x.toJson())),
    "user_details": userDetails == null ? null : userDetails.toJson(),
    "shop_details": shopDetails == null ? null : shopDetails.toJson(),
    "coupon_details": couponDetails == null ? null : couponDetails.toJson(),
    "delivery_address": deliveryAddress == null ? null : deliveryAddress.toJson(),
    "delivery_boy_details": deliveryBoyDetails == null ? null : deliveryBoyDetails.toJson(),
    "booking_status": bookingStatus == null ? null : bookingStatus.toJson(),
    "created_at": createdAt == null ? null : createdAt,
  };
}

class BookingStatus {
  BookingStatus({
    this.the0,
  });

  The0 the0;

  factory BookingStatus.fromJson(Map<String, dynamic> json) => BookingStatus(
    the0: json["0"] == null ? null : The0.fromJson(json["0"]),
  );

  Map<String, dynamic> toJson() => {
    "0": the0 == null ? null : the0.toJson(),
  };
}

class The0 {
  The0({
    this.id,
    this.bookingId,
    this.reason,
    this.value,
    this.statusValue,
    this.actionTime,
  });

  num id;
  num bookingId;
  String reason;
  String value;
  num statusValue;
  DateTime actionTime;

  factory The0.fromJson(Map<String, dynamic> json) => The0(
    id: json["id"] == null ? null : json["id"],
    bookingId: json["bookingId"] == null ? null : json["bookingId"],
    reason: json["reason"] == null ? null : json["reason"],
    value: json["value"] == null ? null : json["value"],
    statusValue: json["status_value"] == null ? null : json["status_value"],
    actionTime: json["action_time"] == null ? null : DateTime.parse(json["action_time"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "bookingId": bookingId == null ? null : bookingId,
    "reason": reason == null ? null : reason,
    "value": value == null ? null : value,
    "status_value": statusValue == null ? null : statusValue,
    "action_time": actionTime == null ? null : actionTime.toIso8601String(),
  };
}

class CouponDetails {
  CouponDetails({
    this.id,
    this.couponName,
    this.couponCode,
    this.couponDescription,
    this.isPrimary,
    this.couponType,
    this.couponDiscount,
  });

  num id;
  String couponName;
  String couponCode;
  String couponDescription;
  num isPrimary;
  num couponType;
  num couponDiscount;

  factory CouponDetails.fromJson(Map<String, dynamic> json) => CouponDetails(
    id: json["id"] == null ? null : json["id"],
    couponName: json["coupon_name"] == null ? null : json["coupon_name"],
    couponCode: json["coupon_code"] == null ? null : json["coupon_code"],
    couponDescription: json["coupon_description"] == null ? null : json["coupon_description"],
    isPrimary: json["isPrimary"] == null ? null : json["isPrimary"],
    couponType: json["coupon_type"] == null ? null : json["coupon_type"],
    couponDiscount: json["coupon_discount"] == null ? null : json["coupon_discount"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "coupon_name": couponName == null ? null : couponName,
    "coupon_code": couponCode == null ? null : couponCode,
    "coupon_description": couponDescription == null ? null : couponDescription,
    "isPrimary": isPrimary == null ? null : isPrimary,
    "coupon_type": couponType == null ? null : couponType,
    "coupon_discount": couponDiscount == null ? null : couponDiscount,
  };
}

class DeliveryAddress {
  DeliveryAddress({
    this.id,
    this.userId,
    this.city,
    this.country,
    this.formattedAddress,
    this.additionalInfo,
    this.pincode,
  });

  num id;
  num userId;
  String city;
  String country;
  String formattedAddress;
  String additionalInfo;
  String pincode;

  factory DeliveryAddress.fromJson(Map<String, dynamic> json) => DeliveryAddress(
    id: json["id"] == null ? null : json["id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    city: json["city"] == null ? null : json["city"],
    country: json["country"] == null ? null : json["country"],
    formattedAddress: json["formatted_address"] == null ? null : json["formatted_address"],
    additionalInfo: json["additional_info"] == null ? null : json["additional_info"],
    pincode: json["pincode"] == null ? null : json["pincode"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_id": userId == null ? null : userId,
    "city": city == null ? null : city,
    "country": country == null ? null : country,
    "formatted_address": formattedAddress == null ? null : formattedAddress,
    "additional_info": additionalInfo == null ? null : additionalInfo,
    "pincode": pincode == null ? null : pincode,
  };
}

class DeliveryBoyDetails {
  DeliveryBoyDetails();

  factory DeliveryBoyDetails.fromJson(Map<String, dynamic> json) => DeliveryBoyDetails(
  );

  Map<String, dynamic> toJson() => {
  };
}

class OrderDetail {
  OrderDetail({
    this.id,
    this.bookingId,
    this.productName,
    this.productDescription,
    this.price,
    this.discount,
    this.newPrice,
  });

  num id;
  num bookingId;
  String productName;
  String productDescription;
  num price;
  num discount;
  num newPrice;

  factory OrderDetail.fromJson(Map<String, dynamic> json) => OrderDetail(
    id: json["id"] == null ? null : json["id"],
    bookingId: json["bookingId"] == null ? null : json["bookingId"],
    productName: json["productName"] == null ? null : json["productName"],
    productDescription: json["productDescription"] == null ? null : json["productDescription"],
    price: json["price"] == null ? null : json["price"],
    discount: json["discount"] == null ? null : json["discount"],
    newPrice: json["new_price"] == null ? null : json["new_price"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "bookingId": bookingId == null ? null : bookingId,
    "productName": productName == null ? null : productName,
    "productDescription": productDescription == null ? null : productDescription,
    "price": price == null ? null : price,
    "discount": discount == null ? null : discount,
    "new_price": newPrice == null ? null : newPrice,
  };
}

class ShopDetails {
  ShopDetails({
    this.id,
    this.userId,
    this.phonenumberWithCountrycode,
    this.shopName,
    this.shopAddress,
    this.additionalAddress,
    this.description,
    this.lat,
    this.lng,
  });

  num id;
  num userId;
  String phonenumberWithCountrycode;
  String shopName;
  String shopAddress;
  String additionalAddress;
  String description;
  String lat;
  String lng;

  factory ShopDetails.fromJson(Map<String, dynamic> json) => ShopDetails(
    id: json["id"] == null ? null : json["id"],
    userId: json["user_id"] == null ? null : json["user_id"],
    phonenumberWithCountrycode: json["phonenumber_with_countrycode"] == null ? null : json["phonenumber_with_countrycode"],
    shopName: json["shop_name"] == null ? null : json["shop_name"],
    shopAddress: json["shop_address"] == null ? null : json["shop_address"],
    additionalAddress: json["additional_address"] == null ? null : json["additional_address"],
    description: json["description"] == null ? null : json["description"],
    lat: json["lat"] == null ? null : json["lat"],
    lng: json["lng"] == null ? null : json["lng"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "user_id": userId == null ? null : userId,
    "phonenumber_with_countrycode": phonenumberWithCountrycode == null ? null : phonenumberWithCountrycode,
    "shop_name": shopName == null ? null : shopName,
    "shop_address": shopAddress == null ? null : shopAddress,
    "additional_address": additionalAddress == null ? null : additionalAddress,
    "description": description == null ? null : description,
    "lat": lat == null ? null : lat,
    "lng": lng == null ? null : lng,
  };
}

class UserDetails {
  UserDetails({
    this.id,
    this.name,
    this.email,
    this.countryCode,
    this.phoneNumber,
    this.profilePicture,
    this.resourceUrl,
  });

  num id;
  dynamic name;
  dynamic email;
  String countryCode;
  String phoneNumber;
  dynamic profilePicture;
  String resourceUrl;

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
    id: json["id"] == null ? null : json["id"],
    name: json["name"],
    email: json["email"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    profilePicture: json["profile_picture"],
    resourceUrl: json["resource_url"] == null ? null : json["resource_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name,
    "email": email,
    "country_code": countryCode == null ? null : countryCode,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "profile_picture": profilePicture,
    "resource_url": resourceUrl == null ? null : resourceUrl,
  };

}

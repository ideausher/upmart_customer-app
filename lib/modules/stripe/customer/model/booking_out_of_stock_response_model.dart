// To parse this JSON data, do
//
//     final bookingOutOfStockResponseModel = bookingOutOfStockResponseModelFromJson(jsonString);

import 'dart:convert';

BookingOutOfStockResponseModel bookingOutOfStockResponseModelFromJson(String str) =>
    BookingOutOfStockResponseModel.fromJson(json.decode(str));

String bookingOutOfStockResponseModelToJson(BookingOutOfStockResponseModel data) => json.encode(data.toJson());

class BookingOutOfStockResponseModel {
  BookingOutOfStockResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<Datum> data;
  String message;
  int statusCode;

  factory BookingOutOfStockResponseModel.fromJson(Map<String, dynamic> json) => BookingOutOfStockResponseModel(
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Datum {
  Datum({
    this.productName,
    this.productDescription,
    this.quantityAvailable,
    this.quantityRequested,
    this.msg,
  });

  String productName;
  String productDescription;
  int quantityAvailable;
  int quantityRequested;
  String msg;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        productName: json["productName"] == null ? null : json["productName"],
        productDescription: json["productDescription"] == null ? null : json["productDescription"],
        quantityAvailable: json["quantity_available"] == null ? null : json["quantity_available"],
        quantityRequested: json["quantity_requested"] == null ? null : json["quantity_requested"],
        msg: json["msg"] == null ? null : json["msg"],
      );

  Map<String, dynamic> toJson() => {
        "productName": productName == null ? null : productName,
        "productDescription": productDescription == null ? null : productDescription,
        "quantity_available": quantityAvailable == null ? null : quantityAvailable,
        "quantity_requested": quantityRequested == null ? null : quantityRequested,
        "msg": msg == null ? null : msg,
      };
}

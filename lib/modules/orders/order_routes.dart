import 'package:flutter/material.dart';

import 'package:user/modules/orders/screen/orders_page.dart';
import 'package:user/modules/orders/sub_module/order_details/page/order_details_page.dart';
import 'package:user/modules/orders/sub_module/order_details/page/tracking_page.dart';

class OrderRoutes {
  static const String MY_ORDERS = '/MY_ORDERS';
  static const String ORDER_DETAILS = '/ORDER_DETAILS';
  static const String TRACKING_PAGE = '/TRACKING_PAGE';

  static Map<String, WidgetBuilder> routes() {
    return {
      MY_ORDERS: (context) => OrdersPage(context),
     ORDER_DETAILS: (context) => OrderDetailsPage(context),
      TRACKING_PAGE: (context) => TrackingPage(context),
    };
  }
}

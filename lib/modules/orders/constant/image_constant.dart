
const String ORDER_ICON = 'lib/modules/orders/image/order.svg';
const String ORDER_PLACED = 'lib/modules/orders/image/order_placed.png';
const String ORDER_CONFIRMED = 'lib/modules/orders/image/order_confirmed.png';
const String DRIVER_ASSIGNED = 'lib/modules/orders/image/delivery_boy.png';
const String ORDER_DISPATCHED = 'lib/modules/orders/image/order_dispatched.png';
const String ORDER_DILIVERED = 'lib/modules/orders/image/order_delivered.png';
const String ORDER_CANCELLED = 'lib/modules/orders/image/order_cancelled.png';

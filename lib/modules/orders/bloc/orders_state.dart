import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

class OrdersState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final int selectedIndex;

  OrdersState({
    this.isLoading,
    this.context,
    this.selectedIndex,
  }) : super(isLoading);

  // not authenticated
  factory OrdersState.initiating({bool isLoading}) {
    return OrdersState(
      isLoading: isLoading,
    );
  }

  factory OrdersState.updateUi({
    bool isLoading,
    BuildContext context,
    int selectedIndex,
  }) {
    return OrdersState(
      isLoading: isLoading,
      context: context,
      selectedIndex: selectedIndex,
    );
  }
}

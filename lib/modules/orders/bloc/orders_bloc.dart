import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/bloc/orders_event.dart';
import 'package:user/modules/orders/bloc/orders_state.dart';

class OrdersBloc extends BlocEventStateBase<OrdersEvent, OrdersState> {
  OrdersBloc({bool isLoading = false})
      : super(initialState: OrdersState.initiating(isLoading: isLoading));

  @override
  Stream<OrdersState> eventHandler(
      OrdersEvent event, OrdersState currentState) async* {
    // used for the select tab
    if (event is SelectTabEvent) {
      yield OrdersState.updateUi(
        isLoading: false,
        context: event.context,
        selectedIndex: event.selectedIndex,
      );
    }
  }
}

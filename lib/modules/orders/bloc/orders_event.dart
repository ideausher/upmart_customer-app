import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class OrdersEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int selectedIndex;

  OrdersEvent({
    this.isLoading: false,
    this.context,
    this.selectedIndex,
  });
}

// for tab selection
class SelectTabEvent extends OrdersEvent {
  SelectTabEvent({
    BuildContext context,
    bool isLoading,
    int selectedIndex,

  }) : super(
      context: context,
      isLoading: isLoading,
      selectedIndex: selectedIndex,
  );
}


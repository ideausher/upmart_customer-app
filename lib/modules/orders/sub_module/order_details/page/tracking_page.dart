import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/constant/image_constant.dart';
import 'package:user/modules/orders/enum/order_enum.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/tracking/tracking_state.dart';
import 'package:user/modules/orders/sub_module/order_details/managers/tracking/tracking_action_manager.dart';
import 'package:user/modules/polyline/api/polyline_points_api/model/poly_lat_long_model.dart';
import 'package:user/modules/polyline/page/polyline_page.dart';

import '../../../../../localizations.dart';

class TrackingPage extends StatefulWidget {
  BuildContext context;
  TrackingPage(this.context);

  @override
  _TrackingPageState createState() => _TrackingPageState();
}

class _TrackingPageState extends State<TrackingPage> implements PushReceived {
  Order _order;

  //Bloc Variables
  TrackingActionManagers _trackingActionManagers = TrackingActionManagers();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _order = ModalRoute
        .of(widget.context)
        .settings
        .arguments;
    _trackingActionManagers?.context = widget?.context;

    _trackingActionManagers?.actionOnInit(order: _order);
  }

  @override
  void dispose() {
    super.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _trackingActionManagers?.trackingBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<TackingState>(
      bloc: _trackingActionManagers?.trackingBloc,
      builder: (BuildContext context, TackingState trackingState) {
        _trackingActionManagers.context = context;

        List<PolyPointLatLngModel> _wayPoints = List<PolyPointLatLngModel>();
        if (trackingState != null &&
            _trackingActionManagers.trackingState != trackingState) {
          _trackingActionManagers.trackingState = trackingState;
          _trackingActionManagers.actionOnOrderDetailsStateChange(
            scaffoldState: _scaffoldKey?.currentState,
          );

          if (trackingState?.getDeliveryLocationResponseModel?.data?.lat !=
              null) {
            _wayPoints.add(PolyPointLatLngModel(
                longitude: num.parse(
                    trackingState?.getDeliveryLocationResponseModel?.data?.lng),
                latitude: num.parse(trackingState
                    ?.getDeliveryLocationResponseModel?.data?.lat)));

            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              PolyLineLatLongMapPage.updatePolyPointsCallback.updatePolyDetails(
                  wayPosPoints: _wayPoints);
            });
          }

          print(
              "orderdetails ${_trackingActionManagers?.trackingState?.order}");
        }
        return ModalProgressHUD(
            inAsyncCall: trackingState?.isLoading ?? false,
            child: WillPopScope(
              onWillPop: () {
                return NavigatorUtils.navigatorUtilsInstance
                    .navigatorPopScreen(context);
              },
              child: Scaffold(
                key: _scaffoldKey,
                appBar: CommonUtils.commonUtilsInstance.getAppBar(
                    context: context,
                    elevation: ELEVATION_0,
                    appBarTitle: AppLocalizations
                        .of(
                        _trackingActionManagers?.trackingState?.context)
                        .trackingpage
                        .appbartitle
                        .trackOrder,
                    appBarTitleStyle: AppConfig
                        .of(context)
                        .themeData
                        .primaryTextTheme
                        .headline3,
                    popScreenOnTapOfLeadingIcon: false,
                    defaultLeadingIcon: Icons.arrow_back,
                    defaultLeadingIconColor: COLOR_BLACK,
                    defaultLeadIconPressed: () {
                      return NavigatorUtils.navigatorUtilsInstance
                          .navigatorPopScreen(context);
                    },
                    actionWidgets: [],
                    backGroundColor: Colors.transparent),
                body: Column(
                  children: [
                    Expanded(
                      child: PolyLineLatLongMapPage(
                        destLatLong: PolyPointLatLngModel(
                            latitude: _trackingActionManagers?.trackingState
                                ?.order?.deliveryAddress?.latitude ??
                                _order?.deliveryAddress?.latitude,
                            longitude: _trackingActionManagers?.trackingState
                                ?.order?.deliveryAddress?.longitude ??
                                _order?.deliveryAddress?.longitude,
                            address: _trackingActionManagers
                                ?.trackingState
                                ?.order
                                ?.deliveryAddress
                                ?.formattedAddress ??
                                _order?.deliveryAddress?.formattedAddress),
                        originLatLong: PolyPointLatLngModel(
                            latitude: double.parse(_trackingActionManagers
                                ?.trackingState
                                ?.order
                                ?.shopDetails
                                ?.latitude ??
                                _order?.shopDetails?.latitude),
                            longitude: double.parse(
                              _trackingActionManagers?.trackingState?.order
                                  ?.shopDetails?.longitude ??
                                  _order?.shopDetails?.longitude,
                            ),
                            address: _trackingActionManagers?.trackingState
                                ?.order?.shopDetails?.address ??
                                _order?.shopDetails?.address),
                        wayPosPoints: _wayPoints,
                      ),
                    ),
                    Expanded(
                      child: _TimelineDelivery(
                        order: _trackingActionManagers?.trackingState?.order ??
                            _order,
                        context:
                        _trackingActionManagers?.trackingState?.context,
                        actionManagers: _trackingActionManagers,
                      ),
                    ),
                  ],
                ),
              ),
            ));
      },
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _trackingActionManagers?.actionOnInit(
        order: _trackingActionManagers?.trackingState?.order ?? _order);
  }
}

class _TimelineDelivery extends StatefulWidget {
  TrackingActionManagers actionManagers;
  BuildContext context;
  Order order;

  _TimelineDelivery({this.order, this.actionManagers, this.context});

  @override
  __TimelineDeliveryState createState() => __TimelineDeliveryState();
}

class __TimelineDeliveryState extends State<_TimelineDelivery> {
  ScrollController _scrollController;

  List<BookingStatus> _bookingStatus = List<BookingStatus>();
  int _currentStep = 3;

  Order _order;

  @override
  void initState() {
    super.initState();
    print('The bookingStatus is ${widget?.order?.bookingStatus}');
    print('init The order id is ${_order?.id}');
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);
    // _showOrderStatusView();
    print('The bookingStatus is ${widget?.order?.bookingStatus}');
    print('update The order id is ${_order?.id}');
  }

  _showOrderStatusView() {
    _order = widget.actionManagers?.trackingState?.order ?? widget?.order;
    _bookingStatus?.clear();
    if (_order.bookingStatus
        .containsKey(OrderStatus.autocancel.value?.toString())) {
      //auto cancel
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? widget
              .order
              .bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));

      if (_order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.orderAcceptedByVendor.value,
            value:
            AppLocalizations
                .of(widget.context)
                .common
                .text
                .orderConfirmed,
            statusValue: _order.bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? 1
                : 0,
            actionTime: _order.bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? widget
                .order
                .bookingStatus[
            OrderStatus.orderAcceptedByVendor.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget.context)
                .common
                .text
                .notAllowed));
        _currentStep = 2;
      } else {
        _currentStep = 3;
      }

      // auto cancel
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.autocancel.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.autocancel.value?.toString())
              ? widget
              .order
              .bookingStatus[OrderStatus.autocancel.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
    } else if (_order.bookingStatus
        .containsKey(OrderStatus.orderRejectedByVendor.value?.toString())) {
      _currentStep = 1;
      // rejected by vendor
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .oderPlaced,
          image: ORDER_PLACED,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? _order.bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderRejectedByVendor.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .orderRejected,
          image: ORDER_CANCELLED,
          statusValue: _order.bookingStatus.containsKey(
              OrderStatus.orderRejectedByVendor.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus.containsKey(
              OrderStatus.orderRejectedByVendor.value?.toString())
              ? _order
              .bookingStatus[
          OrderStatus.orderRejectedByVendor.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.orderRejectedByVendor.value?.toString()))
        _currentStep = 2;
    } else if (_order.bookingStatus
        .containsKey(OrderStatus.cancelledByDeliveryBoy.value?.toString())) {
      _currentStep = 1;
      // rejected by deleivery boy
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .oderPlaced,
          image: ORDER_PLACED,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? _order.bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderAcceptedByVendor.value,
          value:
          AppLocalizations
              .of(widget?.context)
              .common
              .text
              .orderConfirmed,
          image: ORDER_CONFIRMED,
          statusValue: _order.bookingStatus.containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus.containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? _order
              .bookingStatus[
          OrderStatus.orderAcceptedByVendor.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString()))
        _currentStep = 2;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.cancelledByDeliveryBoy.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .orderRejected,
          image: ORDER_CANCELLED,
          statusValue: _order.bookingStatus.containsKey(
              OrderStatus.cancelledByDeliveryBoy.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus.containsKey(
              OrderStatus.cancelledByDeliveryBoy.value?.toString())
              ? _order
              .bookingStatus[
          OrderStatus.cancelledByDeliveryBoy.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.cancelledByDeliveryBoy.value?.toString()))
        _currentStep = 3;
    } else if (_order.bookingStatus
        .containsKey(OrderStatus.cancelledByCustomer.value?.toString())) {
      _currentStep = 1;
      // canceled by customer
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .oderPlaced,
          image: ORDER_PLACED,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? _order.bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));

      if (_order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.orderAcceptedByVendor.value,
            value:
            AppLocalizations
                .of(widget?.context)
                .common
                .text
                .orderConfirmed,
            image: ORDER_CONFIRMED,
            statusValue: _order.bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? 1
                : 0,
            actionTime: _order.bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? _order
                .bookingStatus[
            OrderStatus.orderAcceptedByVendor.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget?.context)
                .common
                .text
                .notAllowed));
        _currentStep = 2;
      }

      if (_order.bookingStatus
          .containsKey(OrderStatus.deliveryBoyAssigned.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.deliveryBoyAssigned.value,
            value:
            AppLocalizations
                .of(widget?.context)
                .common
                .text
                .driverAssigned,
            image: DRIVER_ASSIGNED,
            statusValue: _order.bookingStatus.containsKey(
                OrderStatus.deliveryBoyAssigned.value?.toString())
                ? 1
                : 0,
            actionTime: _order.bookingStatus.containsKey(
                OrderStatus.deliveryBoyAssigned.value?.toString())
                ? _order
                .bookingStatus[
            OrderStatus.deliveryBoyAssigned.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget?.context)
                .common
                .text
                .notAllowed));
        _currentStep = 3;
      }

      if (_order.bookingStatus
          .containsKey(OrderStatus.orderPicked.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.orderPicked.value,
            value: AppLocalizations
                .of(widget?.context)
                .common
                .text
                .dispatched,
            image: ORDER_DISPATCHED,
            statusValue: _order.bookingStatus
                .containsKey(OrderStatus.orderPicked.value?.toString())
                ? 1
                : 0,
            actionTime: _order.bookingStatus
                .containsKey(OrderStatus.orderPicked.value?.toString())
                ? _order
                .bookingStatus[OrderStatus.orderPicked.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget?.context)
                .common
                .text
                .notAllowed));
        _currentStep = 4;
      }
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.deliveredProduct.value,
          value:
          AppLocalizations
              .of(widget?.context)
              .common
              .text
              .orderCancelled,
          image: ORDER_CANCELLED,
          statusValue: _order.bookingStatus.containsKey(
              OrderStatus.cancelledByCustomer.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus.containsKey(
              OrderStatus.cancelledByCustomer.value?.toString())
              ? _order
              .bookingStatus[
          OrderStatus.cancelledByCustomer.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      _currentStep = 5;
    } else {
      _currentStep = 1;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .oderPlaced,
          image: ORDER_PLACED,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? _order.bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderAcceptedByVendor.value,
          value:
          AppLocalizations
              .of(widget?.context)
              .common
              .text
              .orderConfirmed,
          image: ORDER_CONFIRMED,
          statusValue: _order.bookingStatus.containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus.containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? _order
              .bookingStatus[
          OrderStatus.orderAcceptedByVendor.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString()))
        _currentStep = 2;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.deliveryBoyAssigned.value,
          value:
          AppLocalizations
              .of(widget?.context)
              .common
              .text
              .driverAssigned,
          image: DRIVER_ASSIGNED,
          statusValue: _order.bookingStatus.containsKey(
              OrderStatus.deliveryBoyAssigned.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus.containsKey(
              OrderStatus.deliveryBoyAssigned.value?.toString())
              ? _order
              .bookingStatus[
          OrderStatus.deliveryBoyAssigned.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.deliveryBoyAssigned.value?.toString()))
        _currentStep = 3;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPicked.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .dispatched,
          image: ORDER_DISPATCHED,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.orderPicked.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.orderPicked.value?.toString())
              ? _order.bookingStatus[OrderStatus.orderPicked.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.orderPicked.value?.toString()))
        _currentStep = 4;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.deliveredProduct.value,
          value: AppLocalizations
              .of(widget?.context)
              .common
              .text
              .delivered,
          image: ORDER_DILIVERED,
          statusValue: _order.bookingStatus
              .containsKey(OrderStatus.deliveredProduct.value?.toString())
              ? 1
              : 0,
          actionTime: _order.bookingStatus
              .containsKey(OrderStatus.deliveredProduct.value?.toString())
              ? _order
              .bookingStatus[OrderStatus.deliveredProduct.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget?.context)
              .common
              .text
              .notAllowed));
      if (_order.bookingStatus
          .containsKey(OrderStatus.deliveredProduct.value?.toString()))
        _currentStep = 5;
    }

    _scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    _showOrderStatusView();
    return ListView.builder(
        shrinkWrap: true,
        itemCount: _bookingStatus?.length,
        itemBuilder: (BuildContext context, int index) {
          BookingStatus step = _bookingStatus[index];
          return TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: 0.1,
            isFirst: index == 0,
            isLast: index == _bookingStatus.length - 1,
            indicatorStyle: IndicatorStyle(
              width: 15,
              color:
              step?.statusValue == 1 ? COLOR_DARK_PRIMARY : COLOR_GREY_TEXT,
              padding: EdgeInsets.all(6),
            ),
            endChild: _RightChild(
              disabled: true,
              asset: step?.image,
              title: step?.value,
              message: step?.actionTime,
            ),
            beforeLineStyle:
            const LineStyle(color: COLOR_GREY_TEXT, thickness: 2),
          );
        });
  }
}

class _RightChild extends StatelessWidget {
  const _RightChild({
    Key key,
    this.asset,
    this.title,
    this.message,
    this.disabled = false,
  }) : super(key: key);

  final String asset;
  final String title;
  final String message;
  final bool disabled;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(SIZE_16),
      child: Container(
        child: Row(
          children: [
            Image.asset(
              asset,
              height: SIZE_26,
              width: SIZE_26,
            ),
            SizedBox(width: SIZE_10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(title),
                Text(message, style: textStyleSize12WithBlackColor)
              ],
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/launcher_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/enum/order_enum.dart';
import 'package:user/modules/orders/manager/orders_utils_manager.dart';
import 'package:user/modules/orders/manager/orders_widget_manager.dart';
import 'package:intl/intl.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/order_details/oder_details_state.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/order_details/order_details_bloc.dart';
import 'package:user/modules/orders/sub_module/order_details/managers/order_details/order_details_action_manager.dart';

class OrderDetailsPage extends StatefulWidget {
  BuildContext context;

  OrderDetailsPage(this.context);

  @override
  _OrderDetailsPageState createState() => _OrderDetailsPageState();
}

class _OrderDetailsPageState extends State<OrderDetailsPage>
    implements PushReceived {
  //Bloc Variables
  OrderDetailsBloc _orderDetailsBloc = new OrderDetailsBloc();
  OrderDetailsActionManager _orderDetailsActionManager =
      OrderDetailsActionManager();
  TextEditingController _orderCancelReasonController =
      new TextEditingController();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Order _order;

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _order = ModalRoute.of(widget.context).settings.arguments;
    _orderDetailsActionManager.context = widget.context;
    _orderDetailsActionManager.orderDetailsBloc = _orderDetailsBloc;
    _orderDetailsActionManager.actionOnInit(order: _order);
  }

  @override
  void dispose() {
    super.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _orderDetailsBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _orderDetailsActionManager.context = context;
    return BlocEventStateBuilder<OrderDetailsState>(
      bloc: _orderDetailsBloc,
      builder: (BuildContext context, OrderDetailsState orderDetailsState) {
        _orderDetailsActionManager.context = context;
        if (orderDetailsState != null &&
            _orderDetailsActionManager.orderDetailsState != orderDetailsState) {
          _orderDetailsActionManager.orderDetailsState = orderDetailsState;
          _orderDetailsActionManager.actionOnOrderDetailsStateChange(
            scaffoldState: _scaffoldKey?.currentState,
          );
        }
        return ModalProgressHUD(
            inAsyncCall: orderDetailsState?.isLoading ?? false,
            child: WillPopScope(
              onWillPop: () {
                return NavigatorUtils.navigatorUtilsInstance
                    .navigatorPopScreen(context);
              },
              child: Scaffold(
                key: _scaffoldKey,
                appBar: _showAppBar(),
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: SIZE_20),
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      alignment: WrapAlignment.center,
                      runSpacing: SIZE_10,
                      spacing: SIZE_2,
                      children: [
                        _showDivider(),
                        _showOrderDetails(),
                        Visibility(
                            visible: OrdersUtilsManager
                                .ordersUtilsManagerInstance
                                .canOrderCancel(
                                    order: _orderDetailsActionManager
                                        ?.orderDetailsState?.order),
                            child: _showCancelOrderView()),
                        _showOrderCancelReason(),
                        _showCommonHeaderView(
                            titleLeft: /*'Order Status'*/ AppLocalizations
                                    .of(_orderDetailsActionManager?.context)
                                .orderdetailpage
                                .text
                                .orderStatus,
                            titleRight: ''),
                        _showOrderProgressView(),
                        _showCommonHeaderView(
                            titleLeft: AppLocalizations.of(
                                    _orderDetailsActionManager?.context)
                                .orderdetailpage
                                .text
                                .destination),
                        _showUserAddressView(),
                        _showCommonHeaderView(
                            titleLeft: AppLocalizations.of(
                                    _orderDetailsActionManager?.context)
                                .orderdetailpage
                                .text
                                .contact),
                        _showContactView(),
                        _showCommonHeaderView(
                            titleLeft: /*'Item Description'*/ AppLocalizations
                                    .of(_orderDetailsActionManager?.context)
                                .orderdetailpage
                                .text
                                .itemDescription),
                        _showProductItemsPurchased(),
                        _showCommonHeaderView(
                            titleLeft: /* 'Payments'*/ AppLocalizations
                                    .of(_orderDetailsActionManager?.context)
                                .orderdetailpage
                                .text
                                .payments),
                        _showTotalPaymentView(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _showRaiseDisputeButton(),
                            Visibility(
                              visible: (_orderDetailsActionManager
                                              ?.orderDetailsState
                                              ?.order
                                              ?.ratings ==
                                          null ||
                                      _orderDetailsActionManager
                                              ?.orderDetailsState
                                              ?.order
                                              ?.ratings
                                              ?.isNotEmpty ==
                                          false) &&
                                  OrdersUtilsManager.ordersUtilsManagerInstance
                                          .orderCompletedOrCanceled(
                                              order: _orderDetailsActionManager
                                                  ?.orderDetailsState?.order) ==
                                      true,
                              child: _shareFeedback(),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(_orderDetailsActionManager?.context)
            .orderdetailpage
            .appbartitle
            .orderDetail,
        appBarTitleStyle: AppConfig.of(_orderDetailsActionManager.context)
            .themeData
            .primaryTextTheme
            .headline3,
        popScreenOnTapOfLeadingIcon: true,
        defaultLeadingIcon: Icons.arrow_back,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_16),
            child: NotificationReadCountWidget(
              context: _orderDetailsActionManager?.context,
            ),
          )
        ],
        defaultLeadingIconColor: Colors.black,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show order details
  Widget _showOrderDetails() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showOrderStatus(),
          SizedBox(
            height: SIZE_20,
          ),
          _showOrderDetail(),
        ],
      ),
    );
  }

  //method to show order status
  Widget _showOrderStatus() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(SIZE_5)),
            color:
                _orderDetailsActionManager?.orderDetailsState?.order?.status ==
                            OrderStatus.cancelledByCustomer.value ||
                        _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.cancelledByDeliveryBoy.value ||
                        _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.autocancel.value ||
                        _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.orderRejectedByVendor.value
                    ? COLOR_LIGHT_RED
                    : COLOR_LIGHT_BLUE,
          ),
          padding: EdgeInsets.all(SIZE_6),
          child: Text(
            OrdersUtilsManager.ordersUtilsManagerInstance.getOrderStatus(
                order: _orderDetailsActionManager?.orderDetailsState?.order,
                context: context),
            style: TextStyle(
                fontSize: textStyleSize14WithBlueColor?.fontSize,
                fontWeight: textStyleSize14WithBlueColor?.fontWeight,
                fontFamily: textStyleSize14WithBlueColor?.fontFamily,
                color: _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.cancelledByCustomer.value ||
                        _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.autocancel.value ||
                        _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.cancelledByDeliveryBoy.value ||
                        _orderDetailsActionManager
                                ?.orderDetailsState?.order?.status ==
                            OrderStatus.orderRejectedByVendor.value
                    ? COLOR_RED
                    : COLOR_DARK_BLUE),
          ),
        ),
      ],
    );
  }

  //method to show Order status
  Widget _showOrderDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _showDetails(
                title: AppLocalizations.of(_orderDetailsActionManager?.context)
                    .myorderpage
                    .text
                    .orderId,
                subTitle:
                    "#${_orderDetailsActionManager.orderDetailsState?.order?.bookingCode?.toString() ?? ""}"),
            _showDetails(
                title: AppLocalizations.of(_orderDetailsActionManager?.context)
                    .myorderpage
                    .text
                    .totalPayment,
                subTitle: NumberFormatUtils.numberFormatUtilsInstance
                    .formatPriceWithSymbol(
                        price: ((_orderDetailsActionManager.orderDetailsState
                                        ?.order?.amountAfterStripeCharges ??
                                    0) +
                                (_orderDetailsActionManager
                                    .orderDetailsState?.order?.stripeCharges) ??
                            0))),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _showDetails(
                title: AppLocalizations.of(_orderDetailsActionManager?.context)
                    .myorderpage
                    .text
                    .dated,
                subTitle: _orderDetailsActionManager
                            .orderDetailsState?.order?.createdAt?.isNotEmpty ==
                        true
                    ? DateUtils.dateUtilsInstance.getDateTimeDayFormat(
                        dateTime: _orderDetailsActionManager
                            .orderDetailsState?.order?.createdAt,
                        toLocal: true)
                    : ""),
            (_orderDetailsActionManager.orderDetailsState?.order
                        ?.bookingDateTime?.isNotEmpty ==
                    true)
                ? _showDetails(
                    title:
                        AppLocalizations.of(_orderDetailsActionManager?.context)
                            .categoriesshoppage
                            .text
                            .schedulingAt,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    subTitle: _orderDetailsActionManager.orderDetailsState
                                ?.order?.bookingDateTime?.isNotEmpty ==
                            true
                        ? DateUtils.dateUtilsInstance.getDateAndTimeFormat(
                            dateTime: _orderDetailsActionManager
                                .orderDetailsState?.order?.bookingDateTime,
                            toLocal: false)
                        : "")
                : const SizedBox()
          ],
        ),
      ],
    );
  }

  //method to show orders details text data
  Widget _showDetails(
      {String title,
      String subTitle,
      CrossAxisAlignment crossAxisAlignment: CrossAxisAlignment.start}) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      children: [
        Text(
          title,
          style: textStyleSize14GreyColorFont500,
        ),
        Text(
          subTitle,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .primaryTextTheme
              .headline1,
        )
      ],
    );
  }

  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1,
    );
  }

  //this method is used to show order progress view
  Widget _showOrderProgressView() {
    return DeliveryTimeline(
        context: _orderDetailsActionManager?.context,
        actionManager: _orderDetailsActionManager);
  }

  //method to show common header view
  Widget _showCommonHeaderView({String titleLeft, String titleRight}) {
    return Container(
      padding: EdgeInsets.only(
          left: SIZE_20, right: SIZE_20, top: SIZE_10, bottom: SIZE_10),
      color: COLOR_GREY,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            titleLeft,
            style: AppConfig.of(_orderDetailsActionManager.context)
                .themeData
                .primaryTextTheme
                .headline1,
          ),
          (titleRight != null)
              ? Text(
                  titleRight,
                  style: textStyleSize12WithPrimary,
                )
              : const SizedBox()
        ],
      ),
    );
  }

  //method to show user address view
  Widget _showUserAddressView() {
    return InkWell(
      onTap: () {
        if (OrdersUtilsManager.ordersUtilsManagerInstance
                .orderCompletedOrCanceled(
                    order:
                        _orderDetailsActionManager?.orderDetailsState?.order) ==
            false) _orderDetailsActionManager?.actionToTrack();
      },
      child: Row(
        children: [
          Expanded(
              child: Container(
            margin: EdgeInsets.only(left: SIZE_20),
            child: Text(
              _orderDetailsActionManager?.orderDetailsState?.order
                      ?.deliveryAddress?.formattedAddress ??
                  "",
              style: textStyleSize12WithBlackColor,
            ),
          )),
          OrdersUtilsManager.ordersUtilsManagerInstance
                      .orderCompletedOrCanceled(
                          order: _orderDetailsActionManager
                              ?.orderDetailsState?.order) ==
                  false
              ? Padding(
                  padding: const EdgeInsets.only(left: SIZE_10, right: SIZE_10),
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      AppLocalizations.of(_orderDetailsActionManager?.context)
                          .orderdetailpage
                          .text
                          .track
                          .toUpperCase(),
                      style: textStyleSize14WithGreenColor,
                    ),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }

  //this method will return total payment view
  Widget _showTotalPaymentView() {
    return Card(
      margin: EdgeInsets.only(left: SIZE_10, right: SIZE_10),
      elevation: ELEVATION_05,
      child: Wrap(
        runSpacing: SIZE_10,
        spacing: SIZE_10,
        children: [
          Padding(
            padding: const EdgeInsets.all(SIZE_12),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppLocalizations.of(_orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .cartValue,
                      style: textStyleSize16WithGreenColor,
                    ),
                    Text(
                        NumberFormatUtils.numberFormatUtilsInstance
                            .formatPriceWithSymbol(
                                price: _orderDetailsActionManager
                                        ?.orderDetailsState
                                        ?.order
                                        ?.amountBeforeDiscount ??
                                    0),
                        style: textStyleSize16WithGreenColor),
                  ],
                ),
                SizedBox(
                  height: SIZE_10,
                ),

                Visibility(
                    visible: _orderDetailsActionManager
                            ?.orderDetailsState?.order?.tipToDeliveryBoy >
                        0,
                    child: SizedBox(
                      height: SIZE_10,
                    )),

                Visibility(
                  visible: _orderDetailsActionManager
                          ?.orderDetailsState?.order?.tipToDeliveryBoy >
                      0,
                  child: _getKeyValueWidget(
                      key: AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .tipToDriver,
                      value: NumberFormatUtils.numberFormatUtilsInstance
                          .formatPriceWithSymbol(
                              price: _orderDetailsActionManager
                                      ?.orderDetailsState
                                      ?.order
                                      ?.tipToDeliveryBoy ??
                                  0)),
                ),
                Visibility(
                    visible: _orderDetailsActionManager
                            ?.orderDetailsState?.order?.discountAmount >
                        0,
                    child: SizedBox(
                      height: SIZE_10,
                    )),
                // used for the coupon discount
                Visibility(
                  visible: _orderDetailsActionManager
                          ?.orderDetailsState?.order?.discountAmount >
                      0,
                  child: _getKeyValueWidget(
                      key: AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .couponDiscount,
                      value: NumberFormatUtils.numberFormatUtilsInstance
                          .formatPriceWithSymbol(
                              price: (_orderDetailsActionManager
                                          ?.orderDetailsState
                                          ?.order
                                          ?.discountAmount ??
                                      0) *
                                  -1)),
                ),

                //for gst
                Visibility(
                  visible: (_orderDetailsActionManager
                              ?.orderDetailsState?.order?.gst !=
                          null &&
                      _orderDetailsActionManager
                                  ?.orderDetailsState?.order?.gst >
                              0 ==
                          true),
                  child: _getKeyValueWidget(
                      key: AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .gst,
                      value: NumberFormatUtils.numberFormatUtilsInstance
                          .formatPriceWithSymbol(
                              price: _orderDetailsActionManager
                                      ?.orderDetailsState?.order?.gst ??
                                  0)),
                ),
                //for hst
                Visibility(
                  visible: (_orderDetailsActionManager
                              ?.orderDetailsState?.order?.hst !=
                          null &&
                      _orderDetailsActionManager
                                  ?.orderDetailsState?.order?.hst >
                              0 ==
                          true),
                  child: _getKeyValueWidget(
                      key: AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .hst,
                      value: NumberFormatUtils.numberFormatUtilsInstance
                          .formatPriceWithSymbol(
                              price: _orderDetailsActionManager
                                      ?.orderDetailsState?.order?.hst ??
                                  0)),
                ),

                //for pst
                Visibility(
                  visible: (_orderDetailsActionManager
                              ?.orderDetailsState?.order?.pst !=
                          null &&
                      _orderDetailsActionManager
                                  ?.orderDetailsState?.order?.pst >
                              0 ==
                          true),
                  child: _getKeyValueWidget(
                      key: AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .pst,
                      value: NumberFormatUtils.numberFormatUtilsInstance
                          .formatPriceWithSymbol(
                              price: _orderDetailsActionManager
                                      ?.orderDetailsState?.order?.pst ??
                                  0)),
                ),

                // used for the transaction charges
                _getKeyValueWidget(
                    key: AppLocalizations.of(_orderDetailsActionManager.context)
                        .cartpage
                        .text
                        .platformCharges,
                    value: NumberFormatUtils.numberFormatUtilsInstance
                        .formatPriceWithSymbol(
                            price: _orderDetailsActionManager?.orderDetailsState
                                    ?.order?.platformCharge ??
                                0)),

                Visibility(
                    visible: _orderDetailsActionManager?.orderDetailsState
                            ?.order?.deliveryChargeForCustomer >
                        0,
                    child: SizedBox(
                      height: SIZE_0,
                    )),
                // used for the delivery fee
                Visibility(
                  visible: _orderDetailsActionManager?.orderDetailsState?.order
                          ?.deliveryChargeForCustomer >
                      0,
                  child: _getKeyValueWidget(
                      key: AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .cartpage
                          .text
                          .deliveryFee,
                      value: NumberFormatUtils.numberFormatUtilsInstance
                          .formatPriceWithSymbol(
                              price: _orderDetailsActionManager
                                      ?.orderDetailsState
                                      ?.order
                                      ?.deliveryChargeForCustomer ??
                                  0)),
                ),
              ],
            ),
          ),
          Container(
            color: COLOR_LIGHT_RED,
            padding: EdgeInsets.all(SIZE_10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppLocalizations.of(_orderDetailsActionManager.context)
                      .cartpage
                      .text
                      .totalPayment,
                  style: AppConfig.of(_orderDetailsActionManager.context)
                      .themeData
                      .textTheme
                      .bodyText2,
                ),
                Text(
                  NumberFormatUtils.numberFormatUtilsInstance
                      .formatPriceWithSymbol(
                          price: ((_orderDetailsActionManager?.orderDetailsState
                                          ?.order?.amountAfterStripeCharges ??
                                      0.0) +
                                  _orderDetailsActionManager?.orderDetailsState
                                      ?.order?.stripeCharges ??
                              0.0)),
                  style: AppConfig.of(_orderDetailsActionManager.context)
                      .themeData
                      .textTheme
                      .bodyText2,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // used for the key and value widget
  Widget _getKeyValueWidget({
    String key,
    String value,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .primaryTextTheme
              .headline4,
        ),
        Text(
          value,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .primaryTextTheme
              .headline4,
        ),
      ],
    );
  }

  //method to show contact view for delivery boy and shop
  Widget _showContactView() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
      child: Column(
        children: [
          InkWell(
            hoverColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              //make call to shop
              LauncherUtils.launcherUtilsInstance.makePhoneCall(
                  phoneNumber: _orderDetailsActionManager?.orderDetailsState
                      ?.order?.shopDetails?.showOwner?.phoneNumber);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    '${AppLocalizations.of(_orderDetailsActionManager.context).orderdetailpage.text.contact} ${_orderDetailsActionManager?.orderDetailsState?.order?.shopDetails?.name}'),
                Icon(
                  Icons.call,
                  color: COLOR_PRIMARY,
                  size: SIZE_20,
                )
              ],
            ),
          ),
          Visibility(
              visible: _orderDetailsActionManager?.orderDetailsState?.order
                      ?.deliveryBoyDetails?.phoneNumber?.isNotEmpty ==
                  true,
              child: SizedBox(
                height: SIZE_20,
              )),
          Visibility(
            visible: _orderDetailsActionManager?.orderDetailsState?.order
                    ?.deliveryBoyDetails?.phoneNumber?.isNotEmpty ==
                true,
            child: InkWell(
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {
                //make call to delivery boy
                LauncherUtils.launcherUtilsInstance.makePhoneCall(
                    phoneNumber: _orderDetailsActionManager?.orderDetailsState
                        ?.order?.deliveryBoyDetails?.phoneNumber);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      '${AppLocalizations.of(_orderDetailsActionManager.context).orderdetailpage.text.contact} ${_orderDetailsActionManager?.orderDetailsState?.order?.deliveryBoyDetails?.name ?? ""}'),
                  Icon(
                    Icons.call,
                    color: COLOR_DARK_PRIMARY,
                    size: SIZE_20,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  //show cancel order view
  Widget _showCancelOrderView() {
    return InkWell(
      onTap: () {
        _orderDetailsActionManager?.actionOnCancelOrder(
            scaffoldState: _scaffoldKey?.currentState,
            cancelOrderController: _orderCancelReasonController);
        /*    _orderDetailsActionManager?.actionOnCancelOrder(
            scaffoldState: _scaffoldKey?.currentState);*/
      },
      child: Padding(
        padding: const EdgeInsets.only(right: SIZE_20),
        child: Text(
          AppLocalizations.of(_orderDetailsActionManager.context)
              .orderdetailpage
              .text
              .cancelOrder,
          style: AppConfig.of(_orderDetailsActionManager.context)
              .themeData
              .textTheme
              .headline4,
        ),
      ),
    );
  }

  // show product items purchased
  Widget _showProductItemsPurchased() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
      child: (_orderDetailsActionManager
                  ?.orderDetailsState?.order?.orderDetails?.isNotEmpty ==
              true)
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListView.separated(
                    padding: EdgeInsets.all(SIZE_0),
                    itemCount: _orderDetailsActionManager
                        ?.orderDetailsState?.order?.orderDetails?.length,
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(
                        color: COLOR_BORDER_GREY,
                      );
                    },
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      OrderDetail _orderDetail = _orderDetailsActionManager
                          ?.orderDetailsState?.order?.orderDetails[index];
                      return Container(
                        margin: EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                (_orderDetail?.productImage?.fileName?.isNotEmpty == true)
                                    ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                                        context: context,
                                        radius: SIZE_0,
                                        showProgressBarInPlaceHolder: true,
                                        shape: BoxShape.rectangle,
                                        url:
                                            _orderDetail?.productImage?.fileName ??
                                                "",
                                        height: CommonUtils?.commonUtilsInstance
                                            ?.getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: false),
                                        width: CommonUtils.commonUtilsInstance
                                            .getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: true))
                                    : SvgPicture.asset(PRODUCT_LOGO,
                                        height: CommonUtils?.commonUtilsInstance
                                            ?.getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: false),
                                        width: CommonUtils.commonUtilsInstance
                                            .getPercentageSize(
                                                context: context,
                                                percentage: SIZE_5,
                                                ofWidth: true)),
                                SizedBox(width: SIZE_20),
                                Expanded(
                                  child: Text(
                                    toBeginningOfSentenceCase(
                                      _orderDetail?.productName ??
                                          AppLocalizations.of(
                                                  _orderDetailsActionManager
                                                      ?.context)
                                              .common
                                              .text
                                              .notAllowed,
                                    ),
                                  ),
                                ),
                                Text(
                                  "x${_orderDetail?.quantity?.toString()}" ??
                                      "",
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
                Divider(
                  height: SIZE_1,
                  color: COLOR_RATING,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppLocalizations.of(
                              _orderDetailsActionManager.context)
                          .orderdetailpage
                          .text
                          .totalItem),
                      Text(
                          "x${OrdersUtilsManager.ordersUtilsManagerInstance.showTotalProductQuantityOrdered(productList: _orderDetailsActionManager?.orderDetailsState?.order?.orderDetails).toString()}" ??
                              "")
                    ],
                  ),
                ),
                Divider(height: SIZE_1, color: COLOR_RATING)
              ],
            )
          : const SizedBox(),
    );
  }

  //this method is used to show raise dispute button
  Widget _showRaiseDisputeButton() {
    return Container(
      margin: EdgeInsets.all(SIZE_10),
      child: RaisedButton(
        onPressed: () {
          _orderDetailsActionManager?.actionToRaiseDispute();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SIZE_80),
          side: BorderSide(color: COLOR_DARK_PRIMARY),
        ),
        color: Colors.white,
        padding: EdgeInsets.all(SIZE_0),
        child: Padding(
          padding: const EdgeInsets.only(
              top: SIZE_10, bottom: SIZE_10, left: SIZE_20, right: SIZE_20),
          child: Text(
            AppLocalizations.of(_orderDetailsActionManager.context)
                .orderdetailpage
                .button
                .raiseDispute,
            textAlign: TextAlign.center,
            style: textStyleSize14TabGreenColor,
          ),
        ),
      ),
    );
  }

  Widget _shareFeedback() {
    return Container(
      margin: EdgeInsets.all(SIZE_10),
      child: RaisedButton(
        onPressed: () {
          _orderDetailsActionManager?.actionToRating();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SIZE_80),
          side: BorderSide(color: COLOR_DARK_PRIMARY),
        ),
        color: Colors.white,
        padding: EdgeInsets.all(SIZE_0),
        child: Padding(
          padding: const EdgeInsets.only(
              top: SIZE_10, bottom: SIZE_10, left: SIZE_20, right: SIZE_20),
          child: Text(
            AppLocalizations.of(_orderDetailsActionManager.context)
                .orderdetailpage
                .button
                .shareFeedBack,
            textAlign: TextAlign.center,
            style: textStyleSize14TabGreenColor,
          ),
        ),
      ),
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _orderDetailsActionManager.actionOnInit(
        order: _orderDetailsActionManager?.orderDetailsState?.order ?? _order);
  }

  //show order cancellation view
  Widget _showOrderCancelReason() {
    if ((_orderDetailsActionManager?.orderDetailsState?.order?.status ==
                OrderStatus.cancelledByCustomer.value ||
            _orderDetailsActionManager?.orderDetailsState?.order?.status ==
                OrderStatus.cancelledByDeliveryBoy.value ||
            _orderDetailsActionManager?.orderDetailsState?.order?.status ==
                OrderStatus.orderRejectedByVendor.value) &&
        (_orderDetailsActionManager
                ?.orderDetailsState?.order?.orderRejectReason?.isNotEmpty ==
            true)) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showCommonHeaderView(
              titleLeft: 'Reason of Cancellation', titleRight: ''),
          SizedBox(
            height: SIZE_5,
          ),
          Padding(
            padding: const EdgeInsets.only(left: SIZE_20),
            child: Text(
              '${_orderDetailsActionManager?.orderDetailsState?.order?.orderRejectReason}',
              style: textStyleSize12WithBlackColor,
            ),
          )
        ],
      );
    } else {
      return SizedBox();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/orders/sub_module/order_details/api/model/rating/rating_request_model.dart';


class OrderDetailProvider {
  //method to get order details
  Future<dynamic> getOrderDetailApiCall({
    BuildContext context,
    int id,
  }) async {
    var _orderHistory = "booking/detail";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(_orderHistory, context, queryParameters: {
      "id": id,
    });

    return result;
  }

  //method to get order details
  Future<dynamic> cancelOrderApiCall({BuildContext context, int id,String bookingRejectReason}) async {
    var _orderHistory = "booking/update";

    var result = await AppConfig.of(context).baseApi.patchRequest(
        _orderHistory, context,
        data: {"booking_id": id, "status": 11,"reject_reason":bookingRejectReason});

    return result;
  }

  Future<dynamic> callCustomerRating(
      {BuildContext context, RatingRequestModel ratingRequestModel}) async {
    var _rating = "customer/rating";
    var result = await AppConfig.of(context)
        .baseApi
        .postRequest(_rating, context, data: ratingRequestModel?.toJson());

    return result;
  }

  Future<dynamic> getDeliveryBoyLocation(
      {BuildContext context, int deliveryBoyId}) async {
    var _rating = "getcurrentlocation";
    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(_rating, context, queryParameters: {"user_id": deliveryBoyId});
    return result;
  }
}

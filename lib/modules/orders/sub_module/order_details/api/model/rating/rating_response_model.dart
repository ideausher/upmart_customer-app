// To parse this JSON data, do
//
//     final ratingResponseModel = ratingResponseModelFromJson(jsonString);

import 'dart:convert';

RatingResponseModel ratingResponseModelFromJson(String str) => RatingResponseModel.fromMap(json.decode(str));

String ratingResponseModelToJson(RatingResponseModel data) => json.encode(data.toJson());

class RatingResponseModel {
  RatingResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory RatingResponseModel.fromMap(Map<String, dynamic> json) => RatingResponseModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.reviewForShop,
    this.reviewForDeliveryBoy,
  });

  ReviewModel reviewForShop;
  ReviewModel reviewForDeliveryBoy;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    reviewForShop: json["review_for_shop"] == null ? null : ReviewModel.fromJson(json["review_for_shop"]),
    reviewForDeliveryBoy: json["review_for_delivery_boy"] == null ? null : ReviewModel.fromJson(json["review_for_delivery_boy"]),
  );

  Map<String, dynamic> toJson() => {
    "review_for_shop": reviewForShop == null ? null : reviewForShop.toJson(),
    "review_for_delivery_boy": reviewForDeliveryBoy == null ? null : reviewForDeliveryBoy.toJson(),
  };
}



class ReviewModel {
  ReviewModel({
    this.id,
    this.shopId,
    this.deliveryBoyId,
    this.rating,
    this.feedback,
    this.customerIdBy,
    this.deliveryBoyIdBy,
  });

  var id;
  var shopId;
  var deliveryBoyId;
  var rating;
  var feedback;
  var customerIdBy;
  var deliveryBoyIdBy;

  factory ReviewModel.fromJson(Map<String, dynamic> json) => ReviewModel(
    id: json["id"] == null ? null : json["id"],
    shopId: json["shop_id"] == null ? null : json["shop_id"],
    deliveryBoyId: json["delivery_boy_id"] == null ? null : json["delivery_boy_id"],
    rating: json["rating"] == null ? null : json["rating"],
    feedback: json["feedback"] == null ? null : json["feedback"],
    customerIdBy: json["customer_id_by"] == null ? null : json["customer_id_by"],
    deliveryBoyIdBy: json["delivery_boy_id_by"] == null ? null : json["delivery_boy_id_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "shop_id": shopId == null ? null : shopId,
    "delivery_boy_id": deliveryBoyId == null ? null : deliveryBoyId,
    "rating": rating == null ? null : rating,
    "feedback": feedback == null ? null : feedback,
    "customer_id_by": customerIdBy == null ? null : customerIdBy,
    "delivery_boy_id_by": deliveryBoyIdBy == null ? null : deliveryBoyIdBy,
  };
}

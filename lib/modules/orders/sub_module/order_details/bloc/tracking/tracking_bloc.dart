import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/model/delivery_boy_location/get_delivery_boy_location_response_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/tracking/tracking_event.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/tracking/tracking_state.dart';

class TrackingBloc extends BlocEventStateBase<TrackingEvent, TackingState> {
  TrackingBloc({bool initializing = true})
      : super(initialState: TackingState.initiating());

  @override
  Stream<TackingState> eventHandler(
      TrackingEvent event, TackingState currentState) async* {
    //api calling on init state
    if (event is GetOrderDetailsAndDeliveryBoyLocationEvent) {
      String _message = "";
      Order _order = event?.order;
      GetDeliveryBoyLocationResponseModel _deliveryBoyLocationResponseModel =
          event?.getDeliveryBoyLocationResponseModel;

      yield TackingState.updateTrackingState(
          isLoading: true,
          context: event?.context,
          order: event?.order,
          getDeliveryLocationResponseModel:
              event?.getDeliveryBoyLocationResponseModel);

      //here calling order details api
      var _result = await OrderDetailProvider().getOrderDetailApiCall(
        context: event.context,
        id: event.order?.id,
      );

      if (_result != null) {
        // check result status
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _order = Order.fromMap(_result["data"]);

        }
      }

      if (_order?.deliveryBoyId != null && _order?.deliveryBoyId > 0) {
        //here calling delivery boy location api to get his lat lng
        var _locationResult = await OrderDetailProvider()
            .getDeliveryBoyLocation(
                context: event.context, deliveryBoyId: _order?.deliveryBoyId);

        if (_locationResult != null) {
          // check result status
          if (_locationResult[ApiStatusParams.Status.value] != null &&
              _locationResult[ApiStatusParams.Status.value] ==
                  ApiStatus.Success.value) {
            _deliveryBoyLocationResponseModel =
                GetDeliveryBoyLocationResponseModel.fromMap(
                    _locationResult);
          }
        }
      }
      yield TackingState.updateTrackingState(
          isLoading: false,
          context: event?.context,
          order: _order,
          message: _message,
          getDeliveryLocationResponseModel:_deliveryBoyLocationResponseModel);
    }

    //event to get delivery boy location event
    if (event is GetDeliveryBoyLocationEvent) {
      String _message = "";
      Order _order = event?.order;
      GetDeliveryBoyLocationResponseModel _deliveryBoyLocationResponseModel =
          event?.getDeliveryBoyLocationResponseModel;

      yield TackingState.updateTrackingState(
          isLoading: true,
          context: event?.context,
          order: event?.order,
          getDeliveryLocationResponseModel:
              event?.getDeliveryBoyLocationResponseModel);

      //here calling delivery boy location api to get his lat lng
      var _locationResult = await OrderDetailProvider().getDeliveryBoyLocation(
          context: event.context, deliveryBoyId: _order?.deliveryBoyId);

      if (_locationResult != null) {
        // check result status
        if (_locationResult[ApiStatusParams.Status.value] != null &&
            _locationResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          _deliveryBoyLocationResponseModel =
              GetDeliveryBoyLocationResponseModel.fromMap(
                  _locationResult);
        }

      }

      yield TackingState.updateTrackingState(
          isLoading: false,
          context: event?.context,
          order: _order,
          message: _message,
          getDeliveryLocationResponseModel: _deliveryBoyLocationResponseModel);
    }
  }
}

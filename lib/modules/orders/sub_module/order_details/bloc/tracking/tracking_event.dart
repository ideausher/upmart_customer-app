import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/model/delivery_boy_location/get_delivery_boy_location_response_model.dart';

abstract class TrackingEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final Order order;
  final GetDeliveryBoyLocationResponseModel getDeliveryBoyLocationResponseModel;

  TrackingEvent(
      {this.isLoading: false,
      this.context,
      this.order,
      this.getDeliveryBoyLocationResponseModel});
}

//this event is used for getting order details and delivery boy location event
class GetOrderDetailsAndDeliveryBoyLocationEvent extends TrackingEvent {
  GetOrderDetailsAndDeliveryBoyLocationEvent(
      {bool isLoading,
      BuildContext context,
      Order order,
      GetDeliveryBoyLocationResponseModel getDeliveryBoyLocationResponseModel})
      : super(
            isLoading: isLoading,
            context: context,
            order: order,
            getDeliveryBoyLocationResponseModel:
                getDeliveryBoyLocationResponseModel);
}

//this event is used to get delivery boy location
class GetDeliveryBoyLocationEvent extends TrackingEvent {
  GetDeliveryBoyLocationEvent(
      {bool isLoading,
      BuildContext context,
      Order order,
      GetDeliveryBoyLocationResponseModel getDeliveryBoyLocationResponseModel})
      : super(
            isLoading: isLoading,
            context: context,
            order: order,
            getDeliveryBoyLocationResponseModel:
                getDeliveryBoyLocationResponseModel);
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

class OrderDetailsState extends BlocState {
  OrderDetailsState({
    this.isLoading: false,
    this.message,
    this.context,
    this.order,
  }) : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final Order order;


  // used for update order details page state
  factory OrderDetailsState.updateOrderDetails({
    bool isLoading,
    String message,
    BuildContext context,
    Order order,
    int status,
  }) {
    return OrderDetailsState(
      isLoading: isLoading,
      message: message,
      context: context,
      order: order,
    );
  }

  factory OrderDetailsState.initiating() {
    return OrderDetailsState(
      isLoading: false,
    );
  }
}

import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';

import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';
import 'oder_details_state.dart';
import 'order_details_event.dart';


class OrderDetailsBloc
    extends BlocEventStateBase<OrderDetailsEvent, OrderDetailsState> {
  OrderDetailsBloc({bool initializing = true})
      : super(initialState: OrderDetailsState.initiating());

  @override
  Stream<OrderDetailsState> eventHandler(OrderDetailsEvent event,
      OrderDetailsState currentState) async* {
    //update home page UI
    if (event is GetOrderDetailsEvent) {
      yield OrderDetailsState.updateOrderDetails(
        isLoading: true,
        context: event?.context,
        order: event?.order,
      );

      var _result = await OrderDetailProvider().getOrderDetailApiCall(
        context: event.context,
        id: event.order?.id,
      );

      if (_result != null) {
        // check result status
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          Order _order = Order.fromMap(_result["data"]);
          yield OrderDetailsState.updateOrderDetails(
            isLoading: false,
            context: event?.context,
            order: _order,
          );
        }
        // failure case
        else {
          yield OrderDetailsState.updateOrderDetails(
            isLoading: false,
            context: event?.context,
            order: event?.order,
            message: _result[ApiStatusParams.Message.value],
          );
        }
      } else {
        yield OrderDetailsState.updateOrderDetails(
          isLoading: false,
          context: event?.context,
          order: event?.order,
          message: AppLocalizations
              .of(event?.context)
              .common
              .error
              .somethingWentWrong,
        );
      }
    }

    //cancel order api
    if (event is CancelOrderEvent) {
      yield OrderDetailsState.updateOrderDetails(
        isLoading: true,
        context: event?.context,
        order: event?.order,
      );

      var _result = await OrderDetailProvider().cancelOrderApiCall(
        context: event.context,
        id: event.order?.id,
        bookingRejectReason:event?.cancelReason

      );

      if (_result != null) {
        // check result status
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          Order _order = Order.fromMap(_result["data"]);
          yield OrderDetailsState.updateOrderDetails(
            isLoading: false,
            context: event?.context,
            order: _order,
          );
        }
        // failure case
        else {
          yield OrderDetailsState.updateOrderDetails(
            isLoading: false,
            context: event?.context,
            order: event?.order,
            message: _result[ApiStatusParams.Message.value],
          );
        }
      } else {
        yield OrderDetailsState.updateOrderDetails(
          isLoading: false,
          context: event?.context,
          order: event?.order,
          message: AppLocalizations
              .of(event?.context)
              .common
              .error
              .somethingWentWrong,
        );
      }
    }

    // update ui

    if (event is UpdateOrderDetailEvent) {
      yield OrderDetailsState.updateOrderDetails(
        isLoading: event?.isLoading,
        context: event?.context,
        order: event?.order,
      );
    }
  }
}

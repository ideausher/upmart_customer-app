import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/auth/validator/auth_validator.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/constants/font_constant.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';

import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/manager/orders_widget_manager.dart';
import 'package:user/modules/orders/order_routes.dart';
import 'package:user/modules/orders/sub_module/order_details/api/model/rating/rating_request_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/order_details/oder_details_state.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/order_details/order_details_bloc.dart';
import 'package:user/modules/orders/sub_module/order_details/bloc/order_details/order_details_event.dart';

import '../../../../../../localizations.dart';
import '../../../../../../routes.dart';
import 'order_details_widget_manager.dart';

class OrderDetailsActionManager {
  BuildContext context;
  OrderDetailsBloc orderDetailsBloc;
  OrderDetailsState orderDetailsState;

  //used to perform the action on the init
  actionOnInit({Order order}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          orderDetailsBloc.emitEvent(GetOrderDetailsEvent(
              isLoading: true, context: context, order: order));
        }
      },
    );
  }


  actionOnCancelOrder(
      {ScaffoldState scaffoldState,
      TextEditingController cancelOrderController}) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showCancelAlertDialog(
        context: orderDetailsState.context,
        title: 'Cancel Order ',
        negativeButton: 'Confirm',
        positiveButton: 'Cancel',
        buttonTextStyle: textStyleSize14WithRedColor,
        titleTextStyle: textStyleSize14WithRedColor,
        positiveButtonTextStyle: TextStyle(
            color: Color(0xFF34D670),
            fontSize: SIZE_14,
            fontWeight: FontWeight.w600,
            fontFamily: FONT_FAMILY_INTER),
        subTitle: "",
        showWidget: true,
        cancelBookingController: cancelOrderController,
        onNegativeButtonTab: () {
          // check name field is empty or not
          String result = AuthValidator.authValidatorInstance
              .validateCancelOrder(cancelOrderController?.text);
          // if its not empty
          if (result.isEmpty) {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(orderDetailsState.context);
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: true)
                .then(
              (onValue) {
                if (onValue) {
                  orderDetailsBloc.emitEvent(CancelOrderEvent(
                      isLoading: true,
                      context: context,
                      cancelReason: cancelOrderController?.text,
                      order: orderDetailsState?.order));
                }
              },
            );
          } else {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(orderDetailsState.context);
            // if error
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: orderDetailsState.context,
                scaffoldState: scaffoldState,
                message: result);
          }
        },
        onPositiveButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(orderDetailsState.context);
        });
  }

  /* actionOnCancelOrder({ScaffoldState scaffoldState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          orderDetailsBloc.emitEvent(CancelOrderEvent(
              isLoading: true,
              context: context,
              order: orderDetailsState?.order));
        }
      },
    );
  }*/

  //action on products list state change
  actionOnOrderDetailsStateChange({
    ScaffoldState scaffoldState,
  }) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) {
        if (orderDetailsState?.isLoading == false) {
          //error case
          if (orderDetailsState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: orderDetailsState?.message);
            print("order details==> ${orderDetailsState?.message}");
          }
        }
      },
    );
  }

  // action to give rating

  actionToRating() {
    OrderDetailsWidgetManager.orderDetailsWidgetManager.showRatingAlertDialog(
        context: context,
        order: orderDetailsState?.order,
        driverFeedBack: new TextEditingController(),
        shopFeedBack: new TextEditingController(),
        orderDetailsActionManager: this);
  }

  // action to raise dispute

  actionToRaiseDispute() {
    NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
        context, Routes.QUERY_LISTING_PAGE,
        dataToBeSend: orderDetailsState?.order);
  }

  // update ui
  actionToUpdateUi({bool loading}) {
    orderDetailsBloc.emitEvent(UpdateOrderDetailEvent(
        isLoading: loading, context: context, order: orderDetailsState?.order));
  }

  // action to rate
  actionToRate({RatingRequestModel requestModel}) async {
    actionToUpdateUi(loading: true);

    var _result = await OrderDetailProvider()
        .callCustomerRating(context: context, ratingRequestModel: requestModel);

    if (_result != null) {
      // check result status
      if (_result[ApiStatusParams.Status.value] != null &&
          _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
        actionOnInit(order: orderDetailsState?.order);
      }
      // failure case
      else {
        actionToUpdateUi(loading: false);
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
            context: context,
            title: '',
            positiveButton: AppLocalizations.of(context).common.text.ok,
            titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
            buttonTextStyle:
                AppConfig.of(context).themeData.textTheme.headline4,
            subTitle: _result[ApiStatusParams.Message.value],
            onPositiveButtonTab: () async {
              NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            });
      }
    } else {
      actionToUpdateUi(loading: false);
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
          context: context,
          title: '',
          positiveButton: AppLocalizations.of(context).common.text.ok,
          titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
          buttonTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
          subTitle:
              AppLocalizations.of(context).common.error.somethingWentWrong,
          onPositiveButtonTab: () async {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          });
    }
  }

  // action on click of track
  actionToTrack() async {
    await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
        context, OrderRoutes.TRACKING_PAGE,
        dataToBeSend: orderDetailsState?.order);
    actionOnInit(order: orderDetailsState?.order);
  }
}

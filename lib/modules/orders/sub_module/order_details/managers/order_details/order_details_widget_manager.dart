import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/model/rating/rating_request_model.dart';
import '../../../../../../localizations.dart';
import 'order_details_action_manager.dart';

class OrderDetailsWidgetManager {
  static OrderDetailsWidgetManager _orderDetailsWidgetManager =
      OrderDetailsWidgetManager();

  static OrderDetailsWidgetManager get orderDetailsWidgetManager =>
      _orderDetailsWidgetManager;

  //method to show rating alert dialog
  Widget showRatingAlertDialog(
      {BuildContext context,
      Order order,
      OrderDetailsActionManager orderDetailsActionManager,
      TextEditingController driverFeedBack,
      TextEditingController shopFeedBack}) {
    num _shopRating = 0.0;
    num _driverRating = 0.0;

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              contentPadding: EdgeInsets.only(
                  left: SIZE_20, right: SIZE_20, bottom: SIZE_20, top: SIZE_10),
              titlePadding: EdgeInsets.all(SIZE_0),
              content: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      AppLocalizations.of(context).ratingalert.title.giveRating,
                      style: textStyleSize16WithGreenColor,
                    ),
                    SizedBox(
                      height: SIZE_10,
                    ),
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context).feedbackalert.text.shop,
                          style: textStyleSize14BlackColor,
                        ),
                        SizedBox(width: SIZE_10),
                        RatingBar.builder(
                          initialRating: 0,
                          minRating: 0,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemSize: SIZE_20,
                          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {
                            _shopRating = rating;
                          },
                        )
                      ],
                    ),
                    Material(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(SIZE_5),
                      child: Container(
                        padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
                        margin: EdgeInsets.only(top: SIZE_10),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            border: Border.all(
                                color: COLOR_BORDER_GREY, width: SIZE_1),
                            borderRadius: BorderRadius.circular(SIZE_10)),
                        child: TextFormField(
                          style: textStyleSize12WithBlackColor,
                          controller: shopFeedBack,
                          enabled: true,
                          maxLines: 4,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: AppLocalizations.of(context)
                                .ratingalert
                                .hint
                                .comment,
                            hintStyle: textStyleSize14GreyColorFont500,
                            contentPadding: EdgeInsets.all(SIZE_5),
                            border:
                                OutlineInputBorder(borderSide: BorderSide.none),
                          ),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: (order?.deliveryBoyDetails != null &&
                          order?.deliveryBoyDetails?.phoneNumber?.isNotEmpty ==
                              true),
                      child: SizedBox(
                        height: SIZE_10,
                      ),
                    ),
                    Visibility(
                      visible: (order?.deliveryBoyDetails != null &&
                          order?.deliveryBoyDetails?.phoneNumber?.isNotEmpty ==
                              true),
                      child: Row(
                        children: [
                          Text(
                              AppLocalizations.of(context)
                                  .feedbackalert
                                  .text
                                  .driver,
                              style: textStyleSize14BlackColor),
                          SizedBox(width: SIZE_10),
                          RatingBar.builder(
                            initialRating: 0,
                            minRating: 0,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: SIZE_20,
                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              _driverRating = rating;
                            },
                          )
                        ],
                      ),
                    ),
                    Visibility(
                      visible: (order?.deliveryBoyDetails != null &&
                          order?.deliveryBoyDetails?.phoneNumber?.isNotEmpty ==
                              true),
                      child: Material(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(SIZE_5),
                        child: Container(
                          padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
                          margin: EdgeInsets.only(top: SIZE_10),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(
                                  color: COLOR_BORDER_GREY, width: SIZE_1),
                              borderRadius: BorderRadius.circular(SIZE_10)),
                          child: TextFormField(
                            style: textStyleSize12WithBlackColor,
                            controller: driverFeedBack,
                            enabled: true,
                            maxLines: 4,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .ratingalert
                                  .hint
                                  .comment,
                              hintStyle: textStyleSize14GreyColorFont500,
                              contentPadding: EdgeInsets.all(SIZE_5),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none),
                            ),
                          ),
                        ),
                      ),
                    ),
                    CupertinoDialogAction(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: context,
                                    percentage: SIZE_7,
                                    ofWidth: false),
                            width: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: context,
                                    percentage: SIZE_50,
                                    ofWidth: true),
                            child: RaisedButton(
                              onPressed: () {
                                NavigatorUtils.navigatorUtilsInstance
                                    .navigatorPopScreen(context);
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(SIZE_80),
                                  side: BorderSide(color: COLOR_DARK_PRIMARY)),
                              padding: EdgeInsets.all(SIZE_0),
                              child: Ink(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.circular(SIZE_30)),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    AppLocalizations.of(context).feedbackalert.button.skip,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight:
                                        textStyleSizeBlackColor
                                            .fontWeight,
                                        fontSize: textStyleSizeBlackColor
                                            .fontSize,
                                        color: COLOR_PRIMARY),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: context,
                                    percentage: SIZE_7,
                                    ofWidth: false),
                            width: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: context,
                                    percentage: SIZE_50,
                                    ofWidth: true),
                            child: RaisedButton(
                              onPressed: () {
                                List<Rating> _ratings = new List<Rating>();
                                _ratings.add(Rating(
                                    shopId: order?.shopDetails?.id,
                                    feedback: shopFeedBack?.text,
                                    rating: _shopRating));
                                if (order?.deliveryBoyDetails != null &&
                                    order?.deliveryBoyDetails?.phoneNumber
                                            ?.isNotEmpty ==
                                        true) {
                                  _ratings.add(Rating(
                                      deliveryBoyId: order?.deliveryBoyId ?? 0,
                                      feedback: driverFeedBack?.text,
                                      rating: _shopRating));
                                }

                                orderDetailsActionManager?.actionToRate(
                                    requestModel: RatingRequestModel(
                                        ratings: _ratings,
                                        bookingId: order?.id));
                                NavigatorUtils.navigatorUtilsInstance
                                    .navigatorPopScreen(context);
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(SIZE_80)),
                              padding: EdgeInsets.all(SIZE_0),
                              child: Ink(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        COLOR_LIGHT_GREEN,
                                        COLOR_DARK_PRIMARY,
                                      ],
                                    ),
                                    borderRadius:
                                        BorderRadius.circular(SIZE_30)),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .common
                                        .button
                                        .submit,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight:
                                        textStyleSizeBlackColor
                                                .fontWeight,
                                        fontSize: textStyleSizeBlackColor
                                            .fontSize,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      onPressed: () {},
                    )
                  ],
                ),
              ),
            );
          });
        }).then((val) {});
    ;
  }
}

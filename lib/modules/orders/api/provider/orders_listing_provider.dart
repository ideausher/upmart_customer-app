import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/orders/enum/order_enum.dart';

class OrdersListingProvider {
  //method to get customer order history
  Future<dynamic> getOrdersHistoryApiCall({
    BuildContext context,
    int page,
    bool isOngoing = false,
  }) async {
    var _orderHistory = "booking/customer";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(_orderHistory, context, queryParameters: {
      "page": page,
      "status": (isOngoing == true) ? [1,4,5] : [2,3],
      "limit": 10,
    });

    return result;
  }
}

// status : 1 => ORDER_ACCEPTED, 2 => ORDER_REJECTED, 3 => ORDER_COMPLETED, 4 => ORDER_INPROGRESS, 5 => PENDING_ORDERS

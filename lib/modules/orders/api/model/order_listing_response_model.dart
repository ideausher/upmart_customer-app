// To parse this JSON data, do
//
//     final orderListingResponseModel = orderListingResponseModelFromMap(jsonString);
import 'dart:convert';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/orders/sub_module/order_details/api/model/rating/rating_response_model.dart';

OrderListingResponseModel orderListingResponseModelFromMap(String str) =>
    OrderListingResponseModel.fromMap(json.decode(str));

String orderListingResponseModelToMap(OrderListingResponseModel data) =>
    json.encode(data.toMap());

class OrderListingResponseModel {
  OrderListingResponseModel({
    this.orders,
    this.message,
    this.statusCode,
  });

  List<Order> orders;
  String message;
  int statusCode;

  factory OrderListingResponseModel.fromMap(Map<String, dynamic> json) =>
      OrderListingResponseModel(
        orders: json["data"] == null
            ? null
            : List<Order>.from(json["data"].map((x) => Order.fromMap(x))),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toMap() => {
        "data": orders == null
            ? null
            : List<dynamic>.from(orders.map((x) => x.toMap())),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Order {
  Order(
      {this.id,
      this.bookingCode,
      this.deliveryBoyId,
      this.orderRejectReason,
      this.bookingType,
      this.bookingDateTime,
      this.amountBeforeDiscount,
      this.discountAmount,
      this.amountAfterDiscount,
      this.deliveryChargeToDeliveryBoy,
      this.deliveryChargeForCustomer,
      this.commissionCharge,
      this.platformCharge,
      this.afterChargeAmount,
      this.stripeCharges,
      this.amountAfterStripeCharges,
      this.tipToDeliveryBoy,
      this.instruction,
      this.status,
      this.orderDetails,
      this.userDetails,
      this.shopDetails,
      this.couponDetails,
      this.deliveryAddress,
      this.deliveryBoyDetails,
      this.bookingStatus,
      this.createdAt,
      this.ratings,
      this.gst,
      this.hst,
      this.pst,
      this.amountPaidToDeliveryBoyOnOrderDelivery,
      this.amountPaidToDeliveryBoyOnOrderPickUp});

  int id;
  String bookingCode;
  int deliveryBoyId;
  int bookingType;
  String bookingDateTime;
  num amountBeforeDiscount;
  num discountAmount;
  num amountAfterDiscount;
  num deliveryChargeForCustomer;
  num deliveryChargeToDeliveryBoy;
  num commissionCharge;
  num platformCharge;
  num afterChargeAmount;
  num stripeCharges;
  num amountAfterStripeCharges;
  num tipToDeliveryBoy;
  String instruction;
  int status;
  List<OrderDetail> orderDetails;
  UserDetails userDetails;
  ShopDetailsModel shopDetails;
  CouponData couponDetails;
  DeliveryAddress deliveryAddress;
  DeliveryBoy deliveryBoyDetails;
  Map<String, BookingStatus> bookingStatus;
  String createdAt;
  List<ReviewModel> ratings;
  num pst;
  num hst;
  num gst;
  num amountPaidToDeliveryBoyOnOrderPickUp;
  num amountPaidToDeliveryBoyOnOrderDelivery;
  String orderRejectReason;

  factory Order.fromMap(Map<String, dynamic> json) => Order(
        id: json["id"] == null ? null : json["id"],
        pst: json["pst_tax"] == null ? null : json["pst_tax"],
        hst: json["hst_tax"] == null ? null : json["hst_tax"],
        gst: json["gst_tax"] == null ? null : json["gst_tax"],
        orderRejectReason:
            json["reject_reason"] == null ? null : json["reject_reason"],
        amountPaidToDeliveryBoyOnOrderPickUp:
            json["amount_paid_to_delivery_boy_on_order_pickup"] == null
                ? null
                : json["amount_paid_to_delivery_boy_on_order_pickup"],
        amountPaidToDeliveryBoyOnOrderDelivery:
            json["amount_paid_to_delivery_boy_on_order_delivery"] == null
                ? null
                : json["gsamount_paid_to_delivery_boy_on_order_deliveryt_tax"],
        bookingCode: json["booking_code"] == null ? null : json["booking_code"],
        deliveryBoyId:
            json["delivery_boy_id"] == null ? null : json["delivery_boy_id"],
        bookingType: json["booking_type"] == null ? null : json["booking_type"],
        bookingDateTime: json["booking_date_time"] == null
            ? null
            : json["booking_date_time"],
        amountBeforeDiscount: json["amount_before_discount"] == null
            ? null
            : json["amount_before_discount"],
        discountAmount:
            json["discount_amount"] == null ? null : json["discount_amount"],
        amountAfterDiscount: json["amount_after_discount"] == null
            ? null
            : json["amount_after_discount"],
        deliveryChargeToDeliveryBoy:
            json["delivery_charge_to_delivery_boy"] == null
                ? null
                : json["delivery_charge_to_delivery_boy"],
        deliveryChargeForCustomer: json["delivery_charge_for_customer"] == null
            ? null
            : json["delivery_charge_for_customer"],
        commissionCharge: json["commission_charge"] == null
            ? null
            : json["commission_charge"],
        platformCharge:
            json["platform_charge"] == null ? null : json["platform_charge"],
        afterChargeAmount: json["after_charge_amount"] == null
            ? null
            : json["after_charge_amount"],
        stripeCharges: json["stripe_charges"] == null
            ? null
            : json["stripe_charges"].toDouble(),
        amountAfterStripeCharges: json["amount_after_stripe_charges"] == null
            ? null
            : json["amount_after_stripe_charges"],
        tipToDeliveryBoy: json["tip_to_delivery_boy"] == null
            ? null
            : json["tip_to_delivery_boy"],
        instruction: json["instruction"] == null ? null : json["instruction"],
        status: json["status"] == null ? null : json["status"],
        orderDetails: json["order_details"] == null
            ? null
            : List<OrderDetail>.from(
                json["order_details"].map((x) => OrderDetail.fromMap(x))),
        userDetails: json["user_details"] == null
            ? null
            : UserDetails.fromMap(json["user_details"]),
        shopDetails: json["shop_details"] == null
            ? null
            : ShopDetailsModel.fromJson(json["shop_details"]),
        couponDetails: json["coupon_details"] == null
            ? null
            : CouponData.fromJson(json["coupon_details"]),
        deliveryAddress: json["delivery_address"] == null
            ? null
            : DeliveryAddress.fromMap(json["delivery_address"]),
        deliveryBoyDetails: json["delivery_boy_details"] == null
            ? null
            : DeliveryBoy.fromJson(json["delivery_boy_details"]),
        bookingStatus: json["booking_status"] == null
            ? null
            : Map.from(json["booking_status"]).map((k, v) =>
                MapEntry<String, BookingStatus>(k, BookingStatus.fromMap(v))),
        createdAt: json["created_at"] == null ? null : json["created_at"],
        ratings: json["ratings"] == null
            ? null
            : List<ReviewModel>.from(
                json["ratings"].map((x) => ReviewModel.fromJson(x))),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "pst_tax": pst == null ? null : pst,
        "hst_tax": hst == null ? null : hst,
        "gst_tax": gst == null ? null : gst,
        "reject_reason": orderRejectReason == null ? null : orderRejectReason,
        "amount_paid_to_delivery_boy_on_order_pickup":
            amountPaidToDeliveryBoyOnOrderPickUp == null
                ? null
                : amountPaidToDeliveryBoyOnOrderPickUp,
        "amount_paid_to_delivery_boy_on_order_delivery":
            amountPaidToDeliveryBoyOnOrderDelivery == null
                ? null
                : amountPaidToDeliveryBoyOnOrderDelivery,
        "booking_code": bookingCode == null ? null : bookingCode,
        "delivery_boy_id": deliveryBoyId == null ? null : deliveryBoyId,
        "booking_type": bookingType == null ? null : bookingType,
        "booking_date_time": bookingDateTime == null ? null : bookingDateTime,
        "amount_before_discount":
            amountBeforeDiscount == null ? null : amountBeforeDiscount,
        "discount_amount": discountAmount == null ? null : discountAmount,
        "amount_after_discount":
            amountAfterDiscount == null ? null : amountAfterDiscount,
        "delivery_charge_to_delivery_boy": deliveryChargeToDeliveryBoy == null
            ? null
            : deliveryChargeToDeliveryBoy,
        "delivery_charge_for_customer": deliveryChargeForCustomer == null
            ? null
            : deliveryChargeForCustomer,
        "commission_charge": commissionCharge == null ? null : commissionCharge,
        "platform_charge": platformCharge == null ? null : platformCharge,
        "after_charge_amount":
            afterChargeAmount == null ? null : afterChargeAmount,
        "stripe_charges": stripeCharges == null ? null : stripeCharges,
        "amount_after_stripe_charges":
            amountAfterStripeCharges == null ? null : amountAfterStripeCharges,
        "tip_to_delivery_boy":
            tipToDeliveryBoy == null ? null : tipToDeliveryBoy,
        "instruction": instruction == null ? null : instruction,
        "status": status == null ? null : status,
        "order_details": orderDetails == null
            ? null
            : List<dynamic>.from(orderDetails.map((x) => x.toMap())),
        "user_details": userDetails == null ? null : userDetails.toMap(),
        "shop_details": shopDetails == null ? null : shopDetails.toJson(),
        "coupon_details": couponDetails == null ? null : couponDetails.toJson(),
        "delivery_address":
            deliveryAddress == null ? null : deliveryAddress.toMap(),
        "delivery_boy_details":
            deliveryBoyDetails == null ? null : deliveryBoyDetails.toJson(),
        "booking_status": bookingStatus == null
            ? null
            : Map.from(bookingStatus)
                .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
        "created_at": createdAt == null ? null : createdAt,
        "ratings": ratings == null
            ? null
            : List<dynamic>.from(ratings.map((x) => x.toJson())),
      };
}

class BookingStatus {
  BookingStatus(
      {this.id,
      this.bookingId,
      this.reason,
      this.value,
      this.statusValue,
      this.actionTime,
      this.image});

  int id;
  int bookingId;
  String reason;
  String value;
  int statusValue;
  String actionTime;
  String image;

  factory BookingStatus.fromMap(Map<String, dynamic> json) => BookingStatus(
        id: json["id"] == null ? null : json["id"],
        bookingId: json["bookingId"] == null ? null : json["bookingId"],
        reason: json["reason"] == null ? null : json["reason"],
        value: json["value"] == null ? null : json["value"],
        statusValue: json["status_value"] == null ? null : json["status_value"],
        actionTime: json["action_time"] == null
            ? null
            : DateUtils.dateUtilsInstance.getDateAndTimeFormat(
                dateTime: json["action_time"], toLocal: true),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "bookingId": bookingId == null ? null : bookingId,
        "reason": reason == null ? null : reason,
        "value": value == null ? null : value,
        "status_value": statusValue == null ? null : statusValue,
        "action_time": actionTime == null ? null : actionTime,
      };
}

class DeliveryAddress {
  DeliveryAddress({
    this.id,
    this.userId,
    this.city,
    this.country,
    this.formattedAddress,
    this.additionalInfo,
    this.pincode,
    this.latitude,
    this.longitude,
  });

  int id;
  int userId;
  String city;
  String country;
  String formattedAddress;
  String additionalInfo;
  String pincode;
  num latitude;
  num longitude;

  factory DeliveryAddress.fromMap(Map<String, dynamic> json) => DeliveryAddress(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        city: json["city"] == null ? null : json["city"],
        country: json["country"] == null ? null : json["country"],
        formattedAddress: json["formatted_address"] == null
            ? null
            : json["formatted_address"],
        additionalInfo:
            json["additional_info"] == null ? null : json["additional_info"],
        pincode: json["pincode"] == null ? null : json["pincode"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "city": city == null ? null : city,
        "country": country == null ? null : country,
        "formatted_address": formattedAddress == null ? null : formattedAddress,
        "additional_info": additionalInfo == null ? null : additionalInfo,
        "pincode": pincode == null ? null : pincode,
        "longitude": longitude == null ? null : longitude,
        "latitude": latitude == null ? null : latitude,
      };
}

class OrderDetail {
  OrderDetail(
      {this.id,
      this.bookingId,
      this.productName,
      this.productDescription,
      this.price,
      this.discount,
      this.quantity,
      this.newPrice,
      this.productImage,
      this.productId});

  int id;
  int productId;
  int bookingId;
  String productName;
  String productDescription;
  num price;
  num discount;
  num newPrice;
  num quantity;
  ProductImage productImage;

  factory OrderDetail.fromMap(Map<String, dynamic> json) => OrderDetail(
        id: json["id"] == null ? null : json["id"],
        bookingId: json["bookingId"] == null ? null : json["bookingId"],
        productId: json["productId"] == null ? null : json["productId"],
        productName: json["productName"] == null ? null : json["productName"],
        productDescription: json["productDescription"] == null
            ? null
            : json["productDescription"],
        price: json["price"] == null ? null : json["price"],
        discount: json["discount"] == null ? null : json["discount"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        newPrice: json["new_price"] == null ? null : json["new_price"],
        productImage: json["product_image"] == null
            ? null
            : ProductImage.fromJson(json['product_image']),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "bookingId": bookingId == null ? null : bookingId,
        "productId": bookingId == null ? null : bookingId,
        "productName": productName == null ? null : productName,
        "productDescription":
            productDescription == null ? null : productDescription,
        "price": price == null ? null : price,
        "discount": discount == null ? null : discount,
        "quantity": quantity == null ? null : quantity,
        "new_price": newPrice == null ? null : newPrice,
        "product_image": productImage == null ? null : productImage.toJson(),
      };
}

class UserDetails {
  UserDetails({
    this.id,
    this.name,
    this.email,
    this.countryCode,
    this.phoneNumber,
    this.profilePicture,
    this.resourceUrl,
  });

  int id;
  String name;
  dynamic email;
  String countryCode;
  String phoneNumber;
  dynamic profilePicture;
  String resourceUrl;

  factory UserDetails.fromMap(Map<String, dynamic> json) => UserDetails(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        profilePicture: json["profile_picture"],
        resourceUrl: json["resource_url"] == null ? null : json["resource_url"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "email": email,
        "country_code": countryCode == null ? null : countryCode,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "profile_picture": profilePicture,
        "resource_url": resourceUrl == null ? null : resourceUrl,
      };
}

class DeliveryBoy {
  DeliveryBoy({
    this.id,
    this.name,
    this.phoneNumber,
  });

  int id;
  String name;
  String phoneNumber;

  factory DeliveryBoy.fromJson(Map<String, dynamic> json) => DeliveryBoy(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "phone_number": phoneNumber == null ? null : phoneNumber,
      };
}

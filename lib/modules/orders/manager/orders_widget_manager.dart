import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/enum/order_enum.dart';
import 'package:user/modules/orders/sub_module/order_details/managers/order_details/order_details_action_manager.dart';

class OrdersWidgetManager {
  static OrdersWidgetManager _ordersWidgetManager = OrdersWidgetManager();

  static OrdersWidgetManager get ordersWidgetManagerInstance =>
      _ordersWidgetManager;

  //method to return a linear step bar which will show progress of a order
  Widget showOrderStepBar({BuildContext context}) {
    return Center(
      child: Container(
        constraints: const BoxConstraints(maxHeight: 100),
        color: Colors.white,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            TimelineTile(
              axis: TimelineAxis.horizontal,
              alignment: TimelineAlign.manual,
              lineXY: 0.7,
              isFirst: true,
              startChild: Container(
                constraints: const BoxConstraints(
                  minWidth: 120,
                ),
                color: Colors.amberAccent,
              ),
            ),
            TimelineTile(
              axis: TimelineAxis.horizontal,
              alignment: TimelineAlign.manual,
              lineXY: 0.7,
              hasIndicator: false,
              startChild: Container(
                width: 50,
                color: Colors.purple,
              ),
              endChild: Container(
                color: Colors.cyan,
              ),
            ),
            TimelineTile(
              axis: TimelineAxis.horizontal,
              alignment: TimelineAlign.manual,
              lineXY: 0.7,
              isLast: true,
              endChild: Container(
                constraints: const BoxConstraints(
                  minWidth: 80,
                ),
                color: Colors.lightGreenAccent,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DeliveryTimeline extends StatefulWidget {
  OrderDetailsActionManager actionManager;
  BuildContext context;

  DeliveryTimeline({this.actionManager, this.context});

  @override
  _DeliveryTimelineState createState() => _DeliveryTimelineState();
}

class _DeliveryTimelineState extends State<DeliveryTimeline> {
  ScrollController _scrollController;

  List<BookingStatus> _bookingStatus = List<BookingStatus>();
  int _currentStep = 3;

  @override
  void initState() {


    _scrollController = ScrollController();
    super.initState();
  }

  _createSteps(){
    _bookingStatus?.clear();
    print("bookingstatus chirag ${widget.actionManager.orderDetailsState.order.bookingStatus}");
    if (widget.actionManager.orderDetailsState.order.bookingStatus
        .containsKey(OrderStatus.autocancel.value?.toString())) {
      //auto cancel
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));

      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.orderAcceptedByVendor.value,
            value: AppLocalizations
                .of(widget.context)
                .common
                .text
                .orderConfirmed,
            statusValue: widget.actionManager.orderDetailsState.order
                .bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? 1
                : 0,
            actionTime: widget.actionManager.orderDetailsState.order
                .bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? widget.actionManager.orderDetailsState.order
                .bookingStatus[
            OrderStatus.orderAcceptedByVendor.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget.context)
                .common
                .text
                .notAllowed));
        _currentStep = 2;
      } else {
        _currentStep = 3;
      }

      // auto cancel
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.autocancel.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.autocancel.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.autocancel.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
    } else if (widget.actionManager.orderDetailsState.order.bookingStatus
        .containsKey(OrderStatus.orderRejectedByVendor.value?.toString())) {
      _currentStep = 1;
      // rejected by vendor
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderRejectedByVendor.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .orderRejected,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus.containsKey(
              OrderStatus.orderRejectedByVendor.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(
              OrderStatus.orderRejectedByVendor.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[
          OrderStatus.orderRejectedByVendor.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.orderRejectedByVendor.value?.toString()))
        _currentStep = 2;
    } else if (widget.actionManager.orderDetailsState.order.bookingStatus
        .containsKey(OrderStatus.cancelledByDeliveryBoy.value?.toString())) {
      _currentStep = 1;

      // rejected by delivery boy
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderAcceptedByVendor.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .orderConfirmed,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus.containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[
          OrderStatus.orderAcceptedByVendor.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(
          OrderStatus.orderAcceptedByVendor.value?.toString()))
        _currentStep = 2;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.cancelledByDeliveryBoy.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .orderRejected,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus.containsKey(
              OrderStatus.cancelledByDeliveryBoy.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(
              OrderStatus.cancelledByDeliveryBoy.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[
          OrderStatus.cancelledByDeliveryBoy.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(
          OrderStatus.cancelledByDeliveryBoy.value?.toString()))
        _currentStep = 3;
    } else if (widget.actionManager.orderDetailsState.order.bookingStatus
        .containsKey(OrderStatus.cancelledByCustomer.value?.toString())) {
      _currentStep = 1;
      // canceled by customer
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));

      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.orderAcceptedByVendor.value,
            value: AppLocalizations
                .of(widget.context)
                .common
                .text
                .orderConfirmed,
            statusValue: widget.actionManager.orderDetailsState.order
                .bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? 1
                : 0,
            actionTime: widget.actionManager.orderDetailsState.order
                .bookingStatus.containsKey(
                OrderStatus.orderAcceptedByVendor.value?.toString())
                ? widget.actionManager.orderDetailsState.order
                .bookingStatus[
            OrderStatus.orderAcceptedByVendor.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget.context)
                .common
                .text
                .notAllowed));
        _currentStep = 2;
      }

      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.deliveryBoyAssigned.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.deliveryBoyAssigned.value,
            value: AppLocalizations
                .of(widget.context)
                .common
                .text
                .driverAssigned,
            statusValue: widget.actionManager.orderDetailsState.order
                .bookingStatus.containsKey(
                OrderStatus.deliveryBoyAssigned.value?.toString())
                ? 1
                : 0,
            actionTime: widget.actionManager.orderDetailsState.order
                .bookingStatus.containsKey(
                OrderStatus.deliveryBoyAssigned.value?.toString())
                ? widget.actionManager.orderDetailsState.order
                .bookingStatus[
            OrderStatus.deliveryBoyAssigned.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget.context)
                .common
                .text
                .notAllowed));
        _currentStep = 3;
      }

      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.orderPicked.value?.toString())) {
        _bookingStatus.add(BookingStatus(
            id: OrderStatus.orderPicked.value,
            value: AppLocalizations
                .of(widget.context)
                .common
                .text
                .dispatched,
            statusValue: widget.actionManager.orderDetailsState.order
                .bookingStatus
                .containsKey(OrderStatus.orderPicked.value?.toString())
                ? 1
                : 0,
            actionTime: widget.actionManager.orderDetailsState.order
                .bookingStatus
                .containsKey(OrderStatus.orderPicked.value?.toString())
                ? widget.actionManager.orderDetailsState.order
                .bookingStatus[OrderStatus.orderPicked.value?.toString()]
                .actionTime
                : AppLocalizations
                .of(widget.context)
                .common
                .text
                .notAllowed));
        _currentStep = 4;
      }
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.deliveredProduct.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .orderCancelled,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus.containsKey(
              OrderStatus.cancelledByCustomer.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(
              OrderStatus.cancelledByCustomer.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[
          OrderStatus.cancelledByCustomer.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      _currentStep = 5;
    } else {
      _currentStep = 1;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPlaced.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .oderPlaced,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.orderPlaced.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.orderPlaced.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderAcceptedByVendor.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .orderConfirmed,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus.containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(
              OrderStatus.orderAcceptedByVendor.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[
          OrderStatus.orderAcceptedByVendor.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.orderAcceptedByVendor.value?.toString()))
        _currentStep = 2;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.deliveryBoyAssigned.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .driverAssigned,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus.containsKey(
              OrderStatus.deliveryBoyAssigned.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(
              OrderStatus.deliveryBoyAssigned.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[
          OrderStatus.deliveryBoyAssigned.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.deliveryBoyAssigned.value?.toString()))
        _currentStep = 3;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.orderPicked.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .dispatched,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.orderPicked.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.orderPicked.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.orderPicked.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.orderPicked.value?.toString()))
        _currentStep = 4;
      _bookingStatus.add(BookingStatus(
          id: OrderStatus.deliveredProduct.value,
          value: AppLocalizations
              .of(widget.context)
              .common
              .text
              .delivered,
          statusValue: widget.actionManager.orderDetailsState.order
              .bookingStatus
              .containsKey(OrderStatus.deliveredProduct.value?.toString())
              ? 1
              : 0,
          actionTime: widget.actionManager.orderDetailsState.order.bookingStatus
              .containsKey(OrderStatus.deliveredProduct.value?.toString())
              ? widget.actionManager.orderDetailsState.order
              .bookingStatus[OrderStatus.deliveredProduct.value?.toString()]
              .actionTime
              : AppLocalizations
              .of(widget.context)
              .common
              .text
              .notAllowed));
      if (widget.actionManager.orderDetailsState.order.bookingStatus
          .containsKey(OrderStatus.deliveredProduct.value?.toString()))
        _currentStep = 5;
    }
  }

  @override
  Widget build(BuildContext context) {
    _createSteps();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      var value = 75.0 * (_currentStep - 1);
      _scrollController.jumpTo(_currentStep * value);
    });

    return Container(
      margin: const EdgeInsets.all(8),
      constraints: const BoxConstraints(maxHeight: SIZE_100),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        controller: _scrollController,
        itemCount: _bookingStatus.length,
        itemBuilder: (BuildContext context, int index) {
          BookingStatus step = _bookingStatus[index];
          var indicatorSize = 20.0;

          LineStyle afterLineStyle = const LineStyle(
              color: Colors.grey, thickness: 2);

          return TimelineTile(
            key: UniqueKey(),

            axis: TimelineAxis.horizontal,
            alignment: TimelineAlign.center,
            lineXY: 0.5,
            isFirst: index == 0,
            isLast: index == _bookingStatus.length - 1,
            beforeLineStyle: afterLineStyle,
            afterLineStyle: afterLineStyle,
            indicatorStyle: IndicatorStyle(
              width: indicatorSize,
              height: indicatorSize,
              indicator: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: step?.statusValue == 1 ? COLOR_LIGHT_ORANGE : COLOR_GREY_TEXT,
                ),
                child: Center(
                  child: Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color:
                      step?.statusValue == 1 ? COLOR_ORANGE : COLOR_BORDER_GREY,
                    ),
                  ),
                ),
              ),
            ),
            startChild:
            Container(
              child: Center(
                child: Text(
                  step?.actionTime ?? AppLocalizations
                      .of(widget.context)
                      .common
                      .text
                      .notAllowed,
                  style: TextStyle(
                    fontSize: 11,
                    color: Colors.black,
                  ),
                ),
              ),
            ),



            endChild:

            Container(
              constraints: const BoxConstraints(minWidth: 150),
              child: Center(
                child: ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 150),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Text(
                          step?.value,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          );
        },
      ),
    );
  }
}




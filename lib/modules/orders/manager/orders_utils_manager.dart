import 'package:flutter/cupertino.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/enum/order_enum.dart';

class OrdersUtilsManager {
  static OrdersUtilsManager _ordersUtilsManager = OrdersUtilsManager();

  static OrdersUtilsManager get ordersUtilsManagerInstance => _ordersUtilsManager;

  // used to get the order status
  String getOrderStatus({Order order, BuildContext context}) {
    if (order?.status == OrderStatus.orderPlaced.value) {
      return AppLocalizations.of(context).common.text.oderPlaced;
    } else if (order?.status == OrderStatus.orderAcceptedByVendor.value) {
      return "${AppLocalizations.of(context).orderdetailpage.text.orderAcceptedBy} ${order?.shopDetails?.name}";
    } else if (order?.status == OrderStatus.orderRejectedByVendor.value) {
      return "${AppLocalizations.of(context).orderdetailpage.text.orderCancelledBy} ${order?.shopDetails?.name}";
    } else if (order?.status == OrderStatus.orderInProgress.value) {
      return AppLocalizations.of(context).orderdetailpage.text.onProcess;
    } else if (order?.status == OrderStatus.deliveryBoyAssigned.value) {
      return  AppLocalizations.of(context).orderdetailpage.text.deliveryBoyAssigned;
    } else if (order?.status == OrderStatus.journeyStarted.value) {
      return AppLocalizations.of(context).orderdetailpage.text.deliveryBoyOnTheWay;
    } else if (order?.status == OrderStatus.reachedVendor.value) {
      return "${AppLocalizations.of(context).orderdetailpage.text.deliveryBoyReachedAt}  ${order?.shopDetails?.name}";
    } else if (order?.status == OrderStatus.orderPicked.value) {
      return AppLocalizations.of(context).orderdetailpage.text.orderPickedDeliveryBoy;
    } else if (order?.status == OrderStatus.reachedCustomer.value) {
      return AppLocalizations.of(context).orderdetailpage.text.deliveryBoyReached;
    } else if (order?.status == OrderStatus.deliveredProduct.value) {
      return AppLocalizations.of(context).common.text.delivered;
    } else if (order?.status == OrderStatus.cancelledByCustomer.value) {
      return AppLocalizations.of(context).orderdetailpage.text.cancelledByYou;
    } else if (order?.status == OrderStatus.cancelledByDeliveryBoy.value) {
      return AppLocalizations.of(context).orderdetailpage.text.cancelledByDeliveryBoy;
    } else if (order?.status == OrderStatus.other.value) {
      return AppLocalizations.of(context).orderdetailpage.text.otherReason;
    } else if (order?.status == OrderStatus.autocancel.value) {
      return AppLocalizations.of(context).common.text.orderCancelled;
    }else {
      return "";
    }
  }

  //method to for reorder
  reOrderProduct({Order order, List<ShopItem> shopItem}) {
    //updating shop item data to order again
    if (order != null) {
      order?.orderDetails?.forEach((element) {
        for (int offset = 0; offset < shopItem?.length; offset++) {
          if (element.productId == shopItem[offset]?.id) {
            var _item = ShopItem.fromJson(shopItem[offset].toJson());
            _item.itemQuantity = element.quantity;
            CartUtilsManager.cartUtilsInstance.shopItems?.add(_item);
            break;
          }
        }
      });

      CartUtilsManager.cartUtilsInstance.couponData = null;
      CartUtilsManager.cartUtilsInstance.scheduleDateTime = null;
      CartUtilsManager.cartUtilsInstance.notes = order?.instruction;
      CartUtilsManager.cartUtilsInstance.shopType =
          order.bookingType == ShopType.delivery.value ? ShopType.delivery : ShopType.pickUp;
      CartUtilsManager.cartUtilsInstance.tipOfDriver = 0.0;
      CartUtilsManager.cartUtilsInstance.shopDetailsModel = order?.shopDetails;
    }
  }

  bool canOrderCancel({Order order}) {
    if (order?.status == OrderStatus.cancelledByCustomer?.value ||
        order?.status == OrderStatus.cancelledByDeliveryBoy?.value ||
        order?.status == OrderStatus.autocancel?.value ||
        order?.status == OrderStatus.orderRejectedByVendor?.value) {
      return false;
    } else if (order?.bookingStatus?.isNotEmpty == true &&
        (order?.bookingStatus?.containsKey(OrderStatus.orderPicked?.value?.toString()) ||
            order?.bookingStatus?.containsKey(OrderStatus.reachedCustomer?.value?.toString()) ||
            order?.bookingStatus?.containsKey(OrderStatus.deliveredProduct?.value?.toString()) ||
            order?.bookingStatus?.containsKey(OrderStatus.cancelledByCustomer?.value?.toString()) ||
            order?.bookingStatus?.containsKey(OrderStatus.autocancel?.value?.toString()) ||
            order?.bookingStatus?.containsKey(OrderStatus.cancelledByDeliveryBoy?.value?.toString()) ||
            order?.bookingStatus?.containsKey(OrderStatus.orderRejectedByVendor?.value?.toString()))) {
      return false;
    } else {
      return true;
    }
  }

  // order completed or rejected
  bool orderCompletedOrCanceled({Order order}) {
    if (order?.bookingStatus?.containsKey(OrderStatus.deliveredProduct?.value?.toString()) ||
        order?.bookingStatus?.containsKey(OrderStatus.cancelledByCustomer?.value?.toString()) ||
        order?.bookingStatus?.containsKey(OrderStatus.autocancel?.value?.toString()) ||
        order?.bookingStatus?.containsKey(OrderStatus.cancelledByDeliveryBoy?.value?.toString()) ||
        order?.bookingStatus?.containsKey(OrderStatus.orderRejectedByVendor?.value?.toString())) {
      return true;
    } else {
      return false;
    }
  }

  //method to get total item count
  int showTotalProductQuantityOrdered({List<OrderDetail> productList}) {
    int totalProductCount = 0;
    for (var product in productList) {
      totalProductCount = totalProductCount + product?.quantity;
    }
    return totalProductCount;
  }
}

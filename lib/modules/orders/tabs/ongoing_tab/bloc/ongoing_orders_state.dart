import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

class OngoingOrdersState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final String message;
  final int page;
  final List<Order> listOrders;

  OngoingOrdersState({
    this.isLoading,
    this.context,
    this.message,
    this.page,
    this.listOrders,
  }) : super(isLoading);

  // not authenticated
  factory OngoingOrdersState.initiating({bool isLoading}) {
    return OngoingOrdersState(
      isLoading: isLoading,
    );
  }

  factory OngoingOrdersState.updateUi({
    bool isLoading,
    BuildContext context,
    String message,
    int page,
    List<Order> listOrders,
  }) {
    return OngoingOrdersState(
      isLoading: isLoading,
      context: context,
      message: message,
      page: page,
      listOrders: listOrders,
    );
  }
}

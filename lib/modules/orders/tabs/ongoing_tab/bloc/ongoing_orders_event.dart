import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

abstract class OngoingOrdersEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int page;
  final List<Order> listOrders;

  OngoingOrdersEvent({
    this.isLoading: false,
    this.context,
    this.page,
    this.listOrders,
  });
}

// for get ongoing orders
class GetOngoingOrdersEvent extends OngoingOrdersEvent {
  GetOngoingOrdersEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
        );
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/bloc/ongoing_orders_bloc.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/bloc/ongoing_orders_event.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/bloc/ongoing_orders_state.dart';

import '../../../../../routes.dart';
import '../../../order_routes.dart';

class OngoingOrdersActionManager {
  BuildContext context;
  OngoingOrdersBloc ongoingOrdersBloc;
  OngoingOrdersState ongoingOrdersState;

  //used to perform the action on the init
  void actionOnInit({
    ScaffoldState scaffoldState,
    int page,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        ongoingOrdersBloc.emitEvent(GetOngoingOrdersEvent(
          context: context,
          isLoading: true,
          page: page,
          listOrders: ongoingOrdersState?.listOrders,
        ));
      }
    });
  }

  // used to perform the action on the item tap
  void actionOnItemTap(
      {Order order, ScaffoldState scaffoldState, bool raiseAQuery}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        if (raiseAQuery) {
         await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
              context, Routes.SEND_QUERY_PAGE,
              dataToBeSend: order);
        } else {
         await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            context,
            OrderRoutes.ORDER_DETAILS,
            dataToBeSend: order,
          );
        }
        actionOnInit(page: 1);
      }
    });
  }

  //used to perform the action on the state change
  void actionOnStateChanged({ScaffoldState scaffoldState}) {
    if (ongoingOrdersState?.isLoading == false) {
      if (ongoingOrdersState?.message?.trim()?.isNotEmpty == true) {
        WidgetsBinding.instance.addPostFrameCallback(
          (_) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                message: ongoingOrdersState?.message,
                scaffoldState: scaffoldState);
          },
        );
      }
    }
  }
}

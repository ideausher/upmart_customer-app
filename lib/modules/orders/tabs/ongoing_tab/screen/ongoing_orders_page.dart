import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/constant/image_constant.dart';
import 'package:user/modules/orders/manager/orders_utils_manager.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/bloc/ongoing_orders_bloc.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/bloc/ongoing_orders_state.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/manager/ongoing_orders_action_manager.dart';

class OngoingOrderPage extends StatefulWidget {
  BuildContext context;
  ScaffoldState scaffoldState;
  bool raiseAnewQuery;

  OngoingOrderPage({this.context, this.scaffoldState, this.raiseAnewQuery});

  @override
  _OngoingOrderPageState createState() => _OngoingOrderPageState();
}

class _OngoingOrderPageState extends State<OngoingOrderPage>
    implements PushReceived {
  OngoingOrdersActionManager _ongoingOrdersActionManager =
      OngoingOrdersActionManager();
  OngoingOrdersBloc _bloc = OngoingOrdersBloc();

  // initialise controllers
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _ongoingOrdersActionManager.context = widget.context;
    _ongoingOrdersActionManager.ongoingOrdersBloc = _bloc;
    _ongoingOrdersActionManager.actionOnInit(
      scaffoldState: widget.scaffoldState,
      page: 1,
    );
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _bloc?.dispose();
    _scrollController?.dispose();
    super.dispose();
  }

  // usd for the pagination
  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the bottom");

      _ongoingOrdersActionManager.actionOnInit(
        page: _ongoingOrdersActionManager.ongoingOrdersState.page + 1,
        scaffoldState: widget.scaffoldState,
      );
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the top");
    }
  }

  @override
  Widget build(BuildContext context) {
    _ongoingOrdersActionManager.context = context;
    return BlocEventStateBuilder<OngoingOrdersState>(
      bloc: _bloc,
      builder:
          (BuildContext buildContext, OngoingOrdersState ongoingOrdersState) {
        _ongoingOrdersActionManager.context = context;
        if (ongoingOrdersState != null &&
            _ongoingOrdersActionManager?.ongoingOrdersState !=
                ongoingOrdersState) {
          _ongoingOrdersActionManager?.ongoingOrdersState = ongoingOrdersState;
          _ongoingOrdersActionManager.actionOnStateChanged(
            scaffoldState: widget.scaffoldState,
          );
        }
        // main ui started
        return ModalProgressHUD(
          inAsyncCall:
              _ongoingOrdersActionManager?.ongoingOrdersState?.isLoading ??
                  false,
          child: (_ongoingOrdersActionManager
                      ?.ongoingOrdersState?.listOrders?.isNotEmpty ==
                  true)
              ? _showOngoingOrderList()
              : (_ongoingOrdersActionManager?.ongoingOrdersState?.isLoading ==
                      false)
                  ? Center(
                      child: CommonImageWithTextWidget(
                          context: context,
                          localImagePath: ORDER_ICON,
                          imageHeight: SIZE_15,
                          imageWidth: SIZE_15,
                          textToShow: AppLocalizations.of(context)
                              .myorderpage
                              .text
                              .noOrderFound),
                    )
                  : const SizedBox(),
        );
      },
    );
  }

  //this method is used to show active orders list
  Widget _showOngoingOrderList() {
    return ListView.builder(
        shrinkWrap: true,
        key: PageStorageKey("OnGoingOrders"),
        itemCount:
            _ongoingOrdersActionManager?.ongoingOrdersState?.listOrders?.length,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) {
          return _getListItem(
              order: _ongoingOrdersActionManager
                  ?.ongoingOrdersState?.listOrders[index]);
        });
  }

  // used for the list item
  Widget _getListItem({Order order}) {
    return Padding(
      padding:
          const EdgeInsets.only(left: SIZE_10, right: SIZE_10, bottom: SIZE_12),
      child: InkWell(
        onTap: () {
          _ongoingOrdersActionManager.actionOnItemTap(
              scaffoldState: widget.scaffoldState,
              order: order,
              raiseAQuery: widget.raiseAnewQuery);
        },
        child: Card(
          elevation: 2.0,
          child: Padding(
            padding: const EdgeInsets.all(SIZE_10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _showOrderStatus(order: order),
                SizedBox(
                  height: SIZE_10,
                ),
                _showOrderDetail(order: order),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //this method will return order id widget view
  Widget _showOrderStatus({Order order}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(SIZE_5)),
            color: COLOR_LIGHT_BLUE,
          ),
          padding: EdgeInsets.all(SIZE_6),
          child: Text(
            OrdersUtilsManager.ordersUtilsManagerInstance
                .getOrderStatus(order: order, context: context),
            style: textStyleSize14WithBlueColor,
          ),
        ),
      ],
    );
  }

  //method to show Order status
  Widget _showOrderDetail({Order order}) {
    print("order status ${order?.bookingStatus?.toString()}");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _showDetails(
                title: AppLocalizations.of(context).myorderpage.text.orderId,
                subTitle: "#${order?.bookingCode ?? ""} "),
            _showDetails(
                title:
                    AppLocalizations.of(context).myorderpage.text.totalPayment,
                subTitle: NumberFormatUtils.numberFormatUtilsInstance
                    .formatPriceWithSymbol(
                        price: ((order?.amountAfterStripeCharges ?? 0) +
                            (order?.stripeCharges ?? 0)))),
          ],
        ),
        _showDetails(
            title: AppLocalizations.of(context).myorderpage.text.dated,
            subTitle: order?.createdAt?.isNotEmpty == true
                ? DateUtils.dateUtilsInstance.getDateTimeDayFormat(
                    dateTime: order?.createdAt, toLocal: true)
                : ""),
      ],
    );
  }

  //method to show orders details text data
  Widget _showDetails({String title, String subTitle}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyleSize14GreyColorFont500,
        ),
        Text(
          subTitle,
          style: AppConfig.of(_ongoingOrdersActionManager.context)
              .themeData
              .primaryTextTheme
              .headline1,
        )
      ],
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _ongoingOrdersActionManager.actionOnInit(
      scaffoldState: widget.scaffoldState,
      page: 1,
    );
  }
}

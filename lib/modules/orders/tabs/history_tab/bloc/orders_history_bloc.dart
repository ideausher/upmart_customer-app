import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/api/provider/orders_listing_provider.dart';
import 'orders_history_event.dart';
import 'orders_history_state.dart';

class OrdersHistoryBloc
    extends BlocEventStateBase<OrdersHistoryEvent, OrdersHistoryState> {
  OrdersHistoryBloc({bool isLoading = false})
      : super(
      initialState: OrdersHistoryState.initiating(isLoading: isLoading));

  @override
  Stream<OrdersHistoryState> eventHandler(OrdersHistoryEvent event,
      OrdersHistoryState currentState) async* {
    // used for the select tab
    if (event is GetOrdersHistoryEvent) {
      yield OrdersHistoryState.updateUi(
        isLoading: true,
        context: event.context,
        page: event.page,
        listOrders: event.listOrders,
      );

      var _result = await OrdersListingProvider().getOrdersHistoryApiCall(
        context: event.context,
        page: event.page,
        isOngoing: false,
      );

      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<Order> _list;
          // parse result to list
          OrderListingResponseModel _orderListingResponseModel =
          OrderListingResponseModel.fromMap(_result);
          // check if the page is first and event.coupon list is null or empty
          if (event?.listOrders == null ||
              event?.listOrders?.isEmpty == true ||
              event.page == 1) {
            // pass response list t temp value
            _list = _orderListingResponseModel?.orders;
          } else {
            _list = event?.listOrders;

            _list.addAll(_orderListingResponseModel?.orders);
          }

          yield OrdersHistoryState.updateUi(
            isLoading: false,
            context: event.context,
            page: (_orderListingResponseModel?.orders?.isNotEmpty == true)
                ? event.page
                : (event.page > 1)
                ? event.page - 1
                : event.page,
            listOrders: _list,
          );
        } else {
          yield OrdersHistoryState.updateUi(
            isLoading: false,
            context: event.context,
            message: _result[ApiStatusParams.Message.value],
            page: (event.page > 1) ? event.page - 1 : event.page,
            listOrders: event.listOrders,
          );
        }
      } else {
        yield OrdersHistoryState.updateUi(
          isLoading: false,
          context: event.context,
          listOrders: event.listOrders,
          message:
          AppLocalizations
              .of(event.context)
              .common
              .error
              .somethingWentWrong,
          page: (event.page > 1) ? event.page - 1 : event.page,
        );
      }
    }

    if (event is UpdateOrdersHistoryEvent) {
      yield OrdersHistoryState.updateUi(
        isLoading: event?.isLoading,
        context: event.context,
        page: event.page,
        listOrders: event.listOrders,
      );
    }
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

abstract class OrdersHistoryEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int page;
  final List<Order> listOrders;

  OrdersHistoryEvent({
    this.isLoading: false,
    this.context,
    this.page,
    this.listOrders,
  });
}

// for get orders history
class GetOrdersHistoryEvent extends OrdersHistoryEvent {
  GetOrdersHistoryEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
        );
}

// for get orders history
class UpdateOrdersHistoryEvent extends OrdersHistoryEvent {
  UpdateOrdersHistoryEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
    context: context,
    isLoading: isLoading,
    page: page,
    listOrders: listOrders,
  );
}
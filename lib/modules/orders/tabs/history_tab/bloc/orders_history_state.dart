import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';

class OrdersHistoryState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final String message;
  final int page;
  final List<Order> listOrders;

  OrdersHistoryState({
    this.isLoading,
    this.context,
    this.message,
    this.page,
    this.listOrders,
  }) : super(isLoading);

  // not authenticated
  factory OrdersHistoryState.initiating({bool isLoading}) {
    return OrdersHistoryState(
      isLoading: isLoading,
    );
  }

  factory OrdersHistoryState.updateUi({
    bool isLoading,
    BuildContext context,
    String message,
    int page,
    List<Order> listOrders,
  }) {
    return OrdersHistoryState(
      isLoading: isLoading,
      context: context,
      message: message,
      page: page,
      listOrders: listOrders,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/provider/products_provider.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/manager/orders_utils_manager.dart';
import 'package:user/modules/orders/tabs/history_tab/bloc/orders_history_bloc.dart';
import 'package:user/modules/orders/tabs/history_tab/bloc/orders_history_event.dart';
import 'package:user/modules/orders/tabs/history_tab/bloc/orders_history_state.dart';

import '../../../../../localizations.dart';
import '../../../../../routes.dart';
import '../../../order_routes.dart';

class OrdersHistoryActionManager {
  BuildContext context;
  OrdersHistoryBloc ordersHistoryBloc;
  OrdersHistoryState ordersHistoryState;

  //used to perform the action on the init
  void actionOnInit({
    ScaffoldState scaffoldState,
    int page,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        ordersHistoryBloc.emitEvent(GetOrdersHistoryEvent(
          context: context,
          isLoading: true,
          page: page,
          listOrders: ordersHistoryState?.listOrders,
        ));
      }
    });
  }

  // used to perform the action on the item tap
  void actionOnItemTap({Order order, ScaffoldState scaffoldState, bool raiseAQuery}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        if (raiseAQuery) {
          await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
              context, Routes.SEND_QUERY_PAGE,
              dataToBeSend: order);
        } else {
          await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            context,
            OrderRoutes.ORDER_DETAILS,
            dataToBeSend: order,
          );
        }
        actionOnInit(page: 1);
      }
    });
  }

  //used to perform the action on the state change
  void actionOnStateChanged({ScaffoldState scaffoldState}) {
    if (ordersHistoryState?.isLoading == false) {
      if (ordersHistoryState?.message?.trim()?.isNotEmpty == true) {
        WidgetsBinding.instance.addPostFrameCallback(
          (_) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance
                .showSnackbar(context: context, message: ordersHistoryState?.message, scaffoldState: scaffoldState);
          },
        );
      }
    }
  }

  // used to perform the action on the order again tap
  void actionOnOrderAgainTap({Order order}) {
    if (CartUtilsManager.cartUtilsInstance.shopItems?.isNotEmpty == true) {
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
          context: context,
          title: "",
          positiveButton: AppLocalizations.of(context).common.text.ok,
          subTitle: "Please clear your cart first",
          onPositiveButtonTab: () {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          });

      return;
    }
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        ordersHistoryBloc.emitEvent(GetOrdersHistoryEvent(
          context: context,
          isLoading: true,
          page: ordersHistoryState?.page,
          listOrders: ordersHistoryState?.listOrders,
        ));

        CurrentLocation _currentLocation = await FetchPrefsUtils.fetchPrefsUtilsInstance.getCurrentLocationModel();

        var result = await ProductPageProvider().getShopProducts(
          context: context,
          productsRequestModel: ProductDetailsRequestModel(
            lat: _currentLocation?.lat?.toString(),
            lng: _currentLocation?.lng?.toString(),
            categoryId: [],
            shopId: order?.shopDetails?.id,
          ),
        );
        ProductsShopDetailsModel _productsResponseModel;
        if (result != null) {
          // check result status
          if (result[ApiStatusParams.Status.value] != null &&
              result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
            _productsResponseModel = ProductsShopDetailsModel.fromMap(result);
          }
        }

        if (_productsResponseModel?.data?.items?.isNotEmpty == true) {
          OrdersUtilsManager.ordersUtilsManagerInstance
              .reOrderProduct(order: order, shopItem: _productsResponseModel?.data?.items);
          ordersHistoryBloc.emitEvent(GetOrdersHistoryEvent(
            context: context,
            isLoading: false,
            page: ordersHistoryState?.page,
            listOrders: ordersHistoryState?.listOrders,
          ));
          var result = await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(context, Routes.CART_PAGE);
        } else {
          ordersHistoryBloc.emitEvent(GetOrdersHistoryEvent(
            context: context,
            isLoading: false,
            page: ordersHistoryState?.page,
            listOrders: ordersHistoryState?.listOrders,
          ));
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
              context: context,
              title: "",
              positiveButton: AppLocalizations.of(context).common.text.ok,
              subTitle: AppLocalizations.of(context).common.error.somethingWentWrong,
              onPositiveButtonTab: () {
                NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
              });
        }
      }
    });
  }
}

import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/orders/api/model/order_listing_response_model.dart';
import 'package:user/modules/orders/constant/image_constant.dart';
import 'package:user/modules/orders/enum/order_enum.dart';
import 'package:user/modules/orders/manager/orders_utils_manager.dart';
import 'package:user/modules/orders/tabs/history_tab/bloc/orders_history_bloc.dart';
import 'package:user/modules/orders/tabs/history_tab/bloc/orders_history_state.dart';
import 'package:user/modules/orders/tabs/history_tab/manager/orders_history_action_manager.dart';

class OrdersHistoryPage extends StatefulWidget {
  BuildContext context;
  ScaffoldState scaffoldState;
  bool raiseANewQuery;

  OrdersHistoryPage({
    this.context,
    this.scaffoldState,
    this.raiseANewQuery,
  });

  @override
  _OrdersHistoryPageState createState() => _OrdersHistoryPageState();
}

class _OrdersHistoryPageState extends State<OrdersHistoryPage>
    implements PushReceived {
  // initialise managers
  OrdersHistoryActionManager _ordersHistoryActionManager =
  OrdersHistoryActionManager();
  OrdersUtilsManager _ordersUtilsManager = OrdersUtilsManager();

  //init bloc
  OrdersHistoryBloc _bloc = OrdersHistoryBloc();

  // initialise controllers
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _ordersHistoryActionManager.context = widget.context;
    _ordersHistoryActionManager.ordersHistoryBloc = _bloc;
    _ordersHistoryActionManager.actionOnInit(
      scaffoldState: widget.scaffoldState,
      page: 1,
    );
    _scrollController.addListener(_scrollListener);
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    super.initState();
  }

  @override
  void dispose() {
    _bloc?.dispose();
    _scrollController?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    super.dispose();
  }

  // usd for the pagination
  _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the bottom");

      _ordersHistoryActionManager.actionOnInit(
        page: _ordersHistoryActionManager.ordersHistoryState.page + 1,
        scaffoldState: widget.scaffoldState,
      );
    }
    if (_scrollController.offset <=
        _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the top");
    }
  }

  @override
  Widget build(BuildContext context) {
    _ordersHistoryActionManager.context = context;
    return BlocEventStateBuilder<OrdersHistoryState>(
      bloc: _bloc,
      builder:
          (BuildContext buildContext, OrdersHistoryState ordersHistoryState) {
        _ordersHistoryActionManager.context = context;
        if (ordersHistoryState != null &&
            _ordersHistoryActionManager?.ordersHistoryState !=
                ordersHistoryState) {
          _ordersHistoryActionManager?.ordersHistoryState = ordersHistoryState;
          _ordersHistoryActionManager.actionOnStateChanged(
              scaffoldState: widget.scaffoldState);
        }
        // main ui started
        return ModalProgressHUD(
          inAsyncCall:
          _ordersHistoryActionManager?.ordersHistoryState?.isLoading ??
              false,
          child: (_ordersHistoryActionManager
              ?.ordersHistoryState?.listOrders?.isNotEmpty ==
              true)
              ? _showOrdersHistoryList()
              : (_ordersHistoryActionManager?.ordersHistoryState?.isLoading ==
              false)
              ? Center(
            child: CommonImageWithTextWidget(
                context: context,
                localImagePath: ORDER_ICON,
                imageHeight: SIZE_15,
                imageWidth: SIZE_15,
                textToShow: AppLocalizations
                    .of(context)
                    .myorderpage
                    .text
                    .noOrderFound),
          )
              : const SizedBox(),
        );
      },
    );
  }

  //this method is used to show active orders list
  Widget _showOrdersHistoryList() {
    return ListView.builder(
        key: PageStorageKey("listing"),
        shrinkWrap: true,
        itemCount:
        _ordersHistoryActionManager?.ordersHistoryState?.listOrders?.length,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int index) {
          return _getListItem(
              order: _ordersHistoryActionManager
                  ?.ordersHistoryState?.listOrders[index]);
        });
  }

  // used to get the list item
  Widget _getListItem({Order order}) {
    return Padding(
      padding:
      const EdgeInsets.only(left: SIZE_10, right: SIZE_10, bottom: SIZE_12),
      child: InkWell(
        onTap: () {
          _ordersHistoryActionManager.actionOnItemTap(
              order: order,
              scaffoldState: widget.scaffoldState,
              raiseAQuery: widget.raiseANewQuery);
        },
        child: Card(
          elevation: 2.0,
          child: Padding(
            padding: const EdgeInsets.all(SIZE_10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _showOrderStatus(order: order),
                SizedBox(
                  height: SIZE_10,
                ),
                _showOrderDetail(order: order),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //this method will return order id widget view
  Widget _showOrderStatus({Order order}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(SIZE_5)),
                  color: (order?.status == OrderStatus.deliveredProduct.value)
                      ? COLOR_GREEN_COLOR
                      : COLOR_LIGHT_RED,
                ),
                padding: EdgeInsets.all(SIZE_6),
                child: Text(
                  OrdersUtilsManager.ordersUtilsManagerInstance
                      .getOrderStatus(order: order, context: context),
                  style: (order?.status == OrderStatus.deliveredProduct.value)
                      ? textStyleSize14WithGreenColor
                      : textStyleSize14WithRedColor,
                ),
              ),
            ],
          ),
        ),

        InkWell(
          onTap: () {
            _ordersHistoryActionManager.actionOnOrderAgainTap(order: order);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                Icons.refresh,
                color: COLOR_GREEN_TEXT,
                size: SIZE_16,
              ),
              SizedBox(
                width: SIZE_2,
              ),
              Text(
                AppLocalizations
                    .of(context)
                    .myorderpage
                    .text
                    .orderAgain,
                style: textStyleSize14WithGreenColor,
              ),
            ],
          ),
        )

      ],
    );
  }

  //method to show Order status
  Widget _showOrderDetail({Order order}) {
    print("order status ${order?.bookingStatus?.toString()}");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _showDetails(
                title: AppLocalizations
                    .of(context)
                    .myorderpage
                    .text
                    .orderId, subTitle: "#${order?.bookingCode ?? ""} "),
            _showDetails(
                title: AppLocalizations
                    .of(context)
                    .myorderpage
                    .text
                    .totalPayment,
                subTitle: NumberFormatUtils.numberFormatUtilsInstance
                    .formatPriceWithSymbol(
                    price: ((order?.amountAfterStripeCharges ?? 0) +
                        (order?.stripeCharges ?? 0)))),
          ],
        ),
        _showDetails(
            title: AppLocalizations
                .of(context)
                .myorderpage
                .text
                .dated,
            subTitle: order?.createdAt?.isNotEmpty == true
                ? DateUtils.dateUtilsInstance.getDateTimeDayFormat(
                dateTime: order?.createdAt, toLocal: true)
                : ""),
      ],
    );
  }

  //method to show orders details text data
  Widget _showDetails({String title, String subTitle}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyleSize14GreyColorFont500,
        ),
        Text(
          subTitle,
          style: AppConfig
              .of(_ordersHistoryActionManager.context)
              .themeData
              .primaryTextTheme
              .headline1,
        )
      ],
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _ordersHistoryActionManager.actionOnInit(
      page: 1,
      scaffoldState: widget.scaffoldState,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:user/modules/orders/bloc/orders_bloc.dart';
import 'package:user/modules/orders/bloc/orders_state.dart';
import 'package:user/modules/orders/manager/orders_action_manager.dart';
import 'package:user/modules/orders/tabs/ongoing_tab/screen/ongoing_orders_page.dart';
import 'package:user/modules/orders/tabs/history_tab/screen/orders_history_page.dart';

import '../../../localizations.dart';

class OrdersPage extends StatefulWidget {
  BuildContext context;

  OrdersPage(this.context);

  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  OrdersActionManager _ordersActionManager = OrdersActionManager();
  OrdersBloc _bloc = OrdersBloc();
  bool _raiseANewQuery = false;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    if (ModalRoute.of(widget.context).settings?.arguments != null &&
        ModalRoute.of(widget.context).settings?.arguments is bool)
      _raiseANewQuery = ModalRoute.of(widget.context).settings.arguments;
    _ordersActionManager.context = widget.context;
    _ordersActionManager.ordersBloc = _bloc;
    _ordersActionManager.actionOnTabChange(index: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _ordersActionManager.context = context;
    return BlocEventStateBuilder<OrdersState>(
      bloc: _bloc,
      builder: (BuildContext buildContext, OrdersState ordersState) {
        _ordersActionManager.context = context;
        if (ordersState != null &&
            _ordersActionManager?.ordersState != ordersState) {
          _ordersActionManager?.ordersState = ordersState;
        }
        // main ui started
        return ModalProgressHUD(
          inAsyncCall: _ordersActionManager?.ordersState?.isLoading ?? false,
          child: _showTabBar(),
        );
      },
    );
  }

  //method to show tab bar
  Widget _showTabBar() {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: ELEVATION_0,
          title: Text(
            AppLocalizations.of(context).myorderpage.appbartitle.myOrder,
            style: AppConfig.of(_ordersActionManager.context)
                .themeData
                .primaryTextTheme
                .headline3,
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            },
            icon: Icon(Icons.arrow_back,color: Colors.black,),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(40),
            child: Align(
              alignment: Alignment.centerLeft,
              child: TabBar(
                indicatorColor: COLOR_PRIMARY,
                isScrollable: true,
                indicatorSize: TabBarIndicatorSize.label,
                onTap: (int index) {
                  _ordersActionManager.actionOnTabChange(
                    index: index,
                  );
                },
                tabs: [
                  Tab(
                    child: Container(
                      child: Text(
                        AppLocalizations.of(context).myorderpage.text.ongoing,
                        style: textStyleSize14BlackColor,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      AppLocalizations.of(context).myorderpage.text.history,
                      style: textStyleSize14BlackColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(SIZE_16),
              child: NotificationReadCountWidget(
                context: _ordersActionManager?.context,
              ),
            )
          ],
        ),
        body: TabBarView(
          children: [
            OngoingOrderPage(
              context: _ordersActionManager.context,
              scaffoldState: _scaffoldKey?.currentState,
              raiseAnewQuery: _raiseANewQuery,
            ),
            OrdersHistoryPage(
              context: _ordersActionManager.context,
              scaffoldState: _scaffoldKey?.currentState,
              raiseANewQuery: _raiseANewQuery,
            ),
          ],
        ),
      ),
    );
  }
}

enum OrderStatus {
  orderPlaced,
  orderAcceptedByVendor,
  orderRejectedByVendor,
  orderInProgress,
  deliveryBoyAssigned,
  journeyStarted,
  reachedVendor,
  orderPicked,
  reachedCustomer,
  deliveredProduct,
  cancelledByCustomer,
  cancelledByDeliveryBoy,
  other,
  autocancel
}

extension OrderStatusExtension on OrderStatus {
  int get value {
    switch (this) {
      case OrderStatus.orderPlaced:
        return 1;
      case OrderStatus.orderAcceptedByVendor:
        return 2;
      case OrderStatus.orderRejectedByVendor:
        return 3;
      case OrderStatus.orderInProgress:
        return 4;
      case OrderStatus.deliveryBoyAssigned:
        return 5;
      case OrderStatus.journeyStarted:
        return 6;
      case OrderStatus.reachedVendor:
        return 7;
      case OrderStatus.orderPicked:
        return 8;
      case OrderStatus.reachedCustomer:
        return 9;
      case OrderStatus.deliveredProduct:
        return 10;
      case OrderStatus.cancelledByCustomer:
        return 11;
      case OrderStatus.cancelledByDeliveryBoy:
        return 12;
      case OrderStatus.other:
        return 13;
      case OrderStatus.autocancel:
        return 14;
      default:
        return null;
    }
  }
}

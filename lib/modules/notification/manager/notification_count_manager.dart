import 'package:flutter/cupertino.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/notification/notification_count/notification_count_bloc.dart';
import 'package:user/modules/notification/notification_count/notification_count_event.dart';
import 'package:user/modules/notification/notification_count/notification_count_state.dart';

class NotificationCountManager {
  BuildContext context;
  NotificationCountBloc notificationCountBloc = new NotificationCountBloc();
  NotificationCountState notificationCountState;

  // action to get notifcation count
  actionToGetNotificationCount() {
    print("notificationcountupdated");
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context)
        .then((value) {
      notificationCountBloc?.emitEvent(GetNotificationCountEvent(
          context: context,
          isLoading: true,
          count: notificationCountState?.count));
    });
  }
}

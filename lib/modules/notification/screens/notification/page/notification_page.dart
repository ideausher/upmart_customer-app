import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/intro/constant/image_constant.dart';
import 'package:user/modules/notification/manager/notification_action_manager.dart';
import 'package:user/modules/notification/notification_bloc/notification_bloc.dart';
import 'package:user/modules/notification/notification_bloc/notification_state.dart';

class NotificationPage extends StatefulWidget {
  BuildContext context;

  NotificationPage(this.context);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage>
    implements PushReceived {
  // declaration of bloc
  var _notificationBloc = NotificationBloc();

  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // declaration of notification manager
  NotificationActionManager _notificationManager = NotificationActionManager();

  // initialise controllers
  ScrollController _scrollController;

  int _page = 1;

  @override
  void initState() {
    _notificationManager.context = widget.context;
    _notificationManager.notificationBloc = _notificationBloc;
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _notificationManager.actionTogetNotificationList(
      page: _page,
      scaffoldState: _scaffoldKey?.currentState,
    );
    super.initState();
  }

  // used for the pagination
  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the bottom");
      _notificationManager.actionTogetNotificationList(
        page: (_page != null) ? _page + 1 : 1,
        scaffoldState: _scaffoldKey?.currentState,
      );
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      print("reach the top");
    }
  }

  @override
  void dispose() {
    super.dispose();
    _notificationBloc?.dispose();
    _scrollController?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return BlocEventStateBuilder<NotificationState>(
      bloc: _notificationBloc,
      builder: (BuildContext context, NotificationState notificationState) {
        if (_notificationManager.notificationState != notificationState) {
          _notificationManager.notificationState = notificationState;

          if (_notificationManager.notificationState?.isLoading == false) {
            _page = _notificationManager.notificationState?.page;
          }
          _notificationManager.actionOnNotificationScreenStateChange(
            scaffoldState: _scaffoldKey?.currentState,
          );
        }

        return WillPopScope(
          onWillPop: () {
            return NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(context);
          },
          child: Scaffold(
            backgroundColor: AppConfig.of(context).themeData.backgroundColor,
            key: _scaffoldKey,
            appBar: _showAppBar(),
            body: ModalProgressHUD(
              inAsyncCall:
                  _notificationManager.notificationState?.isLoading ?? false,
              child: Column(
                children: <Widget>[
                  // used for the appbar
                  // used for the common app bar

                  (_notificationManager.notificationState
                              ?.notificationResponseModel?.data?.isNotEmpty ==
                          true)
                      ? Expanded(child: _getNotificationList())
                      : Expanded(
                          child: Visibility(
                            visible: _notificationManager
                                    .notificationState?.isLoading ==
                                false,
                            child: InkWell(
                              onTap: () {
                                _page = 1;
                                _notificationManager
                                    .actionTogetNotificationList(
                                  page: _page,
                                  scaffoldState: _scaffoldKey?.currentState,
                                );
                              },
                              child: Center(
                                child: CommonImageWithTextWidget(
                                  context: _context,
                                  localImagePath: NOTIFICATION_ICON,
                                  imageHeight: SIZE_5,
                                  imageWidth: SIZE_5,
                                  textToShow: AppLocalizations.of(context)
                                      .notificationpage
                                      .text
                                      .noNotificationFound,
                                ),
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: false,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () async {
          return NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
            _context,
          );
        },
        appBarTitle: AppLocalizations.of(context)
            .notificationpage
            .appbartitle
            .notification,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_20),
            child: SvgPicture.asset(
              NOTIFICATION_ICON,
              height: SIZE_14,
              width: SIZE_14,
            ),
          )
        ],
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  // used to prepare the chat list
  Widget _getNotificationList() {
    return ListView.builder(
      controller: _scrollController,
      key: PageStorageKey("notification_listing_key"),
      itemCount: _notificationManager
          .notificationState?.notificationResponseModel?.data?.length,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(SIZE_5),
            color: COLOR_LIGHT_RED,

          ),
          margin: EdgeInsets.only(
              left: SIZE_10, right: SIZE_10, top: SIZE_2, bottom: SIZE_2),
          padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: COLOR_PRIMARY,
                ),
                child: Image.asset(APP_ICON,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _context, percentage: SIZE_10, ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _context, percentage: SIZE_10)),
              ),
              SizedBox(
                width: SIZE_10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      _notificationManager.notificationState
                          ?.notificationResponseModel?.data[index].description,
                      style: textStyleSize12WithBlackColor,
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: SIZE_10, bottom: SIZE_5),
                      child: Text(
                        _getNotificationTime(_notificationManager
                            ?.notificationState
                            ?.notificationResponseModel
                            ?.data[index]
                            .createdAt),
                        style: textStyleSize12WithBlackColor,
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  String _getNotificationTime(String dateTime) {
    return dateTime != null
        ? DateUtils.dateUtilsInstance
            .getDateAndTimeFormat(dateTime: dateTime, toLocal: true)
        : "";
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    // if (notificationPushModel?.data?.notificationType != NotificationType.vendorLocationChanged.value) {
    _notificationManager.actionTogetNotificationList(
      page: 1,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }
}

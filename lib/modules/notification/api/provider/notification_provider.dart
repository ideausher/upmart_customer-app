import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';

class NotificationProvider {
  Future<dynamic> fetchNotificationApiCall({
    BuildContext context,
    int page,
  }) async {
    var getNotification = "notifications";

    var result = await AppConfig.of(context).baseApi.getRequest(getNotification, context, queryParameters: {
      "limit": 20,
      "page": page,
    });

    return result;
  }

  Future<dynamic> fetchNotificationCountApiCall({
    BuildContext context,
  }) async {
    var getNotificationCount = "notifications/unread/count";

    var result = await AppConfig.of(context).baseApi.getRequest(
      getNotificationCount,
      context,
    );

    return result;
  }
}
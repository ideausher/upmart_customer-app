import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/notification/api/model/notification_response_model.dart';
import 'package:user/modules/notification/api/provider/notification_provider.dart';

import '../../../localizations.dart';
import 'notification_event.dart';
import 'notification_state.dart';

class NotificationBloc
    extends BlocEventStateBase<NotificationEvent, NotificationState> {
  NotificationBloc({bool isLoading = false})
      : super(initialState: NotificationState.initiating(isLoading: isLoading));

  @override
  Stream<NotificationState> eventHandler(
      NotificationEvent event, NotificationState currentState) async* {
// for fetch notifications
    if (event is FetchNotification) {
      yield NotificationState.getNotifications(
          isLoading: true,
          page: event.notificationRequestModel?.page,
          notificationResponseModel: event.notificationResponseModel);

       var result = await NotificationProvider().fetchNotificationApiCall(
         context: event.context,
         page: event.notificationRequestModel?.page,
       );
       if (result != null) {
         // create temp studio Model list
         List<NotificationModel> _newNotificationList = List();

         // check result status
         if (result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
           NotificationResponseModel _notificationResponseModel =
               NotificationResponseModel.fromJson(result);
           List<NotificationModel> responseNotificationList =
               _notificationResponseModel?.data;
           if (event?.notificationRequestModel?.page == 1) {
             _newNotificationList = responseNotificationList;
           } else {
             // manage list here
             for (var notification in responseNotificationList) {
               bool _notificationContain = false;

               for (var _oldNotification
                   in event?.notificationResponseModel?.data) {
                 if (_oldNotification.id == notification.id) {
                   _notificationContain = true;
                 }
               }

               if (_notificationContain == false) {
                 _newNotificationList.add(notification);
               }
             }
           }

           List<NotificationModel> _oldNotificationList = List();

           if (event.notificationResponseModel?.data?.isNotEmpty ==
                   true &&
               event.notificationRequestModel?.page != 1) {
             _oldNotificationList = event.notificationResponseModel?.data;
           }
           if (_newNotificationList?.isNotEmpty == true)
             _oldNotificationList?.addAll(_newNotificationList);

           responseNotificationList = _oldNotificationList;
           _notificationResponseModel.data = responseNotificationList;
           yield NotificationState.getNotifications(
             isLoading: false,
             context: event.context,
             page: event.notificationRequestModel?.page,
             notificationResponseModel: _notificationResponseModel,
           );
         }
         // failure case
         else {
           yield NotificationState.getNotifications(
             isLoading: false,
             context: event.context,
             page: (event?.notificationRequestModel?.page > 1)
                 ? event.notificationRequestModel?.page - 1
                 : event.notificationRequestModel?.page,
             message: (event?.notificationRequestModel?.page > 1)
                 ? ""
                 : result[ApiStatusParams.Message.value],
             notificationResponseModel: event.notificationResponseModel,
           );
         }
       }
       // failure case
       else {
         yield NotificationState.getNotifications(
           isLoading: false,
           context: event.context,
           page: (event?.notificationRequestModel?.page > 1)
               ? event.notificationRequestModel?.page - 1
               : event.notificationRequestModel?.page,
           message: (event?.notificationRequestModel?.page > 1)
               ? ""
               : AppLocalizations.of(event.context)
                   .common
                   .error
                   .somethingWentWrong,
           notificationResponseModel: event.notificationResponseModel,
         );
       }


    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/notification/api/model/notification_request_model.dart';
import 'package:user/modules/notification/api/model/notification_response_model.dart';

abstract class NotificationEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final NotificationRequestModel notificationRequestModel;
  final NotificationResponseModel notificationResponseModel;

  NotificationEvent({
    this.isLoading: false,
    this.notificationRequestModel,
    this.context,
    this.notificationResponseModel,
  });
}

// for get notification list
class FetchNotification extends NotificationEvent {
  FetchNotification({
    NotificationRequestModel notificationRequestModel,
    bool isLoading,
    BuildContext context,
    NotificationResponseModel notificationResponseModel,
  }) : super(
          notificationRequestModel: notificationRequestModel,
          context: context,
          isLoading: isLoading,
          notificationResponseModel: notificationResponseModel,
        );
}

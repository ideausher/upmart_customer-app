import 'package:flutter/cupertino.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/notification/api/model/notification_response_model.dart';


class NotificationState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final String message;
  final NotificationResponseModel notificationResponseModel;
  final int page;

  NotificationState({
    this.isLoading: false,
    this.notificationResponseModel,
    this.context,
    this.message,
    this.page,
  }) : super(isLoading);

  // not authenticated
  factory NotificationState.initiating({bool isLoading}) {
    return NotificationState(
      isLoading: isLoading,
    );
  }

  // not authenticated
  factory NotificationState.getNotifications({
    bool isLoading,
    NotificationResponseModel notificationResponseModel,
    BuildContext context,
    String message,
    int page,
  }) {
    return NotificationState(
      isLoading: isLoading,
      context: context,
      notificationResponseModel: notificationResponseModel,
      message: message,
      page: page,
    );
  }
}

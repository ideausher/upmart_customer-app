import 'package:flutter/material.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/favourite_shop/fav_bloc/favourite_shop_bloc.dart';
import 'package:user/modules/favourite_shop/fav_bloc/favourite_shop_event.dart';
import 'package:user/modules/favourite_shop/fav_bloc/favourite_shop_state.dart';

class FavShopActionManager {

  // context
  BuildContext context;
  // bloc
  FavShopBloc favShopBloc = FavShopBloc();
  FavShopState favShopState;

  callFavShopListEvent({int page, int limit, ScaffoldState scaffoldState,BuildContext context}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        favShopBloc?.emitEvent(GetFavShopListEvent(
            shopsListResponseModel: favShopState?.shopsListResponseModel,
            page: page,
            isLoading: true,
            limit: favShopState?.limit,
            context: context
        ));
      }
    });
  }

  callMakeShopFavEvent(
      {int page, int limit, ScaffoldState scaffoldState, int shopId, int isFav,BuildContext context}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        favShopBloc?.emitEvent(MakeFavShopEvent(
            shopsListResponseModel: favShopState?.shopsListResponseModel,
            page: favShopState?.page,
            isLoading: true,
            limit: favShopState?.limit,
            isFav: isFav,
            shopId: shopId,
            context: context
        ));
      }
    });
  }

}
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/provider/category_shop_provider.dart';
import 'package:user/modules/favourite_shop/api/provider/fav_list_provider.dart';
import 'package:user/modules/favourite_shop/fav_bloc/favourite_shop_event.dart';
import 'package:user/modules/favourite_shop/fav_bloc/favourite_shop_state.dart';

class FavShopBloc extends BlocEventStateBase<FavShopsEvent, FavShopState> {
  FavShopBloc({bool isLoading = false})
      : super(initialState: FavShopState.initiating(isLoading: isLoading));

  @override
  Stream<FavShopState> eventHandler(FavShopsEvent event,
      FavShopState currentState) async* {
    // used for the select tab
    if (event is GetFavShopListEvent) {
      String _message = "";
      ShopsListResponseModel _shopListResponseModel =
          event?.shopsListResponseModel;

      yield FavShopState.updateUi(
          isLoading: true,
          context: event.context,
          limit: 10,
          page: event?.page,
          shopsListResponseModel: _shopListResponseModel);

      var shopListResult = await FavShopApiProvider()
          .getFavListData(context: event.context, limit: 10, page: event?.page);

      int _page = event.page;
      if (shopListResult != null) {
        // check result status
        if (shopListResult[ApiStatusParams.Status.value] != null &&
            shopListResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          var _shopListModel = ShopsListResponseModel.fromJson(shopListResult);

          if (_shopListModel?.data?.isNotEmpty == true) {
            if (_page > 1) {
              _shopListResponseModel?.data?.addAll(_shopListModel?.data);
            } else {
              _shopListResponseModel = _shopListModel;
            }
          } else {
            _page = _page - 1;
          }
        } else {
          if (_page > 1) {
            _page = _page - 1;
          }
        }
      } else {
        if (_page > 1) {
          _page = _page - 1;
        }
      }

      yield FavShopState.updateUi(
          isLoading: false,
          context: event.context,
          limit: event?.limit,
          page: _page,
          shopsListResponseModel: _shopListResponseModel);
    }

    if (event is MakeFavShopEvent) {
      String _message = "";
      ShopsListResponseModel _shopListResponseModel =
          event?.shopsListResponseModel;

      yield FavShopState.updateUi(
          isLoading: true,
          context: event.context,
          limit: 10,
          page: event?.page,
          shopId: event?.shopId,
          isFav: event?.isFav,
          shopsListResponseModel: event?.shopsListResponseModel);
      print('The length first is ${_shopListResponseModel?.data?.length}');

      var result = await CategoryShopProvider().callFavUnFavApi(
          context: event.context, shopId: event.shopId, isFav: event?.isFav);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          ShopDetailsModel _shopListModel =
          ShopDetailsModel.fromJson(result["data"]);
          event.shopsListResponseModel.data.removeWhere((item) =>
          item.id == event?.shopId);
        }
      }

      print("The length is ${ event?.shopsListResponseModel?.data?.length}");
      yield FavShopState.updateUi(
          isLoading: false,
          context: event.context,
          limit: 10,
          page: event?.page,
          shopId: event?.shopId,
          isFav: event?.isFav,
          shopsListResponseModel: event?.shopsListResponseModel);
    }
  }
}

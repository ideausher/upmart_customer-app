import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';

abstract class FavShopsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  ShopsListResponseModel shopsListResponseModel;
  final int limit, page, shopId, isFav;

  FavShopsEvent(
      {this.isLoading: false,
      this.context,
      this.shopsListResponseModel,
      this.limit,
      this.isFav,
      this.shopId,
      this.page});
}

// for showing data
class GetFavShopListEvent extends FavShopsEvent {
  GetFavShopListEvent(
      {BuildContext context,
      bool isLoading,
      ShopsListResponseModel shopsListResponseModel,
      int limit,
      int isFav,
      int shopId,
      int page})
      : super(
            context: context,
            isLoading: isLoading,
            limit: limit,
            page: page,
            shopId: shopId,
            isFav: isFav,
            shopsListResponseModel: shopsListResponseModel);
}

// for showing data
class MakeFavShopEvent extends FavShopsEvent {
  MakeFavShopEvent(
      {BuildContext context,
      bool isLoading,
      ShopsListResponseModel shopsListResponseModel,
      int limit,
      int isFav,
      int shopId,
      int page})
      : super(
            context: context,
            isLoading: isLoading,
            limit: limit,
            page: page,
            shopId: shopId,
            isFav: isFav,
            shopsListResponseModel: shopsListResponseModel);
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';

class FavShopState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final ShopsListResponseModel shopsListResponseModel;
  final int page, limit, isFav, shopId;

  FavShopState(
      {this.isLoading,
      this.context,
      this.shopsListResponseModel,
      this.limit,
      this.isFav,
      this.shopId,
      this.page})
      : super(isLoading);

  // not authenticated
  factory FavShopState.initiating({bool isLoading}) {
    return FavShopState(
      isLoading: isLoading,
    );
  }

  factory FavShopState.updateUi(
      {bool isLoading,
      BuildContext context,
      ShopsListResponseModel shopsListResponseModel,
      int page,
      int limit,
      int shopId,
      int isFav}) {
    return FavShopState(
        isLoading: isLoading,
        context: context,
        shopsListResponseModel: shopsListResponseModel,
        limit: limit,
        page: page,
        isFav: isFav,
        shopId: shopId);
  }
}

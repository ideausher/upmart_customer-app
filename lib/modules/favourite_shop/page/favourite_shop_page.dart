import 'package:clippy_flutter/label.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/coupon_listing/enums/coupon_enums.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/managers/categories_shop_utils_manager.dart';
import 'package:user/modules/favourite_shop/fav_bloc/favourite_shop_state.dart';
import 'package:user/modules/favourite_shop/managers/fav_shop_action_manager.dart';
import 'package:intl/intl.dart';

class FavShopListing extends StatefulWidget {
  BuildContext context;

  FavShopListing(this.context);

  @override
  _FavShopListingState createState() => _FavShopListingState();
}

class _FavShopListingState extends State<FavShopListing> {
  FavShopActionManager _favShopActionManager;

  //initialise controllers
  ScrollController _scrollController = ScrollController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  num _page = 1;

  @override
  void initState() {
    super.initState();
    _favShopActionManager = new FavShopActionManager();
    _scrollController.addListener(_scrollListener);
    _callFavShopListingApi();
  }

  @override
  void dispose() {
    super.dispose();
    _favShopActionManager?.favShopBloc?.dispose();
  }

  ///scroll listener
  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange &&
        (_favShopActionManager
                ?.favShopState?.shopsListResponseModel?.data?.length ==
            (_page * 10))) {
      _favShopActionManager?.callFavShopListEvent(
          scaffoldState: _scaffoldKey?.currentState, page: _page);
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {}
  }

  @override
  Widget build(BuildContext context) {
    _favShopActionManager?.context = widget?.context;
    return BlocEventStateBuilder<FavShopState>(
      bloc: _favShopActionManager?.favShopBloc,
      builder: (BuildContext buildContext, FavShopState faqState) {
        if (_favShopActionManager?.favShopState != faqState) {
          _favShopActionManager.context = context;
          _favShopActionManager.favShopState = faqState;
        }
        // main ui started
        return ModalProgressHUD(
          inAsyncCall: _favShopActionManager?.favShopState?.isLoading ?? false,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: _showAppBar(),
            body: Padding(
                padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
                child: (_favShopActionManager?.favShopState
                            ?.shopsListResponseModel?.data?.isNotEmpty ==
                        true)
                    ? ListView.builder(
                        itemCount: _favShopActionManager?.favShopState
                            ?.shopsListResponseModel?.data?.length,
                        shrinkWrap: true,
                        controller: _scrollController,
                        key: PageStorageKey("FavShopListing"),
                        padding: EdgeInsets.only(top: SIZE_5),
                        itemBuilder: (BuildContext context, int index) {
                          ShopDetailsModel _shopData = _favShopActionManager
                              ?.favShopState
                              ?.shopsListResponseModel
                              ?.data[index];
                          return InkWell(
                            onTap: () {
                              NetworkConnectionUtils
                                  .networkConnectionUtilsInstance
                                  .getConnectivityStatus(context,
                                      showNetworkDialog: true)
                                  .then((onValue) {
                                if (onValue) {
                                  /*   _actionManager?.actionToOpenDetailPage(
                            shopDetails: _shopData);*/
                                }
                              });
                            },
                            child: Card(
                              margin: EdgeInsets.only(
                                  bottom: SIZE_10, top: SIZE_10),
                              clipBehavior: Clip.antiAlias,
                              elevation: ELEVATION_03,
                              color: (_shopData?.shopAvailability ==
                                      ShopAvailability.available.value)
                                  ? Colors.white
                                  : COLOR_GREY_DEC,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(SIZE_10)),
                                  side: BorderSide(
                                      width: SIZE_1, color: COLOR_BORDER_GREY)),
                              child: ColorFiltered(
                                colorFilter: ColorFilter.mode(
                                  (_shopData?.shopAvailability ==
                                          ShopAvailability.available.value)
                                      ? Colors.transparent
                                      : COLOR_GREY_DEC,
                                  BlendMode.saturation,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    (_shopData?.media?.isNotEmpty == true &&
                                            _shopData?.media[0]?.fileName
                                                    ?.isNotEmpty ==
                                                true)
                                        ? Container(
                                            alignment: Alignment.center,
                                            height: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                    context:
                                                        _favShopActionManager
                                                            ?.context,
                                                    percentage: SIZE_20,
                                                    ofWidth: false),
                                            child: Stack(
                                              alignment: Alignment.topRight,
                                              children: [
                                                ImageUtils.imageUtilsInstance
                                                    .showCacheNetworkImage(
                                                        context: context,
                                                        radius: SIZE_0,
                                                        showProgressBarInPlaceHolder:
                                                            true,
                                                        shape:
                                                            BoxShape.rectangle,
                                                        url: (_shopData?.media
                                                                        ?.isNotEmpty ==
                                                                    true &&
                                                                _shopData
                                                                        ?.media[
                                                                            0]
                                                                        ?.fileName
                                                                        ?.isNotEmpty ==
                                                                    true)
                                                            ? _shopData
                                                                ?.media[0]
                                                                ?.fileName
                                                            : (_shopData
                                                                        ?.profileImage
                                                                        ?.fileName
                                                                        ?.isNotEmpty ==
                                                                    true)
                                                                ? _shopData
                                                                    ?.profileImage
                                                                    ?.fileName
                                                                : "",
                                                        width: CommonUtils
                                                            .commonUtilsInstance
                                                            .getPercentageSize(
                                                                context: _favShopActionManager
                                                                    ?.context,
                                                                percentage:
                                                                    SIZE_100,
                                                                ofWidth: true)),
                                                InkWell(
                                                  onTap: () {
                                                    _makeShopFavourite(
                                                        _shopData);
                                                  },
                                                  child: Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        top: SIZE_10,
                                                        right: SIZE_10),
                                                    child: Container(
                                                      padding: EdgeInsets.all(
                                                          SIZE_2),
                                                      decoration: BoxDecoration(
                                                          shape:
                                                          BoxShape.circle,
                                                          color: COLOR_LIGHT_WHITE),
                                                      child: Icon(
                                                        (_shopData?.isFav == 0)
                                                            ? Icons
                                                            .favorite_border
                                                            : Icons.favorite,
                                                        size: SIZE_24,
                                                        color: Colors.red,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        : Stack(
                                      alignment: Alignment.topRight,
                                      children: [
                                        Container(
                                          width: CommonUtils
                                              .commonUtilsInstance
                                              .getPercentageSize(
                                              context:
                                              _favShopActionManager
                                                  ?.context,
                                              percentage: SIZE_100,
                                              ofWidth: true),
                                          color: Colors.transparent,
                                          child: SvgPicture.asset(
                                            SHOP_LOGO,
                                            height: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                context:
                                                _favShopActionManager
                                                    ?.context,
                                                percentage: SIZE_20,
                                                ofWidth: false),
                                            width: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                context:
                                                _favShopActionManager
                                                    ?.context,
                                                percentage: SIZE_100,
                                                ofWidth: true),
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            _makeShopFavourite(
                                                _shopData);
                                          },
                                          child:Padding(
                                            padding:
                                            const EdgeInsets.only(
                                                top: SIZE_10,
                                                right: SIZE_10),
                                            child: Container(
                                              padding: EdgeInsets.all(
                                                  SIZE_2),
                                              decoration: BoxDecoration(
                                                  shape:
                                                  BoxShape.circle,
                                                  color: COLOR_LIGHT_WHITE),
                                              child: Icon(
                                                (_shopData?.isFav == 0)
                                                    ? Icons
                                                    .favorite_border
                                                    : Icons.favorite,
                                                size: SIZE_24,
                                                color: Colors.red,
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: SIZE_20),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: SIZE_10),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                child: Text(
                                                  toBeginningOfSentenceCase(
                                                      _shopData?.name ??
                                                          AppLocalizations.of(
                                                                  context)
                                                              .common
                                                              .text
                                                              .notAllowed),
                                                  style: AppConfig.of(
                                                          _favShopActionManager
                                                              ?.context)
                                                      .themeData
                                                      .primaryTextTheme
                                                      .headline6,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                ),
                                              ),
                                              (_shopData?.primaryCoupon?.code
                                                          ?.isNotEmpty ==
                                                      true)
                                                  ? Label(
                                                      triangleHeight: 10.0,
                                                      edge: Edge.LEFT,
                                                      child: Container(
                                                        color: COLOR_GREEN_TEXT,
                                                        height: SIZE_22,
                                                        width: CommonUtils
                                                            .commonUtilsInstance
                                                            .getPercentageSize(
                                                                context:
                                                                    context,
                                                                ofWidth: true,
                                                                percentage:
                                                                    SIZE_40),
                                                        child: Center(
                                                            child: Text(
                                                                (_shopData?.primaryCoupon
                                                                            ?.type ==
                                                                        CouponType
                                                                            ?.Fixed
                                                                            ?.value)
                                                                    ? NumberFormatUtils
                                                                        .numberFormatUtilsInstance
                                                                        .formatPriceWithSymbol(
                                                                            price: _shopData
                                                                                ?.primaryCoupon?.discount)
                                                                    : "${_shopData?.primaryCoupon?.discount}% ${AppLocalizations.of(context).shopdetailspage.text.off}" ??
                                                                        "",
                                                                style:
                                                                    textStyleSize12WithWhiteColor)),
                                                      ),
                                                    )
                                                  : const SizedBox()
                                            ],
                                          ),
                                          SizedBox(height: SIZE_10),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: SIZE_20),
                                            child: Text(
                                              toBeginningOfSentenceCase(
                                                  _shopData?.description ??
                                                      AppLocalizations.of(
                                                              context)
                                                          .common
                                                          .text
                                                          .notAllowed),
                                              style: textStyleSize12WithBLACK,
                                              textAlign: TextAlign.justify,
                                            ),
                                          ),
                                          SizedBox(height: SIZE_10),
                                          Row(
                                            children: <Widget>[
                                              Row(
                                                children: [
                                                  Icon(
                                                    Icons.star,
                                                    color: COLOR_PRIMARY,
                                                    size: SIZE_16,
                                                  ),
                                                  SizedBox(
                                                    width: SIZE_5,
                                                  ),
                                                  Text(
                                                    _shopData?.rating
                                                            ?.toStringAsFixed(1) ??
                                                        "",
                                                    style:
                                                        textStyleSize14WithGreenColor,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                width: SIZE_16,
                                              ),
                                              Row(
                                                children: [
                                                  Image.asset(
                                                    CLOCK_IMAGE,
                                                    color: COLOR_LIGHT_GREY,
                                                    height: SIZE_14,
                                                    width: SIZE_14,
                                                  ),
                                                  SizedBox(
                                                    width: SIZE_5,
                                                  ),
                                                  Text(
                                                    CategoriesShopUtilsManager
                                                            .homeUtilsInstance
                                                            ?.getShopDistanceTime(
                                                                expTime: _shopData
                                                                    ?.expTime) ??
                                                        "",
                                                    style:
                                                        textStyleSize12WithBLACK,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                width: SIZE_16,
                                              ),
                                              Row(
                                                children: [
                                                  Image.asset(
                                                    CAR_IMAGE,
                                                    color: COLOR_LIGHT_GREY,
                                                    height: SIZE_14,
                                                    width: SIZE_14,
                                                  ),
                                                  SizedBox(
                                                    width: SIZE_5,
                                                  ),
                                                  Text(
                                                    '${_shopData?.distance?.toString()} ${AppLocalizations.of(_favShopActionManager?.context).common.text.km.toLowerCase()}' ??
                                                        "" +
                                                            "${AppLocalizations.of(_favShopActionManager?.context).common.text.km.toLowerCase()}",
                                                    style:
                                                        textStyleSize12WithBLACK,
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          SizedBox(height: SIZE_10),
                                          _showShopType(
                                              shopType: _shopData?.shopType),
                                          SizedBox(height: SIZE_10),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        })
                    : (_favShopActionManager?.favShopState?.isLoading == false)
                        ? Align(
                            alignment: Alignment.center,
                            child: Container(
                              alignment: Alignment.center,
                              height: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                      context: _favShopActionManager?.context,
                                      percentage: SIZE_50,
                                      ofWidth: false),
                              child: CommonImageWithTextWidget(
                                context: _favShopActionManager?.context,
                                localImagePath: SHOP_LOGO,
                                imageHeight: SIZE_7,
                                imageWidth: SIZE_7,
                                textToShow: AppLocalizations.of(context)
                                    .categoriesshoppage
                                    .text
                                    .noShopFound,
                              ),
                            ),
                          )
                        : const SizedBox()),
          ),
        );
      },
    );
  }

  void _callFavShopListingApi() {
    _favShopActionManager?.callFavShopListEvent(
        page: _page,
        limit: 10,
        context: widget?.context,
        scaffoldState: _scaffoldKey?.currentState);
  }

  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _favShopActionManager?.context,
        elevation: ELEVATION_0,
        appBarTitle:AppLocalizations.of(_favShopActionManager?.context).favouritepage.text.appBarTitle,
        appBarTitleStyle: AppConfig.of(_favShopActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show shop type in flutter
  Widget _showShopType({int shopType}) {
    if (shopType == ShopType?.both?.value) {
      return _showDeliveryAndTakeAway();
    } else {
      return _showDeliveryOrTakeAway(shopType);
    }
  }

  //this method will show both delivery and take away option on shop listing
  Widget _showDeliveryAndTakeAway() {
    return Row(
      children: [
        DottedBorder(
          strokeWidth: 1,
          color: COLOR_DARK_PRIMARY,
          dashPattern: [2, 4],
          strokeCap: StrokeCap.round,
          child: Container(
            padding: EdgeInsets.only(left: SIZE_2, right: SIZE_2),
            child: Text(
              AppLocalizations?.of(_favShopActionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.delivery,
              style: textStyleSize12WithPrimary,
            ),
          ),
        ),
        SizedBox(
          width: SIZE_16,
        ),
        DottedBorder(
          strokeWidth: 1,
          color: COLOR_DARK_PRIMARY,
          dashPattern: [2, 4],
          strokeCap: StrokeCap.round,
          child: Container(
            padding: EdgeInsets.only(left: SIZE_2, right: SIZE_2),
            child: Text(
              AppLocalizations?.of(_favShopActionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.takeAway,
              style: textStyleSize12WithPrimary,
            ),
          ),
        )
      ],
    );
  }

  //this method will return delivery or take away option based on its type
  Widget _showDeliveryOrTakeAway(int shopType) {
    return DottedBorder(
      strokeWidth: 1,
      color: COLOR_DARK_PRIMARY,
      child: Container(
        padding: EdgeInsets.only(left: SIZE_2, right: SIZE_2),
        child: Text(
          (shopType == ShopType?.pickUp?.value)
              ? /*AppLocalizations?.of(_actionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.takeAway*/
             AppLocalizations?.of(_favShopActionManager?.context)?.common?.text?.pickUp?.toUpperCase()
              : AppLocalizations?.of(_favShopActionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.delivery
                  ?.toUpperCase(),
          style: textStyleSize12WithPrimary,
        ),
      ),
    );
  }

  void _makeShopFavourite(ShopDetailsModel shopData) {
    _favShopActionManager?.callMakeShopFavEvent(
        page: _favShopActionManager?.favShopState?.page,
        scaffoldState: _scaffoldKey?.currentState,
        shopId: shopData?.id,
        context: _favShopActionManager?.context,
        isFav: shopData?.isFav);
  }
}

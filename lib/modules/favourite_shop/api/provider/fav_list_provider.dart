import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';

class FavShopApiProvider {
  //get faq data api call
  Future<dynamic> getFavListData(
      {BuildContext context, int limit, int page}) async {
    print("fav list $page");
    var faq = "favourites/shops";
    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(faq, context, queryParameters: {
      "limit": 10,
      "page": page,
    });
    return result;
  }
}

enum AddressRoles { Primary, NonPrimary }

extension UserRolesExtension on AddressRoles {
  int get value {
    switch (this) {
      case AddressRoles.Primary:
        return 1;
      case AddressRoles.NonPrimary:
        return 0;
      default:
        return null;
    }
  }
}

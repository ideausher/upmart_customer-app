import 'package:flutter/material.dart';
import 'package:user/modules/address/api/model/add_address/add_address_request_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';

import 'package:user/modules/common/app_config/app_config.dart';

class AddressProvider {
  Future<dynamic> addAddressApiCall({BuildContext context, AddAddressRequestModel addressRequestModel}) async {
    var addAddress = "add-address";

    //api call to add address
    var result = await AppConfig.of(context)
        .baseApi
        .postRequest(addAddress, context, data: addAddressRequestModelToJson(addressRequestModel));

    return result;
  }

  //api call to remove address
  Future<dynamic> removeAddressApiCall({BuildContext context, String addressId}) async {
    var addAddress = "delete-address";
    var result = await AppConfig.of(context).baseApi.postRequest(addAddress, context, data: {"address_id": addressId});

    return result;
  }

  //api call to make address primary
  Future<dynamic> makeAddressPrimaryApiCall(
      {BuildContext context, EditAddressRequestModel editAddressRequestModel}) async {
    var addAddress = "edit-address";
    var result = await AppConfig.of(context)
        .baseApi
        .postRequest(addAddress, context, data: editAddressRequestModelToJson(editAddressRequestModel));

    return result;
  }

  //get addresses api call
  Future<dynamic> getAddressesList({BuildContext context}) async {
    var addAddress = "get-address";
    var result = await AppConfig.of(context).baseApi.postRequest(addAddress, context);

    return result;
  }
}

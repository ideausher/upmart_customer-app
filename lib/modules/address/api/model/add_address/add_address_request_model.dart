// To parse this JSON data, do
//
//     final addAddressRequestModel = addAddressRequestModelFromJson(jsonString);

import 'dart:convert';

AddAddressRequestModel addAddressRequestModelFromJson(String str) => AddAddressRequestModel.fromJson(json.decode(str));

String addAddressRequestModelToJson(AddAddressRequestModel data) => json.encode(data.toJson());

class AddAddressRequestModel {
  AddAddressRequestModel({
    this.address,
  });

  List<UserAddresses> address;

  factory AddAddressRequestModel.fromJson(Map<String, dynamic> json) => AddAddressRequestModel(
        address: json["address"] == null
            ? null
            : List<UserAddresses>.from(json["address"].map((x) => UserAddresses.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "address": address == null ? null : List<dynamic>.from(address.map((x) => x.toJson())),
      };
}

class UserAddresses {
  UserAddresses(
      {this.city,
      this.country,
      this.addressType,
      this.formattedAddress,
      this.additionalInfo,
      this.latitude,
      this.longitude,
      this.pincode,
      this.isPrimary});

  String city;
  String country;
  String addressType;
  String formattedAddress;
  String additionalInfo;
  double latitude;
  double longitude;
  String pincode;
  int isPrimary;

  factory UserAddresses.fromJson(Map<String, dynamic> json) => UserAddresses(
        isPrimary: json["is_primary"] == null ? null : json["is_primary"],
        city: json["city"] == null ? null : json["city"],
        country: json["country"] == null ? null : json["country"],
        addressType: json["address_type"] == null ? null : json["address_type"],
        formattedAddress: json["formatted_address"] == null ? null : json["formatted_address"],
        additionalInfo: json["additional_info"] == null ? "hiii" : json["additional_info"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude: json["longitude"] == null ? null : json["longitude"].toDouble(),
        pincode: json["pincode"] == null ? null : json["pincode"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "is_primary": isPrimary == null ? null : isPrimary,
        "city": city == null ? null : city,
        "country": country == null ? null : country,
        "address_type": addressType == null ? null : addressType,
        "formatted_address": formattedAddress == null ? null : formattedAddress,
        "additional_info": additionalInfo == null ? "hiii" : additionalInfo,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "pincode": pincode == null ? null : pincode,
      };
}

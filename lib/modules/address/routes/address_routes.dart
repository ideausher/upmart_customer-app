import 'package:flutter/material.dart';
import 'package:user/modules/address/page/address_listing/page/address_listing_page.dart';
import 'package:user/modules/address/page/select_delivery_address/page/select_delivery_address_page.dart';
import 'package:user/modules/address/page/set_address/page/set_address_page.dart';


class AddressRoutes {
  static const String SET_ADDRESS = '/SET_ADDRESS';
  static const String SET_DELIVERY_ADDRESS = '/SET_DELIVERY_ADDRESS';
  static const String ADDRESS_LIST = '/ADDRESS_LIST';

  static Map<String, WidgetBuilder> routes() {
    return {
      SET_ADDRESS: (context) => SetAddressPage(context),
      SET_DELIVERY_ADDRESS: (context) => SelectDeliveryAddressPage(context),
      ADDRESS_LIST: (context) => AddressListingPage(context),
    };
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/enum/address_enum.dart';
import 'package:user/modules/address/page/address_listing/bloc/address_list_bloc.dart';
import 'package:user/modules/address/page/address_listing/bloc/address_list_state.dart';
import 'package:user/modules/address/page/address_listing/managers/address_list_utils_manager.dart';
import 'package:user/modules/address/page/address_listing/managers/addresss_list_action_managers.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';

class AddressListingPage extends StatefulWidget {
  BuildContext context;
  AuthResponseModel _authResponseModel;

  AddressListingPage(this.context) {
    _authResponseModel = ModalRoute.of(context).settings.arguments;
  }

  @override
  _AddressListingPageState createState() => _AddressListingPageState();
}

class _AddressListingPageState extends State<AddressListingPage> {
  //class variables
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Auth bloc variables
  AuthState _authState;
  AuthBloc _authBloc;
  AuthManager _authManager;

  //Address bloc
  AddressListBloc _addressListBloc;
  AddressListActionManagers _addressListActionManagers;

  AddressListResponseModel _addressListResponseModel;
  AddressListState _addressListState;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _addressListBloc = new AddressListBloc();
    _addressListResponseModel = new AddressListResponseModel();
    _authManager = new AuthManager();
    _addressListActionManagers =
        AddressListActionManagers(context, _addressListBloc);
    _callAddressListing(widget?._authResponseModel);
  }

  @override
  void dispose() {
    super.dispose();
    _addressListBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return WillPopScope(
      onWillPop: () async {
        CurrentLocation _currentLocation = await FetchPrefsUtils
            .fetchPrefsUtilsInstance
            .getCurrentLocationModel();
        return NavigatorUtils.navigatorUtilsInstance
            .navigatorPopScreen(context, dataToBeSend: _currentLocation);
      },
      child: SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: _showAppBar(),
          body: BlocEventStateBuilder<AuthState>(
            bloc: _authBloc,
            builder: (BuildContext context, AuthState authState) {
              _context = context;
              if (_authState != authState) {
                _authState = authState;
                _authManager.actionOnStateUpdate(
                    context: _context,
                    authBloc: _authBloc,
                    authState: _authState,
                    scaffoldState: _scaffoldKey.currentState);
              }
              return BlocEventStateBuilder<AddressListState>(
                bloc: _addressListBloc,
                builder:
                    (BuildContext context, AddressListState addressListState) {
                  if (addressListState != null &&
                      _addressListState != addressListState) {
                    _context = context;
                    _addressListActionManagers?.context = context;
                    _addressListState = addressListState;
                    _addressListActionManagers?.actionAddressListStateChange(
                        context: context,
                        scaffoldState: _scaffoldKey?.currentState,
                        authState: _authState,
                        addressListState: _addressListState,
                        authBloc: _authBloc);
                  }
                  return ModalProgressHUD(
                    inAsyncCall: (_authState?.isLoading ?? false) ||
                        (_addressListState?.isLoading ?? false),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        _showDivider(),
                        SizedBox(
                          height: SIZE_10,
                        ),
                        _showAddressList(),
                        _addAddressWidget()
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: false,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () async {
          CurrentLocation _currentLocation = await FetchPrefsUtils
              .fetchPrefsUtilsInstance
              .getCurrentLocationModel();
          return NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(_context, dataToBeSend: _currentLocation);
        },
        appBarTitle: AppLocalizations.of(context).addresslistingpage.appbartitle.myAddress,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_16),
            child: NotificationReadCountWidget(
              context: _context,
            ),
          )
        ],
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }



  //show divider
  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1_5,
    );
  }

  //method to show address list view
  Widget _showAddressList() {
    return Expanded(
      child: (_addressListState?.addressListResponseModel?.data?.isNotEmpty ==
              true)
          ? ListView.builder(
              padding: EdgeInsets.only(
                  left: SIZE_12, right: SIZE_12, bottom: SIZE_5),
              itemCount:
                  _addressListState?.addressListResponseModel?.data?.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                Datum address =
                    _addressListState?.addressListResponseModel?.data[index];
                return IntrinsicHeight(
                  child: InkWell(
                    onTap: () async {

                      CurrentLocation _currentLocation = new CurrentLocation(
                          id: address?.id.toString(),
                          lat: address?.latitude,
                          lng: address?.longitude,
                          city: address?.city,
                          country: address?.country,
                          postalCode: address?.pincode,
                          currentAddress: address?.formattedAddress);
                      await SharedPrefUtils.sharedPrefUtilsInstance
                          .saveLocationObject(_currentLocation,
                              PrefsEnum.UserLocationData.value);
                      if (widget._authResponseModel?.popScreen == true) {

                        NavigatorUtils.navigatorUtilsInstance
                            .navigatorPopScreen(context,
                                dataToBeSend: _currentLocation);
                      } else {

                        _addressListActionManagers?.actionToUpdateAddressUi(
                            addressListState: _addressListState,currentLocation: _currentLocation);
                      }
                    },
                    child: Card(
                      elevation: ELEVATION_05,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(SIZE_5)),
                          side: BorderSide(
                              width: SIZE_1,
                              color: (address?.id?.toString() ==
                                      _addressListState?.currentLocation?.id)
                                  ? COLOR_PRIMARY
                                  : Colors.transparent)),
                      child: Padding(
                        padding: const EdgeInsets.all(SIZE_12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  _authState
                                          ?.authResponseModel?.userData?.name ??
                                      "",
                                  style: AppConfig.of(_context)
                                      .themeData
                                      .primaryTextTheme
                                      .headline6,
                                ),
                                (address?.isPrimary ==
                                        AddressRoles.Primary.value)
                                    ? Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(SIZE_5)),
                                          color: COLOR_PRIMARY,
                                        ),
                                        padding: EdgeInsets.only(
                                            left: SIZE_5,
                                            right: SIZE_5,
                                            top: SIZE_3,
                                            bottom: SIZE_3),
                                        child: Text(
                                          (address?.isPrimary ==
                                                  AddressRoles.Primary.value)
                                              ? AppLocalizations.of(context).addresslistingpage.text.primary
                                              : "",
                                          style: textStyleSize12WithWhiteColor,
                                        ),
                                      )
                                    : const SizedBox()
                              ],
                            ),
                            SizedBox(
                              height: SIZE_10,
                            ),
                            Text(
                              address?.formattedAddress,
                              style: textStyleSize14WithGREY,
                            ),
                            /*Text("${address?.city},${address?.country}",
                                style: textStyleSize14WithGREY)*/
                            SizedBox(
                              height: SIZE_10,
                            ),
                            SizedBox(
                              height: SIZE_10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                (address?.isPrimary ==
                                        AddressRoles.Primary.value)
                                    ? const SizedBox()
                                    : InkWell(
                                        onTap: () {
                                          NetworkConnectionUtils
                                              .networkConnectionUtilsInstance
                                              .getConnectivityStatus(context,
                                                showNetworkDialog: true)
                                              .then((onValue) {
                                            if (onValue) {
                                              // hide keyboard
                                              AddressListUtilsManager
                                                  .addressListUtilsManager
                                                  .openRemoveAddressDialog(
                                                      address,
                                                      actionManager:
                                                          _addressListActionManagers,
                                                      key: _scaffoldKey,
                                                      context: _context,
                                                      authState: _authState,
                                                      addressListState:
                                                          _addressListState);
                                            }
                                          });
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(SIZE_8),
                                          child: Text(
                                            AppLocalizations?.of(_context)
                                                ?.common
                                                ?.text
                                                ?.remove,
                                            style: textStyleSize12WithRed,
                                          ),
                                        ),
                                      ),
                                InkWell(
                                  onTap: () {
                                    print('Calling edit');
                                    AddressListUtilsManager
                                        .addressListUtilsManager
                                        .callEditAddress(address, _context,
                                            _addressListActionManagers,
                                            pop: widget
                                                ._authResponseModel?.popScreen,
                                            scaffoldState:
                                                _scaffoldKey?.currentState,
                                            authResponseModel:
                                                _authState?.authResponseModel,
                                            addressListResponseModel:
                                                _addressListState
                                                    ?.addressListResponseModel,
                                            addressListState:
                                                _addressListState);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(SIZE_8),
                                    child: Text(
                                      AppLocalizations.of(_context)
                                          .addresslistingpage.text.edit,
                                      style: textStyleSize12WithPrimary,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: SIZE_10,
                            ),
                            (address?.isPrimary != AddressRoles.Primary.value)
                                ? Align(
                                    alignment: Alignment.center,
                                    child: InkWell(
                                      onTap: () {
                                        _changePrimaryAddress(address);
                                      },
                                      child: Text(
                                       AppLocalizations.of(context).addresslistingpage.text.setPrimary,
                                        style: textStyleSize12WithPrimary,
                                      ),
                                    ),
                                  )
                                : const SizedBox()
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              })
          : _addressListState?.isLoading == true ||
                  _authState?.isLoading == true
              ? const SizedBox()
              : Container(
                  alignment: Alignment.center,
                  child: Text(AppLocalizations?.of(_context)
                      .addresslistingpage.text.noAddressAdded),
                ),
    );
  }

  //this method is used to get users address listing
  void _callAddressListing(AuthResponseModel authResponseModel) {
    _addressListActionManagers?.callAddressListEvent(
        addressListState: _addressListState,
        authResponseModel:
            widget?._authResponseModel ?? _authState?.authResponseModel,
        addressListResponseModel: _addressListState?.addressListResponseModel ??
            _addressListResponseModel,
        scaffoldState: _scaffoldKey?.currentState);
  }

  //this method is used to change primary address
  void _changePrimaryAddress(Datum address) {
    _addressListActionManagers?.callChangePrimaryAddress(
        scaffoldState: _scaffoldKey?.currentState,
        addressListState: _addressListState,
        addressListResponseModel: _addressListState?.addressListResponseModel,
        authResponseModel: _authState?.authResponseModel,
        editAddressRequestModel: new EditAddressRequestModel(
            pincode: address?.pincode,
            latitude: address?.latitude,
            longitude: address?.longitude,
            addressType: AppLocalizations.of(_context).addresslistingpage.text.addressType,
            additionalInfo: address?.additionalInfo,
            addressId: address?.id.toString(),
            formattedAddress: address?.formattedAddress,
            isPrimary: AddressRoles.Primary.value,
            country: address?.country,
            city: address?.city));
  }

  //method to show add address widget
  Widget _addAddressWidget() {
    return Padding(
      padding: const EdgeInsets.all(SIZE_20),
      child: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () async {
            var result = await NavigatorUtils.navigatorUtilsInstance
                .navigatorPushedNameResult(context, AddressRoutes.SET_ADDRESS,
                    dataToBeSend: new EditAddressRequestModel(
                        isForEdit: false,
                        pop: widget._authResponseModel?.popScreen));

            _addressListActionManagers?.callAddressListEvent(
                addressListState: _addressListState,
                authResponseModel:
                    widget?._authResponseModel ?? _authState?.authResponseModel,
                addressListResponseModel:
                    _addressListState?.addressListResponseModel ??
                        _addressListResponseModel,
                scaffoldState: _scaffoldKey?.currentState);
          },
          child: Center(
            child: Text(
              '+${AppLocalizations.of(context).addresslistingpage.text.addAddres}',
              style: textStyleSize16WithGreenColor,
            ),
          )),
    );
  }
}

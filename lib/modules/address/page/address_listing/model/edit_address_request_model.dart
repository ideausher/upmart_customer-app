// To parse this JSON data, do
//
//     final editAddressRequestModel = editAddressRequestModelFromJson(jsonString);

import 'dart:convert';

EditAddressRequestModel editAddressRequestModelFromJson(String str) =>
    EditAddressRequestModel.fromJson(json.decode(str));

String editAddressRequestModelToJson(EditAddressRequestModel data) =>
    json.encode(data.toJson());

class EditAddressRequestModel {
  EditAddressRequestModel({
    this.addressId,
    this.addressType,
    this.isPrimary,
    this.city,
    this.country,
    this.formattedAddress,
    this.additionalInfo,
    this.latitude,
    this.longitude,
    this.pincode,
    this.pop = false,
    this.isForEdit: false,
  });

  String addressId;
  String addressType;
  num isPrimary;
  String city;
  String country;
  String formattedAddress;
  String additionalInfo;
  double latitude;
  double longitude;
  String pincode;
  bool isForEdit;
  bool pop;

  factory EditAddressRequestModel.fromJson(Map<String, dynamic> json) =>
      EditAddressRequestModel(
        addressId: json["address_id"] == null ? null : json["address_id"],
        addressType: json["address_type"] == null ? null : json["address_type"],
        isPrimary: json["is_primary"] == null ? null : json["is_primary"],
        city: json["city"] == null ? null : json["city"],
        country: json["country"] == null ? null : json["country"],
        formattedAddress: json["formatted_address"] == null
            ? null
            : json["formatted_address"],
        additionalInfo:
            json["additional_info"] == null ? "hiii" : json["additional_info"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        pincode: json["pincode"] == null ? null : json["pincode"],
        isForEdit: json["isForEdit"] == null ? null : json["isForEdit"],
        pop: json["pop"] == null ? null : json["pop"],
      );

  Map<String, dynamic> toJson() => {
        "address_id": addressId == null ? null : addressId,
        "address_type": addressType == null ? null : addressType,
        "is_primary": isPrimary == null ? null : isPrimary,
        "city": city == null ? null : city,
        "country": country == null ? null : country,
        "formatted_address": formattedAddress == null ? null : formattedAddress,
        "additional_info": additionalInfo == null ? "hii" : additionalInfo,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "pincode": pincode == null ? null : pincode,
        "isForEdit": isForEdit == null ? null : isForEdit,
        "pop": pop == null ? null : pop,
      };
}

// To parse this JSON data, do
//
//     final addressListResponseModel = addressListResponseModelFromJson(jsonString);

import 'dart:convert';

AddressListResponseModel addressListResponseModelFromJson(String str) =>
    AddressListResponseModel.fromMap(json.decode(str));

String addressListResponseModelToJson(AddressListResponseModel data) => json.encode(data.toJson());

class AddressListResponseModel {
  AddressListResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<Datum> data;
  String message;
  int statusCode;

  factory AddressListResponseModel.fromMap(Map<String, dynamic> json) => AddressListResponseModel(
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromMap(x))),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Datum {
  Datum({
    this.id,
    this.userId,
    this.type,
    this.city,
    this.country,
    this.formattedAddress,
    this.additionalInfo,
    this.latitude,
    this.longitude,
    this.createdAt,
    this.updatedAt,
    this.isPrimary,
    this.pincode,
  });

  int id;
  int userId;
  int type;
  String city;
  String country;
  String formattedAddress;
  String additionalInfo;
  double latitude;
  double longitude;
  dynamic createdAt;
  dynamic updatedAt;
  int isPrimary;
  String pincode;

  factory Datum.fromMap(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        type: json["type"] == null ? null : json["type"],
        city: json["city"] == null ? null : json["city"],
        country: json["country"] == null ? null : json["country"],
        formattedAddress: json["formatted_address"] == null ? null : json["formatted_address"],
        additionalInfo: json["additional_info"] == null ? null : json["additional_info"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude: json["longitude"] == null ? null : json["longitude"].toDouble(),
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        isPrimary: json["is_primary"] == null ? null : json["is_primary"],
        pincode: json["pincode"] == null ? null : json["pincode"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "type": type == null ? null : type,
        "city": city == null ? null : city,
        "country": country == null ? null : country,
        "formatted_address": formattedAddress == null ? null : formattedAddress,
        "additional_info": additionalInfo == null ? null : additionalInfo,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "is_primary": isPrimary == null ? null : isPrimary,
        "pincode": pincode == null ? null : pincode,
      };
}

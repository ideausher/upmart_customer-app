import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/address/page/address_listing/bloc/address_list_bloc.dart';
import 'package:user/modules/address/page/address_listing/bloc/address_list_event.dart';
import 'package:user/modules/address/page/address_listing/bloc/address_list_state.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';

import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_event.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';

class AddressListActionManagers {
  BuildContext context;
  AddressListBloc addressListBloc;

  AddressListActionManagers(this.context, this.addressListBloc);

  //to remove address from the list
  callRemoveAddressEvent(
      {EditAddressRequestModel editAddressRequestModel,
      AuthResponseModel authResponseModel,
      ScaffoldState scaffoldState,
      AddressListState addressListState,
      AddressListResponseModel addressListResponseModel}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        addressListBloc?.emitEvent(RemoveAddressFromListEvent(
            commonResponseModel: new CommonResponseModel(),
            currentLocation: addressListState?.currentLocation,
            context: context,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            isLoading: true,
            editAddressRequestModel: editAddressRequestModel));
      }
    });
  }

  //to remove address from the list
  callAddressListEvent(
      {EditAddressRequestModel editAddressRequestModel,
      AuthResponseModel authResponseModel,
      ScaffoldState scaffoldState,
      AddressListState addressListState,
      AddressListResponseModel addressListResponseModel}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        addressListBloc?.emitEvent(GetAddressListEvent(
            commonResponseModel: new CommonResponseModel(),
            context: context,
            currentLocation: addressListState?.currentLocation,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            isLoading: true,
            editAddressRequestModel: editAddressRequestModel));
      }
    });
  }

  //to change primary address
  callChangePrimaryAddress(
      {EditAddressRequestModel editAddressRequestModel,
      AuthResponseModel authResponseModel,
      ScaffoldState scaffoldState,
      AddressListState addressListState,
      AddressListResponseModel addressListResponseModel}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        addressListBloc?.emitEvent(MakeAddressPrimaryEvent(
            editAddressRequestModel: editAddressRequestModel,
            addressListResponseModel: addressListResponseModel,
            commonResponseModel: new CommonResponseModel(),
            authResponseModel: authResponseModel,
            currentLocation: addressListState?.currentLocation,
            context: context,
            isLoading: true));
      }
    });
  }

  //address listing state change
  actionAddressListStateChange({
    BuildContext context,
    ScaffoldState scaffoldState,
    TextEditingController phoneController,
    AuthState authState,
    AddressListState addressListState,
    AuthBloc authBloc,
  }) {
    if (addressListState.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) async {
          //this case is when adress will be deleted
          if (addressListState?.commonResponseModel != null &&
              addressListState?.commonResponseModel?.status ==
                  ApiStatus.Success.value) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: addressListState?.commonResponseModel?.message);
          } else if (addressListState?.addressListResponseModel != null &&
              addressListState?.addressListResponseModel?.statusCode ==
                  ApiStatus.Success.value) {
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                authResponseModel: addressListState?.authResponseModel));
          }

          //this case is when the address gets edited or updated
          else if (addressListState?.authResponseModel != null &&
              addressListState?.authResponseModel?.statusCode ==
                  ApiStatus.Success.value) {
            print('Calling this every time');
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                authResponseModel: addressListState?.authResponseModel));
            callAddressListEvent(
                addressListState: addressListState,
                authResponseModel: addressListState?.authResponseModel,
                addressListResponseModel:
                    addressListState?.addressListResponseModel ??
                        new AddressListResponseModel(),
                scaffoldState: scaffoldState);
          }

          //error case
          else if (addressListState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: addressListState?.message);
            print("signUp==> ${authState?.message}");
          }
        },
      );
    }
  }

  // action to update ui
  actionToUpdateAddressUi({
    AddressListState addressListState,
    CurrentLocation currentLocation
  }) {


    addressListBloc?.emitEvent(UpdateAddressListEvent(
      commonResponseModel: addressListState?.commonResponseModel,
      context: context,
      currentLocation:currentLocation,
      authResponseModel: addressListState?.authResponseModel,
      addressListResponseModel: addressListState?.addressListResponseModel,
      isLoading: false,
    ));
  }
}

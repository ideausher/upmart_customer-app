import 'package:flutter/cupertino.dart';
import 'package:flutter/src/material/scaffold.dart';
import 'package:user/modules/address/enum/address_enum.dart';
import 'package:user/modules/address/page/address_listing/bloc/address_list_state.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';

import '../../../../../localizations.dart';
import 'addresss_list_action_managers.dart';

class AddressListUtilsManager {
  BuildContext context;
  static AddressListUtilsManager _addressListUtilsManager =
      AddressListUtilsManager();

  static AddressListUtilsManager get addressListUtilsManager =>
      _addressListUtilsManager;

  CurrentLocation userCurrentLocation;

  saveCurrentLocation(CurrentLocation currentLocation) {
    userCurrentLocation = currentLocation;
  }

  //method call to edit address
  Future<void> callEditAddress(Datum address, BuildContext context,
      AddressListActionManagers actionManager,
      {bool pop,
      AuthResponseModel authResponseModel,
      ScaffoldState scaffoldState,
      AddressListState addressListState,
      AddressListResponseModel addressListResponseModel}) async {
    EditAddressRequestModel editAddressRequestModel =
        new EditAddressRequestModel(
            pincode: address?.pincode ?? "",
            isForEdit: true,
            latitude: address?.latitude,
            longitude: address?.longitude,
            addressType: AppLocalizations.of(context).addresslistingpage.text.addressType,
            additionalInfo: address?.additionalInfo,
            addressId: address?.id.toString(),
            formattedAddress: address?.formattedAddress,
            isPrimary: (address?.isPrimary == AddressRoles.Primary.value)
                ? AddressRoles.Primary.value
                : AddressRoles.NonPrimary.value,
            country: address?.country,
            city: address?.city,
            pop: pop);
    var result = await NavigatorUtils.navigatorUtilsInstance
        .navigatorPushedNameResult(context, AddressRoutes.SET_ADDRESS,
            dataToBeSend: editAddressRequestModel);
    actionManager?.callAddressListEvent(
      authResponseModel: authResponseModel,
        addressListState: addressListState,
        addressListResponseModel: addressListState?.addressListResponseModel,
        scaffoldState: scaffoldState);
  }

  //this method is used to show remove address dialog
  void openRemoveAddressDialog(
    Datum address, {
    AddressListActionManagers actionManager,
    GlobalKey<ScaffoldState> key,
    AuthState authState,
    BuildContext context,
    AddressListState addressListState,
  }) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: AppLocalizations.of(context).addressalert.title.removeAddress,
        negativeButton: AppLocalizations.of(context).common.text.no,
        positiveButton: AppLocalizations.of(context).common.text.yes,
        titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        buttonTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        subTitle: AppLocalizations.of(context).addressalert.subtitle.sureToRemoveAddress,
        onNegativeButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        onPositiveButtonTab: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          callRemoveAddress(
            address,
            context: context,
            actionManager: actionManager,
            key: key,
            authState: authState,
            addressListState: addressListState,
          );
        });
  }

  //this method is used to call remove address event
  void callRemoveAddress(
    Datum address, {
    AddressListActionManagers actionManager,
    GlobalKey<ScaffoldState> key,
    AuthState authState,
    BuildContext context,
    AddressListState addressListState,
  }) {
    actionManager?.callRemoveAddressEvent(
        scaffoldState: key?.currentState,
        addressListState: addressListState,
        authResponseModel: authState?.authResponseModel,
        addressListResponseModel: addressListState?.addressListResponseModel,
        editAddressRequestModel: new EditAddressRequestModel(
            pincode: address?.pincode,
            latitude: address?.latitude,
            longitude: address?.longitude,
            addressType: AppLocalizations.of(context).addresslistingpage.text.addressType,
            additionalInfo: address?.additionalInfo,
            addressId: address?.id.toString(),
            formattedAddress: address?.formattedAddress,
            isPrimary: AddressRoles.Primary.value,
            country: address?.country,
            city: address?.city));
  }
}

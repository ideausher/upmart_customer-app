import 'package:flutter/material.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';

class AddressListState extends BlocState {
  AddressListState(
      {this.isLoading: false,
      this.message,
      this.context,
      this.currentLocation,
      this.commonResponseModel,
      this.addressListResponseModel,
      this.authResponseModel,
      this.isPrimaryAddress: false})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final CommonResponseModel commonResponseModel;
  final AuthResponseModel authResponseModel;
  final bool isPrimaryAddress;
  final AddressListResponseModel addressListResponseModel;
  final CurrentLocation currentLocation;

  // used for update address state
  factory AddressListState.updateAddressList(
      {bool isLoading,
      String message,
      CurrentLocation currentLocation,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      AuthResponseModel authResponseModel,
      AddressListResponseModel addressListResponseModel,
      bool isPrimaryAddress}) {
    return AddressListState(
        isLoading: isLoading,
        message: message,
        context: context,
        commonResponseModel: commonResponseModel,
        currentLocation: currentLocation,
        addressListResponseModel: addressListResponseModel,
        authResponseModel: authResponseModel,
        isPrimaryAddress: isPrimaryAddress);
  }

  factory AddressListState.initiating() {
    return AddressListState(
      isLoading: false,
    );
  }
}

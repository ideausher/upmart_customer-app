import 'package:flutter/material.dart';
import 'package:user/modules/address/api/model/add_address/add_address_request_model.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';

import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';

abstract class AddressListEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final CommonResponseModel commonResponseModel;
  final EditAddressRequestModel editAddressRequestModel;
  final AuthResponseModel authResponseModel;
  final bool isPrimaryAddress;
  final AddressListResponseModel addressListResponseModel;
  final AddAddressRequestModel addressRequestModel;
  final CurrentLocation currentLocation;

  AddressListEvent(
      {this.isLoading: false,
      this.context,
      this.commonResponseModel,
      this.currentLocation,
      this.addressListResponseModel,
      this.addressRequestModel,
      this.authResponseModel,
      this.isPrimaryAddress: false,
      this.editAddressRequestModel});
}

//this event is used for removing the address form the list
class RemoveAddressFromListEvent extends AddressListEvent {
  RemoveAddressFromListEvent(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      AddressListResponseModel addressListResponseModel,
      EditAddressRequestModel editAddressRequestModel,
      AuthResponseModel authResponseModel,
      bool isPrimaryAddress,
      CurrentLocation currentLocation})
      : super(
            isLoading: isLoading,
            context: context,
            currentLocation: currentLocation,
            commonResponseModel: commonResponseModel,
            addressListResponseModel: addressListResponseModel,
            editAddressRequestModel: editAddressRequestModel,
            authResponseModel: authResponseModel,
            isPrimaryAddress: isPrimaryAddress);
}

//this event is used for making address primary
class MakeAddressPrimaryEvent extends AddressListEvent {
  MakeAddressPrimaryEvent(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      AddressListResponseModel addressListResponseModel,
      EditAddressRequestModel editAddressRequestModel,
      AuthResponseModel authResponseModel,
      bool isPrimaryAddress,
      CurrentLocation currentLocation})
      : super(
            isLoading: isLoading,
            currentLocation: currentLocation,
            context: context,
            commonResponseModel: commonResponseModel,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            editAddressRequestModel: editAddressRequestModel,
            isPrimaryAddress: isPrimaryAddress);
}

class GetAddressListEvent extends AddressListEvent {
  GetAddressListEvent(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      EditAddressRequestModel editAddressRequestModel,
      AddressListResponseModel addressListResponseModel,
      AuthResponseModel authResponseModel,
      CurrentLocation currentLocation,
      AddAddressRequestModel addressRequestModel})
      : super(
            isLoading: isLoading,
            currentLocation: currentLocation,
            context: context,
            editAddressRequestModel: editAddressRequestModel,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            commonResponseModel: commonResponseModel,
            addressRequestModel: addressRequestModel);
}

//this event is used for removing the address form the list
class UpdateAddressListEvent extends AddressListEvent {
  UpdateAddressListEvent(
      {bool isLoading,
        BuildContext context,
        CommonResponseModel commonResponseModel,
        EditAddressRequestModel editAddressRequestModel,
        AddressListResponseModel addressListResponseModel,
        AuthResponseModel authResponseModel,
        CurrentLocation currentLocation,
        bool isPrimaryAddress,
        AddAddressRequestModel addressRequestModel})
      : super(
      isLoading: isLoading,
      context: context,
      currentLocation: currentLocation,
      commonResponseModel: commonResponseModel,
      addressListResponseModel: addressListResponseModel,
      editAddressRequestModel: editAddressRequestModel,
      authResponseModel: authResponseModel,
      isPrimaryAddress: isPrimaryAddress,addressRequestModel: addressRequestModel);
}
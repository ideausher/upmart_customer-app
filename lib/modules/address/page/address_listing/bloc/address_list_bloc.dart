import 'package:geolocator/geolocator.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/api/provider/address_api_provider.dart';
import 'package:user/modules/address/enum/address_enum.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';

import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';

import 'address_list_event.dart';
import 'address_list_state.dart';

class AddressListBloc
    extends BlocEventStateBase<AddressListEvent, AddressListState> {
  AddressListBloc({bool initializing = true})
      : super(initialState: AddressListState.initiating());

  @override
  Stream<AddressListState> eventHandler(AddressListEvent event,
      AddressListState currentState) async* {
    // update address page UI when address removed from the list

    if (event is UpdateAddressListEvent) {
      yield AddressListState.updateAddressList(
          isLoading: event?.isLoading,
          context: event?.context,
          message: "",
          currentLocation: event?.currentLocation,
          addressListResponseModel: event?.addressListResponseModel,
          authResponseModel: event?.authResponseModel,
          isPrimaryAddress: event?.isPrimaryAddress,
          commonResponseModel: event?.commonResponseModel);
    }
    if (event is RemoveAddressFromListEvent) {
      String _message = "";
      bool isPrimaryAddress = false;
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      AddressListResponseModel _addressListResponseModel =
          event?.addressListResponseModel;
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      //removing the address form the address list
      List<Datum> _addressList = List<Datum>();

      yield AddressListState.updateAddressList(
          isLoading: true,
          context: event?.context,
          message: "",
          currentLocation: event?.currentLocation,
          addressListResponseModel: event?.addressListResponseModel,
          authResponseModel: event?.authResponseModel,
          commonResponseModel: event?.commonResponseModel);

      CurrentLocation currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();
      if (event?.editAddressRequestModel?.addressId == currentLocation?.id) {
        // if size is 1
        if (_addressListResponseModel?.data?.length == 1) {
          var geolocator = await Geolocator.getCurrentPosition();
          currentLocation =
          await CurrentLocationManger?.locationMangerInstance
              ?.getAddressUsingLocation(event?.context, geolocator);
          await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
              currentLocation,
              PrefsEnum.UserLocationData.value);
        } else {
          for (var address in _addressListResponseModel?.data) {
            if (address.id.toString() !=
                event?.editAddressRequestModel?.addressId) {
              currentLocation = new CurrentLocation(
                  id: address?.id.toString(),
                  lat: address?.latitude,
                  lng: address?.longitude,
                  city: address?.city,
                  country: address?.country,
                  postalCode: address?.pincode,
                  currentAddress: address?.formattedAddress);
              if (address?.isPrimary == 1) {
                await SharedPrefUtils.sharedPrefUtilsInstance
                    .saveLocationObject(
                    currentLocation,
                    PrefsEnum.UserLocationData.value);
                break;
              } else {
                await SharedPrefUtils.sharedPrefUtilsInstance
                    .saveLocationObject(
                    currentLocation,
                    PrefsEnum.UserLocationData.value);
              }
            }
          }
        }
      }


      //<----------------api call --------------------------->
      var result = await AddressProvider().removeAddressApiCall(
          context: event.context,
          addressId: event?.editAddressRequestModel?.addressId);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = commonResponseModel?.message;

          //first case check if the address object did not consist the address id which is to remove add it in the list
          for (var address in _addressListResponseModel?.data) {
            if (address.id.toString() !=
                event?.editAddressRequestModel?.addressId) {
              _addressList.add(address);
            }
          }

          _addressListResponseModel?.data = _addressList;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations
                .of(event?.context)
                .common
                .error
                .somethingWentWrong;
      }

      yield AddressListState.updateAddressList(
          isLoading: false,
          context: event?.context,
          message: _message,
          currentLocation: currentLocation,
          isPrimaryAddress: isPrimaryAddress,
          authResponseModel: _authResponseModel,
          addressListResponseModel: _addressListResponseModel,
          commonResponseModel: commonResponseModel);
    }

    //Event for making address Primary
    if (event is MakeAddressPrimaryEvent) {
      String _message = "";
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      String accessToken = _authResponseModel?.userData?.accessToken;
      AddressListResponseModel _addressListResponseModel =
          event?.addressListResponseModel;

      yield AddressListState.updateAddressList(
          isLoading: true,
          context: event?.context,
          message: "",
          currentLocation: event?.currentLocation,
          authResponseModel: event?.authResponseModel,
          isPrimaryAddress: event?.isPrimaryAddress,
          addressListResponseModel: event?.addressListResponseModel,
          commonResponseModel: event?.commonResponseModel);

      var result = await AddressProvider().makeAddressPrimaryApiCall(
          context: event.context,
          editAddressRequestModel: event?.editAddressRequestModel);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          _authResponseModel = AuthResponseModel.fromMap(result);
          _authResponseModel?.userData?.accessToken = accessToken;
          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);

          //here store newly created primary address in the shared pref
          for (var address in _authResponseModel?.userData?.address) {
            if (address?.primary == AddressRoles.Primary.value) {
              await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
                  new CurrentLocation(
                      id: address?.addressId.toString(),
                      lat: address?.latitude,
                      lng: address?.longitude,
                      city: address?.city,
                      country: address?.country,
                      postalCode: address?.pincode,
                      currentAddress: address?.formattedAddress),
                  PrefsEnum.UserLocationData.value);
            }
          }
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations
                .of(event?.context)
                .common
                .error
                .somethingWentWrong;
      }

      //save new data in shared prefs
      yield AddressListState.updateAddressList(
          isLoading: false,
          context: event?.context,
          message: _message,
          currentLocation: event?.currentLocation,
          addressListResponseModel:
          new AddressListResponseModel() /*_addressListResponseModel*/,
          isPrimaryAddress: false,
          authResponseModel: _authResponseModel,
          commonResponseModel: event?.commonResponseModel);
    }

    //for getting address list
    if (event is GetAddressListEvent) {
      String _message = "";
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      AddressListResponseModel addressListResponseModel;
      List<Address> addresses = new List();
      CurrentLocation currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel(); /*new CurrentLocation();*/
      yield AddressListState.updateAddressList(
          isLoading: true,
          context: event?.context,
          currentLocation: event?.currentLocation,
          message: "",
          authResponseModel: event?.authResponseModel,
          commonResponseModel: new CommonResponseModel());
      //<----------------api call --------------------------->
      var result = await AddressProvider().getAddressesList(
        context: event.context,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          addressListResponseModel = AddressListResponseModel.fromMap(result);

          //here store newly created primary address in the shared pref
          if (addressListResponseModel?.data?.isNotEmpty == true) {
            for (var address in addressListResponseModel?.data) {
              CurrentLocation _currentLocation = await FetchPrefsUtils
                  .fetchPrefsUtilsInstance.getCurrentLocationModel();
              if (address?.isPrimary == AddressRoles.Primary.value) {
                if (_currentLocation?.id == null ||
                    _currentLocation?.id?.isEmpty == true) {
                  currentLocation = new CurrentLocation(
                      id: address?.id.toString(),
                      lat: address?.latitude,
                      lng: address?.longitude,
                      city: address?.city,
                      country: address?.country,
                      postalCode: address?.pincode,
                      currentAddress: address?.formattedAddress);

                  await SharedPrefUtils.sharedPrefUtilsInstance
                      .saveLocationObject(
                      currentLocation, PrefsEnum.UserLocationData.value);
                }
              }

              if (_currentLocation?.id == address?.id?.toString()) {
                currentLocation = new CurrentLocation(
                    id: address?.id.toString(),
                    lat: address?.latitude,
                    lng: address?.longitude,
                    city: address?.city,
                    country: address?.country,
                    postalCode: address?.pincode,
                    currentAddress: address?.formattedAddress);

                await SharedPrefUtils.sharedPrefUtilsInstance
                    .saveLocationObject(
                    currentLocation, PrefsEnum.UserLocationData.value);
              }
            }
          }
          //this is done because response params were not same then manually add data to the auth response model
          if (addressListResponseModel?.data?.isNotEmpty == true) {
            for (int index = 0;
            index < addressListResponseModel?.data?.length;
            index++) {
              Address address = new Address();
              address?.addressId = addressListResponseModel?.data[index].id;
              address?.additionalInfo =
                  addressListResponseModel?.data[index].additionalInfo;
              address?.city = addressListResponseModel?.data[index].city;
              address?.country = addressListResponseModel?.data[index].country;
              address?.addressType = addressListResponseModel?.data[index].type;
              address?.latitude =
                  addressListResponseModel?.data[index].latitude;
              address?.longitude =
                  addressListResponseModel?.data[index].longitude;
              address?.primary =
                  addressListResponseModel?.data[index].isPrimary;
              address?.pincode = addressListResponseModel?.data[index].pincode;
              address?.formattedAddress =
                  addressListResponseModel?.data[index]?.formattedAddress;
              addresses.add(address);
            }
          }
          _authResponseModel?.userData?.address = addresses;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations
                .of(event?.context)
                .common
                .error
                .somethingWentWrong;
      }

      yield AddressListState.updateAddressList(
          isLoading: false,
          context: event?.context,
          message: _message,
          currentLocation: currentLocation,
          isPrimaryAddress: false,
          addressListResponseModel: addressListResponseModel,
          authResponseModel: _authResponseModel,
          commonResponseModel: new CommonResponseModel());
    }
  }
}

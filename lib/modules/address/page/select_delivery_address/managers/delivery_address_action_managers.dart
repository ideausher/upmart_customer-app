import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:user/modules/address/page/select_delivery_address/bloc/delivery_address_bloc.dart';
import 'package:user/modules/address/page/select_delivery_address/bloc/delivery_address_event.dart';
import 'package:user/modules/address/page/select_delivery_address/bloc/delivery_address_state.dart';

import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
class DeliveryActionManagers {
  BuildContext context;
  DeliveryAddressBloc deliveryAddressBloc;
  DeliveryActionManagers(this.context, this.deliveryAddressBloc);

  callCustomMarkerEvent(Set<Marker> markers, CurrentLocation currentLocation, bool isForCurrentAddress) {
    deliveryAddressBloc?.emitEvent(GetCustomMarkerEvent(
        isLoading: true,
        currentLocation: currentLocation,
        context: context,
        marker: markers,
        isForCurrentAddress: isForCurrentAddress));
  }

  callOnDragMarkerEvent(Set<Marker> markers, double lat, double lng, bool isForCurrentAddress) {
    deliveryAddressBloc?.emitEvent(GetAddressFromMapEvent(
      isLoading: true,
      isForCurrentAddress: isForCurrentAddress,
      currentLocation: new CurrentLocation(lat: lat, lng: lng),
      context: context,
      marker: markers,
    ));
  }

  //action on set address page state change
  actionOnDeliveryAddressStateChange(
      {ScaffoldState scaffoldState,
      DeliveryAddressState deliveryAddressState,
      BuildContext context,
      CurrentLocation currentLocation,
      TextEditingController addressController}) {
    if (deliveryAddressState.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          if (deliveryAddressState?.currentLocation?.lat != null &&
              deliveryAddressState?.currentLocation?.lng != null) {
            currentLocation = deliveryAddressState?.currentLocation;
            addressController.text = currentLocation?.currentAddress ?? "";

            //_updateMarkers();
          } else if (deliveryAddressState?.message?.toString()?.trim()?.isNotEmpty == true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance
                .showSnackbar(context: context, scaffoldState: scaffoldState, message: deliveryAddressState?.message);
            print("set address==> ${deliveryAddressState?.message}");
          }
        },
      );
    }
  }
}

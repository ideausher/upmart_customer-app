import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/constants/image_constant.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';
import 'package:user/modules/address/page/select_delivery_address/bloc/delivery_address_bloc.dart';
import 'package:user/modules/address/page/select_delivery_address/bloc/delivery_address_state.dart';
import 'package:user/modules/address/page/select_delivery_address/managers/delivery_address_action_managers.dart';
import 'package:user/modules/address/page/select_delivery_address/managers/delivery_address_utils_managers.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/search_places/model/search_place_address_model.dart';
import 'package:user/modules/common/search_places/utils/search_places_utils.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';

class SelectDeliveryAddressPage extends StatefulWidget {
  BuildContext context;
  EditAddressRequestModel editAddressRequestModel;

  SelectDeliveryAddressPage(this.context) {
    editAddressRequestModel = ModalRoute.of(context).settings.arguments;
  }

  @override
  _SelectDeliveryAddressPageState createState() =>
      _SelectDeliveryAddressPageState();
}

class _SelectDeliveryAddressPageState extends State<SelectDeliveryAddressPage> {
  //class variables
  BuildContext _context;
  TextEditingController _addressController = new TextEditingController();
  GoogleMapController mapController;

  // Custom marker icon
  BitmapDescriptor icon;

  // List for storing markers
  Set<Marker> _markers = {};

  //Bloc Variables
  DeliveryAddressBloc _deliveryAddressBloc;
  DeliveryAddressState _deliveryAddressState;

  //Managers variables
  DeliveryActionManagers _deliveryActionManagers;
  DeliveryUtilsManager _deliveryUtilsManager;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  var latLng;

  @override
  void initState() {
    super.initState();
    _deliveryUtilsManager =
        DeliveryUtilsManager(widget?.context, _deliveryAddressBloc);
    _deliveryAddressBloc = new DeliveryAddressBloc();
    _deliveryActionManagers =
        DeliveryActionManagers(widget?.context, _deliveryAddressBloc);
    _setCustomImageAsMarker();
    _getSavedLocation();
  }

  //get location from the shared preferences
  void _getSavedLocation() async {
    //here ger user current location data on the map
    var _serviceEnabled =
        await CurrentLocationManger.locationMangerInstance.serviceEnabled();

    if (_serviceEnabled) {
      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          if (widget?.editAddressRequestModel?.isForEdit == false) {
            print('Add address');
            CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
            _deliveryActionManagers?.callCustomMarkerEvent(
                _markers, _currentLocation, false);
          } else {
            print('Edit address');
            CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
            _deliveryActionManagers?.callOnDragMarkerEvent(
                _markers,
                widget?.editAddressRequestModel?.latitude,
                widget?.editAddressRequestModel?.longitude,
                false);
          }
        }
      });
    } else {
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: _context,
          scaffoldState: _scaffoldKey?.currentState,
          message: AppLocalizations.of(_context)
              .enablelocationpage
              .error
              .locationNotAllowed);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _addressController?.dispose();
    _deliveryAddressBloc?.dispose();
    _markers?.clear();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      key: _scaffoldKey,
      body: BlocEventStateBuilder<DeliveryAddressState>(
        bloc: _deliveryAddressBloc,
        builder:
            (BuildContext context, DeliveryAddressState deliveryAddressState) {
          _context = context;
          _deliveryActionManagers.context = context;
          _deliveryUtilsManager.context = context;
          if (deliveryAddressState != null &&
              _deliveryAddressState != deliveryAddressState) {
            _deliveryAddressState = deliveryAddressState;
            if (_deliveryAddressState?.isLoading == false) {
              _updateMarkers(_deliveryAddressState);
            }

            _deliveryActionManagers?.actionOnDeliveryAddressStateChange(
                scaffoldState: _scaffoldKey?.currentState,
                context: _context,
                currentLocation: _deliveryAddressState.currentLocation,
                deliveryAddressState: _deliveryAddressState,
                addressController: _addressController);
          }
          return ModalProgressHUD(
              inAsyncCall: deliveryAddressState?.isLoading ?? false,
              child: WillPopScope(
                onWillPop: () {
                  return NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(_context,
                          dataToBeSend: deliveryAddressState.currentLocation);
                },
                child: SafeArea(
                  bottom: true,
                  top: true,
                  child: Scaffold(
                    appBar: _showAppBar(),
                    body: Column(
                      //runSpacing: SIZE_20,
                      children: <Widget>[
                        _showDeliveryAddress(),
                        _showAddressTextField(),
                        _showMapView()
                      ],
                    ),
                  ),
                ),
              ));
        },
      ),
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: true,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        defaultLeadIconPressed: () {
          return NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _context,
              dataToBeSend: _deliveryAddressState.currentLocation);
        },
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_10),
            child: NotificationReadCountWidget(
              context: _context,
            ),
          )
        ],
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.bodyText2,
        backGroundColor: Colors.transparent);
  }

  //method to show delivery address title text
  Widget _showDeliveryAddress() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          AppLocalizations.of(_context).deliveryaddresspage.title.selectAddress,
          style: AppConfig.of(_context).themeData.textTheme.headline6,
        ),
        SizedBox(
          width: SIZE_10,
        ),
        Image.asset(
          SET_LOCATION_ICON,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, percentage: SIZE_8, ofWidth: false),
          width: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, percentage: SIZE_8, ofWidth: true),
        )
      ],
    );
  }

  //method to show address text form field
  Widget _showAddressTextField() {
    return Padding(
      padding: const EdgeInsets.all(SIZE_20),
      child: Container(
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_10),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 10,
              child: InkWell(
                onTap: () async {
                  _deliveryActionManagers?.callOnDragMarkerEvent(
                      _markers,
                      _deliveryAddressState?.currentLocation?.lat,
                      _deliveryAddressState?.currentLocation?.lng,
                      true);
                },
                child: SvgPicture.asset(
                  LOCATION_ICON,
                  height: SIZE_20,
                  width: SIZE_20,
                  color: Colors.blueAccent,
                ),
              ),
            ),
            SizedBox(
              width: SIZE_10,
            ),
            Expanded(
              flex: 90,
              child: _textFieldForm(
                keyboardType: TextInputType.text,
                hint: AppLocalizations?.of(_context)
                    ?.deliveryaddresspage
                    .title
                    .yourAddress,
                elevation: ELEVATION_0,
                controller: _addressController,
              ),
            ),
          ],
        ),
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      Widget icon,
      double elevation,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_10),
      elevation: elevation,
      color: Colors.white,
      child: InkWell(
        onTap: () async {
          SearchPlaceAddressModel addresses = await SearchPlacesUtils
              ?.searchPlacesUtilsInstance
              ?.showSearchPlacesDialog(context: _context);
          if (addresses != null &&
              addresses?.latitude != null &&
              addresses?.longitude != null)
            _deliveryActionManagers?.callOnDragMarkerEvent(
                _markers, addresses?.latitude, addresses?.longitude, false);
        },
        child: TextFormField(
          validator: validator,
          controller: controller,
          readOnly: true,
          enabled: false,
          keyboardType: keyboardType,
          style: AppConfig.of(_context).themeData.textTheme.headline2,
          decoration: InputDecoration(
              hintText: hint,
              contentPadding: EdgeInsets.all(SIZE_0),
              border: InputBorder.none),
        ),
      ),
    );
  }

  //method to show map view and get location from this map
  Widget _showMapView() {
    // add marker
    _markers.clear();
    print('the lat lng is ${_deliveryAddressState.currentLocation?.lat},${{
      _deliveryAddressState.currentLocation?.lng
    }}');
    latLng = LatLng(_deliveryAddressState.currentLocation?.lat ?? DEFAULT_LAT,
        _deliveryAddressState.currentLocation?.lng ?? DEFAULT_LNG);
    _markers.add(Marker(
        draggable: true,
        markerId: MarkerId(MARKER_ID),
        position: latLng,
        consumeTapEvents: true,
        icon: icon,
        onDragEnd: ((newPosition) {
          print(newPosition.latitude);
          print(newPosition.longitude);
          NetworkConnectionUtils.networkConnectionUtilsInstance
              .getConnectivityStatus(context, showNetworkDialog: true)
              .then((onValue) {
            if (onValue) {
              // hide keyboard
              CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
              _deliveryActionManagers?.callOnDragMarkerEvent(
                  _markers, newPosition.latitude, newPosition.longitude, false);
            }
          });
        })));
    return Expanded(
      child: GoogleMap(
          initialCameraPosition: CameraPosition(
            target: latLng,
            zoom: SIZE_14,
          ),
          onMapCreated: _onMapCreated,
          mapType: MapType.terrain,
          myLocationEnabled: true,
          markers: _markers,
          gestureRecognizers: Set()
            ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))),
    );
  }

  //on map created
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    /* controller.moveCamera(
      CameraUpdate.newLatLngZoom(latLng, ZOOM),
    );*/
  }

  //this method is used to add as a custom marker on the map
  void _setCustomImageAsMarker() {
    _deliveryUtilsManager
        .getBitmapDescriptorFromAssetBytes(
            'assets/set_location_icon.png', SIZE_MARKER)
        .then((onValue) {
      icon = onValue;
    });
  }

  //method to update markers with updated location on map
  void _updateMarkers(DeliveryAddressState deliveryAddressState) {
    if (mapController != null) {
      print('Calling animate');
      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(deliveryAddressState?.currentLocation?.lat ?? 30.7333,
              deliveryAddressState?.currentLocation?.lng ?? 76.7794),
          zoom: ZOOM)));
    }
  }
}

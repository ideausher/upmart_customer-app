import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';


class DeliveryAddressState extends BlocState {
  DeliveryAddressState(
      {this.isLoading: false,
      this.message,
      this.context,
      this.commonResponseModel,
      this.currentLocation,
      this.isForCurrentAddress,
      this.marker})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final CommonResponseModel commonResponseModel;
  final CurrentLocation currentLocation;
  final Set<Marker> marker;
  final bool isForCurrentAddress;

  // used for update address state
  factory DeliveryAddressState.updateDeliveryAddress(
      {bool isLoading,
      String message,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      CurrentLocation currentLocation,
      bool isForCurrentAddress,
      Set<Marker> marker}) {
    return DeliveryAddressState(
        isLoading: isLoading,
        message: message,
        context: context,
        commonResponseModel: commonResponseModel,
        currentLocation: currentLocation,
        isForCurrentAddress: isForCurrentAddress,
        marker: marker);
  }

  factory DeliveryAddressState.initiating() {
    return DeliveryAddressState(
      isLoading: false,
    );
  }
}

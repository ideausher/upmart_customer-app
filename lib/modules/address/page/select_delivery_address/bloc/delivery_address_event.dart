import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';


abstract class DeliveryAddressEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final CurrentLocation currentLocation;
  final Set<Marker> marker;
  final bool isForCurrentAddress;

  DeliveryAddressEvent(
      {this.isLoading: false, this.context, this.currentLocation, this.marker, this.isForCurrentAddress});
}

//this event is getting location when user drags the marker
class GetAddressFromMapEvent extends DeliveryAddressEvent {
  GetAddressFromMapEvent({
    bool isLoading,
    BuildContext context,
    Set<Marker> marker,
    bool isForCurrentAddress,
    CurrentLocation currentLocation,
  }) : super(
            isLoading: isLoading,
            context: context,
            marker: marker,
            isForCurrentAddress: isForCurrentAddress,
            currentLocation: currentLocation);
}

//this event is used for getting custom marker when map is displayed
class GetCustomMarkerEvent extends DeliveryAddressEvent {
  GetCustomMarkerEvent(
      {bool isLoading,
      BuildContext context,
      bool isForCurrentAddress,
      Set<Marker> marker,
      CurrentLocation currentLocation})
      : super(
            isLoading: isLoading,
            context: context,
            marker: marker,
            isForCurrentAddress: isForCurrentAddress,
            currentLocation: currentLocation);
}

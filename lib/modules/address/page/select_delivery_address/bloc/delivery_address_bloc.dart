import 'package:geolocator/geolocator.dart';
import 'package:user/localizations.dart';

import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';

import 'delivery_address_event.dart';
import 'delivery_address_state.dart';

class DeliveryAddressBloc extends BlocEventStateBase<DeliveryAddressEvent, DeliveryAddressState> {
  DeliveryAddressBloc({bool initializing = true}) : super(initialState: DeliveryAddressState.initiating());

  @override
  Stream<DeliveryAddressState> eventHandler(DeliveryAddressEvent event, DeliveryAddressState currentState) async* {
    // event to set current location on the map and custom marker
    if (event is GetCustomMarkerEvent) {
      CurrentLocation _currentLocation;
      String _message = "";
      yield DeliveryAddressState.updateDeliveryAddress(
          isLoading: true,
          context: event?.context,
          message: "",
          marker: event?.marker,
          currentLocation: event?.currentLocation);

      try {
        Position position = await Geolocator.getCurrentPosition();
        if (position != null) {
          _currentLocation =
              await CurrentLocationManger?.locationMangerInstance?.getAddressUsingLocation(event?.context, position);
        } else {
          _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
        }
      } catch (e) {
        _message =  AppLocalizations.of(event.context).enablelocationpage.error.locationNotAllowed;
      }

      yield DeliveryAddressState.updateDeliveryAddress(
          isLoading: false,
          context: event?.context,
          message: _message,
          marker: event?.marker,
          isForCurrentAddress: event?.isForCurrentAddress,
          currentLocation: _currentLocation);
    }

    //call get addresses on the basis of user drag marker on map
    if (event is GetAddressFromMapEvent) {
      String _message = "";
      CurrentLocation _currentLocation;
      yield DeliveryAddressState.updateDeliveryAddress(
          isLoading: true,
          context: event?.context,
          message: "",
          marker: event?.marker,
          currentLocation: event?.currentLocation);

      //here ger user current location data on the map
      if (event?.isForCurrentAddress == true) {
        try {
          Position position = await Geolocator.getCurrentPosition();
          if (position != null) {
            _currentLocation =
                await CurrentLocationManger?.locationMangerInstance?.getAddressUsingLocation(event?.context, position);
          } else {
            _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
          }
        } catch (e) {
          print("error1${e}");
          _message = AppLocalizations.of(event.context).enablelocationpage.error.locationNotAllowed;
        }
      } else {
        try {
          Position position =
              new Position(latitude: event?.currentLocation?.lat, longitude: event?.currentLocation?.lng);
          if (position != null) {
            _currentLocation =
                await CurrentLocationManger?.locationMangerInstance?.getAddressUsingLocation(event?.context, position);
          } else {
            _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
          }
        } catch (e) {
          print("error${e}");
          _message = AppLocalizations.of(event.context).enablelocationpage.error.locationNotAllowed;
        }
      }

      yield DeliveryAddressState.updateDeliveryAddress(
          isLoading: false,
          context: event?.context,
          message: _message,
          marker: event?.marker,
          isForCurrentAddress: event?.isForCurrentAddress,
          currentLocation: _currentLocation);
    }
  }
}

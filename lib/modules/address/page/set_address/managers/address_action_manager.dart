import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/api/model/add_address/add_address_request_model.dart';
import 'package:user/modules/address/enum/address_enum.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';
import 'package:user/modules/address/page/set_address/bloc/address_bloc.dart';
import 'package:user/modules/address/page/set_address/bloc/address_event.dart';
import 'package:user/modules/address/page/set_address/bloc/address_state.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_event.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';

import 'package:user/routes.dart';

class AddressActionManager {
  BuildContext context;
  AddressBloc addressBloc;

  AddressActionManager({this.context, this.addressBloc});

  //method to add address
  callAddAddressEvent(
      CurrentLocation currentLocation, String comment, AuthState authState) {
    List<UserAddresses> userAddress = new List();
    userAddress.add(UserAddresses(
        latitude: currentLocation?.lat,
        longitude: currentLocation?.lng,
        city: currentLocation?.city,
        country: currentLocation?.country,
        formattedAddress: currentLocation?.currentAddress,
        additionalInfo: comment,
        addressType: AppLocalizations.of(context).addresslistingpage.text.addressType,
        isPrimary:
            (authState?.authResponseModel?.userData?.address?.length == 0)
                ? AddressRoles?.Primary?.value
                : AddressRoles?.NonPrimary?.value,
        pincode: currentLocation?.postalCode));
    addressBloc?.emitEvent(AddAddressEvent(
        context: context,
        isLoading: true,
        authResponseModel: authState?.authResponseModel,
        commonResponseModel: new CommonResponseModel(),
        addressRequestModel: new AddAddressRequestModel(address: userAddress)));
  }

  //method to add address
  callEditAddressEvent(CurrentLocation currentLocation, String comment,
      AuthState authState, EditAddressRequestModel editAddressRequestModel) {
    editAddressRequestModel.longitude =
        currentLocation?.lng ?? editAddressRequestModel?.longitude;
    editAddressRequestModel.latitude =
        currentLocation?.lat ?? editAddressRequestModel?.latitude;
    editAddressRequestModel.additionalInfo = comment;
    editAddressRequestModel.pincode =
        currentLocation?.postalCode ?? editAddressRequestModel?.pincode;
    editAddressRequestModel.country =
        currentLocation?.country ?? editAddressRequestModel?.country;
    editAddressRequestModel.city =
        currentLocation?.city ?? editAddressRequestModel?.city;
    editAddressRequestModel.formattedAddress =
        currentLocation?.currentAddress ??
            editAddressRequestModel?.formattedAddress;
    editAddressRequestModel.isForEdit = true;
    addressBloc?.emitEvent(EditAddressEvent(
      context: context,
      isLoading: true,
      editAddressRequestModel: editAddressRequestModel,
      authResponseModel: authState?.authResponseModel,
      commonResponseModel: new CommonResponseModel(),
    ));
  }

  //action on set address page state change
  actionOnSetAddressStateChange(
      {AddressState addressState,
      ScaffoldState scaffoldState,
      BuildContext context,
      AuthBloc authBloc,
      TextEditingController commentController,
      TextEditingController addressController,
      EditAddressRequestModel editAddressRequestModel}) {
    if (addressState.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) async {
          if (addressState?.commonResponseModel != null &&
              addressState?.commonResponseModel?.status ==
                  ApiStatus.Success.value) {
            addressController.text = "";
            commentController.text = "";
            authBloc?.emitEvent(UpdateBlocEvent(
                context: context,
                authResponseModel: addressState?.authResponseModel));

            AuthResponseModel _authResponse = AuthResponseModel.fromMap( addressState?.authResponseModel?.toMap());
            _authResponse?.popScreen = editAddressRequestModel?.pop;

            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);

            /* DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: addressState?.commonResponseModel?.message);*/
          } else if (addressState?.commonResponseModel != null &&
              addressState?.commonResponseModel?.status !=
                  ApiStatus.Success.value) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: addressState?.commonResponseModel?.message);
          } else if (addressState?.authResponseModel != null &&
              addressState?.authResponseModel?.statusCode ==
                  ApiStatus.Success.value) {
            addressController.text = "";
            authBloc?.emitEvent(UpdateBlocEvent(
                context: context,
                authResponseModel: addressState?.authResponseModel));

            AuthResponseModel _authResponse = AuthResponseModel.fromMap( addressState?.authResponseModel?.toMap());
            _authResponse?.popScreen = editAddressRequestModel?.pop;
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          } else if (addressState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: addressState?.message);
            print("set address==> ${addressState?.message}");
          }
        },
      );
    }
  }
}

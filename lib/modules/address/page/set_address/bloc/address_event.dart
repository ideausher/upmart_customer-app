import 'package:flutter/material.dart';
import 'package:user/modules/address/api/model/add_address/add_address_request_model.dart';
import 'package:user/modules/address/page/address_listing/model/address_list_response_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';

import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/common_response_model.dart';

abstract class AddressEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final AddAddressRequestModel addressRequestModel;
  final CommonResponseModel commonResponseModel;
  final EditAddressRequestModel editAddressRequestModel;
  final AuthResponseModel authResponseModel;
  final AddressListResponseModel addressListResponseModel;

  AddressEvent(
      {this.isLoading: false,
      this.context,
      this.addressRequestModel,
      this.commonResponseModel,
      this.editAddressRequestModel,
      this.addressListResponseModel,
      this.authResponseModel});
}

//this event is used for updating the home page UI data
class AddAddressEvent extends AddressEvent {
  AddAddressEvent(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      AuthResponseModel authResponseModel,
      AddressListResponseModel addressListResponseModel,
      AddAddressRequestModel addressRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            commonResponseModel: commonResponseModel,
            addressRequestModel: addressRequestModel);
}

class EditAddressEvent extends AddressEvent {
  EditAddressEvent(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      EditAddressRequestModel editAddressRequestModel,
      AddressListResponseModel addressListResponseModel,
      AuthResponseModel authResponseModel,
      AddAddressRequestModel addressRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            editAddressRequestModel: editAddressRequestModel,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            commonResponseModel: commonResponseModel,
            addressRequestModel: addressRequestModel);
}

class GetAddressList extends AddressEvent {
  GetAddressList(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      EditAddressRequestModel editAddressRequestModel,
      AddressListResponseModel addressListResponseModel,
      AuthResponseModel authResponseModel,
      AddAddressRequestModel addressRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            editAddressRequestModel: editAddressRequestModel,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            commonResponseModel: commonResponseModel,
            addressRequestModel: addressRequestModel);
}

class SetAddressUIChangeEvent extends AddressEvent {
  SetAddressUIChangeEvent(
      {bool isLoading,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      EditAddressRequestModel editAddressRequestModel,
      AddressListResponseModel addressListResponseModel,
      AuthResponseModel authResponseModel,
      AddAddressRequestModel addressRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            editAddressRequestModel: editAddressRequestModel,
            authResponseModel: authResponseModel,
            addressListResponseModel: addressListResponseModel,
            commonResponseModel: commonResponseModel,
            addressRequestModel: addressRequestModel);
}

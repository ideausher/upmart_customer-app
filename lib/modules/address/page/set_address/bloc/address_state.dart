import 'package:flutter/material.dart';
import 'package:user/modules/address/api/model/add_address/add_address_request_model.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';

import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/common_response_model.dart';

class AddressState extends BlocState {
  AddressState(
      {this.isLoading: false,
      this.message,
      this.context,
      this.commonResponseModel,
      this.editAddressRequestModel,
      this.addressRequestModel,
      this.authResponseModel})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final CommonResponseModel commonResponseModel;
  final AddAddressRequestModel addressRequestModel;
  final AuthResponseModel authResponseModel;
  final EditAddressRequestModel editAddressRequestModel;

  // used for update address state
  factory AddressState.updateAddress(
      {bool isLoading,
      String message,
      BuildContext context,
      CommonResponseModel commonResponseModel,
      AddAddressRequestModel addressRequestModel,
      AuthResponseModel authResponseModel,
      EditAddressRequestModel editAddressRequestModel}) {
    return AddressState(
        isLoading: isLoading,
        message: message,
        context: context,
        commonResponseModel: commonResponseModel,
        addressRequestModel: addressRequestModel,
        authResponseModel: authResponseModel,
        editAddressRequestModel: editAddressRequestModel);
  }

  factory AddressState.initiating() {
    return AddressState(
      isLoading: false,
    );
  }
}

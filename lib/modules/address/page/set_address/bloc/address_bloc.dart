import 'package:user/localizations.dart';
import 'package:user/modules/address/api/model/add_address/add_address_request_model.dart';
import 'package:user/modules/address/api/provider/address_api_provider.dart';

import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';

import 'address_event.dart';
import 'address_state.dart';

class AddressBloc extends BlocEventStateBase<AddressEvent, AddressState> {
  AddressBloc({bool initializing = true}) : super(initialState: AddressState.initiating());

  @override
  Stream<AddressState> eventHandler(AddressEvent event, AddressState currentState) async* {
    // update address page UI
    if (event is AddAddressEvent) {
      String _message = "";
      AuthResponseModel _authResponseModel;
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      AddAddressRequestModel addressRequestModel = event?.addressRequestModel;

      yield AddressState.updateAddress(
          isLoading: true,
          context: event?.context,
          message: "",
          authResponseModel: event?.authResponseModel,
          addressRequestModel: event?.addressRequestModel,
          commonResponseModel: event?.commonResponseModel);
      var result = await AddressProvider().addAddressApiCall(
        context: event.context,
        addressRequestModel: event.addressRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
          addressRequestModel = event?.addressRequestModel;
        }
        // failure case
        else {
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield AddressState.updateAddress(
          isLoading: false,
          context: event?.context,
          message: _message,
          addressRequestModel: addressRequestModel,
          authResponseModel: event?.authResponseModel,
          editAddressRequestModel: event?.editAddressRequestModel,
          commonResponseModel: commonResponseModel);
    }

    if (event is EditAddressEvent) {
      String _message = "";
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      String accessToken = _authResponseModel?.userData?.accessToken;
      yield AddressState.updateAddress(
          isLoading: true,
          context: event?.context,
          message: "",
          authResponseModel: event?.authResponseModel,
          commonResponseModel: null);

      //TODO uncomment this later
      var result = await AddressProvider()
          .makeAddressPrimaryApiCall(context: event.context, editAddressRequestModel: event?.editAddressRequestModel);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          _authResponseModel = AuthResponseModel.fromMap(result);
          _authResponseModel?.userData?.accessToken = accessToken;
          await SharedPrefUtils.sharedPrefUtilsInstance.saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);

          _message = _authResponseModel?.message;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      //save new data in shared prefs
      yield AddressState.updateAddress(
          isLoading: false,
          context: event?.context,
          message: _message,
          authResponseModel: _authResponseModel,
          editAddressRequestModel: event?.editAddressRequestModel,
          addressRequestModel: event?.addressRequestModel,
          commonResponseModel: null);
    }

    if (event is SetAddressUIChangeEvent) {
      yield AddressState.updateAddress(
          isLoading: false,
          context: event?.context,
          message: "",
          authResponseModel: event?.authResponseModel,
          editAddressRequestModel: event?.editAddressRequestModel,
          addressRequestModel: event?.addressRequestModel,
          commonResponseModel: null);
    }
  }
}

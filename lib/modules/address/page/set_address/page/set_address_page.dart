import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/page/address_listing/model/edit_address_request_model.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import '../bloc/address_bloc.dart';
import '../bloc/address_state.dart';
import '../managers/address_action_manager.dart';

class SetAddressPage extends StatefulWidget {
  BuildContext context;
  EditAddressRequestModel editAddressRequestModel;

  SetAddressPage(this.context) {
    editAddressRequestModel = ModalRoute.of(context).settings.arguments;
  }

  @override
  _SetAddressPageState createState() => _SetAddressPageState();
}

class _SetAddressPageState extends State<SetAddressPage> {
  //class variables
  BuildContext _context;
  TextEditingController _addressController = new TextEditingController();
  TextEditingController _commentController = new TextEditingController();
  CurrentLocation _selectedLocation;

  //bloc variables
  AddressBloc _addressBloc;
  AuthBloc _authBloc;
  AddressState _addressState;
  AuthState _authState;

  //global key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //managers variable
  AddressActionManager _addressActionManager;
  AuthManager _authManager;

  bool _serviceEnabled = false;

  LocationPermission permission;

  @override
  void initState() {
    super.initState();
    _checkSeviceEnabled();
    _setData();
    _addressBloc = new AddressBloc();
    _authManager = new AuthManager();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _addressActionManager = new AddressActionManager(
        context: widget?.context, addressBloc: _addressBloc);
  }

  @override
  void dispose() {
    super.dispose();
    _addressBloc?.dispose();
    _addressController?.dispose();
    _commentController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      bottom: true,
      top: true,
      child: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        appBar: _showAppBar(),
        body: BlocEventStateBuilder<AuthState>(
          bloc: _authBloc,
          builder: (BuildContext context, AuthState authState) {
            _context = context;
            if (_authState != authState) {
              _authState = authState;
              _authManager.actionOnStateUpdate(
                  context: _context,
                  authBloc: _authBloc,
                  authState: _authState,
                  scaffoldState: _scaffoldKey.currentState);
            }
            return ModalProgressHUD(
                inAsyncCall: _authState?.isLoading ?? false,
                child: (_authState?.authResponseModel != null)
                    ? BlocEventStateBuilder<AddressState>(
                        bloc: _addressBloc,
                        builder: (BuildContext context,
                            AddressState addAddressState) {
                          _context = context;
                          _addressActionManager.context = context;
                          if (addAddressState != null &&
                              _addressState != addAddressState) {
                            _addressState = addAddressState;
                            _addressActionManager
                                ?.actionOnSetAddressStateChange(
                                    addressState: _addressState,
                                    context: _context,
                                    authBloc: _authBloc,
                                    commentController: _commentController,
                                    addressController: _addressController,
                                    scaffoldState: _scaffoldKey?.currentState,
                                    editAddressRequestModel:
                                        widget.editAddressRequestModel);
                          }
                          return ModalProgressHUD(
                            inAsyncCall: addAddressState?.isLoading ?? false,
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        _showDivider(),
                                        //_showDeliveryPointTitle(),
                                        _showSpace(SIZE_16),
                                        _showAddressFormField(),
                                        // _showReset(),
                                      ],
                                    ),
                                  ),
                                ),
                                _continueButton(),
                                _showSpace(SIZE_20),
                              ],
                            ),
                          );
                        },
                      )
                    : const SizedBox());
          },
        ),
      ),
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        appBarTitle:
            AppLocalizations.of(context).addresslistingpage.text.addAddres,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_15),
            child: NotificationReadCountWidget(
              context: _context,
            ),
          )
        ],
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show space
  Widget _showSpace(double size) {
    return SizedBox(
      height: size,
    );
  }

  //method to show horizontal divider
  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1,
    );
  }

  //method to show address form field
  Widget _showAddressFormField() {
    return InkWell(
      onTap: () async {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          permission = await Geolocator.requestPermission();
          if (permission != LocationPermission.whileInUse &&
              permission != LocationPermission.always) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: _context,
                scaffoldState: _scaffoldKey?.currentState,
                message: AppLocalizations.of(context)
                    .enablelocationpage
                    .error
                    .pleaseGrantPermission);
          }
        } else if (permission == LocationPermission.deniedForever) {
          permission = await Geolocator.requestPermission();
          if (permission == LocationPermission.whileInUse ||
              permission == LocationPermission.always) {
            _serviceEnabled = await CurrentLocationManger.locationMangerInstance
                .serviceEnabled();
            if (_serviceEnabled) {
              _selectedLocation = await NavigatorUtils.navigatorUtilsInstance
                  .navigatorPushedNameResult(
                      _context, AddressRoutes.SET_DELIVERY_ADDRESS,
                      dataToBeSend: widget?.editAddressRequestModel);
              if (_selectedLocation != null) {
                _addressController.text = _selectedLocation.currentAddress;
              }
            } else {
              DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                  context: _context,
                  scaffoldState: _scaffoldKey?.currentState,
                  message: AppLocalizations.of(_context)
                      .enablelocationpage
                      .error
                      .locationNotAllowed);
            }
          } else {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: _context,
                scaffoldState: _scaffoldKey?.currentState,
                message: AppLocalizations.of(context)
                    .enablelocationpage
                    .error
                    .permissionDenied);
          }
        } else {
          _serviceEnabled = await CurrentLocationManger.locationMangerInstance
              .serviceEnabled();

          if (_serviceEnabled) {
            _selectedLocation = await NavigatorUtils.navigatorUtilsInstance
                .navigatorPushedNameResult(
                    _context, AddressRoutes.SET_DELIVERY_ADDRESS,
                    dataToBeSend: widget?.editAddressRequestModel);
            if (_selectedLocation != null) {
              _addressController.text = _selectedLocation.currentAddress;
            }
          } else {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: _context,
                scaffoldState: _scaffoldKey?.currentState,
                message: AppLocalizations.of(_context)
                    .enablelocationpage
                    .error
                    .locationNotAllowed);
          }
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(left: SIZE_20, right: SIZE_20),
        child: _textFieldForm(
          keyboardType: TextInputType.text,
          icon: SvgPicture.asset(
            LOCATION_ICON,
            height: SIZE_10,
            width: SIZE_10,
            color: Colors.black,
          ),
          hint: AppLocalizations.of(context).setaddresspage.hint.emptyAddress,
          enabled: false,
          elevation: ELEVATION_0,
          controller: _addressController,
        ),
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      bool enabled,
      Widget icon,
      double elevation,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_30),
      elevation: elevation,
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        controller: controller,
        textCapitalization: TextCapitalization.words,
        enabled: enabled,
        keyboardType: keyboardType,
        style: AppConfig.of(_context).themeData.textTheme.headline2,
        decoration: InputDecoration(
          prefixIcon: new Padding(
              padding: const EdgeInsets.only(top: SIZE_15, bottom: SIZE_15),
              child: new SizedBox(
                height: SIZE_4,
                child: icon,
              )),
          hintText: hint,
          contentPadding: EdgeInsets.all(SIZE_0),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
              borderSide: BorderSide(
                color: COLOR_BORDER_GREY,
              )),
        ),
      ),
    );
  }

  //method to show  proceed button
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.all(SIZE_20),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_7, ofWidth: false),
      child: RaisedButton(
        onPressed: () {
          NetworkConnectionUtils.networkConnectionUtilsInstance
              .getConnectivityStatus(context, showNetworkDialog: true)
              .then((onValue) {
            if (onValue) {
              // hide keyboard
              if (_addressController?.text?.isNotEmpty == true) {
                _callAddOrEditAddress();
              } else {
                DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                    context: _context,
                    scaffoldState: _scaffoldKey?.currentState,
                    message: AppLocalizations.of(context)
                        .setaddresspage
                        .error
                        .emptyAddress);
              }
            }
          });
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SIZE_80)),
        padding: EdgeInsets.all(SIZE_0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [COLOR_LIGHT_GREEN, COLOR_DARK_PRIMARY],
              ),
              borderRadius: BorderRadius.circular(SIZE_30)),
          child: Container(
            alignment: Alignment.center,
            child: Text(
             'Select',
              textAlign: TextAlign.center,
              style: textStyleSize14WithWhiteColor,
            ),
          ),
        ),
      ),
    );
  }

  //here first check user wants to add or edit the address
  void _callAddOrEditAddress() {
    if (widget?.editAddressRequestModel?.isForEdit == true) {
      _addressActionManager?.callEditAddressEvent(
          _selectedLocation,
          _commentController?.text,
          _authState,
          widget?.editAddressRequestModel);
    } else {
      _addressActionManager?.callAddAddressEvent(
          _selectedLocation, _commentController?.text, _authState);
    }
  }

  //if user came to edit address then set his address in textFormField by default
  void _setData() {
    if (widget?.editAddressRequestModel?.isForEdit == true) {
      _addressController?.text =
          widget?.editAddressRequestModel?.formattedAddress;
      _commentController?.text =
          widget?.editAddressRequestModel?.additionalInfo;
    }
  }

  Future<void> _checkSeviceEnabled() async {
    _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!_serviceEnabled) {
      permission = await Geolocator.requestPermission();
    }
  }
}

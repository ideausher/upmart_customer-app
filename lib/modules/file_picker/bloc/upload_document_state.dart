import 'dart:io';

import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/file_picker/model/file_picker_model.dart';
import '../../../modules/file_picker/model/file_picker_response_model.dart';

class UploadDocumentState extends BlocState {
  UploadDocumentState(
      {this.isLoading: false, this.message, this.file, this.filePickerResponseModel, this.filePickerIntentModel})
      : super(
          isLoading,
        );

  final bool isLoading;
  String message;
  File file;
  FilePickerResponseModel filePickerResponseModel;
  FilePickerModel filePickerIntentModel;

  // used for the sign up api call
  factory UploadDocumentState.onDocumentUpdate(
      {bool isLoading,
      String message,
      File file,
      FilePickerResponseModel filePickerResponseModel,
      FilePickerModel filePickerIntentModel}) {
    return UploadDocumentState(
        isLoading: isLoading,
        message: message,
        file: file,
        filePickerResponseModel: filePickerResponseModel,
        filePickerIntentModel: filePickerIntentModel);
  }

  // used for the sign up api call
  factory UploadDocumentState.onDocumentAdded(
      {bool isLoading,
      String message,
      File file,
      FilePickerResponseModel filePickerResponseModel,
      FilePickerModel filePickerIntentModel}) {
    return UploadDocumentState(
        isLoading: isLoading,
        message: message,
        file: file,
        filePickerResponseModel: filePickerResponseModel,
        filePickerIntentModel: filePickerIntentModel);
  }

  // used for the sign up api call
  factory UploadDocumentState.onDocumentRemoved(
      {bool isLoading,
      String message,
      File file,
      FilePickerResponseModel filePickerResponseModel,
      FilePickerModel filePickerIntentModel}) {
    return UploadDocumentState(
        isLoading: isLoading,
        message: message,
        file: file,
        filePickerResponseModel: filePickerResponseModel,
        filePickerIntentModel: filePickerIntentModel);
  }

  factory UploadDocumentState.initiating(
      {FilePickerModel filePickerIntentModel, FilePickerResponseModel filePickerResponseModel}) {
    return UploadDocumentState(
        isLoading: false,
        filePickerResponseModel: filePickerResponseModel,
        filePickerIntentModel: filePickerIntentModel);
  }
}

// upload document
const String UPLOAD_DOCUMENT_ICON = 'lib/modules/file_picker/images/upload_document.png';
const String DOC_PREVIEW = 'lib/modules/file_picker/images/doc_preview.png';
const String PDF_PREVIEW = 'lib/modules/file_picker/images/pdf_preview.png';
const String NO_PREVIEW = 'lib/modules/file_picker/images/no_preview.png';

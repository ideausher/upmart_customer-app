import 'package:flutter/material.dart';

import 'page/file_picker_multiple_page.dart';

class FilePickerRoutes {
  static const String FILE_PICKER_MULTIPLE_SCREEN_ROOT = '/FILE_PICKER_MULTIPLE_SCREEN_ROOT';

  static Map<String, WidgetBuilder> routes() {
    return {
      FILE_PICKER_MULTIPLE_SCREEN_ROOT: (context) => FilePickerMultiplePage(context),
    };
  }
}

import '../../../modules/file_picker/enums/file_picker_enums.dart';

class FilePickerModel {
  /// Document/Image Type*/
  FilePickerTypeEnum pickerType;

  ///Camera gallery both
  FilePickFromEnum pickFrom;

  ///number of files to select or upload
  num maxFiles;

  ///true -server/local
  ///else -local
  ///default=false
  bool uploadEnable;

  ///used in case of uploadEnable  (api endpoint to upload)
  String endPointUpload;

  ///used in case of uploadEnable (api endpoint to delete)
  String endPointRemove;

  ///used in case of Image Type default=true
  bool cropEnable;

  ///used in case of Document Type
  List<String> docTypes;
  int compressedQuality;

  FilePickerModel(
      {this.pickerType,
      this.pickFrom,
      this.maxFiles = 1,
      this.uploadEnable = false,
      this.endPointUpload = "",
      this.endPointRemove = "",
      this.docTypes,
      this.cropEnable = true,
      this.compressedQuality = 75});
}

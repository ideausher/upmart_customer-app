import 'dart:io';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:user/modules/common/app_config/app_config.dart';

class ForceUpdateProvider {
  Future<dynamic> forceUpdateApiCall({
    BuildContext context,
  }) async {
    PackageInfo _packageInfo = await PackageInfo.fromPlatform();
    print(
        "info --- ${_packageInfo.packageName}  ,  ${_packageInfo.appName}  ,  ${_packageInfo.buildNumber}  , ${_packageInfo.version}");
    var _forceUpdate = "/app_version";
    var result = await AppConfig.of(context)
        .baseApi
        .postRequest(_forceUpdate, context, data: {
      "bundle_id": _packageInfo.packageName,
      "platform": (Platform.isAndroid) ? "0" : "1",
    });

    return result;
  }
}

// To parse this JSON data, do
//
//     final forceUodateResponseModel = forceUodateResponseModelFromMap(jsonString);

import 'dart:convert';

ForceUpdateResponseModel forceUodateResponseModelFromMap(String str) => ForceUpdateResponseModel.fromMap(json.decode(str));

String forceUodateResponseModelToMap(ForceUpdateResponseModel data) => json.encode(data.toMap());

class ForceUpdateResponseModel {
  ForceUpdateResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  ForceUpdate data;
  String message;
  int statusCode;

  factory ForceUpdateResponseModel.fromMap(Map<String, dynamic> json) => ForceUpdateResponseModel(
    data: json["data"] == null ? null : ForceUpdate.fromMap(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toMap() => {
    "data": data == null ? null : data.toMap(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class ForceUpdate {
  ForceUpdate({
    this.forceUpdate,
    this.message,
    this.version,
    this.code,
    this.platform,
  });

  int forceUpdate;
  String message;
  int version;
  String code;
  int platform;

  factory ForceUpdate.fromMap(Map<String, dynamic> json) => ForceUpdate(
    forceUpdate: json["force_update"] == null ? null : json["force_update"],
    message: json["message"] == null ? null : json["message"],
    version: json["version"] == null ? null : json["version"],
    code: json["code"] == null ? null : json["code"],
    platform: json["platform"] == null ? null : json["platform"],
  );

  Map<String, dynamic> toMap() => {
    "force_update": forceUpdate == null ? null : forceUpdate,
    "message": message == null ? null : message,
    "version": version == null ? null : version,
    "code": code == null ? null : code,
    "platform": platform == null ? null : platform,
  };
}

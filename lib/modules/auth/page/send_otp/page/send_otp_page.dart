import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:user/modules/auth/api/social_login/model/social_login_request_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_event.dart';
import 'package:user/modules/auth/constants/image_constant.dart';
import 'package:user/modules/auth/enums/auth_enums.dart';
import 'package:user/modules/auth/social_login/apple/model/apple_signin_response_model.dart';
import 'package:user/modules/auth/social_login/apple/utils/apple_sign_in_utils.dart';
import 'package:user/modules/auth/social_login/facebook/model/facebook_auth_response_model.dart';
import 'package:user/modules/auth/social_login/facebook/utils/facebook_auth_utils.dart';
import 'package:user/modules/auth/social_login/google/model/google_auth_response_model.dart';
import 'package:user/modules/auth/social_login/google/utils/google_auth_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/model/update_ui_data_model.dart';
import 'package:user/modules/common/utils/launcher_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../common/app_config/app_config.dart';
import '../../../../common/constants/dimens_constants.dart';
import '../../../auth_bloc/auth_bloc.dart';
import '../../../auth_bloc/auth_state.dart';

class SendOtpPage extends StatefulWidget {
  BuildContext context;
  bool isGuestUser;
  bool showBackButton;
  SendOtpPage({this.context, this.isGuestUser, this.showBackButton: true});

  @override
  _SendOtpPageState createState() => _SendOtpPageState();
}

class _SendOtpPageState extends State<SendOtpPage> {
  //Declaration of auth bloc
  var _authBloc;
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Declaration text editing controller
  final TextEditingController _phoneController = TextEditingController();

  //Declaration of manager
  AuthManager _authManager = AuthManager();

  //Declaration of UI updation model
  UpdateUiDataModel _updateUiDataModel;

  bool _checkAppleSignInAvailable = false;

  @override
  void initState() {
    super.initState();
    _updateUiDataModel = new UpdateUiDataModel();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    if (ModalRoute
        .of(widget.context)
        .settings
        .arguments is bool) {
      widget?.isGuestUser = ModalRoute
          .of(widget.context)
          .settings
          .arguments;
    }
    _authManager.actionOnInit(
        context: widget.context,
        authBloc: _authBloc,
        updateUiDataModel: _updateUiDataModel);
    _showAppleSignIn();
  }

  @override
  void dispose() {
    super.dispose();
    _phoneController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      key: _scaffoldKey,
      body: BlocEventStateBuilder<AuthState>(
        bloc: _authBloc,
        builder: (BuildContext context, AuthState authState) {
          _context = context;
          if (authState != null && _authState != authState) {
            print('Calling this send otp page..');
            _authState = authState;
            _authManager.actionSendOtpStateChange(
                context: context,
                scaffoldState: _scaffoldKey?.currentState,
                phoneController: _phoneController,
                authBloc: _authBloc,
                authState: _authState,
                updateUiDataModel: _updateUiDataModel);
          }
          return ModalProgressHUD(
            inAsyncCall: authState?.isLoading ?? false,
            child: SafeArea(
              bottom: true,
              top: true,
              child: SingleChildScrollView(
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Image.asset(UPMART_BACKGROUND,
                        height: MediaQuery
                            .of(context)
                            .size
                            .height,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        fit: BoxFit.cover),
                    Column(
                      children: <Widget>[
                        _showBackButton(),
                        _showLogInLogo(),
                        SizedBox(
                          height: SIZE_50,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: SIZE_20),
                          child: Container(
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                context: _context,
                                percentage: SIZE_60,
                                ofWidth: false),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(SIZE_10))),
                              margin: EdgeInsets.all(SIZE_20),
                              child: Padding(
                                padding: const EdgeInsets.all(SIZE_16),
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: SIZE_10,
                                      ),
                                      _showTitleText(),
                                      _form(),
                                      _acceptTermsAndConditionWidget(),
                                      SizedBox(
                                        height: SIZE_10,
                                      ),
                                      _continueButton(),
                                      SizedBox(
                                        height: SIZE_10,
                                      ),
                                      _showOrView(),
                                      _showContinueWithText(),
                                      _showSocialLoginView(),
                                      SizedBox(
                                        height: SIZE_20,
                                      ),
                                      _showLoginAsGuest()
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  // used to create a top text view
  Widget _showLogInLogo() {
    return Container(
        margin: EdgeInsets.only(top: SIZE_20),
        width: MediaQuery
            .of(_context)
            .size
            .width,
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, ofWidth: false, percentage: SIZE_20),
        child: Image.asset(
          SIGN_UP_LOGO,
        ));
  }

  //used to create a form
  Widget _form() {
    return Form(
      child: _showCountryCodePickerAndPhoneNumber(),
    );
  }

  // used to create a  phone text field form
  Widget _phoneTextFormField() {
    return Row(
      children: <Widget>[
        Expanded(
          child: _textFieldForm(
            keyboardType: TextInputType.number,
            hint: AppLocalizations
                .of(_context)
                .sendotppage
                .hint
                .phoneNumber,
            elevation: ELEVATION_0,
            controller: _phoneController,
            perfixIcon: Padding(
              padding: const EdgeInsets.only(top: SIZE_5),
              child: Image.asset(
                USER_PHONE_ICON,
                height: SIZE_10,
                width: SIZE_10,
              ),
            ),
          ),
        )
      ],
    );
  }

  // used to create a  phone country picker  text field form
  Widget _showCountryCodePickerAndPhoneNumber() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      margin: EdgeInsets.only(top: SIZE_20),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: Row(
        children: <Widget>[
          Expanded(
            child: _phoneTextFormField(),
          )
        ],
      ),
    );
  }

  //method to show  proceed button
  Widget _continueButton() {
    return Container(
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_DARK_PRIMARY, COLOR_PRIMARY],
        ),
        onPressed: () {
          _validateSignUp();
        },
        child: Text(
          AppLocalizations
              .of(_context)
              .common
              .button
              .continueText,
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm({TextEditingController controller,
    TextInputType keyboardType,
    Widget perfixIcon,
    String hint,
    double elevation,
    FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_20),
      elevation: elevation,
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        onFieldSubmitted: _showTickMark,
        onChanged: (phoneNumber) {
          if (phoneNumber.length >= MaxLength.MinPhoneLength.value) {
            _showTickMark(phoneNumber);
          } else {
            _showTickMark(phoneNumber);
          }
        },
        controller: controller,
        keyboardType: keyboardType,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
          new LengthLimitingTextInputFormatter(14),
        ],
        style: AppConfig
            .of(_context)
            .themeData
            .textTheme
            .headline2,
        decoration: InputDecoration(
          suffixIcon: (_updateUiDataModel?.phoneNumber != null &&
              _updateUiDataModel?.phoneNumber?.length >=
                  MaxLength.MinPhoneLength.value)
              ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
              : null,
          hintText: hint,
          contentPadding: EdgeInsets.only(left: SIZE_10),
          prefixIcon: perfixIcon,
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  //this method will show the tick mark icon when user hits done button while typing number
  void _showTickMark(String values) {
    _updateUiDataModel.phoneNumber = values;
    _authManager?.updateUI(
      context: _context,
      updateUiDataModel: _authState?.updateUiDataModel ?? _updateUiDataModel,
      authBloc: _authBloc,
      authState: _authState,
    );
  }

  //method to return accept terms and services widget
  Widget _acceptTermsAndConditionWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Theme(
          data: Theme.of(_context).copyWith(
            unselectedWidgetColor: COLOR_BORDER_GREY,
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: SIZE_12),
            child: Checkbox(
                value: _authState?.updateUiDataModel?.isTermsAccepted ?? false,
                activeColor: COLOR_PRIMARY,
                onChanged: (bool value) {
                  _updateUiDataModel?.isTermsAccepted = value;
                  _updateUiDataModel?.phoneNumber =
                      _phoneController?.text.toString();
                  _authManager?.updateUI(
                      context: _context,
                      authState: _authState,
                      updateUiDataModel: _updateUiDataModel,
                      authBloc: _authBloc);
                }),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: SIZE_0, bottom: SIZE_10),
          child: Center(
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: AppLocalizations
                          .of(_context)
                          .sendotppage
                          .text
                          .termsAndPolicies +
                          " ",
                      style: textStyleSize28WithBlackColor,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                            LauncherUtils.launcherUtilsInstance.launchInBrowser(
                              url: AppConfig.of(context).termsUrl);
                        }),
                ],
              ),
              textScaleFactor: 0.5,
            ),
          ),
        ),
      ],
    );
  }

  //method to call validate for when proceed button is pressed
  void _validateSignUp() {
    _authManager.sendOtpCall(
      context: _context,
      authBloc: _authBloc,
      phoneNumber: _phoneController?.text,
      updateUiDataModel: _updateUiDataModel,
      selectedCountry:
      Country.CA /*_updateUiDataModel?.selectedCountry ?? Country.IN*/,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }

  //this method is used to show title text
  Widget _showTitleText() {
    return Text(AppLocalizations
        .of(_context)
        .sendotppage
        .text
        .signIn,
        textAlign: TextAlign.center,
        style: AppConfig
            .of(_context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show or view widget
  Widget _showOrView() {
    return Row(
      children: [
        Expanded(
          child: Divider(
            thickness: SIZE_1,
            color: COLOR_LIGHT_GREY,
          ),
        ),
        SizedBox(
          width: SIZE_10,
        ),
        Text(
          'OR',
          style: textStyleSize14WithGREY,
        ),
        SizedBox(
          width: SIZE_10,
        ),
        Expanded(
          child: Divider(
            thickness: SIZE_1,
            color: COLOR_LIGHT_GREY,
          ),
        ),
      ],
    );
  }

  Widget _showContinueWithText() {
    return Padding(
      padding: const EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
      child: Align(
        alignment: Alignment.center,
        child: Text('Continue With',
            style: AppConfig
                .of(_context)
                .themeData
                .primaryTextTheme
                .headline6),
      ),
    );
  }

  //this method is used to show social login options
  Widget _showSocialLoginView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _checkPlatform(),
        InkWell(
          onTap: () async {
            GoogleAuthResponseModel googleAuthResponse = await GoogleAuthUtils
                .googleAuthUtilsInstance
                .initiateGoogleLogin(_context);
            if (googleAuthResponse?.data != null) {
              _callSocialLoginEvent(
                  isForGoogle: true, googleData: googleAuthResponse?.data);
            }
          },
          child: Container(
            padding: EdgeInsets.all(SIZE_10),
            margin: EdgeInsets.only(right: SIZE_20, top: SIZE_20),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: COLOR_GREY_PICKER,
                ),
                borderRadius: BorderRadius.all(Radius.circular(SIZE_10))),
            child: Image.asset(GOOGLE_ICON,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, percentage: SIZE_3, ofWidth: false),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, percentage: SIZE_3, ofWidth: false)),
          ),
        ),
        InkWell(
          onTap: () async {
            FacebookResponseModel facebookResponse = await FacebookAuthUtils
                .fbAuthUtilsInstance
                .initiateFacebookLogin(_context);
            if (facebookResponse?.data != null) {
              _callSocialLoginEvent(
                  isForGoogle: false, data: facebookResponse?.data);
            }
          },
          child: Container(
            padding: EdgeInsets.all(SIZE_10),
            margin: EdgeInsets.only(top: SIZE_20),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: COLOR_GREY_PICKER,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(SIZE_10),
                )),
            child: Image.asset(
              FACEBOOK_ICON,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, percentage: SIZE_3, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, percentage: SIZE_3, ofWidth: false),
            ),
          ),
        ),
      ],
    );
  }

  //show apple logo on the basis of condition
  Widget _checkPlatform() {
    if (Platform.isIOS) {
      return Visibility(
        visible: _checkAppleSignInAvailable,
        child: InkWell(
          onTap: () {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context)
                .then((onValue) async {
              if (onValue) {
                AppleResponseModel result = await AppleSignInUtils
                    .appleSignInUtilsInstance
                    .appleSignIn(context: context);
                if (result?.data != null) {
                  _authBloc?.emitEvent(SocialLoginEvent(
                      context: _context,
                      isLoading: true,
                      isResendOtp: false,
                      isUserBlock: false,
                      socialLoginRequestModel: new SocialLoginRequestModel(
                          socialId: result?.data?.user,
                          socialType: "2",
                          userType: UserRoles.Customer.value,
                          name: result?.data?.fullName ?? "",
                          profilePicture: "",
                          email: result?.data?.email ?? "",
                          phoneNumber: "" ?? "")));
                  }
                  }
                  });
          },
          child: Container(
            padding: EdgeInsets.all(SIZE_10),
            margin: EdgeInsets.only(right: SIZE_20, top: SIZE_20),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: COLOR_GREY_PICKER,
                ),
                borderRadius: BorderRadius.all(Radius.circular(SIZE_10))),
            child: Image.asset(APPLE_ICON,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, percentage: SIZE_3, ofWidth: false),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, percentage: SIZE_3, ofWidth: false)),
          ),
        ),
      );
    } else {
      return const SizedBox();
    }
  }

  //this method is used to show user a option for guest Login
  Widget _showLoginAsGuest() {
    return (widget?.isGuestUser == false)
        ? InkWell(
      onTap: () {
        _authManager?.checkLocationIsEnabled(
            context: widget?.context,
            authState: _authState,
            authBloc: _authBloc);
      },
      child: Align(
        alignment: Alignment.center,
        child: Text(
          AppLocalizations
              .of(context)
              .sendotppage
              .text
              .guestLogin,
          style: AppConfig
              .of(_context)
              .themeData
              .textTheme
              .headline3,
        ),
      ),
    )
        : const SizedBox();
  }

  //method to show back button
  Widget _showBackButton() {
    return (widget?.isGuestUser == true && widget?.showBackButton == true)
        ? InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        return NavigatorUtils.navigatorUtilsInstance
            .navigatorPopScreen(context, dataToBeSend: true);
      },
      child: Align(
        alignment: Alignment.topLeft,
        child: Padding(
          padding: const EdgeInsets.only(left: SIZE_20),
          child: Icon(
            Icons.arrow_back,
            size: SIZE_20,
            color: Colors.white,
          ),
        ),
      ),
    )
        : const SizedBox();
  }

  //this method is used to call social login event
  void _callSocialLoginEvent(
      {FacebookData data, bool isForGoogle: false, GoogleData googleData}) {
    print('the facebook id is ${data?.fbId}');
    if (!isForGoogle) {
      _authBloc?.emitEvent(SocialLoginEvent(
          context: _context,
          isLoading: true,
          isResendOtp: false,
          isUserBlock: false,
          socialLoginRequestModel: new SocialLoginRequestModel(
              socialId: data?.fbId,
              socialType: "2",
              userType: UserRoles.Customer.value,
              name: data?.firstName ?? "",
              profilePicture: data?.profilePicture,
              email: data?.email ?? "",
              phoneNumber: data?.phone ?? "")));
    } else {
      _authBloc?.emitEvent(SocialLoginEvent(
          context: _context,
          isLoading: true,
          isResendOtp: false,
          isUserBlock: false,
          socialLoginRequestModel: new SocialLoginRequestModel(
              socialId: googleData.providerUserId,
              socialType: "2",
              userType: UserRoles.Customer.value,
              name: googleData?.name ?? "",
              profilePicture: googleData?.photoUrl,
              email: googleData?.email ?? "",
              phoneNumber: googleData?.phone ?? "")));
    }
  }

  Future<void> _showAppleSignIn() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      num _version = double.parse(iosInfo?.systemVersion ?? "0.0");
      if (_version >= 13.0) {
        setState(() {
          _checkAppleSignInAvailable = true;
        });
      }
    }
  }
}

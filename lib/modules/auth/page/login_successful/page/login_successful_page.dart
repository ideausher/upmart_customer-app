import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:user/modules/address/enum/address_enum.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/auth/auth_routes.dart';
import 'package:user/modules/auth/constants/image_constant.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:user/routes.dart';
import '../../../../../localizations.dart';

class LogInSuccessFullScreen extends StatefulWidget {
  BuildContext _context;

  LogInSuccessFullScreen(this._context);

  @override
  _LogInSuccessFullScreenState createState() => _LogInSuccessFullScreenState();
}

class _LogInSuccessFullScreenState extends State<LogInSuccessFullScreen> {
  bool _serviceEnabled;
  Position _myLocation;
  CurrentLocation _currentLocation;

  //Declaration of Bloc Variables
  var _authBloc = AuthBloc(); // bloc
  AuthState _authState;

  //class variables
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    //here starting the timer of 2 sec after this navigate to next screen
     Future.delayed(Duration(seconds: 2), () async {
      _checkLocationPermissionStatus();
    });
    _authBloc = BlocProvider.of<AuthBloc>(widget._context);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _context = context;
        if (authState != null && _authState != authState) {
          _authState = authState;
        }
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            key: _scaffoldKey,
            body: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Image.asset(
                  UPMART_BACKGROUND,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  fit: BoxFit.cover,
                ),
                SingleChildScrollView(
                  child: Column(
                    children: [
                      _showLoginLogo(),
                      SizedBox(
                        height: SIZE_50,
                      ),
                      Container(
                        width: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                            context: _context,
                            percentage: SIZE_100,
                            ofWidth: true),
                        child: Container(
                          height: CommonUtils.commonUtilsInstance
                              .getPercentageSize(
                              context: _context,
                              percentage: SIZE_55,
                              ofWidth: false),
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(SIZE_10))),
                            margin: EdgeInsets.all(SIZE_20),
                            child: Column(

                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top:SIZE_20,left: SIZE_10),
                                  child: _showTitleText(),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        LOGIN_SUCCESSFUL_LOGO,
                                        height: CommonUtils.commonUtilsInstance.getPercentageSize(context: _context,percentage: SIZE_10,ofWidth:false),
                                      ),
                                      Text(
                                        'Successful',
                                        style: AppConfig
                                            .of(context)
                                            .themeData
                                            .textTheme
                                            .headline6,
                                      ),
                                    ],
                                  ),
                                )


                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
        ;
      },
    );
  }

  //this method will check the location permission status if enables navigate to home else navigate to enable location screen
  void _checkLocationPermissionStatus() async {
    _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!_serviceEnabled) {
      NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
          widget?._context, AuthRoutes.ENABLE_LOCATION_ROOT);
    } else {
      LocationPermission permissionStatus = await CurrentLocationManger
          .locationMangerInstance
          .permissionEnabled();

      //if location permission is allowed only then get user location else show grant location meassage
      if (permissionStatus == LocationPermission.always ||
          permissionStatus == LocationPermission.whileInUse) {
        _getLocationAndFindAddress();
      }
      //if user denies the permission navigate to location screen
      else {
        NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
            widget?._context, AuthRoutes.ENABLE_LOCATION_ROOT);
      }
    }
  }

  //method to get location and find address this will execute when user have added any address
  void _getLocationAndFindAddress() async {
    if (_authState?.authResponseModel?.userData?.address?.isNotEmpty == true) {
      CurrentLocation currentLocation = new CurrentLocation();
      if (_authState?.authResponseModel?.userData?.address?.isNotEmpty ==
          true) {
        for (var address in _authState?.authResponseModel?.userData?.address) {
          if (address?.primary == AddressRoles.Primary.value) {
            currentLocation?.id = address?.addressId?.toString();
            currentLocation?.lat = address?.latitude;
            currentLocation?.lng = address?.longitude;
            currentLocation?.currentAddress = address?.formattedAddress;
            currentLocation?.city = address?.city;
            currentLocation?.postalCode = address?.pincode;
            currentLocation?.country = address?.country;
          }
        }
      }
      //here i am saving data in shared preferences
      await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
          currentLocation, PrefsEnum.UserLocationData.value);
      NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
          widget?._context, Routes.DASH_BOARD,
          dataToBeSend: currentLocation);
    }

    //this case will be executed when no address is added
    else {
      _myLocation = await Geolocator.getCurrentPosition();
      _currentLocation = await CurrentLocationManger?.locationMangerInstance
          ?.getAddressUsingLocation(widget?._context, _myLocation);
      print('current location is ${_currentLocation?.currentAddress}');
      await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
          _currentLocation, PrefsEnum.UserLocationData.value);
      NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
          widget?._context, Routes.DASH_BOARD,
          dataToBeSend: _currentLocation);
    }
  }

  // used to create a top text view
  Widget _showLoginLogo() {
    return Container(
        margin: EdgeInsets.only(top: SIZE_20),
        width: MediaQuery
            .of(_context)
            .size
            .width,
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, ofWidth: false, percentage: SIZE_20),
        child: Image.asset(
          SIGN_UP_LOGO,
        ));
  }

  //this method is used to show title text
  Widget _showTitleText() {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(AppLocalizations
          .of(_context)
          .sendotppage
          .text
          .signIn,
          style: AppConfig
              .of(_context)
              .themeData
              .primaryTextTheme
              .headline5),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_country_picker/country.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:user/modules/auth/constants/image_constant.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/constants/color_constants.dart';

import 'package:user/modules/common/model/update_ui_data_model.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/enums/auth_enums.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../auth/auth_bloc/auth_bloc.dart';
import '../../../../auth/auth_bloc/auth_state.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../common/app_config/app_config.dart';
import '../../../../common/constants/dimens_constants.dart';

class EnterDetailsPage extends StatefulWidget {
  BuildContext _context;

  EnterDetailsPage(this._context);

  @override
  _EnterDetailsPageState createState() => _EnterDetailsPageState();
}

class _EnterDetailsPageState extends State<EnterDetailsPage> {
  //Declaration of auth bloc
  var _authBloc;
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Declaration of edit text controller
  final TextEditingController _referralCodeController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  //Class variables
  AuthManager _authManager = AuthManager();
  Country _selectedCountry = Country.IN;
  UpdateUiDataModel updateUiDataModel;
  String userName = "";

  //Dropdown item
  List<int> _dropdownValues = [
    UserGender.Mrs.value,
    UserGender.Mr.value,
    UserGender.Ms.value
  ];

  @override
  Future<void> initState() {
    super.initState();
    updateUiDataModel = UpdateUiDataModel();
    _authBloc = BlocProvider.of<AuthBloc>(widget._context);
  }

  @override
  void dispose() {
    super.dispose();
    _referralCodeController.dispose();
    _nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;

    return WillPopScope(
      onWillPop: () {
        return _authManager?.signOutDialog(
            context: _context,
            authState: _authState,
            scaffoldState: _scaffoldKey.currentState,
            authBloc: _authBloc);
      },
      child: SafeArea(
        bottom: true,
        top: true,
        child: Scaffold(
          key: _scaffoldKey,
          body: BlocEventStateBuilder<AuthState>(
            bloc: _authBloc,
            builder: (BuildContext context, AuthState authState) {
              _context = context;
              if (authState != null && _authState != authState) {
                _authState = authState;
                if (authState?.authResponseModel?.userData != null &&
                    _authState?.authResponseModel?.userData?.countryIsoCode
                        ?.isNotEmpty ==
                        true) {
                  _selectedCountry = Country.findByIsoCode(authState
                      ?.authResponseModel?.userData?.countryIsoCode ??
                      authState?.updateUiDataModel?.selectedCountry?.isoCode);
                }

                _authManager.actionOnEnterDetailsStateChanged(
                    context: context,
                    authBloc: _authBloc,
                    authState: authState,
                    updateUiDataModel: authState?.updateUiDataModel,
                    scaffoldState: _scaffoldKey?.currentState,
                    selectedCountry: _selectedCountry);
              }
              return ModalProgressHUD(
                inAsyncCall: authState?.isLoading ?? false,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Image.asset(
                      UPMART_BACKGROUND,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      fit: BoxFit.cover,
                    ),
                    SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          _showLoginLogo(),
                          SizedBox(
                            height: SIZE_50,
                          ),

                          Container(
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                context: _context,
                                percentage: SIZE_55,
                                ofWidth: false),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(SIZE_10))),
                              margin: EdgeInsets.all(SIZE_20),
                              child: Padding(
                                padding: const EdgeInsets.all(SIZE_16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: SIZE_10,
                                    ),
                                    _showTitleText(),
                                    _showNameTextField(),
                                    _showRefferalCodeTextField(),
                                    _showSpace(),
                                    _continueButton(),
                                    SizedBox(
                                        height: CommonUtils?.commonUtilsInstance
                                            ?.getPercentageSize(
                                            context: _context,
                                            ofWidth: false,
                                            percentage: SIZE_12))
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  // used to create a top text view
  Widget _showLoginLogo() {
    return Container(
        margin: EdgeInsets.only(top: SIZE_20),
        width: MediaQuery
            .of(_context)
            .size
            .width,
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, ofWidth: false, percentage: SIZE_20),
        child: Image.asset(
          SIGN_UP_LOGO,
        ));
  }

  //method to show continue button
  Widget _continueButton() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_6, ofWidth: false),
      child: RaisedButton(
        onPressed: () {
          _validateInput();
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SIZE_80)),
        padding: EdgeInsets.all(SIZE_0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
                  COLOR_DARK_PRIMARY,
                  COLOR_LIGHT_GREEN,
                ],
              ),
              borderRadius: BorderRadius.circular(SIZE_30)),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              'Continue',
              textAlign: TextAlign.center,
              style: textStyleSize14WithWhiteColor,
            ),
          ),
        ),
      ),
    );
  }

  //method to return textform field
  Widget _textFieldForm({TextEditingController controller,
    TextInputType keyboardType,
    IconData icon,
    Widget prefixIcon,
    String hint,
    double elevation,
    bool enabled,
    bool showTickIcon = false,
    FormFieldValidator<String> validator}) {
    return Material(
      color: Colors.white,
      borderRadius: BorderRadius.circular(SIZE_20),
      elevation: elevation,
      child: TextFormField(
        validator: validator,
        onFieldSubmitted: _updateUserName,
        textCapitalization: TextCapitalization.words,
        onChanged: (name) {
          userName = name;
          if (showTickIcon) {
            if (name.length >= MinLength.Username.value) {
              _updateUserName(name);
            } else {
              _updateUserName(name);
            }
          }
        },
        style: AppConfig
            .of(_context)
            .themeData
            .textTheme
            .headline2,
        controller: controller,
        enabled: enabled ?? true,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          suffixIcon: (showTickIcon == true)
              ? (userName.isNotEmpty &&
              userName?.length >= MinLength.Username.value)
              ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
              : null
              : null,
          hintText: hint,
          prefixIcon: prefixIcon,
          contentPadding: EdgeInsets.all(SIZE_0),
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),

      ),
    );
  }

  //method to check validation of phone field
  void _validateInput() {
    _authManager.enterDetailsUpdate(
      scaffoldState: _scaffoldKey?.currentState,
      context: _context,
      authBloc: _authBloc,
      updateUiDataModel: _authState?.updateUiDataModel,
      authResponseModel: _authState?.authResponseModel,
      name: _nameController?.text,
      referralCode: _referralCodeController?.text
    );
  }

  //method to return text for filed of name
  Widget _showNameTextField() {
    return Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: _nameTextFormField());
  }

  //enter name field
  Widget _nameTextFormField() {
    return _textFieldForm(
        keyboardType: TextInputType.text,
        hint: 'Username',
        elevation: ELEVATION_0,
        icon: null,
        controller: _nameController,
        showTickIcon: true,
        prefixIcon: Padding(
          padding: const EdgeInsets.only(top: SIZE_5),
          child: Image.asset(
            USER_PHONE_ICON,
            height: SIZE_10,
            width: SIZE_10,
          ),
        ));
  }

  //this method will give the space between the widgets
  Widget _showSpace() {
    return SizedBox(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, ofWidth: false, percentage: SIZE_5),
    );
  }

  void _updateUserName(String userName) {
    _authManager?.updateUI(
        context: _context,
        authBloc: _authBloc,
        authState: _authState,
        updateUiDataModel: _authState?.updateUiDataModel);
  }

  //this method is used to show title text
  Widget _showTitleText() {
    return Text(AppLocalizations
        .of(_context)
        .sendotppage
        .text
        .signIn,
        textAlign: TextAlign.center,
        style: AppConfig
            .of(_context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  Widget _showRefferalCodeTextField() {
    return Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: SIZE_16),
        padding: EdgeInsets.only(left: SIZE_10, right: SIZE_5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: _textFieldForm(
            keyboardType: TextInputType.text,
            hint: 'Referral Code',
            elevation: ELEVATION_0,
            icon: null,
            showTickIcon: false,
            controller: _referralCodeController,
            prefixIcon: Padding(
              padding: const EdgeInsets.only(top: SIZE_5),
              child: Image.asset(
                USER_PHONE_ICON,
                height: SIZE_10,
                width: SIZE_10,
              ),
            )));
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';

class EnableCurrentLocationState extends BlocState {
  EnableCurrentLocationState({this.isLoading: false, this.message, this.context, this.currentLocation})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final CurrentLocation currentLocation;

  // used for update home page state
  factory EnableCurrentLocationState.updateLocation(
      {bool isLoading, String message, BuildContext context, CurrentLocation currentLocation}) {
    return EnableCurrentLocationState(
        isLoading: isLoading, message: message, context: context, currentLocation: currentLocation);
  }

  factory EnableCurrentLocationState.initiating() {
    return EnableCurrentLocationState(
      isLoading: false,
    );
  }
}

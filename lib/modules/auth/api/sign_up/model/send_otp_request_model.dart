// To parse this JSON data, do
//
//     final sendOtpRequestModel = sendOtpRequestModelFromJson(jsonString);

import 'dart:convert';

SendOtpRequestModel sendOtpRequestModelFromJson(String str) => SendOtpRequestModel.fromJson(json.decode(str));

String sendOtpRequestModelToJson(SendOtpRequestModel data) => json.encode(data.toJson());

class SendOtpRequestModel {
  SendOtpRequestModel({this.type, this.phoneNumber, this.countryCode, this.countryIsoCode, this.email});

  String type;
  String phoneNumber;
  String countryCode;
  String countryIsoCode;
  String email;

  factory SendOtpRequestModel.fromJson(Map<String, dynamic> json) => SendOtpRequestModel(
        type: json["type"] == null ? null : json["type"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
        email: json["email"] == null ? null : json["email"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "country_code": countryCode == null ? null : countryCode,
        "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
        "email": email == null ? null : email,
      };
}

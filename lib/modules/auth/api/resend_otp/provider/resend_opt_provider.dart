import 'package:flutter/material.dart';

import '../../../../common/app_config/app_config.dart';

class ResendOtpProvider {
  Future<dynamic> resendOtpApiCall({
    BuildContext context,
  }) async {
    var resentOtp = "v1/resendOtp";

    var result = await AppConfig.of(context).baseApi.getRequest(
          resentOtp,
          context,
        );

    return result;
  }
}

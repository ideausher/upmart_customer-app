// To parse this JSON data, do
//
//     final verifyOtpRequestModel = verifyOtpRequestModelFromJson(jsonString);

import 'dart:convert';

VerifyOtpRequestModel verifyOtpRequestModelFromJson(String str) => VerifyOtpRequestModel.fromJson(json.decode(str));

String verifyOtpRequestModelToJson(VerifyOtpRequestModel data) => json.encode(data.toJson());

class VerifyOtpRequestModel {
  VerifyOtpRequestModel({
    this.otp,
    this.phoneNumber,
    this.countryCode,
    this.type,
    this.action,
  });

  String otp;
  String phoneNumber;
  String countryCode;
  String type;
  String action;

  factory VerifyOtpRequestModel.fromJson(Map<String, dynamic> json) => VerifyOtpRequestModel(
        otp: json["otp"] == null ? null : json["otp"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        type: json["type"] == null ? null : json["type"],
        action: json["action"] == null ? null : json["action"],
      );

  Map<String, dynamic> toJson() => {
        "otp": otp == null ? null : otp,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "country_code": countryCode == null ? null : countryCode,
        "type": type == null ? null : type,
        "action": action == null ? null : action,
      };
}

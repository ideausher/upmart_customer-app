// To parse this JSON data, do
//
//     final isUserBlockedRequestModel = isUserBlockedRequestModelFromJson(jsonString);

import 'dart:convert';

UserBlocRequestModel isUserBlockedRequestModelFromJson(String str) => UserBlocRequestModel.fromJson(json.decode(str));

String isUserBlockedRequestModelToJson(UserBlocRequestModel data) => json.encode(data.toJson());

class UserBlocRequestModel {
  UserBlocRequestModel({
    this.type,
    this.phoneNumber,
    this.countryCode,
  });

  int type;
  String phoneNumber;
  String countryCode;

  factory UserBlocRequestModel.fromJson(Map<String, dynamic> json) => UserBlocRequestModel(
        type: json["type"] == null ? null : json["type"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "country_code": countryCode == null ? null : countryCode,
      };
}

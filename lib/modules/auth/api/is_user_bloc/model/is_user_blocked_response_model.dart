// To parse this JSON data, do
//
//     final isUserBlockedResponseModel = isUserBlockedResponseModelFromJson(jsonString);

import 'dart:convert';

IsUserBlockedResponseModel isUserBlockedResponseModelFromJson(String str) =>
    IsUserBlockedResponseModel.fromMap(json.decode(str));

String isUserBlockedResponseModelToJson(IsUserBlockedResponseModel data) => json.encode(data.toJson());

class IsUserBlockedResponseModel {
  IsUserBlockedResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory IsUserBlockedResponseModel.fromMap(Map<String, dynamic> json) => IsUserBlockedResponseModel(
        data: json["data"] == null ? null : Data.fromMap(json["data"]),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Data {
  Data({
    this.userType,
    this.isBlocked,
  });

  int userType;
  int isBlocked;

  factory Data.fromMap(Map<String, dynamic> json) => Data(
        userType: json["user_type"] == null ? null : json["user_type"],
        isBlocked: json["is_blocked"] == null ? null : json["is_blocked"],
      );

  Map<String, dynamic> toJson() => {
        "user_type": userType == null ? null : userType,
        "is_blocked": isBlocked == null ? null : isBlocked,
      };
}

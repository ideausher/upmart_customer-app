// To parse this JSON data, do
//
//     final socialLoginRequestModel = socialLoginRequestModelFromJson(jsonString);

import 'dart:convert';

SocialLoginRequestModel socialLoginRequestModelFromJson(String str) => SocialLoginRequestModel.fromJson(json.decode(str));

String socialLoginRequestModelToJson(SocialLoginRequestModel data) => json.encode(data.toJson());

class SocialLoginRequestModel {
  SocialLoginRequestModel({
    this.socialType,
    this.socialId,
    this.userType,
    this.name,
    this.email,
    this.phoneNumber,
    this.countryCode,
    this.countryIsoCode,
    this.dateOfBirth,
    this.profilePicture,
  });

  String socialType;
  String socialId;
  int userType;
  String name;
  String email;
  String phoneNumber;
  String countryCode;
  String countryIsoCode;
  String dateOfBirth;
  String profilePicture;

  factory SocialLoginRequestModel.fromJson(Map<String, dynamic> json) => SocialLoginRequestModel(
    socialType: json["social_type"] == null ? null : json["social_type"],
    socialId: json["social_id"] == null ? null : json["social_id"],
    userType: json["user_type"] == null ? null : json["user_type"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
    dateOfBirth: json["date_of_birth"] == null ? null : json["date_of_birth"],
    profilePicture: json["profile_picture"] == null ? null : json["profile_picture"],
  );

  Map<String, dynamic> toJson() => {
    "social_type": socialType == null ? null : socialType,
    "social_id": socialId == null ? null : socialId,
    "user_type": userType == null ? null : userType,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "country_code": countryCode == null ? null : countryCode,
    "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
    "date_of_birth": dateOfBirth == null ? null : dateOfBirth,
    "profile_picture": profilePicture == null ? null : profilePicture,
  };
}

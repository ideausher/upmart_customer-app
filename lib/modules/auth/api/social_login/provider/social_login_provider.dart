import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/social_login/model/social_login_request_model.dart';
import 'package:user/modules/common/app_config/app_config.dart';

class SocialLoginProvider {
  Future<dynamic> callSocialLoginApi(
      {BuildContext context,
      SocialLoginRequestModel socialLoginRequestModel}) async {
    print("the social login request is ${socialLoginRequestModel.toJson()}");
    var getProfile = "social-login";
    var result = await AppConfig.of(context).baseApi.postRequest(
        getProfile, context,
        data: socialLoginRequestModelToJson(socialLoginRequestModel));

    return result;
  }
}

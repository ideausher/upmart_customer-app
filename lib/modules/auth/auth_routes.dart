import 'package:flutter/material.dart';
import 'package:user/modules/auth/page/enable_location/page/enable_location_page.dart';
import 'package:user/modules/auth/page/enter_details/page/enter_details_page.dart';
import 'package:user/modules/auth/page/login_successful/page/login_successful_page.dart';
import 'package:user/modules/auth/page/send_otp/page/send_otp_page.dart';
import 'package:user/modules/intro/page/intro_page.dart';

import '../../modules/auth/page/verify_otp/page/verify_otp_page.dart';
import '../../modules/auth/profile/screens/edit_email_phone/profile_edit_phone_number_or_email_page.dart';
import '../../modules/auth/profile/screens/edit_profile_verify_otp/profile_edit_verify_otp_page.dart';
import '../../modules/auth/profile/screens/profile/profile_page.dart';

class AuthRoutes {
  static const String ENTER_DETAILS_SCREEN = '/sign_in_screen';
  static const String SEND_OTP_SCREEN = '/sign_up_screen';
  static const String VERIFY_OTP_SCREEN = '/verify_otp_screen';
  static const String SPLASH_SCREEN_ROOT = '/splash_screen';
  static const String EDIT_PHONE_EMAIL_SCREEN_ROOT = '/edit_phone_email_screen';
  static const String PROFILE_EDIT_VERIFY_OTP_SCREEN_ROOT = '/profile_edit_verify_otp_screen';
  static const String PROFILE_SCREEN_ROOT = '/profile_screen';
  static const String LOGIN_SUCCESSFUL_ROOT = '/login_successful_screen';
  static const String ENABLE_LOCATION_ROOT = '/enable_location_screen';
  static const String INTRO_ROOT = '/intro_root_screen';

  static Map<String, WidgetBuilder> routes() {
    return {
      // When we navigate to the "/" route, build the FirstScreen Widget
      ENTER_DETAILS_SCREEN: (context) => EnterDetailsPage(context),
      SEND_OTP_SCREEN: (context) => SendOtpPage(context:context, isGuestUser:false),
      VERIFY_OTP_SCREEN: (context) => VerifyOtpPage(context),
      EDIT_PHONE_EMAIL_SCREEN_ROOT: (context) => ProfileEditPhoneNumberOrEmailPage(context: context),
      PROFILE_EDIT_VERIFY_OTP_SCREEN_ROOT: (context) => ProfileEditVerifyOTPPage(context),
      PROFILE_SCREEN_ROOT: (context) => ProfilePage(context),
     // SPLASH_SCREEN_ROOT: (context) => SplashPage(),
      LOGIN_SUCCESSFUL_ROOT: (context) => LogInSuccessFullScreen(context),
      ENABLE_LOCATION_ROOT: (context) => EnableLocationPage(context),
      INTRO_ROOT: (context) => IntroPage(),
    };
  }
}

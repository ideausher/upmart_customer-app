import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:user/localizations.dart';
import '../../../modules/auth/enums/auth_enums.dart';

class AuthValidator {
  static AuthValidator _authValidator = AuthValidator();

  static AuthValidator get authValidatorInstance => _authValidator;

  final String _emailvalidRegex =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  final int _nameMinLength = 3;

  final int _phoneMinLength = 14;

// used to check empty field
  bool checkEmptyField({String value}) {
    return value?.trim()?.isEmpty;
  }

  //method to check valid phone number
  bool checkValidReason({String value}) {
    if (value?.length < 30) {
      return true;
    }
  }

  //method to check valid phone number
  bool checkValidPhoneNumber({String value}) {
    if (value?.length > _phoneMinLength || value?.length < 10) {
      return true;
    }
  }

  //method to check valid phone number
  bool checkValidPhoneLength({String value}) {
    print('The phone number is $value');
    if (value?.length > _phoneMinLength) {
      return true;
    }
  }

// used to check valid email or not
  bool checkValidEmail({String value}) {
    RegExp regExp = new RegExp(_emailvalidRegex);
    return regExp.hasMatch(value.toString().trim());
  }

  // used to check name Validation
  bool checkValidName({String value}) {
    return value.length < _nameMinLength;
  }

  // check otpLength
  bool checkOtpLength({String value}) {
    return value.length < MaxLength.OTP.value;
  }

// used to trim string
  String trimValue({String value}) {
    return value?.trim();
  }

  // validate signup screen
  String validateSendOtpScreen(
      {String name, String email, String phoneNumber, Country country, BuildContext context, bool isConditionAgreed}) {
    String result = "";

    // validate phone is empty
    if (checkValidPhoneNumber(value: phoneNumber) == true)
      return result = AppLocalizations.of(context).sendotppage.error.validPhoneNumber;
    if (checkValidPhoneLength(value: phoneNumber) == true)
      return result = AppLocalizations.of(context).sendotppage.error.validPhoneNumber;
    if (isConditionAgreed == false) return result = AppLocalizations.of(context).sendotppage.error.acceptTermsAndServices;

    return result;
  }

  String validateVerifyOtpScreen({String value, BuildContext context}) {
    String result = "";
    // validate otp is empty
    if (checkEmptyField(value: value) == true) return result = AppLocalizations.of(context).verifyotppage.error.emptyOtp;

    // validate otp is valid
    if (checkOtpLength(value: value) == true) return result = AppLocalizations.of(context).verifyotppage.error.emptyOtp;

    return result;
  }

  String enterDetailsScreen({String name, Country country, BuildContext context}) {
    String result = "";
    // validate otp is empty
    if (checkEmptyField(value: name) == true) return result = AppLocalizations.of(context).enterdetailspage.error.emptyName;
    if (checkNameLength(value: name) == true) return result = AppLocalizations.of(context).enterdetailspage.error.minNameLength;
    if (checkValidName(value: name) == true) return result = AppLocalizations.of(context).enterdetailspage.error.minNameLength;

    return result;
  }

  bool checkNameLength({String value}) {
    return value.length > 25;
  }

  String validateSendQuery({String queryString, BuildContext context}) {
    var result;
// validate name is empty
    result = (checkEmptyField(value: queryString) == true)
        ? AppLocalizations.of(context).helpandSupport.error.enterQuery
        : (queryString?.trim()?.length < 50)
        ? AppLocalizations.of(context).helpandSupport.error.minCharecters
        : "";
    return result;
  }

  String validateCancelOrder(String cancelOrderReason){
    String result = "";
    //check cancel order
    if (checkEmptyField(value: cancelOrderReason) == true)
      return result ='Please enter a reason to cancel order.';

    if (checkValidReason(value: cancelOrderReason) == true)
      return result =
      'Reason must be of minimum 30 charecters.';

    return result;
  }
}

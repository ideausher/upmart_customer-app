import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/auth/page/enable_location/location_bloc/enable_location_bloc.dart';
import 'package:user/modules/auth/page/enable_location/location_bloc/enable_location_event.dart';
import 'package:user/modules/auth/page/enable_location/location_bloc/enable_location_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/routes.dart';

class EnableLocationManager {
  BuildContext context;
  EnableCurrentLocationBloc enableCurrentLocationBloc;
  ScaffoldState scaffoldState;

  EnableLocationManager(
      {this.context, this.enableCurrentLocationBloc, this.scaffoldState});

  //action on products list state change
  actionOnInitializationScreenStateChange(
      {EnableCurrentLocationState enableCurrentLocationState,
      ScaffoldState scaffoldState}) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        if (enableCurrentLocationState?.isLoading == false) {
          if (enableCurrentLocationState?.message
                  ?.toString()
                  ?.trim()
                  ?.isNotEmpty ==
              true) {
            print(
                'The message enable location is is ${enableCurrentLocationState?.message?.toString()}');
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
                context: context,
                title: AppLocalizations.of(context)
                    .locationAlert
                    .title
                    .permissionAlert,
                negativeButton: AppLocalizations.of(context).common.text.ok,
                positiveButton:
                    AppLocalizations.of(context).locationAlert.text.setting,
                subTitle: AppLocalizations.of(context)
                    .locationAlert
                    .subtitle
                    .locationPermission,
                onNegativeButtonTab: () {
                  NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(context);
                },
                onPositiveButtonTab: () async {
                  NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(context);
                  if (Platform.isIOS)
                    AppSettings.openLocationSettings();
                  else
                    AppSettings.openAppSettings();
                });

            print("location update==> ${enableCurrentLocationState?.message}");
          } else {
            if (enableCurrentLocationState?.currentLocation != null) {
              await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
                  enableCurrentLocationState?.currentLocation,
                  PrefsEnum.UserLocationData.value);
              NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                  context, Routes.DASH_BOARD,
                  dataToBeSend: enableCurrentLocationState?.currentLocation);
            }
          }
        }
      },
    );
  }

  //call enable location emit event
  callEnableLocationEvent() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        // enable location emit event
        enableCurrentLocationBloc?.emitEvent(GetCurrentLocationEvent(
            isLoading: true,
            context: context,
            currentLocation: new CurrentLocation()));
      }
    });
  }
}

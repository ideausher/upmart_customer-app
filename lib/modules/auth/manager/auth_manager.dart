import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:geolocator/geolocator.dart';
import 'package:user/modules/auth/api/is_user_bloc/model/is_user_blocked_request_model.dart';
import 'package:user/modules/auth/api/login_with_otp/model/login_with_otp_request_model.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:user/modules/common/constants/constants.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/model/update_ui_data_model.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:user/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';
import '../../../localizations.dart';
import '../../../modules/auth/api/sign_in/model/enter_details_request_model.dart';
import '../../../modules/auth/api/verify_otp/model/verify_otp_request_model.dart';
import '../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../modules/auth/auth_bloc/auth_event.dart';
import '../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../modules/auth/auth_routes.dart';
import '../../../modules/auth/enums/auth_enums.dart';
import '../../../modules/auth/validator/auth_validator.dart';
import '../../../modules/common/utils/common_utils.dart';
import '../../../modules/common/utils/dialog_snackbar_utils.dart';
import '../../../modules/common/utils/navigator_utils.dart';
import '../../../modules/common/utils/network_connectivity_utils.dart';
import '../../../routes.dart';

class AuthManager {
  // for initialization screen authentication check
  actionOnIntializationScreenStateChange(
      {AuthState authState, BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        if (authState?.isLoading == false) {
          //if user is not logged in and auth model is null
          if (authState?.authResponseModel?.userData == null) {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, AuthRoutes.INTRO_ROOT);
          } else {
            if (authState?.authResponseModel?.userData?.name?.isNotEmpty ==
                true) {
              //here checking if current of location exist in the preference navigate user to dashboard

              if (authState?.authResponseModel?.userData?.address?.isNotEmpty ==
                  true) {
                CurrentLocation currentLocation = await FetchPrefsUtils
                    .fetchPrefsUtilsInstance
                    .getCurrentLocationModel();

                if (currentLocation != null) {
                  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                      context, Routes.DASH_BOARD,
                      dataToBeSend: currentLocation);
                }
                //else navigate to enable location screen to get the current location of the user
                else {
                  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                      context, AuthRoutes.ENABLE_LOCATION_ROOT,
                      dataToBeSend: currentLocation);
                }
              } else {
                CurrentLocation currentLocation = await FetchPrefsUtils
                    .fetchPrefsUtilsInstance
                    .getCurrentLocationModel();

                if (currentLocation != null) {
                  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                      context, Routes.DASH_BOARD,
                      dataToBeSend: currentLocation);
                }
                //else navigate to enable location screen to get the current location of the user
                else {
                  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                      context, AuthRoutes.ENABLE_LOCATION_ROOT,
                      dataToBeSend: currentLocation);
                }
              }
            }

            //if otp is verified but the name is empty firs fill the name of the user then move to next screen
            else {
              NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context,
                AuthRoutes.ENTER_DETAILS_SCREEN,
                /*dataToBeSend: authState?.updateUiDataModel*/
              );
            }
          }
        }
      },
    );
  }

  actionOnIntroScreenStateChange({BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 2), () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, Routes.START_SCREENS);
      });
    });
  }

  // for splash screen state changed
  actionOnSplashScreenStateChange({BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 4), () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorClearStack(context, AuthRoutes.INTRO_ROOT);
      });
    });
  }

  // for sign in

  enterDetailsUpdate({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    String name,
    String referralCode,
    UpdateUiDataModel updateUiDataModel,
    AuthResponseModel authResponseModel,
  }) {
    // check name field is empty or not
    String result = AuthValidator.authValidatorInstance.enterDetailsScreen(
      context: context,
      name: name,
    );
    // if its not empty
    if (result.isEmpty) {
      // check internet connection
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          // if its available
          authBloc.emitEvent(UpdateDetailsEvent(
            isLoading: true,
            updateUiDataModel: updateUiDataModel,
            context: context,
            authResponseModel: authResponseModel,
            enterDetailsRequestModel: EnterDetailsRequestModel(
                name: name,
                referralCode: referralCode,
                title: (authResponseModel?.userData?.title?.isNotEmpty == true)
                    ? authResponseModel?.userData?.title
                    : "",
                countryCode: authResponseModel?.userData?.countryCode,
                countryIsoCode: Country.CA
                    .isoCode /*updateUiDataModel?.selectedCountry?.isoCode ?? Country.IN?.isoCode*/),
          ));
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for sign in state change
  actionOnEnterDetailsStateChanged(
      {BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      AuthState authState,
      Country selectedCountry,
      UpdateUiDataModel updateUiDataModel}) {
    if (authState.isLoading == false) {
      updateUiDataModel = authState?.updateUiDataModel ?? updateUiDataModel;

      WidgetsBinding.instance.addPostFrameCallback(
        (_) {
          if (authState?.authResponseModel == null) {
            // check message not empty so that it will loop again and again
            if (authState?.message?.isNotEmpty == true) {
              NavigatorUtils.navigatorUtilsInstance
                  .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
              // update bloc to update message
              authBloc.emitEvent(UpdateBlocEvent(
                  isLoading: false,
                  context: context,
                  authResponseModel: authState.authResponseModel));
            }
          } else if (authState?.authResponseModel?.userData?.name?.isNotEmpty ==
              true) {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, AuthRoutes.LOGIN_SUCCESSFUL_ROOT);
          } else if (authState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: authState?.message);
            print("login==> ${authState?.message}");
            // update bloc to update message
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                updateUiDataModel: authState?.updateUiDataModel,
                authResponseModel: authState.authResponseModel));
          }
        },
      );
    }
  }

  // for send otp call
  sendOtpCall(
      {BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      String phoneNumber,
      Country selectedCountry,
      UpdateUiDataModel updateUiDataModel}) {
    // validate data
    String result = AuthValidator.authValidatorInstance.validateSendOtpScreen(
        phoneNumber: phoneNumber,
        context: context,
        country: /*updateUiDataModel?.selectedCountry ?? Country.IN*/ Country
            .US,
        isConditionAgreed: updateUiDataModel?.isTermsAccepted ?? false);
    // if no error
    if (result.isEmpty) {
      // connection check
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          authBloc?.emitEvent(IsUserBlocEvent(
            isLoading: true,
            context: context,
            isUserBlockedRequestModel: UserBlocRequestModel(
                type: UserRoles?.Customer?.value,
                phoneNumber: AuthValidator.authValidatorInstance
                    .trimValue(value: phoneNumber),
                countryCode:
                    "+${/*selectedCountry?.dialingCode*/ Country.CA.dialingCode}"),
            updateUiDataModel: updateUiDataModel,
          ));
        }
      });
    } else {
      // if error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for send otp state change
  actionSendOtpStateChange(
      {BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      TextEditingController phoneController,
      AuthState authState,
      UpdateUiDataModel updateUiDataModel}) {
    if (authState.isLoading == false) {
      updateUiDataModel = authState?.updateUiDataModel ?? updateUiDataModel;
      //phoneController.text = updateUiDataModel?.phoneNumber ?? "";
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        //case 1: when calling is block api
        if (authState?.isUserBlock == true) {
          //when calling is bloc api when number is entered first check whether number doest exist on any of the app
          if (authState?.commonResponseModel != null &&
              authState?.commonResponseModel?.status ==
                  ApiStatus.NotFound.value) {
            //call send otp api if number does not exist on delivery app
            sendOtp(context, scaffoldState, authBloc, phoneController.text,
                updateUiDataModel);
          }
          //this case will be called when number is already exist in db
          else if (authState?.isUserBlockedResponseModel != null &&
              authState?.isUserBlockedResponseModel?.statusCode ==
                  ApiStatus.Success.value) {
            //check if user type is coming 1 i.e customer for 2 i.e delivery boy
            if (authState?.isUserBlockedResponseModel?.data?.userType ==
                UserRoles?.Customer?.value) {
              if (authState?.isUserBlockedResponseModel?.data?.isBlocked == 0) {
                sendOtp(context, scaffoldState, authBloc, phoneController.text,
                    updateUiDataModel);
              } else {
                DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                    context: context,
                    scaffoldState: scaffoldState,
                    message: AppLocalizations.of(context)
                        .sendotppage
                        .error
                        .youAreBlocked);
              }
            }
            //if user type value is coming 2 then show a message
            else if (authState?.isUserBlockedResponseModel?.data?.userType ==
                UserRoles?.Vendor?.value) {
              DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                  context: context,
                  scaffoldState: scaffoldState,
                  message: AppLocalizations.of(context)
                      .sendotppage
                      .error
                      .numberAlreadyExist);
            }
          } else {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: authState?.message);
          }
        } //Case to check force update required or not
        else if (authState?.forceUpdateResponseModel != null &&
            authState?.forceUpdateResponseModel?.data != null &&
            authState?.needToUpdate == true) {
          ForceUpdateActionManager _forceUpdateActionManager =
              ForceUpdateActionManager();
          _forceUpdateActionManager.showForceUpdateDialog(
            context: context,
            forcefullyUpdate:
                (authState?.forceUpdateResponseModel?.data?.forceUpdate == 1)
                    ? true
                    : false,
            message: authState?.forceUpdateResponseModel?.data?.message,
          );
        }
        //Case 2:this else is for verifying send otp api call conditions
        else if (authState?.commonResponseModel != null &&
            authState?.commonResponseModel?.status == ApiStatus.Success.value) {
          authBloc.emitEvent(UpdateBlocEvent(
              isLoading: false,
              context: context,
              commonResponseModel: new CommonResponseModel(),
              updateUiDataModel: authState?.updateUiDataModel,
              authResponseModel: authState.authResponseModel));
          NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
            context,
            AuthRoutes.VERIFY_OTP_SCREEN,
          );
        }
        //this case will be executed in the case of guest user
        else if (authState?.isLocationServiceEnabled == true &&
            authState?.currentLocation != null) {
          print('Executing guest user');
          NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
              context, Routes.DASH_BOARD,
              dataToBeSend: authState?.currentLocation);
        }

        //this case will be executed for social login
        else if (authState?.authResponseModel?.userData != null) {
          if ((authState?.authResponseModel?.userData?.name?.isEmpty == true) &&
              authState?.authResponseModel?.userData?.userType !=
                  UserRoles.Vendor.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
              context,
              AuthRoutes.ENTER_DETAILS_SCREEN,
            );
          } else {
            CurrentLocation currentLocation = await FetchPrefsUtils
                .fetchPrefsUtilsInstance
                .getCurrentLocationModel();

            if (currentLocation != null) {
              NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                  context, Routes.DASH_BOARD,
                  dataToBeSend: currentLocation);
            }
            //else navigate to enable location screen to get the current location of the user
            else {
              NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                  context, AuthRoutes.LOGIN_SUCCESSFUL_ROOT,
                  dataToBeSend: currentLocation);
            }
          }
        } else if (authState?.message?.toString()?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: authState?.message);
          print("send otp==> ${authState?.message}");
        }
      });
    }
  }

  // for verify otp
  verifyOtpCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    String otp,
    AuthState authState,
  }) {
    // check otp
    String result = AuthValidator.authValidatorInstance.validateVerifyOtpScreen(
      value: otp,
      context: context,
    );
    // if no error
    if (result.isEmpty) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          authBloc.emitEvent(
            VerifyOtpEvent(
              isLoading: true,
              context: context,
              updateUiDataModel: authState?.updateUiDataModel,
              verifyOtpRequestModel: VerifyOtpRequestModel(
                  otp:
                      AuthValidator.authValidatorInstance.trimValue(value: otp),
                  phoneNumber: authState?.updateUiDataModel?.phoneNumber,
                  action: ACTION,
                  countryCode:
                      "+${Country.CA.dialingCode /*authState?.updateUiDataModel?.selectedCountry?.dialingCode*/}",
                  type: LoginWithPhoneOrEmail.phone.value.toString()),
            ),
          );
        }
      });
    } else {
      // show error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for state  update
  actionOnStateUpdate({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthState authState,
    AuthBloc authBloc,
  }) {
    if (authState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (authState?.authResponseModel == null) {
          // check message not empty so that it will loop again and again
          if (authState?.message?.isNotEmpty == true) {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
            // update bloc to update message
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                authResponseModel: authState.authResponseModel));
          }
        } else if (authState?.message?.toString()?.trim()?.isNotEmpty == true) {
          /* DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: authState?.message,
          );*/
          // update bloc to update message
          authBloc.emitEvent(UpdateBlocEvent(
              isLoading: false,
              context: context,
              authResponseModel: authState.authResponseModel));
        }
      });
    }
  }

  // for state change in verify otp
  actionOnVerifyOtpStateChanged({
    BuildContext context,
    ScaffoldState scaffoldState,
    TextEditingController verifyOtpController,
    AuthState authState,
    AuthBloc authBloc,
  }) {
    if (authState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (authState?.commonResponseModel != null &&
            authState?.commonResponseModel?.status == ApiStatus.Success.value) {
          //if user call resend otp api just show the message here
          if (authState?.isResend == true) {
            verifyOtpController?.clear();
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: authState?.commonResponseModel?.message,
            );
          }
          //this else will be called when the otp is verified successfully then call login api.not in the case of resend otp
          else {
            //if user otp is verified successfully call login with otp api
            callLoginWithOtp(authBloc, context, authState?.updateUiDataModel);
          }
        } else if (authState?.authResponseModel != null) {
          if (authState?.message?.isNotEmpty == true) {
            // update bloc to update message
            authBloc.emitEvent(UpdateBlocEvent(
                isLoading: false,
                context: context,
                commonResponseModel: new CommonResponseModel(),
                authResponseModel: authState.authResponseModel));
          }

          //here check a condition if username is empty then navigate user to
          // sign in screen and update user name there.
          if (authState?.authResponseModel?.userData?.name?.isEmpty == true) {
            print(
                'the name is ${authState?.authResponseModel?.userData?.name}');
            NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
                context, AuthRoutes.ENTER_DETAILS_SCREEN,
                dataToBeSend: authState?.updateUiDataModel);
          }
          //else if name is present then navigate user to login successful screen
          else {
            NavigatorUtils.navigatorUtilsInstance
                .navigatorClearStack(context, AuthRoutes.LOGIN_SUCCESSFUL_ROOT);
          }
        } else if (authState?.commonResponseModel?.status ==
            ApiStatus.Failure.value) {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
        } else if (authState?.message?.toString()?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: authState?.message,
          );
          // update bloc to update message
          authBloc.emitEvent(UpdateBlocEvent(
              isLoading: false,
              context: context,
              updateUiDataModel: authState?.updateUiDataModel,
              authResponseModel: authState.authResponseModel));
        }
      });
    }
  }

  // for resend otp
  resendOtpCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  }) {
    // check internet connection
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        // bloc update
        authBloc.emitEvent(
          ResendOtpEvent(
              isLoading: true,
              context: context,
              updateUiDataModel: authState?.updateUiDataModel),
        );
      }
    });
  }

  // for sign out
  signOutCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        authBloc.emitEvent(
          SignOutEvent(
            isLoading: true,
            context: context,
            authResponseModel: authState?.authResponseModel,
          ),
        );
      }
    });
  }

  //method to call change number event
  changeNumberEventCall(
    BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  ) {
    authBloc?.emitEvent(ChangeNumberEvent(
        isLoading: true,
        context: context,
        commonResponseModel: authState?.commonResponseModel,
        updateUiDataModel: authState?.updateUiDataModel));
  }

  // open logout Dialog
  signOutDialog({
    @required BuildContext context,
    ScaffoldState scaffoldState,
    AuthBloc authBloc,
    AuthState authState,
  }) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: AppLocalizations.of(context).logoutalert.title.logOutTitle,
        negativeButton: AppLocalizations.of(context).common.text.no,
        positiveButton: AppLocalizations.of(context).common.text.yes,
        subTitle: AppLocalizations.of(context).logoutalert.title.logOutSubTitle,
        onNegativeButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        onPositiveButtonTab: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          signOutCall(
              context: context,
              authBloc: authBloc,
              scaffoldState: scaffoldState,
              authState: authState);
        });
  }

  //method to update ui
  updateUI(
      {BuildContext context,
      AuthState authState,
      UpdateUiDataModel updateUiDataModel,
      AuthResponseModel authResponseModel,
      AuthBloc authBloc}) {
    authBloc?.emitEvent(UpdateUIEvent(
      context: context,
      authResponseModel: authState?.authResponseModel,
      updateUiDataModel: updateUiDataModel,
      isLoading: false,
    ));
  }

  //call login with otp api event
  void callLoginWithOtp(AuthBloc authBloc, BuildContext context,
      UpdateUiDataModel updateUiDataModel) {
    authBloc?.emitEvent(LoginWithOtpEvent(
        context: context,
        isLoading: true,
        updateUiDataModel: updateUiDataModel,
        loginWithOtpRequestModel: new LoginWithOtpRequestModel(
            type: TYPE,
            countryIsoCode: /*updateUiDataModel?.selectedCountry?.isoCode*/ Country
                .US.isoCode,
            phoneNumber: updateUiDataModel?.phoneNumber,
            countryCode:
                "+${Country.CA.dialingCode /*updateUiDataModel?.selectedCountry?.dialingCode*/}",
            token: TOKEN,
            registration: REGISTRATION,
            userType: UserRoles?.Customer?.value)));
  }

  void sendOtp(
      BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      String phoneNumber,
      UpdateUiDataModel updateUiDataModel) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        authBloc.emitEvent(SendOtpEvent(
          isLoading: true,
          context: context,
          updateUiDataModel: updateUiDataModel,
          sendOtpRequestModel: SendOtpRequestModel(
              type: UserRoles.Customer.value.toString(),
              countryIsoCode: Country
                  ?.US?.isoCode /*updateUiDataModel?.selectedCountry?.isoCode*/,
              phoneNumber: AuthValidator.authValidatorInstance
                  .trimValue(value: phoneNumber),
              countryCode:
                  "+${/*updateUiDataModel?.selectedCountry?.dialingCode*/ Country.CA.dialingCode}"),
        ));
      }
    });
  }

  void actionOnInit(
      {BuildContext context,
      AuthBloc authBloc,
      UpdateUiDataModel updateUiDataModel}) {
    // connection check
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: false)
        .then((onValue) {
      if (onValue) {
        authBloc?.emitEvent(CheckUpdateEvent(
          context: context,
          isLoading: false,
        ));
      }
    });
  }

  //method to check location permission and navigate only in the case of guest user
  checkLocationIsEnabled(
      {BuildContext context, AuthBloc authBloc, AuthState authState}) async {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: false)
        .then((onValue) async {
      if (onValue) {
        //check location permission status first
        LocationPermission permissionStatus = await CurrentLocationManger
            .locationMangerInstance
            .permissionEnabled();

        //if user denies the permission navigate user to enable location screen
        if ((permissionStatus == LocationPermission.denied ||
            permissionStatus == LocationPermission.deniedForever)) {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorClearStack(context, AuthRoutes.ENABLE_LOCATION_ROOT);
        } else {
          authBloc?.emitEvent(GuestUserLoginEvent(
              context: context,
              isLoading: true,
              isLocationServiceEnabled: false,
              updateUiDataModel: authState?.updateUiDataModel));
        }
      }
    });
  }
}

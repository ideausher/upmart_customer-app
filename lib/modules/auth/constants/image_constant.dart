const String SIGN_UP_LOGO = 'lib/modules/auth/images/sign_up_logo.png';
const String LIMIT_LESS_LOGO = 'lib/modules/auth/images/limitless.svg';
const String LIMIT_LESS_TITLE = 'lib/modules/auth/images/limitless_title.svg';
const String LOGIN_LOGO = 'lib/modules/auth/images/login_logo.png';
const String VERIFY_OTP_LOGO = 'lib/modules/auth/images/verify_otp_logo.png';
const String LOGIN_SUCCESSFUL_LOGO = 'lib/modules/auth/images/login_successful.png';
const String ENABLE_LOCATION_LOGO = 'lib/modules/auth/images/enable_location.png';
const String EMAIL_ICON = 'lib/modules/auth/images/email.png';
const String DIVIDER_ICON = 'lib/modules/auth/images/divider.png';
const String TICK_ICON = 'lib/modules/auth/images/tick_icon.png';
const String USER_PROFILE_ICON = 'lib/modules/auth/images/user.png';
const String UPMART_BACKGROUND = 'lib/modules/auth/images/upmart_background.png';
const String LOADING_ICON = 'lib/modules/auth/images/loading.gif';
const String APPLE_ICON = 'lib/modules/auth/images/apple.png';
const String GOOGLE_ICON = 'lib/modules/auth/images/google.png';
const String FACEBOOK_ICON = 'lib/modules/auth/images/facebook.png';
const String USER_PHONE_ICON = 'lib/modules/auth/images/user_phone.png';




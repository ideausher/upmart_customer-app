import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:user/modules/auth/social_login/facebook/model/facebook_auth_response_model.dart';
import '../../../../../modules/common/enum/enums.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class FacebookAuthUtils {
  static FacebookAuthUtils _fbAuthUtils = FacebookAuthUtils();

  static FacebookAuthUtils get fbAuthUtilsInstance => _fbAuthUtils;

  //class variables
  FacebookLogin _facebookLogin = FacebookLogin();
  FacebookResponseModel _facebookResponseModel;

  //method to initiate facebook login
  Future<FacebookResponseModel> initiateFacebookLogin(
      BuildContext buildContext) async {
    //first logout the user if user is already signed in
    //await facebookLogin?.logOut();
    await logout();

    try {
      _facebookLogin.loginBehavior = FacebookLoginBehavior.webOnly;
      var facebookLoginResult = await _facebookLogin.logIn(['email']);
      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.error:
          _facebookResponseModel = FacebookResponseModel(
              message: facebookLoginResult.errorMessage,
              status: ApiStatus.Failure.value);
          return _facebookResponseModel;
        case FacebookLoginStatus.cancelledByUser:
          _facebookResponseModel = FacebookResponseModel(
              message: facebookLoginResult.errorMessage,
              status: ApiStatus.Failure.value);
          return _facebookResponseModel;
        case FacebookLoginStatus.loggedIn:
          var graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=id,name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult.accessToken.token}');
          var profile = json.decode(graphResponse.body);
          print(profile.toString());

          if (profile['id'] != null) {
            _facebookResponseModel = new FacebookResponseModel(
              status: ApiStatus.Success.value,
              message: "",
              data: FacebookData(
                  fbId: profile['id'],
                  email: (profile['email'] != null) ? profile['email'] : "",
                  phone: (profile['phone'] != null) ? profile['phone'] : "",
                  firstName: (profile['first_name'] != null)
                      ? profile['first_name']
                      : "",
                  lastname: (profile['last_name'] != null)
                      ? profile['last_name']
                      : "",
                  profilePicture: (profile["picture"]["data"]["url"] != null)
                      ? profile["picture"]["data"]["url"]
                      : ""),
            );
            return _facebookResponseModel;
          }
      }
    } catch (e) {
      print("faceBookAuth:faceBook Auth exception");
      _facebookResponseModel = FacebookResponseModel(
          message: e?.toString(), status: ApiStatus.Failure.value);
      return _facebookResponseModel;
    }
  }

  //method to logout from facebook
  Future logout() async {
    if (_facebookLogin != null) {
      try {
        return await _facebookLogin.logOut();
        print("Logged out");
      } catch (e) {
        print("faceBookAuth:faceBook logout exception");
        return null;
      }
    }

    return null;
  }
}

// To parse this JSON data, do
//
//     final facebookResponseModel = facebookResponseModelFromJson(jsonString);

import 'dart:convert';

FacebookResponseModel facebookResponseModelFromJson(String str) => FacebookResponseModel.fromJson(json.decode(str));

String facebookResponseModelToJson(FacebookResponseModel data) => json.encode(data.toJson());

class FacebookResponseModel {
  FacebookData data;
  int status;
  String message;

  FacebookResponseModel({
    this.data,
    this.message,
    this.status
  });

  factory FacebookResponseModel.fromJson(Map<String, dynamic> json) => FacebookResponseModel(
    data: json["data"] == null ? null : FacebookData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
  };
}

class FacebookData {
  String fbId;
  String firstName;
  String lastname;
  String email;
  String phone;
  String profilePicture;

  FacebookData({
    this.fbId,
    this.firstName,
    this.lastname,
    this.email,
    this.phone,
    this.profilePicture
  });

  factory FacebookData.fromJson(Map<String, dynamic> json) => FacebookData(
    fbId: json["fbId"] == null ? null : json["fbId"],
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastname: json["lastname"] == null ? null : json["lastname"],
    email: json["email"] == null ? null : json["email"],
    phone: json["phone"] == null ? null : json["phone"],
    profilePicture: json["profilePicture"] == null ? null : json["profilePicture"],
  );

  Map<String, dynamic> toJson() => {
    "fbId": fbId == null ? null : fbId,
    "firstName": firstName == null ? null : firstName,
    "lastname": lastname == null ? null : lastname,
    "email": email == null ? null : email,
    "phone": phone == null ? null : phone,
    "profilePicture": profilePicture == null ? null : profilePicture,
  };
}

//flutter package used
  flutter_facebook_login: ^3.0.0

follow the below link
https://pub.dev/packages/flutter_facebook_login

*hash key code already in mainactivity u can copy the hashkey from log by searching hashkey*
<--------import the facebook package------>
import 'package:flutter_facebook_login/flutter_facebook_login.dart';


//Steps to follow for facebook login for Android.
1.)Create your project on facebook developer console.
2.)Associate Your Package Name and Default Class with Your App
3.)Provide the Development and Release Key Hashes for Your App
4.)Enable Single Sign On for Your App(Facebook will provide a unique id for your app named as APP ID Use this in your project)
6.)Add implementation 'com.facebook.android:facebook-login:[5,6) in build.gradle (Module: app)

7.)Edit Your Resources and Manifest
->Add the following in /app/res/values/strings.xml file
<string name="facebook_app_id">[APP_ID]</string>
<string name="fb_login_protocol_scheme">fb[APP_ID]</string>
->Add following permission in manifest file
  <uses-permission android:name="android.permission.INTERNET"/>

8.)Add the following meta-data element inside application element in Manifest file.
<meta-data android:name="com.facebook.sdk.ApplicationId"
        android:value="@string/facebook_app_id"/>

    <activity android:name="com.facebook.FacebookActivity"
        android:configChanges=
                "keyboard|keyboardHidden|screenLayout|screenSize|orientation"
        android:label="@string/app_name" />



//Steps to follow for facebook login for iOS.
1)In Xcode, click File > Swift Packages > Add Package Dependency.
2)In the dialog that appears, enter the repository URL: https://github.com/facebook/facebook-ios-sdk.
3)In Version, select Up to Next Major and take the default option.
4)Complete the prompts to select the libraries you'd like to use in your project.
  To use the Objective-C interface exclusively, import the 'FBSDK'-prefixed module.
  Example: import FBSDKCoreKit
  To use the enhanced Swift interface in your code, import the 'Facebook'-prefixed module.
  Example: import FacebookCore
5)Configure Your Project
6)Configure the information property list file (info.plist) with an XML snippet that contains data about your app.
7.)Right-click info.plist, and choose Open As Source Code.
   Copy and paste the following XML snippet into the body of your file ( <dict>...</dict>)
8.)<key>CFBundleURLTypes</key>
   <array>
       <!--
       <dict>
       ... Some other CFBundleURLTypes definition.
       </dict>
       -->
       <dict>
           <key>CFBundleURLSchemes</key>
           <array>
               <!--
                 Replace "000000000000" with your Facebook App ID here.
                 **NOTE**: The scheme needs to start with `fb` and then your ID.
               -->
               <string>fb000000000000</string>
           </array>
       </dict>
   </array>

   <key>FacebookAppID</key>

   <!-- Replace "000000000000" with your Facebook App ID here. -->
   <string>000000000000</string>
   <key>FacebookDisplayName</key>

   <!-- Replace "YOUR_APP_NAME" with your Facebook App name. -->
   <string>YOUR_APP_NAME</string>

   <key>LSApplicationQueriesSchemes</key>
   <array>
       <string>fbapi</string>
       <string>fb-messenger-share-api</string>
       <string>fbauth2</string>
       <string>fbshareextension</string>
   </array>

 9)Connect Your App Delegate

 //  Swift
 //  AppDelegate.swift
 //  Replace the code in AppDelegate.swift with the following code.
 //
 //  Copyright © 2020 Facebook. All rights reserved.
 //

 import UIKit
 import FBSDKCoreKit

 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {

     // Swift
     // AppDelegate.swift

     func application(
         _ application: UIApplication,
         didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
     ) -> Bool {

         ApplicationDelegate.shared.application(
             application,
             didFinishLaunchingWithOptions: launchOptions
         )

         return true
     }

     func application(
         _ app: UIApplication,
         open url: URL,
         options: [UIApplication.OpenURLOptionsKey : Any] = [:]
     ) -> Bool {

         ApplicationDelegate.shared.application(
             app,
             open: url,
             sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
             annotation: options[UIApplication.OpenURLOptionsKey.annotation]
         )

     }

 }


//for objective C Code
// Objective-C
// AppDelegate.m

#import <FBSDKCoreKit/FBSDKCoreKit.h>

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

  [[ApplicationDelegate sharedInstance] application:application
    didFinishLaunchingWithOptions:launchOptions];
  // Add any custom logic here.
  return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {

  BOOL handled = [[ApplicationDelegate sharedInstance] application:application
    openURL:url
    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
    annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
  ];
  // Add any custom logic here.
  return handled;
}

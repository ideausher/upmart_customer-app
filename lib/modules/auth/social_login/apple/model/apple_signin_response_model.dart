// To parse this JSON data, do
//
//     final facebookResponseModel = facebookResponseModelFromJson(jsonString);

import 'dart:convert';

AppleResponseModel facebookResponseModelFromJson(String str) => AppleResponseModel.fromJson(json.decode(str));

String facebookResponseModelToJson(AppleResponseModel data) => json.encode(data.toJson());

class AppleResponseModel {
  AppleData data;
  int status;
  String message;

  AppleResponseModel({
    this.data,
    this.message,
    this.status
  });

  factory AppleResponseModel.fromJson(Map<String, dynamic> json) => AppleResponseModel(
    data: json["data"] == null ? null : AppleData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
  };
}

class AppleData {
  String appleId;
  String fullName;
  String email;
  String user;
  String profilePicture;

  AppleData({
    this.appleId,
    this.fullName,
    this.email,
    this.user,
    this.profilePicture
  });

  factory AppleData.fromJson(Map<String, dynamic> json) => AppleData(
    appleId: json["appleId"] == null ? null : json["appleId"],
    fullName: json["fullName"] == null ? null : json["fullName"],

    email: json["email"] == null ? null : json["email"],
    user: json["user"] == null ? null : json["user"],
    profilePicture: json["profilePicture"] == null ? null : json["profilePicture"],
  );

  Map<String, dynamic> toJson() => {
    "appleId": appleId == null ? null : appleId,
    "fullName": fullName == null ? null : fullName,
    "email": email == null ? null : email,
    "user": user == null ? null : user,
    "profilePicture": profilePicture == null ? null : profilePicture,
  };
}

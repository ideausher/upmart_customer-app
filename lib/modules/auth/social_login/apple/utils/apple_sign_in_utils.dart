import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:flutter/cupertino.dart';
import 'package:user/modules/auth/social_login/apple/model/apple_signin_response_model.dart';

class AppleSignInUtils {
  static AppleSignInUtils _appleSignInUtils = AppleSignInUtils();

  static AppleSignInUtils get appleSignInUtilsInstance => _appleSignInUtils;
//
 Future<AppleResponseModel> appleSignIn({BuildContext context}) async {
   final credential = await SignInWithApple.getAppleIDCredential(
       scopes: [
         AppleIDAuthorizationScopes.email,
         AppleIDAuthorizationScopes.fullName,
       ],);

   if(credential?.userIdentifier?.isNotEmpty == true){
     return AppleResponseModel(
         message: "",
         status: 1,
         data: AppleData(
             email: credential?.email ?? "",
             fullName: credential?.givenName ?? "",
             user: credential?.userIdentifier ?? ""));

   }else{
     return AppleResponseModel(
               message: "Something Went Wrong",
               status: 0,
             );
   }
   // switch (credential?.email) {
   //   case AuthorizationStatus.authorized:
   //
   //     break;
   //
   //   case AuthorizationStatus.error:
   //     print("Sign in failed: ${result.error.localizedDescription}");
   //
   //     return AppleResponseModel(
   //       message: result.error.localizedDescription,
   //       status: 0,
   //     );
   //
   //   case AuthorizationStatus.cancelled:
   //     print('User cancelled');
   //     return AppleResponseModel(
   //       message: "User Cancelled",
   //       status: 0,
   //     );
   // }
 }
}

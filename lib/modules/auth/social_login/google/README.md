//flutter package used
 google_sign_in: ^4.5.1

https://pub.dev/packages/google_sign_in

<--------import the google package------>
import 'package:google_sign_in/google_sign_in.dart';


//important note
SHA KEY differ from system to system.So If you are using any other developer code.
Please add your system SHA KEY to developer console in order to proceed.

- Generate signed key for your android app for two channels debug and release.
- Registering the app in Google cloud services

https://console.developers.google.com/apis/credentials?authuser=4&project=modulerepository

- Configure your OAuth client
  For Android

Signing-certificate fingerprint
Add your package name and SHA-1 signing-certificate fingerprint to restrict usage to your Android apps. Learn more
Get the package name from your AndroidManifest.xml file. Then use the following command to get the fingerprint:
keytool -keystore path-to-debug-or-production-keystore -list -v or https://aboutreact.com/getting-sha1-fingerprint-for-google-api-console/

Package name
com.example

SHA-1 signing certificate
12:34:56:78:90:AB:CD:EF:12:34:56:78:90:AB:CD:EF:AA:BB:CC:DD



//For Android
1.)Add Google Play services in build.gradle(Project Level).
allprojects {
    repositories {
        google()

        // If you're using a version of Gradle lower than 4.1, you must instead use:
        // maven {
        //     url 'https://maven.google.com'
        // }
    }
}
2.)Then, in your app-level build.gradle file, declare Google Play services as a dependency:

   apply plugin: 'com.android.application'
       ...

       dependencies {

       }
apply plugin: 'com.google.gms.google-services'
3.)Add this permisson to your manifest file
<uses-permission android:name="android.permission.INTERNET" />




//for iOS

Follow this links for any help:
(https://developers.google.com/identity/sign-in/ios/start-integrating,
https://pub.dev/packages/google_sign_in


1)Set up your CocoaPods dependencies
pod init
pod 'GoogleSignIn'
pod install



2)Add a URL scheme to your project
Google Sign-in requires a custom URL Scheme to be added to your project. To add the custom scheme:

Open your project configuration: double-click the project name in the left tree view. Select your app from the TARGETS section, then select the Info tab, and expand the URL Types section.

Click the + button, and add your reversed client ID as a URL scheme.

The reversed client ID is your client ID with the order of the dot-delimited fields reversed. For example:

com.googleusercontent.apps.1234567890-abcdefg


3.)Register Your Project on Firebase To get the GoogleService-Info.plist.
4.)Move or copy GoogleService-Info.plist into the [my_project]/ios/Runner directory.
5.)Open Xcode, then right-click on Runner directory and select Add Files to "Runner".
6.)Select GoogleService-Info.plist from the file manager.
7.)A dialog will show up and ask you to select the targets, select the Runner target.
8.)Then add the CFBundleURLTypes attributes below into the [my_project]/ios/Runner/Info.plist file
<!-- Put me in the [my_project]/ios/Runner/Info.plist file -->
<!-- Google Sign-in Section -->

<key>CFBundleURLTypes</key>
<array>
	<dict>
		<key>CFBundleTypeRole</key>
		<string>Editor</string>
		<key>CFBundleURLSchemes</key>
		<array>

			<!-- Copied from GoogleService-Info.plist key REVERSED_CLIENT_ID -->
			<string>com.googleusercontent.apps.861823949799-vc35cprkp249096uujjn0vvnmcvjppkn</string>
		</array>
	</dict>
</array>
<!-- End of the Google Sign-in Section -->


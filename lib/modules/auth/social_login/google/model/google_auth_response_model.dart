// To parse this JSON data, do
//
//     final googleSignInRequestModel = googleSignInRequestModelFromJson(jsonString);

import 'dart:convert';

GoogleAuthResponseModel googleSignInResponseModelFromJson(String str) => GoogleAuthResponseModel.fromJson(json.decode(str));

String googleSignInRequestModelToJson(GoogleAuthResponseModel data) => json.encode(data.toJson());

class GoogleAuthResponseModel {
  GoogleData data;
  int status;
  String message;

  GoogleAuthResponseModel({
    this.data,
    this.status,
    this.message
  });

  factory GoogleAuthResponseModel.fromJson(Map<String, dynamic> json) => GoogleAuthResponseModel(
    data: json["data"] == null ? null : GoogleData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
  };
}

class GoogleData {
  String providerUserId;
  String email;
  String provider;
  String name;
  String phone;
  String photoUrl;

  GoogleData({
    this.providerUserId,
    this.email,
    this.provider,
    this.name,
    this.phone,
    this.photoUrl
  });

  factory GoogleData.fromJson(Map<String, dynamic> json) => GoogleData(
      providerUserId: json["providerUserId"] == null ? null : json["providerUserId"],
      email: json["email"] == null ? null : json["email"],
      provider: json["provider"] == null ? null : json["provider"],
      name: json["name"] == null ? null : json["name"],
      phone: json["phone"] == null ? " " : json["phone"],
      photoUrl: json["photoUrl"] == null ? " " : json["photoUrl"]
  );

  Map<String, dynamic> toJson() => {
    "providerUserId": providerUserId == null ? null : providerUserId,
    "email": email == null ? null : email,
    "provider": provider == null ? null : provider,
    "name": name == null ? null : name,
    "phone": phone == null ? null : phone,
    "photoUrl": photoUrl == null ? null : photoUrl,
  };
}

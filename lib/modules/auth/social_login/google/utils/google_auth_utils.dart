import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:user/modules/auth/social_login/google/model/google_auth_response_model.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/enum/enums.dart';

class GoogleAuthUtils {
  static GoogleAuthUtils _googleAuthUtils = GoogleAuthUtils();

  static GoogleAuthUtils get googleAuthUtilsInstance => _googleAuthUtils;

  bool _isLoggedIn = false;
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email',   'https://www.googleapis.com/auth/contacts.readonly',]);
  GoogleAuthResponseModel _googleAuthResponseModel;

  //method to initiate google login
  Future<GoogleAuthResponseModel> initiateGoogleLogin(BuildContext buildContext) async {
    try {
      var result = await _googleSignIn.signIn();

      if (result != null) {
        _isLoggedIn = true;
        //  print("pic "+result.photoUrl);
        _googleAuthResponseModel = new GoogleAuthResponseModel(
          status: ApiStatus.Success.value,
            data: GoogleData(
                providerUserId: result.id,
                email: result.email,
                provider: "google",
                name: result.displayName,
                photoUrl: result.photoUrl),message: "");
        return _googleAuthResponseModel;
      } else {
        _isLoggedIn = false;
        _googleAuthResponseModel = new GoogleAuthResponseModel(
            status: ApiStatus.Failure.value, message: AppLocalizations.of(buildContext).common.error.somethingWentWrong);
        return _googleAuthResponseModel;
      }
    } catch (err) {
      print("GoogleAuth:Google Sign In exception ${err}");
      _isLoggedIn = false;
      _googleAuthResponseModel = new GoogleAuthResponseModel(
          status: ApiStatus.Failure.value, message: err?.toString());
      return _googleAuthResponseModel;
    }
  }

  //method to logout signed in user from google
  Future logout() async {
    if (_googleSignIn != null) {
      try {
        print("Logged out");
        return await _googleSignIn.signOut();
      } catch (e) {
        print('GoogleAuth : Google Log Out Exception');
        return null;
      }
    }
    return null;
  }
}

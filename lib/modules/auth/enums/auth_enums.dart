enum SocialLogin { facebook, google, apple }

enum UserRoles { Customer, Vendor }

enum UserGender { Mr, Mrs, Ms }
enum Timer {
  sec,
}

enum LocationStatus { denied, neverAsk }

enum LoginWithPhoneOrEmail { email, phone }

extension UserGnederExtension on UserGender {
  int get value {
    switch (this) {
      case UserGender.Mr:
        return 0;
      case UserGender.Mrs:
        return 1;
      case UserGender.Ms:
        return 2;
      default:
        return null;
    }
  }
}

extension PermissionExtention on LocationStatus {
  String get value {
    switch (this) {
      case LocationStatus.denied:
        return 'PERMISSION_DENIED';
        break;
      case LocationStatus.neverAsk:
        return 'PERMISSION_DENIED_NEVER_ASK';
        break;
      default:
        return null;
    }
  }
}

extension TimerExtention on Timer {
  int get value {
    switch (this) {
      case Timer.sec:
        return 2;
      default:
        return null;
    }
  }
}

extension LoginWithPhoneOrEmailExtension on LoginWithPhoneOrEmail {
  int get value {
    switch (this) {
      case LoginWithPhoneOrEmail.phone:
        return 1;
      case LoginWithPhoneOrEmail.email:
        return 2;

      default:
        return null;
    }
  }
}

extension UserRolesExtension on UserRoles {
  int get value {
    switch (this) {
      case UserRoles.Customer:
        return 1;
      case UserRoles.Vendor:
        return 2;
      default:
        return null;
    }
  }
}

enum LoginType { EmailPassword, onlyPhone }

extension LoginTypeExtension on LoginType {
  int get value {
    switch (this) {
      case LoginType.EmailPassword:
        return 1;
      case LoginType.onlyPhone:
        return 0;
      default:
        return null;
    }
  }
}

enum MaxLength { OTP, Phone, OtpErrorLength, MinPhoneLength }

extension MaxLengthExtension on MaxLength {
  int get value {
    switch (this) {
      case MaxLength.OTP:
        return 4;
      case MaxLength.Phone:
        return 14;
      case MaxLength.OtpErrorLength:
        return 3;
      case MaxLength.MinPhoneLength:
        return 10;

      default:
        return null;
    }
  }
}

enum MinLength { Username }

extension MinLengthExtension on MinLength {
  int get value {
    switch (this) {
      case MinLength.Username:
        return 3;
      default:
        return null;
    }
  }
}

enum VerifyOtpFrom { Email, PhoneNumber, Normal }

extension VerifyOtpFromExtension on VerifyOtpFrom {
  int get value {
    switch (this) {
      case VerifyOtpFrom.Email:
        return 1;
      case VerifyOtpFrom.PhoneNumber:
        return 0;
      case VerifyOtpFrom.Normal:
        return 2;
      default:
        return null;
    }
  }
}

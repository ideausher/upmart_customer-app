import 'package:geolocator/geolocator.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/auth/api/is_user_bloc/model/is_user_blocked_response_model.dart';
import 'package:user/modules/auth/api/is_user_bloc/provider/is_user_blocked_provider.dart';
import 'package:user/modules/auth/api/login_with_otp/provider/login_with_otp_provider.dart';
import 'package:user/modules/auth/api/sign_in/model/enter_details_response_model.dart';
import 'package:user/modules/auth/api/sign_in/provider/sign_in_provider.dart';
import 'package:user/modules/auth/api/sign_out/provider/sign_out_provider.dart';
import 'package:user/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:user/modules/auth/api/sign_up/provider/sign_up_provider.dart';
import 'package:user/modules/auth/api/social_login/provider/social_login_provider.dart';
import 'package:user/modules/auth/api/verify_otp/provider/verify_otp_provider.dart';
import 'package:user/modules/auth/enums/auth_enums.dart';
import 'package:user/modules/auth/social_login/facebook/utils/facebook_auth_utils.dart';
import 'package:user/modules/auth/social_login/google/utils/google_auth_utils.dart';
import 'package:user/modules/auth/validator/auth_validator.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/force_update/api/provider/force_update_provider.dart';
import 'package:user/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';
import 'package:user/modules/force_update/model/force_update_response_model.dart';
import '../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../modules/auth/auth_bloc/auth_event.dart';
import '../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/common/enum/enums.dart';
import '../../../modules/common/utils/fetch_prefs_utils.dart';
import '../../../modules/common/utils/shared_prefs_utils.dart';

class AuthBloc extends BlocEventStateBase<AuthEvent, AuthState> {
  AuthBloc({bool isLoading = true})
      : super(initialState: AuthState.initiating(isLoading: isLoading));

  @override
  Stream<AuthState> eventHandler(
      AuthEvent event, AuthState currentState) async* {
    // updateBloc
    if (event is UpdateBlocEvent) {
      yield AuthState.updateUi(
          isLoading: event.isLoading,
          context: event.context,
          message: "",
          isResend: false,
          updateUiDataModel: event?.updateUiDataModel,
          authResponseModel: event.authResponseModel);
    }

    // for check authentication
    if (event is CheckAuthentication) {
      yield AuthState.updateUi(isLoading: true);

      AuthResponseModel _authResponseModel =
          await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();

      if (_authResponseModel != null) {
        yield AuthState.updateUi(
          isLoading: false,
          authResponseModel: _authResponseModel,
        );
      } else {
        yield AuthState.updateUi(
          isLoading: false,
          authResponseModel: null,
        );
      }
    }

    // event for send otp
    if (event is SendOtpEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      // update UI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: false,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call send otp screen-------------->
      var result = await SendOtpProvider().sendOtp(
        context: event.context,
        sendOtpRequestModel: event.sendOtpRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      // update UI
      yield AuthState.updateUi(
          isLoading: false,
          isResend: false,
          message: _message,
          context: event.context,
          commonResponseModel: commonResponseModel,
          updateUiDataModel: event?.updateUiDataModel);
    }

    // for sign in
    if (event is UpdateDetailsEvent) {
      String _message = "";
      EnterDetailsResponseModel _enterDetailsResponseModel;
      AuthResponseModel _authResponseModel = event?.authResponseModel;
      String token = _authResponseModel?.userData?.accessToken;

      // update uI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: false,
          authResponseModel: event?.authResponseModel,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call enter details screen-------------->
      var result = await EnterDetailsProvider().updateProfileApiCall(
          context: event.context,
          enterDetailsRequestModel: event?.enterDetailsRequestModel);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _authResponseModel = AuthResponseModel.fromMap(result);
          _authResponseModel?.userData?.accessToken = token;

          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield AuthState.updateUi(
        isLoading: false,
        message: _message,
        isResend: false,
        context: event.context,
        updateUiDataModel: event?.updateUiDataModel,
        authResponseModel: _authResponseModel,
      );
    }

    // for verify otp
    if (event is VerifyOtpEvent) {
      CommonResponseModel commonResponseModel;
      String _message = '';
      // update uo
      yield AuthState.updateUi(
        isLoading: true,
        message: "",
        isResend: false,
        context: event.context,
        updateUiDataModel: event?.updateUiDataModel,
      );

      //<-----------------api call verify otp screen-------------->

      var result = await VerifyOtpProvider().verifyOtpApiCall(
        context: event.context,
        verifyOtpRequestModel: event.verifyOtpRequestModel,
      );
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      // update UI
      yield AuthState.updateUi(
          isLoading: false,
          isResend: false,
          message: _message,
          context: event.context,
          commonResponseModel: commonResponseModel,
          updateUiDataModel: event?.updateUiDataModel);
    }

    // for resend otp
    if (event is ResendOtpEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();

      // update UI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: true,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call resend otp screen-------------->

      var result = await SendOtpProvider().sendOtp(
        context: event.context,
        sendOtpRequestModel: SendOtpRequestModel(
            type: UserRoles.Customer.value.toString(),
            phoneNumber: AuthValidator.authValidatorInstance
                .trimValue(value: event?.updateUiDataModel?.phoneNumber),
            countryIsoCode: event?.updateUiDataModel?.selectedCountry?.isoCode,
            countryCode:
                "+${event?.updateUiDataModel?.selectedCountry?.dialingCode}"),
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = commonResponseModel?.message;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield AuthState.updateUi(
          isLoading: false,
          message: _message,
          isResend: true,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel,
          commonResponseModel: commonResponseModel);
    }

    // for signout
    if (event is SignOutEvent) {
      // update  UI
      yield AuthState.updateUi(
        isLoading: true,
        message: "",
        isResend: false,
        context: event.context,
        authResponseModel: event.authResponseModel,
      );

      //<-----------------api call sign out screen-------------->

      var result = await SignOutProvider().signOutApiCall(
        context: event.context,
      );

      if (result != null) {
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          //for social login
          if (event?.authResponseModel?.userData?.socialId?.isNotEmpty ==
              true) {
            print('Logged aout from social login');
            FacebookAuthUtils.fbAuthUtilsInstance.logout();
            GoogleAuthUtils.googleAuthUtilsInstance.logout();
          }
          await SharedPrefUtils.sharedPrefUtilsInstance.clearPref();
          CartUtilsManager?.cartUtilsInstance?.clearData();


          yield AuthState.updateUi(
            isLoading: false,
            isResend: false,
            authResponseModel: null,
            message: result[ApiStatusParams.Message.value],
          );
        } else {
          yield AuthState.updateUi(
            isLoading: false,
            isResend: false,
            authResponseModel: event.authResponseModel,
            message: result[ApiStatusParams.Message.value],
          );
        }
      } else {
        yield AuthState.updateUi(
          isLoading: false,
          isResend: false,
          authResponseModel: event.authResponseModel,
          message: AppLocalizations.of(event?.context)
              .common
              .error
              .somethingWentWrong,
        );
      }

      yield AuthState.updateUi(
        isLoading: false,
        isResend: false,
        authResponseModel: null,
        message: AppLocalizations.of(event.context).profilepage.text.logout,
      );
    }

    if (event is LoginWithOtpEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      AuthResponseModel _authResponseModel;
      // update UI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: false,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call login with otp api call-------------->
      var result = await LoginWithOtpProvider().loginWithOtpApiCall(
        context: event.context,
        loginWithOtpRequestModel: event.loginWithOtpRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          _authResponseModel = AuthResponseModel.fromMap(result);
          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      // update UI
      yield AuthState.updateUi(
          isLoading: false,
          isResend: false,
          message: _message,
          context: event.context,
          authResponseModel: _authResponseModel,
          commonResponseModel: commonResponseModel,
          updateUiDataModel: event?.updateUiDataModel);
    }

    //event for changing the button color on the basis of text controller length
    if (event is UpdateUIEvent) {
      yield AuthState.updateUi(
        isLoading: false,
        isResend: false,
        authResponseModel: event?.authResponseModel,
        updateUiDataModel: event?.updateUiDataModel,
        message: '',
      );
    }

    //event calling when user press change number on verify otp screen
    if (event is ChangeNumberEvent) {
      CommonResponseModel commonResponseModel = CommonResponseModel();
      commonResponseModel.status = ApiStatus.Failure.value;
      yield AuthState.updateUi(
        isLoading: false,
        isResend: false,
        commonResponseModel: commonResponseModel,
        updateUiDataModel: null,
        message: "",
      );
    }

    //event to check whether user is blocked or if some one tries to login on user app with delivery end app credentials
    if (event is IsUserBlocEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      IsUserBlockedResponseModel isUserBlockedResponseModel;
      // update UI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: false,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call to check credentials-------------->
      var result = await IsUserBlockedProvider().isUserBlockApiCall(
        context: event.context,
        isUserBlockedRequestModel: event.isUserBlockedRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          isUserBlockedResponseModel =
              IsUserBlockedResponseModel.fromMap(result);
          _message = isUserBlockedResponseModel?.message;
        }
        //if entered number not found it means number is not registered on any of the two app
        else if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.NotFound.value) {
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = commonResponseModel?.message;
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      // update UI
      yield AuthState.updateUi(
          isLoading: false,
          message: _message,
          context: event.context,
          isResend: false,
          isUserBlock: true,
          commonResponseModel: commonResponseModel,
          isUserBlockedResponseModel: isUserBlockedResponseModel,
          updateUiDataModel: event?.updateUiDataModel);
    }

    if (event is GetProfileEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      AuthResponseModel _authResponseModel = event?.authResponseModel;

      String accessToken = _authResponseModel?.userData?.accessToken;
      // update UI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: false,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call to get profile-------------->
      var result = await IsUserBlockedProvider().getProfileApiCall(
        context: event.context,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          _authResponseModel = AuthResponseModel.fromMap(result);
          _authResponseModel?.userData?.accessToken = accessToken;
          _message = _authResponseModel?.message;
          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);

          //this case is when only one address is present make it primary
          if (_authResponseModel?.userData?.address?.length == 1) {
            await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
                new CurrentLocation(
                    id: _authResponseModel?.userData?.address[0]?.addressId
                        ?.toString(),
                    lat: _authResponseModel?.userData?.address[0]?.latitude,
                    lng: _authResponseModel?.userData?.address[0]?.longitude,
                    city: _authResponseModel?.userData?.address[0]?.city,
                    country: _authResponseModel?.userData?.address[0]?.country,
                    postalCode:
                        _authResponseModel?.userData?.address[0]?.pincode,
                    currentAddress: _authResponseModel
                        ?.userData?.address[0]?.formattedAddress),
                PrefsEnum.UserLocationData.value);
          }
        }

        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      // update UI
      yield AuthState.updateUi(
          isLoading: false,
          message: _message,
          context: event.context,
          isUserBlock: true,
          isResend: false,
          commonResponseModel: commonResponseModel,
          authResponseModel: _authResponseModel,
          updateUiDataModel: event?.updateUiDataModel);
    }

    // for check update
    if (event is CheckUpdateEvent) {
      // update UI
      yield AuthState.updateUi(
          isLoading: false,
          context: event.context,
          updateUiDataModel: event.updateUiDataModel);

      var _result = await ForceUpdateProvider()
          .forceUpdateApiCall(context: event.context);

      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          ForceUpdateActionManager _forceUpdateActionManager =
              ForceUpdateActionManager();
          ForceUpdateResponseModel _forceUpdateResponseModel =
              ForceUpdateResponseModel.fromMap(_result);
          bool _needToUpdate = await _forceUpdateActionManager
              .isNeedToUpdate(_forceUpdateResponseModel?.data?.version);

          // update UI
          yield AuthState.updateUi(
              isLoading: false,
              context: event.context,
              forceUpdateResponseModel: _forceUpdateResponseModel,
              needToUpdate: _needToUpdate,
              updateUiDataModel: event.updateUiDataModel);
        } else {
          // update UI
          yield AuthState.updateUi(
              isLoading: false,
              context: event.context,
              updateUiDataModel: event.updateUiDataModel);
        }
      } else {
        // update UI
        yield AuthState.updateUi(
            isLoading: false,
            context: event.context,
            updateUiDataModel: event.updateUiDataModel);
      }
    }

    //guest user login event
    if (event is GuestUserLoginEvent) {
      Position _myLocation;
      CurrentLocation _currentLocation;
      String _message = "";

      yield AuthState.updateUi(
          isLoading: true,
          context: event.context,
          isLocationServiceEnabled: false,
          currentLocation: event?.currentLocation,
          updateUiDataModel: event.updateUiDataModel);
      try {
        print('in try block');
        _myLocation = await Geolocator.getCurrentPosition();
        print('the lat lng are ${_myLocation?.longitude}');
        _currentLocation = await CurrentLocationManger?.locationMangerInstance
            ?.getAddressUsingLocation(event?.context, _myLocation);
        await SharedPrefUtils.sharedPrefUtilsInstance.saveLocationObject(
            _currentLocation, PrefsEnum.UserLocationData.value);
        print('the current location is ${_currentLocation?.currentAddress}');
      } catch (e) {
        print('in catch block');
        _message = AppLocalizations.of(event?.context)
            .enablelocationpage
            .title
            .enableLocation;
      }

      yield AuthState.updateUi(
          isLoading: false,
          context: event.context,
          isLocationServiceEnabled: true,
          message: _message,
          currentLocation: _currentLocation,
          updateUiDataModel: event.updateUiDataModel);
    }

    if (event is SocialLoginEvent) {
      AuthResponseModel authResponseModel;
      String _message = "";
      // update UI
      yield AuthState.updateUi(
          isLoading: true,
          message: "",
          isResend: false,
          isUserBlock: event?.isUserBlock,
          isLocationServiceEnabled: false,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);

      //<-----------------api call send otp screen-------------->
      var result = await SocialLoginProvider().callSocialLoginApi(
        context: event.context,
        socialLoginRequestModel: event?.socialLoginRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          authResponseModel = AuthResponseModel.fromMap(result);
          await SharedPrefUtils.sharedPrefUtilsInstance
              .saveObject(authResponseModel, PrefsEnum.UserProfileData.value);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield AuthState.updateUi(
          isLoading: false,
          message: _message,
          isResend: false,
          isUserBlock: event?.isUserBlock,
          authResponseModel: authResponseModel,
          isLocationServiceEnabled: false,
          context: event.context,
          updateUiDataModel: event?.updateUiDataModel);
    }
  }
}

// To parse this JSON data, do
//
//     final editProfileVerifyOtpRequestModel = editProfileVerifyOtpRequestModelFromJson(jsonString);

import 'dart:convert';

EditProfileVerifyOtpRequestModel editProfileVerifyOtpRequestModelFromJson(String str) =>
    EditProfileVerifyOtpRequestModel.fromJson(json.decode(str));

String editProfileVerifyOtpRequestModelToJson(EditProfileVerifyOtpRequestModel data) => json.encode(data.toJson());

class EditProfileVerifyOtpRequestModel {
  EditProfileVerifyOtpRequestModel(
      {this.otp, this.phoneNumber, this.countryCode, this.email, this.type, this.action, this.countryIsoCode});

  String otp;
  String phoneNumber;
  String countryCode;
  String email;
  String type;
  String action;
  String countryIsoCode;

  factory EditProfileVerifyOtpRequestModel.fromJson(Map<String, dynamic> json) => EditProfileVerifyOtpRequestModel(
        otp: json["otp"] == null ? null : json["otp"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        email: json["email"] == null ? null : json["email"],
        type: json["type"] == null ? null : json["type"],
        action: json["action"] == null ? null : json["action"],
        countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
      );

  Map<String, dynamic> toJson() => {
        "otp": otp == null ? null : otp,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "country_code": countryCode == null ? null : countryCode,
        "email": email == null ? null : email,
        "type": type == null ? null : type,
        "action": action == null ? null : action,
        "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
      };
}

// To parse this JSON data, do
//
//     final editPhoneNumberResponse = editPhoneNumberResponseFromJson(jsonString);

import 'dart:convert';

EditEmailPhoneResponseModel editPhoneNumberResponseFromJson(String str) =>
    EditEmailPhoneResponseModel.fromMap(json.decode(str));

String editPhoneNumberResponseToJson(EditEmailPhoneResponseModel data) => json.encode(data.toJson());

class EditEmailPhoneResponseModel {
  int status;
  String message;

  EditEmailPhoneResponseModel({
    this.status,
    this.message,
  });

  factory EditEmailPhoneResponseModel.fromMap(Map<String, dynamic> json) => EditEmailPhoneResponseModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
      };
}

// To parse this JSON data, do
//
//     final editEmailPhoneRequest = editEmailPhoneRequestFromJson(jsonString);

import 'dart:convert';

import 'package:flutter_country_picker/country.dart';

EditEmailPhoneRequestModel editEmailPhoneRequestFromJson(String str) =>
    EditEmailPhoneRequestModel.fromJson(json.decode(str));

String editEmailPhoneRequestToJson(EditEmailPhoneRequestModel data) => json.encode(data.toJson());

class EditEmailPhoneRequestModel {
  int type;
  String value;
  String countryCode;
  String countryIsoCode;
  String diallingCode;
  Country country;

  EditEmailPhoneRequestModel(
      {this.type, this.value, this.countryCode, this.country, this.countryIsoCode, this.diallingCode});

  factory EditEmailPhoneRequestModel.fromJson(Map<String, dynamic> json) => EditEmailPhoneRequestModel(
        type: json["type"] == null ? null : json["type"],
        value: json["value"] == null ? null : json["value"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "value": value == null ? null : value,
        "country_code": countryCode == null ? null : countryCode,
        "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
      };
}

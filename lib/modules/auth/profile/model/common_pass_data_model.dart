import 'package:flutter_country_picker/country.dart';
import 'package:user/modules/auth/api/sign_up/model/send_otp_request_model.dart';

class CommonPassDataModel {
  int tag;
  String value;
  Country country;
  SendOtpRequestModel sendOtpRequestModel;

  CommonPassDataModel(this.tag, this.value, this.country, {this.sendOtpRequestModel});
}

import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:user/modules/auth/auth_routes.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/verify_otp_widget.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/auth/profile/bloc/edit_profile_verify_otp/edit_profile_verify_otp_bloc.dart';
import '../../../../../modules/auth/profile/bloc/edit_profile_verify_otp/edit_profile_verify_otp_state.dart';
import '../../../../../modules/auth/profile/manager/profile_manager.dart';
import '../../../../../modules/auth/profile/model/common_pass_data_model.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../auth_bloc/auth_bloc.dart';
import '../../../auth_bloc/auth_state.dart';
import '../../../enums/auth_enums.dart';

class ProfileEditVerifyOTPPage extends StatefulWidget {
  BuildContext context;

  ProfileEditVerifyOTPPage(this.context);

  @override
  _ProfileEditVerifyOTPPageState createState() => _ProfileEditVerifyOTPPageState();
}

class _ProfileEditVerifyOTPPageState extends State<ProfileEditVerifyOTPPage> {
  final GlobalKey<FormState> verifyAccountKey = GlobalKey<FormState>();

  //Declaring variable
  final TextEditingController _phoneController = new TextEditingController();
  final TextEditingController _otpControllerFirst = new TextEditingController();

  var _editProfileVerifyOtpBloc = EditProfileVerifyOtpBloc(); // bloc
  EditProfileVerifyOtpState _editProfileVerifyOtpState; // state
  var _authBloc = AuthBloc();
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ProfileManager _profileManager = ProfileManager();
  AuthManager _authManager = new AuthManager();
  CommonPassDataModel _data;
  SendOtpRequestModel sendOtpRequestModel;
  var _loggedOut = false;
  ProfileManager profileManager;

  @override
  void initState() {
    super.initState();
    _data = ModalRoute.of(widget.context).settings.arguments;
    sendOtpRequestModel = _data?.sendOtpRequestModel;
    profileManager = ProfileManager();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
  }

  @override
  void dispose() {
    super.dispose();
    _editProfileVerifyOtpBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _context = context;
        if (authState != null && _authState != authState) {
          _authState = authState;
        }

        return ModalProgressHUD(
          inAsyncCall: false,
          child: BlocEventStateBuilder<EditProfileVerifyOtpState>(
            bloc: _editProfileVerifyOtpBloc,
            builder: (BuildContext context, EditProfileVerifyOtpState editProfileVerifyOtpState) {
              if (editProfileVerifyOtpState != null && _editProfileVerifyOtpState != editProfileVerifyOtpState) {
                _editProfileVerifyOtpState = editProfileVerifyOtpState;

                //profileManager?.actionOnVerifyOtpStateChange(authState,_loggedOut,_context,editProfileVerifyOtpState);

                if (_authState?.authResponseModel == null && authState.isLoading == false && _loggedOut == false) {
                  _loggedOut = true;
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
                  });
                  //set Data to the controllers to show user details
                }
                _profileManager.actionOnEditProfileVerifyOtpStateChange(
                  context: _context,
                  scaffoldState: _scaffoldKey?.currentState,
                  enableProfileEditing: false,
                  authBloc: _authBloc,
                  otpController: _phoneController,
                  editProfileVerifyOtpState: _editProfileVerifyOtpState,
                );
              }
              return ModalProgressHUD(
                inAsyncCall: _editProfileVerifyOtpState?.isLoading ?? false,
                child: WillPopScope(
                  onWillPop: () {
                    return NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
                  },
                  child: SafeArea(
                    bottom: true,
                    top: true,
                    child: Scaffold(
                      key: _scaffoldKey,
                      body: VerifyOTPWidget(
                        context: _context,
                        authState: _authState,
                        authBloc: _authBloc,
                        phoneNumberController: _phoneController,
                        otpController:_otpControllerFirst ,
                        isComingFromEditEmailPhone: true,
                        isFromEmailEdit: true,
                        authManager: _authManager,
                        onSendOtpButtonPressed: (String otp) {
                          print('The Otp is ${otp}');
                          _validateOtp(otp: otp);
                        },
                        //call for resend otp call
                        onResendOtpButtonPressed: () {
                          _resendOtpCall();
                        },
                        onChangeNumberPressed: () {
                          _authManager.signOutCall(
                              context: _context,
                              scaffoldState: _scaffoldKey.currentState,
                              authBloc: _authBloc,
                              authState: _authState);
                        },

                        userPhoneEmail:
                            '${(_data?.tag == VerifyOtpFrom.PhoneNumber.value) ? _getValue() : _data.value ?? ""}',
                        isForEmail: (_data?.tag == VerifyOtpFrom.PhoneNumber.value) ? false : true,
                        title: (_data?.tag == VerifyOtpFrom.PhoneNumber.value)
                            ? AppLocalizations.of(_context).verifyotppage.title.verifyMobileNumber
                            : AppLocalizations.of(_context).verifyotppage.title.verifyEmail,
                        subtitle: '${AppLocalizations.of(_context).verifyotppage.subtitle.enterOtp} ',
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }

  // validate otp and api call
  void _validateOtp({String otp}) {
    _profileManager.editProfileVerifyOtpCall(
        context: _context,
        authResponseModel: _authState?.authResponseModel,
        editProfileVerifyOtpBloc: _editProfileVerifyOtpBloc,
        otp: otp,
        scaffoldState: _scaffoldKey?.currentState,
        dataModel: _data);
  }

  // resend otp call
  void _resendOtpCall() {
    _profileManager.editProfileResendOtpCall(
      context: _context,
      editProfileVerifyOtpBloc: _editProfileVerifyOtpBloc,
      authResponseModel: _authState?.authResponseModel,
      scaffoldState: _scaffoldKey?.currentState,
      sendOtpRequestModel: sendOtpRequestModel,
    );
  }

  //method to return phone number with dialing code
  String _getValue() {
    return '(+ ${_data?.country?.dialingCode ?? ""}) ${_data.value ?? ""} ';
  }

  getNumber() {
    if (_data?.tag == VerifyOtpFrom.PhoneNumber.value) {
      return _data.value;
    } else {
      return _data.value;
    }
  }
}

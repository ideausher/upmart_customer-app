import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:user/modules/auth/constants/image_constant.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/auth/profile/bloc/profile_bloc/profile_event.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import '../../../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../../../modules/auth/enums/auth_enums.dart';
import '../../../../../modules/auth/profile/manager/profile_manager.dart';
import '../../../../../modules/auth/profile/model/common_pass_data_model.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../../modules/common/app_config/app_config.dart';
import '../../../../../modules/common/constants/color_constants.dart';
import '../../../../../modules/common/constants/dimens_constants.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/common/utils/image_utils.dart';
import '../../../../../modules/common/utils/navigator_utils.dart';
import '../../../../../localizations.dart';
import '../../../auth_routes.dart';
import '../../bloc/profile_bloc/profile_bloc.dart';
import '../../bloc/profile_bloc/profile_state.dart';

class ProfilePage extends StatefulWidget {
  BuildContext context;

  ProfilePage(this.context);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Declaration Of Form Key
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  //Declaration Of controller
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _phoneController = new TextEditingController();

  //Declaration of Bloc Variables
  var _authBloc = AuthBloc(); // bloc
  var _profileBloc = ProfileBloc(); // bloc
  AuthState _authState;
  ProfileState _profileState;

  BuildContext _context;
  ProfileManager _profileManager = ProfileManager();
  AuthManager _authManager = new AuthManager();

  //Declaration of County Picker Var
  Country _selectedCountry = Country.IN;
  String userName = "";

  //dropdown item
  List<int> _dropdownValues = [
    UserGender.Mrs.value,
    UserGender.Mr.value,
    UserGender.Ms.value
  ];

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    _profileBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _context = context;
        if (authState != null && _authState != authState) {
          _authState = authState;
          if (_authState?.authResponseModel?.userData != null &&
              _authState?.authResponseModel?.userData?.countryIsoCode
                      ?.isNotEmpty ==
                  true) {
            _selectedCountry = Country.findByIsoCode(
                authState?.authResponseModel?.userData?.countryIsoCode);
          }
          //set Data to the controllers to show user details
          _profileManager.setUserData(
              authResponseModel: _authState?.authResponseModel,
              nameController: _nameController,
              emailController: _emailController,
              phoneController: _phoneController,
              authState: _authState);
        }
        return ModalProgressHUD(
            inAsyncCall: _authState?.isLoading ?? false,
            child: (_authState?.authResponseModel != null)
                ? BlocEventStateBuilder<ProfileState>(
                    bloc: _profileBloc,
                    builder: (BuildContext context, ProfileState profileState) {
                      if (profileState != null &&
                          _profileState != profileState) {
                        _context = context;
                        _profileState = profileState;

                        _profileManager.actionOnProfileStateChange(
                            authBloc: _authBloc,
                            nameController: _nameController,
                            enableProfileEditing:
                                profileState?.enableProfileEditing,
                            profileState: _profileState,
                            scaffoldState: _scaffoldKey?.currentState,
                            context: _context,
                            profileBloc: _profileBloc);
                      }
                      return ModalProgressHUD(
                        inAsyncCall: _profileState?.isLoading ?? false,
                        child: Scaffold(
                          key: _scaffoldKey,
                          appBar: _showAppBar(),
                          body: Column(
                            children: [
                              Expanded(
                                  flex: 80,
                                  child: Wrap(
                                    alignment: WrapAlignment.center,
                                    runSpacing: SIZE_2,
                                    children: <Widget>[
                                      _showDivider(),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: SIZE_25,
                                            right: SIZE_25,
                                            bottom: SIZE_10),
                                        child: Column(
                                          children: [
                                            _showUserProfilePic(
                                                context: context),
                                            _showUserName(),
                                            _showProfileForm(),
                                            _showSpace(),
                                          ],
                                        ),
                                      )
                                    ],
                                  )),
                              Expanded(flex: 30, child: _showSaveButton())
                            ],
                          ),
                        ),
                      );
                    },
                  )
                : const SizedBox());
      },
    );
  }

  //this method will return user profile pic view
  Widget _showUserProfilePic({BuildContext context}) {
    return Align(
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          Container(
            width: CommonUtils.commonUtilsInstance
                .getPercentageSize(context: context, percentage: SIZE_20),
            height: CommonUtils.commonUtilsInstance
                .getPercentageSize(context: context, percentage: SIZE_20),
            child: (_authState?.authResponseModel?.userData?.profilePicture !=
                        null &&
                    _authState?.authResponseModel?.userData?.profilePicture
                            ?.isNotEmpty ==
                        true)
                ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                    context: context,
                    showProgressBarInPlaceHolder: true,
                    radius: 0.0,
                    placeHolderImage: DEFAULT_PROFILE_ICON,
                    // todo later change to profile icon
                    height: double.infinity,
                    width: double.infinity,
                    url: _authState
                            ?.authResponseModel?.userData?.profilePicture ??
                        null,
                  )
                : CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: SIZE_30,
                    backgroundImage: AssetImage(DEFAULT_PROFILE_ICON)),
          ),
          Positioned(
              bottom: -15,
              left: SIZE_48,
              top: SIZE_35,
              right: 0,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                padding: EdgeInsets.all(SIZE_1),
                child: CircleAvatar(
                  backgroundColor: Colors.blue,
                  radius: SIZE_16,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.edit,
                      size: SIZE_12,
                      color: Colors.white,
                    ),
                    color: Colors.white,
                    onPressed: () {
                      _profileManager.onProfileImageUpdate(
                          profileState: _profileState,
                          context: _context,
                          authResponseModel: _authState?.authResponseModel,
                          profileBloc: _profileBloc,
                          scaffoldState: _scaffoldKey.currentState);
                    },
                  ),
                ),
              ))
        ],
      ),
    );
  }

  //this method will show profile form widget
  _showProfileForm() {
    return Form(
      key: _formKey,
      child: Wrap(
        runSpacing: SIZE_15,
        children: <Widget>[
          //this personal was not showing correct in parent
          // wrap widget it was not aligned vertically
          _showPersonalInfoTitle(),
          _showNameFormField(),
          _showCommonWidget(
              title: AppLocalizations.of(_context)
                  .editprofilepage
                  .text
                  .phoneNumber,
              editType: 2,
              value: (_authState?.authResponseModel?.userData?.phoneNumber !=
                          null &&
                      _authState?.authResponseModel?.userData?.phoneNumber
                          ?.isNotEmpty)
                  ? '(${_authState?.authResponseModel?.userData?.countryCode}) ${_authState?.authResponseModel?.userData?.phoneNumber ?? ""}'
                  : ""),
          _showCommonWidget(
              title: AppLocalizations.of(_context).editprofilepage.text.email,
              editType: 3,
              value: '${_authState?.authResponseModel?.userData?.email ?? ""}'),
          /* _showPhoneNumberFormField(),
          _showEmailFormField(),*/
        ],
      ),
    );
  }

  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    IconData icon,
    String prefixIcon,
    String hint,
    bool isInputFormater,
    double elevation,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Padding(
      padding: EdgeInsets.only(left: SIZE_0),
      child: TextFormField(
        validator: validator,
        style: AppConfig.of(_context).themeData.textTheme.headline2,
        enabled: enable,
        controller: controller,
        keyboardType: keyboardType,
        textCapitalization: TextCapitalization.words,
        /* onChanged: (name){
            userName=name;
            if(name.length<3) {
              _updateUserName(name);
            }

          }*/
        decoration: InputDecoration(
          hintText: hint,
          contentPadding: EdgeInsets.only(left: SIZE_10),
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  //this method will return a app bar widget
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(context)
            .editprofilepage
            .appbartitle
            .editProfile,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_16),
            child: NotificationReadCountWidget(
              context: _context,
            ),
          )
        ],
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show user name
  Widget _showUserName() {
    return Column(
      children: [
        Text(_authState?.authResponseModel?.userData?.name ?? "",
            style: textStyleSizeBlackColor),
        _showUserPhoneNumber(),
      ],
    );
  }

  //method to show personal info title
  Widget _showPersonalInfoTitle() {
    return Padding(
      padding: const EdgeInsets.only(top: SIZE_15),
      child: Text(
        AppLocalizations.of(_context).editprofilepage.title.personalInfo,
        style: textStyleSize18WithGreenColor,
      ),
    );
  }

  //method to show phone number form field
  Widget _showPhoneNumberFormField() {
    return InkWell(
      onTap: () {
        NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            _context, AuthRoutes.EDIT_PHONE_EMAIL_SCREEN_ROOT,
            dataToBeSend: CommonPassDataModel(VerifyOtpFrom.PhoneNumber.value,
                _phoneController?.text, _selectedCountry));
      },
      child: Container(
        padding: EdgeInsets.only(
            left: SIZE_10, right: SIZE_5, top: SIZE_8, bottom: SIZE_8),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_10)),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 22,
              child: CountryPicker(
                dense: false,
                showFlag: true,
                // isShowDialog: false,
                //displays flag, true by default
                showDialingCode: false,
                //displays dialing code, false by default
                showName: false,
                //displays country name, true by default
                showCurrency: false,
                //eg. 'British pound'
                showCurrencyISO: false,
                //eg. 'GBP'
                onChanged: (Country country) {},
                selectedCountry: _selectedCountry ?? Country.IN,
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                margin: EdgeInsets.only(right: SIZE_12),
                child: Image.asset(DIVIDER_ICON),
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, ofWidth: false, percentage: SIZE_5),
              ),
            ),
            Expanded(
              flex: 77,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      (_authState?.authResponseModel?.userData?.countryCode ??
                              Country.IN.dialingCode + " ") +
                          " " +
                          _phoneController.text,
                      style:
                          AppConfig.of(_context).themeData.textTheme.headline2,
                    ),
                  ),
                  Image.asset(TICK_ICON,
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _context,
                          percentage: SIZE_6,
                          ofWidth: false),
                      width: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _context, percentage: SIZE_8, ofWidth: true))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  //method to show email form field
  Widget _showEmailFormField() {
    return InkWell(
        onTap: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
              _context, AuthRoutes.EDIT_PHONE_EMAIL_SCREEN_ROOT,
              dataToBeSend: CommonPassDataModel(VerifyOtpFrom.Email.value,
                  _emailController?.text, _selectedCountry));
        },
        child: Container(
          padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
              borderRadius: BorderRadius.circular(SIZE_30)),
          child: Row(
            children: <Widget>[
              Expanded(
                  flex: 12,
                  child: Padding(
                    padding: const EdgeInsets.only(top: SIZE_2),
                    child: Image.asset(
                      EMAIL_ICON,
                      height: SIZE_16,
                      width: SIZE_16,
                      alignment: Alignment.center,
                    ),
                  )),
              Expanded(
                flex: 88,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: _textFieldForm(
                          controller: _emailController,
                          enable: false,
                          icon: null,
                          isInputFormater: false,
                          hint: AppLocalizations.of(_context)
                              .editphoneoremailpage
                              .hint
                              .enterEmail,
                          elevation: _profileState.enableProfileEditing
                              ? ELEVATION_05
                              : ELEVATION_0),
                    ),
                    (_emailController?.text?.isNotEmpty == true)
                        ? Image.asset(TICK_ICON,
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: _context,
                                    percentage: SIZE_6,
                                    ofWidth: false),
                            width: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: _context,
                                    percentage: SIZE_8,
                                    ofWidth: true))
                        : const SizedBox()
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  //this method is used to show name form field
  Widget _showNameFormField() {
    return Container(
        child: (_profileState?.enableProfileEditing == false)
            ? _showCommonWidget(
                title: AppLocalizations.of(_context).editprofilepage.text.name,
                editType: 1,
                value: '${_authState?.authResponseModel?.userData?.name ?? ""}')
            : Container(
                padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
                    borderRadius: BorderRadius.circular(SIZE_30)),
                child: _textFieldForm(
                    controller: _nameController,
                    icon: null,
                    isInputFormater: false,
                    enable:
                        true /*_profileState.enableProfileEditing ?? false*/,
                    elevation: _profileState.enableProfileEditing
                        ? ELEVATION_05
                        : ELEVATION_0),
              ));
  }

  //this method is used to show user phone number widget
  Widget _showCommonWidget({String title, String value, int editType}) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        if (editType == 1) {
          _profileBloc.emitEvent(UpdateEvent(
              isLoading: false,
              context: context,
              authResponseModel: _profileState?.authResponseModel,
              enableProfileEditing: true));
        } else if (editType == 2) {
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
              _context, AuthRoutes.EDIT_PHONE_EMAIL_SCREEN_ROOT,
              dataToBeSend: CommonPassDataModel(VerifyOtpFrom.PhoneNumber.value,
                  _phoneController?.text, _selectedCountry));
        } else {
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
              _context, AuthRoutes.EDIT_PHONE_EMAIL_SCREEN_ROOT,
              dataToBeSend: CommonPassDataModel(VerifyOtpFrom.Email.value,
                  _emailController?.text, _selectedCountry));
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            child: Text(title,
                style: AppConfig.of(_context).themeData.textTheme.bodyText2),
          ),
          Row(
            children: <Widget>[
              Text(
                value,
                style:
                    AppConfig.of(_context).themeData.primaryTextTheme.headline4,
                textAlign: TextAlign.end,
              ),
              Padding(
                padding: const EdgeInsets.only(left: SIZE_5),
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: SIZE_16,
                  color: COLOR_GREY_LIGHT,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  //this method is used to show save button
  Widget _showSaveButton() {
    return (_profileState?.enableProfileEditing == false)
        ? const SizedBox()
        : Align(
            alignment: Alignment.center,
            child: Container(
              padding: const EdgeInsets.only(
                left: SIZE_25,
                right: SIZE_25,
              ),
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, percentage: SIZE_6, ofWidth: false),
              child: RaisedButton(
                onPressed: () {
                  _validateProfile();
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(SIZE_80)),
                padding: EdgeInsets.all(SIZE_0),
                child: Ink(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [COLOR_PRIMARY, COLOR_PRIMARY],
                      ),
                      borderRadius: BorderRadius.circular(SIZE_30)),
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      AppLocalizations.of(context).common.text.done,
                      textAlign: TextAlign.center,
                      style: textStyleSize14WithWhiteColor,
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  //method to validate edit profile fields
  void _validateProfile() {
    _profileManager.actionOnContinueButton(
      name: _nameController.text,
      title: _authState?.authResponseModel?.userData?.title,
      profileBloc: _profileBloc,
      enableProfileEditing: /*_profileState.enableProfileEditing ??*/ false,
      authResponseModel: _authState?.authResponseModel,
      context: _context,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }

  //method to give space btwn form and widget
  Widget _showSpace() {
    return SizedBox(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
      context: context,
      percentage: SIZE_15,
      ofWidth: false,
    ));
  }

/*  getTitleValue(String title) {
    switch (title) {
      case "Ms.":
        return 2;
        break;
      case "Mrs.":
        return 1;
        break;
      case "Mr.":
        return 0;
        break;
    }
  }

  String _showGenderValue(int value) {
    switch (value) {
      case 0:
        return "Mr.";
        break;
      case 1:
        return "Mrs.";
        break;
      case 2:
        return "Ms.";
        break;
    }
  }*/

  void _updateUserName(String name) {
    _authManager?.updateUI(
        context: _context, authBloc: _authBloc, authState: _authState);
  }

  Widget _showUserPhoneNumber() {
    return Text(
        (_authState?.authResponseModel?.userData?.phoneNumber?.isNotEmpty==true)?'(${_authState?.authResponseModel?.userData?.countryCode}) '
                '${_authState?.authResponseModel?.userData?.phoneNumber}' ??
            "":"",
        style: textStyleSize14WithGREY);
  }

  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1,
    );
  }
}

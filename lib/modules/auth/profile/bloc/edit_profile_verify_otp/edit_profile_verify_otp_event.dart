import 'package:flutter/cupertino.dart';
import 'package:user/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:user/modules/common/model/update_ui_data_model.dart';

import '../../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../../modules/auth/profile/api/edit_profile_verify_otp/model/edit_profile_verify_otp_request_model.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class EditProfileVerifyOtpEvent extends BlocEvent {
  final bool isLoading;
  final String otp;
  final BuildContext context;
  final AuthResponseModel authResponseModel;
  final EditProfileVerifyOtpRequestModel verifyOtpRequestModel;
  final UpdateUiDataModel updateUiDataModel;
  final SendOtpRequestModel sendOtpRequestModel;

  EditProfileVerifyOtpEvent(
      {this.isLoading,
      this.otp,
      this.updateUiDataModel,
      this.context,
      this.authResponseModel,
      this.verifyOtpRequestModel,
      this.sendOtpRequestModel});
}

// edit profile verify otp event
class ProfileEditVerifyOtpEvent extends EditProfileVerifyOtpEvent {
  ProfileEditVerifyOtpEvent({
    bool isLoading,
    String otp,
    BuildContext context,
    AuthResponseModel authResponseModel,
    EditProfileVerifyOtpRequestModel verifyOtpRequestModel,
  }) : super(
          isLoading: isLoading,
          otp: otp,
          context: context,
          authResponseModel: authResponseModel,
          verifyOtpRequestModel: verifyOtpRequestModel,
        );
}

// edit profile verify otp event
class ProfileEditResendOtpEvent extends EditProfileVerifyOtpEvent {
  ProfileEditResendOtpEvent(
      {bool isLoading,
      BuildContext context,
      AuthResponseModel authResponseModel,
      SendOtpRequestModel sendOtpRequestModel})
      : super(
            isLoading: isLoading,
            context: context,
            authResponseModel: authResponseModel,
            sendOtpRequestModel: sendOtpRequestModel);
}

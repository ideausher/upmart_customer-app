import 'package:user/modules/auth/api/sign_up/provider/sign_up_provider.dart';
import 'package:user/modules/common/model/common_response_model.dart';

import '../../../../../localizations.dart';
import '../../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../../modules/auth/profile/api/edit_profile_verify_otp/provider/edit_profile_verify_otp_provider.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../../../modules/common/enum/enums.dart';
import '../../../../../modules/common/utils/shared_prefs_utils.dart';
import 'edit_profile_verify_otp_event.dart';
import 'edit_profile_verify_otp_state.dart';

class EditProfileVerifyOtpBloc extends BlocEventStateBase<EditProfileVerifyOtpEvent, EditProfileVerifyOtpState> {
  EditProfileVerifyOtpBloc({bool initializing = true}) : super(initialState: EditProfileVerifyOtpState.initiating());

  @override
  Stream<EditProfileVerifyOtpState> eventHandler(
      EditProfileVerifyOtpEvent event, EditProfileVerifyOtpState currentState) async* {
    // used for the call sign up api

    // for verify otp
    if (event is ProfileEditVerifyOtpEvent) {
      AuthResponseModel _authResponseModel;
      String _message = "";
      ApiStatus status;
      // update ui
      yield EditProfileVerifyOtpState.updateUi(
          isLoading: true,
          message: "",
          context: event.context,
          authResponseModel: event.authResponseModel,
          status: ApiStatus.NoChange);

      _authResponseModel = event?.authResponseModel;
      String accessToken = _authResponseModel?.userData?.accessToken;



      var result = await EditProfileVerifyOtpProvider().editProfileVerifyOtpApiCall(
        context: event.context,
        editProfileVerifyOtpRequestModel: event.verifyOtpRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _authResponseModel = AuthResponseModel.fromMap(result);
          _authResponseModel?.userData?.accessToken = accessToken;
          await SharedPrefUtils.sharedPrefUtilsInstance.saveObject(_authResponseModel, PrefsEnum.UserProfileData.value);
          _message = _authResponseModel?.message;
          status = ApiStatus.Updated;


        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];

          status = ApiStatus.NoChange;
        }
      } else {
        _message = AppLocalizations?.of(event?.context)?.common?.error?.somethingWentWrong;
        status = ApiStatus.NoChange;


      }
      yield EditProfileVerifyOtpState.updateUi(
          isLoading: false,
          message: _message /*result[ApiStatusParams.Message.value]*/,
          authResponseModel: _authResponseModel,
          status: status);
    }

    // for resend otp
    if (event is ProfileEditResendOtpEvent) {
      String _message = ""; // message
      CommonResponseModel commonResponseModel = new CommonResponseModel();
      // update UI
      yield EditProfileVerifyOtpState.updateUi(
          isLoading: true,
          message: "",
          context: event.context,
          authResponseModel: event?.authResponseModel,
          commonResponseModel: commonResponseModel);

      // api calling
      var result = await SendOtpProvider().sendOtp(
        context: event.context,
        sendOtpRequestModel: event.sendOtpRequestModel,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          commonResponseModel = CommonResponseModel.fromJson(result);
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message = AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }

      yield EditProfileVerifyOtpState.updateUi(
          isLoading: false,
          message: "",
          context: event.context,
          authResponseModel: event?.authResponseModel,
          commonResponseModel: commonResponseModel);
    }
  }
}

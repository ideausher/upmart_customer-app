import 'package:flutter/cupertino.dart';
import 'package:user/modules/common/model/common_response_model.dart';

import '../../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../../../modules/common/enum/enums.dart';

class EditProfileVerifyOtpState extends BlocState {
  EditProfileVerifyOtpState(
      {this.isLoading: false,
      this.message,
      this.authResponseModel,
      this.otp,
      this.context,
      this.status = ApiStatus.NoChange,
      this.commonResponseModel})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final AuthResponseModel authResponseModel;
  final String otp;
  final BuildContext context;
  final ApiStatus status;
  final CommonResponseModel commonResponseModel;

  // used to Update UI
  factory EditProfileVerifyOtpState.updateUi(
      {bool isLoading,
      String message,
      AuthResponseModel authResponseModel,
      String otp,
      BuildContext context,
      ApiStatus status,
      CommonResponseModel commonResponseModel}) {
    return EditProfileVerifyOtpState(
        isLoading: isLoading,
        message: message,
        authResponseModel: authResponseModel,
        otp: otp,
        context: context,
        status: status,
        commonResponseModel: commonResponseModel);
  }

  // init
  factory EditProfileVerifyOtpState.initiating() {
    return EditProfileVerifyOtpState(
      isLoading: false,
    );
  }
}

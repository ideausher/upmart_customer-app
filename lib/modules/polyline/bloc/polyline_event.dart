import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/polyline/api/polyline_points_api/model/poly_lat_long_model.dart';
import '../../../modules/polyline/api/polyline_points_api/repo/polyline_api.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class PolyLineEvent extends BlocEvent {
  final bool isLoading;
  final String googleApiKey;
  final PolyPointLatLngModel originLatLong;
  final PolyPointLatLngModel destLatLong;
  final TravelMode travelMode;
  final List<PolyPointLatLngModel> wayPosPoints;
  final Set<Polyline> polylines;

  final BuildContext context;

  PolyLineEvent({
    this.isLoading,
    this.googleApiKey,
    this.originLatLong,
    this.destLatLong,
    this.travelMode,
    this.wayPosPoints,
    this.polylines,
    this.context,
  });
}

// used to check authentication
class FetchPolyLineEvent extends PolyLineEvent {
  FetchPolyLineEvent(
      {bool isLoading,
      String googleApiKey,
      PolyPointLatLngModel originLatLong,
      PolyPointLatLngModel destLatLong,
      TravelMode travelMode,
      List<PolyPointLatLngModel> wayPosPoints,
      Set<Polyline> polylines,
      BuildContext context})
      : super(
            isLoading: isLoading,
            wayPosPoints: wayPosPoints,
            travelMode: travelMode,
            googleApiKey: googleApiKey,
            originLatLong: originLatLong,
            destLatLong: destLatLong,
            polylines: polylines,
            context: context);
}

import 'package:flutter/material.dart';

import '../../../../modules/common/app_config/app_config.dart';
import '../../../../modules/current_location_updator/api/model/location_request_model.dart';

class LocationApi {
  // Location
  Future<dynamic> callLocationApiCall({BuildContext context, LocationRequestModel currentLocationModel}) async {
    var path = "v1/updateDriverLocation";
    var result = await AppConfig.of(context).baseApi.postRequest(
          path,
          context,
          data: currentLocationModel.toJson(),
        );

    return result;
  }
}

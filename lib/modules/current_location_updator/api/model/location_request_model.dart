// To parse this JSON data, do
//
//     final locationRequestModel = locationRequestModelFromJson(jsonString);

import 'dart:convert';

LocationRequestModel locationRequestModelFromJson(String str) => LocationRequestModel.fromJson(json.decode(str));

String locationRequestModelToJson(LocationRequestModel data) => json.encode(data.toJson());

class LocationRequestModel {
  LocationModel location;

  LocationRequestModel({
    this.location,
  });

  factory LocationRequestModel.fromJson(Map<String, dynamic> json) => LocationRequestModel(
        location: json["location"] == null ? null : LocationModel.fromJson(json["location"]),
      );

  Map<String, dynamic> toJson() => {
        "location": location == null ? null : location.toJson(),
      };
}

class LocationModel {
  double latitude;
  double longitude;
  double accuracy;
  double altitude;
  double speed;
  double speedAccuracy;
  double heading;
  double time;

  LocationModel({
    this.latitude,
    this.longitude,
    this.accuracy,
    this.altitude,
    this.speed,
    this.speedAccuracy,
    this.heading,
    this.time,
  });

  factory LocationModel.fromJson(Map<String, dynamic> json) => LocationModel(
        latitude: json["latitude"] == null ? null : json["latitude"]?.toDouble(),
        longitude: json["longitude"] == null ? null : json["longitude"]?.toDouble(),
        accuracy: json["accuracy"] == null ? null : json["accuracy"]?.toDouble(),
        altitude: json["altitude"] == null ? null : json["altitude"]?.toDouble(),
        speed: json["speed"] == null ? null : json["speed"]?.toDouble(),
        speedAccuracy: json["speed_accuracy"] == null ? null : json["speed_accuracy"]?.toDouble(),
        heading: json["heading"] == null ? null : json["heading"]?.toDouble(),
        time: json["time"] == null ? null : json["time"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "accuracy": accuracy == null ? null : accuracy,
        "altitude": altitude == null ? null : altitude,
        "speed": speed == null ? null : speed,
        "speed_accuracy": speedAccuracy == null ? null : speedAccuracy,
        "heading": heading == null ? null : heading,
        "time": time == null ? null : time,
      };
}

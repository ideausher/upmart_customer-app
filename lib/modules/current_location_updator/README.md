follow this link to add location dependency

https://pub.dev/packages/location#-readme-tab-


# import location: ^3.0.2 on yaml file

And to use it in iOS, you have to add this permission in Info.plist :

NSLocationWhenInUseUsageDescription
NSLocationAlwaysUsageDescription




We have different function to use it if we want to init directly
just call the initLocationManager

And you need to add listener on the screen where you want to add and remove it in dispose

and on location change , call this function if you want to sync to server , just change the url callLocationApi
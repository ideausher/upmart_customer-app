import 'package:flutter/cupertino.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/intro/constant/image_constant.dart';
import 'package:user/modules/intro/enum/slider_enum.dart';
import 'package:user/modules/slider_image_video/enums/slider_image_video_enum.dart';
import 'package:user/modules/slider_image_video/model/slider_model.dart';

class StartScreenActionManager {

  BuildContext context;
  StartScreenActionManager(this.context);
  List<SliderModel> sliderImagesList = List();
  SliderImageVideoModel sliderImageVideoModel = new SliderImageVideoModel();

  //method to set Images to the image List
  List<SliderModel> getImagesList() {
    sliderImagesList = List();
    for (int index = 0; index < 3; index++) {
      SliderModel sliderModel = new SliderModel();
      sliderModel.mediaType = MediaType.Image;
      sliderModel.url = getImages(index);
      sliderModel.titleText = getTitleText(index);
      sliderModel.subTitleText = getSubTitleText(index);
      sliderImagesList.add(sliderModel);
    }
    return sliderImagesList;
  }

  //get image video model //basically used at passing to common slider widget
  SliderImageVideoModel getImageVideoModel() {
    if (sliderImageVideoModel == null) {
      sliderImageVideoModel = SliderImageVideoModel();
    }
    sliderImagesList = getImagesList();
    sliderImageVideoModel.sliderModelList = sliderImagesList;
    sliderImageVideoModel.autoPlay = false;
    sliderImageVideoModel.enlargeCenterPage = false;
    return sliderImageVideoModel;
  }

  //method to get images
  String getImages(int index) {
    if (index == StartScreen.SearchOnline.value) {
      return SEARCH_ONLINE_LOGO;
    } else if (index == StartScreen.PlaceOrder.value) {
      return PLACE_ORDER_LOGO;
    } else {
      return TRACK_PROGRESS_LOGO;
    }
  }

  //method to get title text
  String getTitleText(int index) {
    if (index == StartScreen.SearchOnline.value) {
      return /*'Search for any\nitem online'*/ AppLocalizations.of(context).intropage.title.searchItemOnline;
    } else if (index == StartScreen.PlaceOrder.value) {
      return AppLocalizations.of(context).intropage.title.placeOrder;
    } else {
      return AppLocalizations.of(context).intropage.title.trackYourOrder;
    }
  }

  //method to get sub title text
  String getSubTitleText(index) {
    if (index == StartScreen.SearchOnline.value) {
      return AppLocalizations.of(context).intropage.subtitle.quickSearch;
    } else if (index == StartScreen.PlaceOrder.value) {
      return AppLocalizations.of(context).intropage.subtitle.searchAndPlaceOrder;
    } else {
      return AppLocalizations.of(context).intropage.subtitle.realTimeTracking;
    }
  }
}

import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/auth/auth_routes.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/intro/constant/image_constant.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  AuthManager _authManager = AuthManager();
  var _onceUiBuild = false;
  BuildContext _context;
  bool isAnimationFinished = false;

  @override
  Widget build(BuildContext context) {
    _context = context;
    if (!_onceUiBuild) {
      _onceUiBuild = true;
      /* _authManager.actionOnIntroScreenStateChange(
        context: context,
      );*/
    }
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover, image: AssetImage(INTRO_BACKGROUND))),
          ),
          Padding(
            padding: const EdgeInsets.all(SIZE_30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    flex: 15,
                    child: Container(
                      color: Colors.transparent,
                    )),
                Expanded(
                  flex: 85,
                  child: Container(
                    color: Colors.transparent,
                    child: Column(
                      children: <Widget>[_showIntroLogo(), showUpmartLogo()],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  //this method will show intro logo
  Widget _showIntroLogo() {
    return Expanded(
      flex: 70,
      child: (isAnimationFinished == true)
          ? Align(
              alignment: Alignment.topCenter,
              child: Image.asset(
                INTRO_LOGO,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, ofWidth: false, percentage: SIZE_20),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, ofWidth: true, percentage: SIZE_36),
              ),
            )
          : Align(
              alignment: Alignment.topCenter,
              child: Animator(
                tween: Tween<Offset>(begin: Offset(-1, 0), end: Offset(0.0, 0)),
                duration: Duration(seconds: 2),
                curve: Curves.decelerate,
                repeats: 1,
                cycles: 1,
                endAnimationListener: (animation) {
                  if (animation.controller.isCompleted == true) {
                    isAnimationFinished = true;
                    setState(() {});
                  }
                },
                builder: (_, animatorState, __) => FractionalTranslation(
                  translation: animatorState.value,
                  child: Image.asset(
                    INTRO_TILT_BACKGROUND,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _context, ofWidth: false, percentage: SIZE_20),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _context, ofWidth: true, percentage: SIZE_36),
                  ),
                ),
              ),
            ),
    );
  }

  //this method will show Limit less logo
  Widget showUpmartLogo() {
    return Animator(
      tween: Tween<Offset>(begin: Offset(0, 0.5), end: Offset(0, 0)),
      duration: Duration(milliseconds: 500),
      repeats: 1,
      cycles: 1,
      builder: (_, animatorState, __) => FractionalTranslation(
          translation: animatorState.value,
          child: Container(
            margin: EdgeInsets.only(bottom: SIZE_16),
              child: RaisedGradientButton(
            radious: SIZE_30,
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: <Color>[COLOR_DARK_PRIMARY, COLOR_PRIMARY],
            ),
            onPressed: () {
              //navigate to send otp screen
              _navigateToSendOtpScreen();
            },
            child: Text(
              'Get Started',
              textAlign: TextAlign.center,
              style: textStyleSize14WithWhiteColor,
            ),
          ))),
    );
  }

  //this method will navigate user to send otp screen
  void _navigateToSendOtpScreen() {
    NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
      context,
      AuthRoutes.SEND_OTP_SCREEN,
    );
  }
}

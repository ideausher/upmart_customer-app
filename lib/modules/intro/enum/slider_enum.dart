enum StartScreen { SearchOnline, PlaceOrder, LiveTrack }

extension StartScreenExtension on StartScreen {
  int get value {
    switch (this) {
      case StartScreen.SearchOnline:
        return 0;
      case StartScreen.PlaceOrder:
        return 1;
      case StartScreen.LiveTrack:
        return 2;

      default:
        return null;
    }
  }
}

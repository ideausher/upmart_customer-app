import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';

abstract class DashboardEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final BottomBarEnum bottomEnumType;
  final AuthResponseModel authResponseModel;

  DashboardEvent({
    this.isLoading: false,
    this.context,
    this.authResponseModel,
    this.bottomEnumType = BottomBarEnum.Home,
  });
}

// for select bottom tab index
class SelectBottomTabEvent extends DashboardEvent {
  SelectBottomTabEvent({
    BuildContext context,
    bool isLoading,
    AuthResponseModel authResponseModel,
    BottomBarEnum bottomEnumType,
  }) : super(
          context: context,
          isLoading: isLoading,
          authResponseModel: authResponseModel,
          bottomEnumType: bottomEnumType,
        );
}

class GetAuthenticationDataEvent extends DashboardEvent {
  GetAuthenticationDataEvent({
    BuildContext context,
    bool isLoading,
    AuthResponseModel authResponseModel,
    BottomBarEnum bottomEnumType,
  }) : super(
    context: context,
    isLoading: isLoading,
    authResponseModel: authResponseModel,
    bottomEnumType: bottomEnumType,
  );
}
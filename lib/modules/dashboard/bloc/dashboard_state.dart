import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/force_update/model/force_update_response_model.dart';

class DashboardState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final BottomBarEnum bottomEnumType;
  final AuthResponseModel authResponseModel;
  final ForceUpdateResponseModel forceUpdateResponseModel;
  final bool needToUpdate;
  DashboardState(
      {this.isLoading,
      this.context,
      this.bottomEnumType = BottomBarEnum.Home,
      this.authResponseModel,
      this.forceUpdateResponseModel,
      this.needToUpdate})
      : super(isLoading);

  // not authenticated
  factory DashboardState.initiating({bool isLoading}) {
    return DashboardState(
      isLoading: isLoading,
    );
  }

  factory DashboardState.updateUi({
    bool isLoading,
    BuildContext context,
    BottomBarEnum bottomEnumType,
    AuthResponseModel authResponseModel,
    ForceUpdateResponseModel forceUpdateResponseModel,
    bool needToUpdate,
  }) {
    return DashboardState(
      isLoading: isLoading,
      context: context,
      bottomEnumType: bottomEnumType,
      authResponseModel: authResponseModel,
      forceUpdateResponseModel: forceUpdateResponseModel,
      needToUpdate: needToUpdate,
    );
  }
}

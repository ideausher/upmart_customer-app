import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/force_update/api/provider/force_update_provider.dart';
import 'package:user/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';
import 'package:user/modules/force_update/model/force_update_response_model.dart';

import 'dashboard_event.dart';
import 'dashboard_state.dart';

class DashboardBloc extends BlocEventStateBase<DashboardEvent, DashboardState> {
  DashboardBloc({bool isLoading = false})
      : super(initialState: DashboardState.initiating(isLoading: isLoading));

  @override
  Stream<DashboardState> eventHandler(DashboardEvent event,
      DashboardState currentState) async* {
    // used for the select bottom tab
    if (event is SelectBottomTabEvent) {
      yield DashboardState.updateUi(
          isLoading: false,
          context: event.context,
          bottomEnumType: event.bottomEnumType,
          authResponseModel: event.authResponseModel);
    }

    // used to get authentication data
    if (event is GetAuthenticationDataEvent) {
      yield DashboardState.updateUi(
          isLoading: true,
          context: event.context,
          bottomEnumType: BottomBarEnum.Home,
          authResponseModel: event.authResponseModel);

      AuthResponseModel _authResponseModel = await FetchPrefsUtils
          .fetchPrefsUtilsInstance.getAuthResponseModel();



      //To Check force update
      var _result = await ForceUpdateProvider()
          .forceUpdateApiCall(context: event.context);
      ForceUpdateResponseModel _forceUpdateResponseModel;
      bool _needToUpdate;
      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          ForceUpdateActionManager _forceUpdateActionManager =
          ForceUpdateActionManager();
          _forceUpdateResponseModel = ForceUpdateResponseModel.fromMap(_result);
          _needToUpdate = await _forceUpdateActionManager
              .isNeedToUpdate(_forceUpdateResponseModel?.data?.version);
        }
      }



      yield DashboardState.updateUi(
          isLoading: false,
          context: event.context,
          bottomEnumType: BottomBarEnum.Home,
          authResponseModel: _authResponseModel,
          forceUpdateResponseModel: _forceUpdateResponseModel,
          needToUpdate: _needToUpdate
      );
    }
  }
}

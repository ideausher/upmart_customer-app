const String HOME_ICON = 'lib/modules/dashboard/images/home.svg';
const String SEARCH_ICON = 'lib/modules/dashboard/images/cart.svg';
const String PROFILE_ICON = 'lib/modules/dashboard/images/person.svg';
const String DEFAULT_PROFILE_ICON = 'lib/modules/dashboard/images/default_profile_image.png';
const String TAB_BG = 'lib/modules/dashboard/images/tab_bg.png';
const String TAB_HOME = 'lib/modules/dashboard/images/tab_home.svg';

//setting list icons
const String USER_ICON = 'lib/modules/dashboard/images/user_icon.png';
const String UPLOAD_ICON = 'lib/modules/dashboard/images/upload_icon.png';
const String HELP_AND_SUPPORT_ICON = 'lib/modules/dashboard/images/help_and_support.svg';
const String ORDERS_ICON = 'lib/modules/dashboard/images/orders_icon.png';
const String CHAT_ICON = 'lib/modules/dashboard/images/chat_icon.png';

const String LOGOUT_ICON = 'lib/modules/dashboard/images/logout_icon.png';
const String POLICY_ICON = 'lib/modules/dashboard/images/policy_icon.png';
const String FAVOURITE_ICON = 'lib/modules/dashboard/images/favourite.png';
const String REFER_ICON = 'lib/modules/dashboard/images/refer.png';
const String ADDRESS_ICON = 'lib/modules/dashboard/images/address.png';



const String SHOP_LOGO= 'lib/modules/dashboard/images/shop.svg';
const String PRODUCT_LOGO= 'lib/modules/dashboard/images/product.svg';
const String EMPTY_CART_LOGO= 'lib/modules/dashboard/images/empty_cart.svg';
const String EMPTY_FAQ_LOGO= 'lib/modules/dashboard/images/faq_empty.svg';
const String EMPTY_NOTIFICATION_LOGO= 'lib/modules/dashboard/images/empty_notification.png';
const String APPLE_LOGO= 'lib/modules/dashboard/images/app_pay.png';


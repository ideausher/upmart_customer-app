import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';


class ProductPageProvider {


  Future<dynamic> getShopProducts({BuildContext context,
    ProductDetailsRequestModel productsRequestModel}) async {
    var getProducts = "products";

    var result = await AppConfig
        .of(context)
        .baseApi
        .getRequest(
        getProducts, context,
        queryParameters: productsRequestModel?.toJson());

    return result;
  }
}

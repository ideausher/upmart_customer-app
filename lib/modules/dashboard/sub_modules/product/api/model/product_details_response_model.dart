// To parse this JSON data, do
//
//     final productsShopDetailsModel = productsShopDetailsModelFromJson(jsonString);
/*
import 'dart:convert';

ProductsShopDetailsModel productsShopDetailsModelFromJson(String str) =>
    ProductsShopDetailsModel.fromMap(json.decode(str));

String productsShopDetailsModelToJson(ProductsShopDetailsModel data) => json.encode(data.toJson());

class ProductsShopDetailsModel {
  ProductsShopDetailsModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory ProductsShopDetailsModel.fromMap(Map<String, dynamic> json) => ProductsShopDetailsModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        message: json["message"] == null ? null : json["message"],
        statusCode: json["status_code"] == null ? null : json["status_code"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "message": message == null ? null : message,
        "status_code": statusCode == null ? null : statusCode,
      };
}

class Data {
  Data({
    this.categoryServed,
    this.items,
  });

  List<CategoryServed> categoryServed;
  List<ShopItem> items;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        categoryServed: json["categoryServed"] == null
            ? null
            : List<CategoryServed>.from(json["categoryServed"].map((x) => CategoryServed.fromJson(x))),
        items: json["items"] == null ? null : List<ShopItem>.from(json["items"].map((x) => ShopItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "categoryServed": categoryServed == null ? null : List<dynamic>.from(categoryServed.map((x) => x.toJson())),
        "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class CategoryServed {
  CategoryServed({
    this.id,
    this.categoryName,
    this.categoryDescription,
    this.image,
  });

  int id;
  String categoryName;
  String categoryDescription;
  String image;

  List<ShopItem> itemsPerCategory=new List();

  factory CategoryServed.fromJson(Map<String, dynamic> json) => CategoryServed(
        id: json["id"] == null ? null : json["id"],
        categoryName: json["category_name"] == null ? null : json["category_name"],
        categoryDescription: json["category_description"] == null ? null : json["category_description"],
        image: json["image"] == null ? null : json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "category_description": categoryDescription == null ? null : categoryDescription,
        "image": image == null ? null : image,
      };
}

 class ShopItem {
  ShopItem({
    this.id,
    this.productName,
    this.productDescription,
    this.stockQuantity,
    this.mrpPrice,
    this.productImage,
    this.categoryName,
    this.companyName,
    this.shopId,
    this.shopName,
    this.categoryId,
    this.isDisabled,
    this.discountPercentage,
    this.discountedPrice,
    this.itemQuantity,
    this.showInDecButtton:false,
     this.shopMedia,
    this.shopProfilePicture,
  });

  num id;
  String productName;
  String productDescription;
  num stockQuantity;
  num mrpPrice;
  ProductImage productImage;
  String categoryName;
  String companyName;
  num shopId;
  String shopName;
  num categoryId;
  num isDisabled;
  num discountPercentage;
  num discountedPrice;
  num itemQuantity;
  bool showInDecButtton;
  List<ProductImage> shopMedia;
  ProductImage shopProfilePicture;




  factory ShopItem.fromJson(Map<String, dynamic> json) => ShopItem(
        id: json["id"] == null ? null : json["id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productDescription: json["product_description"] == null ? null : json["product_description"],
        stockQuantity: json["stock_quantity"] == null ? null : json["stock_quantity"],
        mrpPrice: json["mrp_price"] == null ? 0.0 as num : json["mrp_price"] as num,
        productImage: json["product_image"] == null ? null : ProductImage.fromJson(json["product_image"]),
        categoryName: json["category_name"] == null ? null : json["category_name"],
        companyName: json["company_name"] == null ? null : json["company_name"],
        shopId: json["shop_id"] == null ? null : json["shop_id"],
        shopName: json["shop_name"] == null ? null : json["shop_name"],
         shopMedia: json["shop_media"] == null ? null : List<ProductImage>.from(json["shop_media"].map((x) => ProductImage.fromJson(x))),
        shopProfilePicture: json["shop_profile_picture"] == null ? null : ProductImage.fromJson(json["shop_profile_picture"]),
        categoryId: json["category_id"] == null ? null : json["category_id"],
        isDisabled: json["is_disabled"] == null ? null : json["is_disabled"],
        discountPercentage: json["discount_percentage"] == null ? null : json["discount_percentage"],
        discountedPrice: json["discounted_price"] == null ? null : json["discounted_price"],
    itemQuantity: json["itemQuantity"] == null ? null : json["itemQuantity"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "product_name": productName == null ? null : productName,
        "product_description": productDescription == null ? null : productDescription,
        "stock_quantity": stockQuantity == null ? null : stockQuantity,
        "mrp_price": mrpPrice == null ? null : mrpPrice,
        "product_image": productImage == null ? null : productImage.toJson(),
        "category_name": categoryName == null ? null : categoryName,
        "company_name": companyName == null ? null : companyName,
        "shop_id": shopId == null ? null : shopId,
        "shop_name": shopName == null ? null : shopName,
        "category_id": categoryId == null ? null : categoryId,
        "is_disabled": isDisabled == null ? null : isDisabled,
         "shop_media": shopMedia == null ? null : List<dynamic>.from(shopMedia.map((x) => x.toJson())),
    "shop_profile_picture": shopProfilePicture == null ? null : shopProfilePicture.toJson(),
        "discount_percentage": discountPercentage == null ? null : discountPercentage,
        "discounted_price": discountedPrice == null ? null : discountedPrice,
        "itemQuantity": itemQuantity == null ? null : itemQuantity,
      };
}

class ProductImage {
  ProductImage({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory ProductImage.fromJson(Map<String, dynamic> json) => ProductImage(
        id: json["id"] == null ? null : json["id"],
        modelId: json["model_id"] == null ? null : json["model_id"],
        modelType: json["model_type"] == null ? null : json["model_type"],
        fileName: json["file_name"] == null ? null : json["file_name"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "model_id": modelId == null ? null : modelId,
        "model_type": modelType == null ? null : modelType,
        "file_name": fileName == null ? null : fileName,
        "name": name == null ? null : name,
      };
}*/



// To parse this JSON data, do
//
//     final productsShopDetailsModel = productsShopDetailsModelFromJson(jsonString);

import 'dart:convert';

import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';

ProductsShopDetailsModel productsShopDetailsModelFromJson(String str) =>
    ProductsShopDetailsModel.fromMap(json.decode(str));

String productsShopDetailsModelToJson(ProductsShopDetailsModel data) => json.encode(data.toJson());

class ProductsShopDetailsModel {
  ProductsShopDetailsModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory ProductsShopDetailsModel.fromMap(Map<String, dynamic> json) => ProductsShopDetailsModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.categoryServed,
    this.items,
  });

  List<CategoriesData> categoryServed;
  List<ShopItem> items;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    categoryServed: json["categoryServed"] == null
        ? null
        : List<CategoriesData>.from(json["categoryServed"].map((x) => CategoriesData.fromMap(x))),
    items: json["items"] == null ? null : List<ShopItem>.from(json["items"].map((x) => ShopItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categoryServed": categoryServed == null ? null : List<dynamic>.from(categoryServed.map((x) => x.toJson())),
    "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
  };
}



class ShopItem {
  ShopItem({
    this.id,
    this.productName,
    this.productDescription,
    this.stockQuantity,
    this.mrpPrice,
    this.productImage,
    this.categoryName,
    this.companyName,
    this.shopId,
    this.shopName,
    this.categoryId,
    this.isDisabled,
    this.discountPercentage,
    this.discountedPrice,
    this.itemQuantity,
    this.shopDetails,
    this.gstPrice,
    this.priceAfterGst,
    this.gstEnabled

  });

  num id;
  String productName;
  String productDescription;
  num stockQuantity;
  num mrpPrice;
  ProductImage productImage;
  String categoryName;
  String companyName;
  num shopId;
  String shopName;
  num categoryId;
  num isDisabled;
  num discountPercentage;
  num discountedPrice;
  num itemQuantity;
  num gstPrice;
  int gstEnabled;
  num priceAfterGst;
  ShopDetailsModel shopDetails;
  factory ShopItem.fromJson(Map<String, dynamic> json) => ShopItem(
    id: json["id"] == null ? null : json["id"],
    productName: json["product_name"] == null ? null : json["product_name"],
    productDescription: json["product_description"] == null ? null : json["product_description"],
    stockQuantity: json["stock_quantity"] == null ? null : json["stock_quantity"],
    mrpPrice: json["mrp_price"] == null ? 0.0 as num : json["mrp_price"] as num,
    productImage: json["product_image"] == null ? null : ProductImage.fromJson(json["product_image"]),
    categoryName: json["category_name"] == null ? null : json["category_name"],
    companyName: json["company_name"] == null ? null : json["company_name"],
    shopId: json["shop_id"] == null ? null : json["shop_id"],
    shopName: json["shop_name"] == null ? null : json["shop_name"],
    shopDetails: json["shop_details"] == null ? null : ShopDetailsModel.fromJson(json["shop_details"]),
    categoryId: json["category_id"] == null ? null : json["category_id"],
    isDisabled: json["is_disabled"] == null ? null : json["is_disabled"],
    discountPercentage: json["discount_percentage"] == null ? null : json["discount_percentage"],
    discountedPrice: json["discounted_price"] == null ? null : json["discounted_price"],
    itemQuantity: json["itemQuantity"] == null ? 0 : json["itemQuantity"],
    gstPrice: json["gst_price"] == null ? 0 : json["gst_price"],
    priceAfterGst: json["price_after_gst"] == null ? 0 : json["price_after_gst"],
    gstEnabled: json["gst_enable"] == null ? 0 : json["gst_enable"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "product_name": productName == null ? null : productName,
    "product_description": productDescription == null ? null : productDescription,
    "stock_quantity": stockQuantity == null ? null : stockQuantity,
    "mrp_price": mrpPrice == null ? null : mrpPrice,
    "product_image": productImage == null ? null : productImage.toJson(),
    "category_name": categoryName == null ? null : categoryName,
    "company_name": companyName == null ? null : companyName,
    "shop_id": shopId == null ? null : shopId,
    "shop_name": shopName == null ? null : shopName,
    "shop_details": shopDetails == null ? null : shopDetails.toJson(),
    "category_id": categoryId == null ? null : categoryId,
    "is_disabled": isDisabled == null ? null : isDisabled,
    "discount_percentage": discountPercentage == null ? null : discountPercentage,
    "discounted_price": discountedPrice == null ? null : discountedPrice,
    "itemQuantity": itemQuantity == null ? 0 : itemQuantity,
    "gst_price": gstPrice == null ? 0 : gstPrice,
    "price_after_gst": priceAfterGst == null ? 0 : priceAfterGst,
    "gst_enable": gstEnabled == null ? 0 : gstEnabled,
  };
}

class ProductImage {
  ProductImage({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory ProductImage.fromJson(Map<String, dynamic> json) => ProductImage(
    id: json["id"] == null ? null : json["id"],
    modelId: json["model_id"] == null ? null : json["model_id"],
    modelType: json["model_type"] == null ? null : json["model_type"],
    fileName: json["file_name"] == null ? null : json["file_name"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "model_id": modelId == null ? null : modelId,
    "model_type": modelType == null ? null : modelType,
    "file_name": fileName == null ? null : fileName,
    "name": name == null ? null : name,
  };
}






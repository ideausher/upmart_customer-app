// To parse this JSON data, do
//
//     final productDetailsRequestModel = productDetailsRequestModelFromJson(jsonString);

import 'dart:convert';

import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';

ProductDetailsRequestModel productDetailsRequestModelFromJson(String str) => ProductDetailsRequestModel.fromJson(json.decode(str));

String productDetailsRequestModelToJson(ProductDetailsRequestModel data) => json.encode(data.toJson());

class ProductDetailsRequestModel {
  ProductDetailsRequestModel({
    this.categoryId,
    this.searchText,
    this.lat,
    this.lng,
    this.shopId,
    this.limit,
    this.page,
    this.shopType,


  });

  List<int> categoryId;
  String searchText;
  String lat;
  String lng;
  int shopId;
  int limit;
  int page;
  int shopType;




  factory ProductDetailsRequestModel.fromJson(Map<String, dynamic> json) => ProductDetailsRequestModel(
    categoryId: json["category_id"] == null ? null : List<int>.from(json["category_id"].map((x) => x)),
    searchText: json["search_text"] == null ? null : json["search_text"],
    lat: json["lat"] == null ? null : json["lat"],
    lng: json["lng"] == null ? null : json["lng"],
    shopType: json["shop_type"] == null ? null : json["shop_type"],
    shopId: json["shop_id"] == null ? null : json["shop_id"],
    limit: json["limit"] == null ? null : json["limit"],
    page: json["page"] == null ? null : json["page"],

  );

  Map<String, dynamic> toJson() => {
    "category_id": categoryId == null ? null : List<dynamic>.from(categoryId.map((x) => x)),
    "search_text": searchText == null ? null : searchText,
    "lat": lat == null ? null : lat,
    "shop_type": shopType == null ? null : shopType,
    "lng": lng == null ? null : lng,
    "shop_id": shopId == null ? null : shopId,
    "limit": limit == null ? null : limit,
    "page": page == null ? null : page,
  };
}

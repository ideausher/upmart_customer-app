import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/provider/products_provider.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/product_search_bloc/product_search_event.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/product_search_bloc/product_search_state.dart';

class ProductSearchBloc
    extends BlocEventStateBase<ProductSearchEvent, ProductSearchState> {
  ProductSearchBloc({bool initializing = true})
      : super(initialState: ProductSearchState.initiating());

  @override
  Stream<ProductSearchState> eventHandler(
      ProductSearchEvent event, ProductSearchState currentState) async* {
    //update products page UI

    if (event is UpdateProductSearcUIEvent) {
      yield ProductSearchState.updateProducts(
          isLoading: event?.isLoading,
          context: event?.context,
          page: event?.page,
          productsResponseModel: event.productsResponseModel,
          searchText: event.searchText);
    }

    //method to get products category wise
    if (event is GetSearchDataEvent) {
      yield ProductSearchState.updateProducts(
          isLoading: true,
          context: event?.context,
          page: event?.page,
          productsResponseModel: event.productsResponseModel,
          searchText: event.searchText);
      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();
      int _value =
          CartUtilsManager.cartUtilsInstance.shopType == ShopType.delivery
              ? ShopType.delivery.value
              : ShopType.pickUp.value;

      var result = await ProductPageProvider().getShopProducts(
        context: event.context,
        productsRequestModel: ProductDetailsRequestModel(
            searchText: event?.searchText,
            page: event.page,
            limit: 10,
            lat: _currentLocation?.lat?.toString(),
            lng: _currentLocation?.lng?.toString(),
            categoryId: [],
            shopType: _value),
      );
      ProductsShopDetailsModel _productsResponseModel =
          event?.productsResponseModel;
      int _page = event?.page;
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          var _productModel = ProductsShopDetailsModel.fromMap(result);
          print(
              'the response model is ${productsShopDetailsModelToJson(_productModel)}');
          if (_productModel?.data?.items?.isNotEmpty == true) {
            if (_page > 1) {
              _productsResponseModel?.data?.items
                  ?.addAll(_productModel?.data?.items);
            } else {
              _productsResponseModel = _productModel;
            }
          } else {
            if (_page > 1) {
              _page = _page - 1;
            }
          }
        }
        // failure case
        else {
          if (_page > 1) {
            _page = _page - 1;
          }
        }
      } else {
        if (_page > 1) {
          _page = _page - 1;
        }
      }

      yield ProductSearchState.updateProducts(
          isLoading: false,
          context: event?.context,
          page: event?.page,
          productsResponseModel: _productsResponseModel,
          searchText: event.searchText);
    }
  }
}

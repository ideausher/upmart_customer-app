import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

abstract class ProductSearchEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final ProductsShopDetailsModel productsResponseModel;
  final String searchText;
  final int page;

  ProductSearchEvent(
      {this.isLoading: false,
      this.context,
      this.searchText,
      this.page,
      this.productsResponseModel});
}

//this event is used for updating the home page UI data
class UpdateProductSearcUIEvent extends ProductSearchEvent {
  UpdateProductSearcUIEvent({
    bool isLoading,
    BuildContext context,
    ProductsShopDetailsModel productsResponseModel,
    String searchText,
    int page,
  }) : super(
          isLoading: isLoading,
          page: page,
          productsResponseModel: productsResponseModel,
          searchText: searchText,
          context: context,
        );
}

//this event is used for updating the home page UI data
class GetSearchDataEvent extends ProductSearchEvent {
  GetSearchDataEvent({
    bool isLoading,
    BuildContext context,
    ProductsShopDetailsModel productsResponseModel,
    String searchText,
    int page,
  }) : super(
          isLoading: isLoading,
          page: page,
          productsResponseModel: productsResponseModel,
          searchText: searchText,
          context: context,
        );
}

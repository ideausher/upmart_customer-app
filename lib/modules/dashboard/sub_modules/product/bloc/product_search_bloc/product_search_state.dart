import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

class ProductSearchState extends BlocState {
  ProductSearchState(
      {this.isLoading: false,
      this.context,
      this.searchText,
      this.page,
      this.productsResponseModel})
      : super(isLoading);

  final bool isLoading;
  final BuildContext context;
  final ProductsShopDetailsModel productsResponseModel;
  final String searchText;
  final int page;

  // used for update profile api call
  factory ProductSearchState.updateProducts({
    bool isLoading,
    BuildContext context,
    ProductsShopDetailsModel productsResponseModel,
    String searchText,
    int page,
  }) {
    return ProductSearchState(
      isLoading: isLoading,
      page: page,
      productsResponseModel: productsResponseModel,
      searchText: searchText,
      context: context,
    );
  }

  factory ProductSearchState.initiating() {
    return ProductSearchState(
      isLoading: false,
    );
  }
}

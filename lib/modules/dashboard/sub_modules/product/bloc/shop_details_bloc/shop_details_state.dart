import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

class ShopDetailsState extends BlocState {
  ShopDetailsState({
    this.isLoading: false,
    this.context,
    this.productsResponseModel,
    this.shopDetailsModel,
    this.selectedCategoryId
  }) : super(isLoading);

  final bool isLoading;
  final BuildContext context;
  final ProductsShopDetailsModel productsResponseModel;
  final int selectedCategoryId;
  final ShopDetailsModel shopDetailsModel;

  // used for update profile api call
  factory ShopDetailsState.updateProducts({
    bool isLoading,
    BuildContext context,
    ProductsShopDetailsModel productsResponseModel,
    int selectedCategoryId,
    ShopDetailsModel shopDetailsModel,
  }) {
    return ShopDetailsState(
        isLoading: isLoading,
        context: context,
        selectedCategoryId: selectedCategoryId,
        shopDetailsModel: shopDetailsModel,
        productsResponseModel: productsResponseModel
      );
  }

  factory ShopDetailsState.initiating() {
    return ShopDetailsState(
      isLoading: false,
    );
  }
}

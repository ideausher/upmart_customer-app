import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

abstract class ShopDetailsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final ProductsShopDetailsModel productsResponseModel;
  final int selectedCategoryId;
  final ShopDetailsModel shopDetailsModel;

  ShopDetailsEvent(
      {this.isLoading: false,
      this.context,
      this.productsResponseModel,
      this.shopDetailsModel,
      this.selectedCategoryId});
}

//this event is used for updating the home page UI data
class UpdateShopDetailUIEvent extends ShopDetailsEvent {
  UpdateShopDetailUIEvent({
    bool isLoading,
    BuildContext context,
    ProductsShopDetailsModel productsResponseModel,
    int selectedCategoryId,
    ShopDetailsModel shopDetailsModel,
  }) : super(
            isLoading: isLoading,
            context: context,
            selectedCategoryId: selectedCategoryId,
            shopDetailsModel: shopDetailsModel,
            productsResponseModel: productsResponseModel);
}

//this event is used for updating the home page UI data
class GetProductsOfShopEvent extends ShopDetailsEvent {
  GetProductsOfShopEvent({
    bool isLoading,
    BuildContext context,
    ProductsShopDetailsModel productsResponseModel,
    int selectedCategoryId,
    ShopDetailsModel shopDetailsModel,
  }) : super(
            isLoading: isLoading,
            context: context,
            selectedCategoryId: selectedCategoryId,
            shopDetailsModel: shopDetailsModel,
            productsResponseModel: productsResponseModel);
}

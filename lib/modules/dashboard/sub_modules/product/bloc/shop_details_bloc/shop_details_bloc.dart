import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

import 'package:user/modules/dashboard/sub_modules/product/api/provider/products_provider.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_event.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_state.dart';

class ShopDetailsBloc extends BlocEventStateBase<ShopDetailsEvent, ShopDetailsState> {
  ShopDetailsBloc({bool initializing = true})
      : super(initialState: ShopDetailsState.initiating());

  @override
  Stream<ShopDetailsState> eventHandler(ShopDetailsEvent event,
      ShopDetailsState currentState) async* {
    //update products page UI
    if (event is UpdateShopDetailUIEvent) {
      yield ShopDetailsState.updateProducts(
        isLoading: false,
        context: event?.context,
        selectedCategoryId: event?.selectedCategoryId,
        shopDetailsModel: event?.shopDetailsModel,
        productsResponseModel: event?.productsResponseModel,
      );
    }

    //method to get products data based on shop id
    if (event is GetProductsOfShopEvent) {
      String _message = "";
      ProductsShopDetailsModel _productsResponseModel =
      new ProductsShopDetailsModel();
      yield ShopDetailsState.updateProducts(
        isLoading: true,
        context: event?.context,
        selectedCategoryId: event?.selectedCategoryId,
        shopDetailsModel: event?.shopDetailsModel,
        productsResponseModel: event?.productsResponseModel,
      );

      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance.getCurrentLocationModel();

      var result = await ProductPageProvider().getShopProducts(
        context: event.context,
        productsRequestModel: ProductDetailsRequestModel(
          lat: _currentLocation?.lat?.toString(),
          lng: _currentLocation?.lng?.toString(),
          categoryId: [],
          shopId: event?.shopDetailsModel?.id,

        ),
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _productsResponseModel = ProductsShopDetailsModel.fromMap(result);
        }
      }

      yield ShopDetailsState.updateProducts(
        isLoading: false,
        context: event?.context,
        selectedCategoryId: event?.selectedCategoryId,
        shopDetailsModel: event?.shopDetailsModel,
        productsResponseModel: _productsResponseModel,
      );
    }
  }
}

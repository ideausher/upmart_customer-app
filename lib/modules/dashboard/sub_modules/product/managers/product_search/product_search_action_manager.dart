import 'package:flutter/cupertino.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/category_shop_routes.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/product_search_bloc/product_search_bloc.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/product_search_bloc/product_search_event.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/product_search_bloc/product_search_state.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_bloc.dart';

class ProductSearchActionManager {
  BuildContext context;
  AuthBloc authBloc;
  AuthState authState;
  ProductSearchBloc productSearchBloc = new ProductSearchBloc();
  ProductSearchState productSearchState;

  // action to get Products
  actionToGetSearchedData({String text, int page, bool showAlert = false}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance.getConnectivityStatus(
        context, showNetworkDialog: showAlert).then((value) {
      productSearchBloc?.emitEvent(GetSearchDataEvent(
          page: page ?? productSearchState?.page,
          searchText: text ?? productSearchState?.searchText,
          productsResponseModel: productSearchState?.productsResponseModel,
          context: context,
          isLoading: true));
    });
  }

  // action to clear data
  actionToClearSearchData() {
    productSearchBloc?.emitEvent(UpdateProductSearcUIEvent(
        page: 1,
        searchText: "",
        context: context,
        isLoading: false));
  }

  // actionToNavigateTo shop detail

  actionToNavigateToDetailPage({ShopDetailsModel shopDetailsModel}) {
    NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
        context, CategoryShopRoutes.SHOPS_PRODUCTS_DETAIL,
        dataToBeSend: shopDetailsModel);
  }
}

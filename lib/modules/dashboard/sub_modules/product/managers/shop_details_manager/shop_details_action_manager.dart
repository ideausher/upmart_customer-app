import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/auth/auth_routes.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_bloc.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_event.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_state.dart';
import '../../../../../../routes.dart';

class ShopDetailsActionManager {
  BuildContext context;
  AuthBloc authBloc;
  AuthState authState;

  // bloc
  ShopDetailsBloc shopDetailsBloc = ShopDetailsBloc();
  ShopDetailsState shopDetailsState;

  // action to get products of shops
  actionToGetProductsOfShops({ShopDetailsModel shopDetailsModel}) {
    shopDetailsBloc?.emitEvent(GetProductsOfShopEvent(
        context: context,
        isLoading: true,
        shopDetailsModel:
            shopDetailsModel ?? shopDetailsState?.shopDetailsModel,
        selectedCategoryId: shopDetailsState?.selectedCategoryId,
        productsResponseModel: shopDetailsState?.productsResponseModel));
  }

  // action to update ui
  actionToUpdateUi() {
    shopDetailsBloc?.emitEvent(UpdateShopDetailUIEvent(
        context: context,
        isLoading: true,
        shopDetailsModel: shopDetailsState?.shopDetailsModel,
        productsResponseModel: shopDetailsState?.productsResponseModel,
        selectedCategoryId: shopDetailsState?.selectedCategoryId));
  }

  // actionToUpdateLatestDataInCart
  actionToUpdateLatestDataInCart({ShopDetailsModel shopDetailsModel}) {
    if (CartUtilsManager.cartUtilsInstance.shopItems?.isNotEmpty == true) {
      if (CartUtilsManager.cartUtilsInstance?.shopDetailsModel?.id !=
          shopDetailsModel?.id) {
        // show error
        return;
      }
    }
    CartUtilsManager.cartUtilsInstance.updateLatestDataInProducts(
        latestShopItems: shopDetailsState?.productsResponseModel?.data?.items);
  }

  // action to add Quanity in a cart
  actionToAddOrRemoveQuantityInACart(
      {ShopItem shopItem,
      BuildContext context,
      bool addOrRemove,
      ShopDetailsModel shopDetailsModel,
      ScaffoldState scaffoldState}) async {
    // check if cart empty or not
    if (CartUtilsManager.cartUtilsInstance.shopItems?.isNotEmpty == true) {
      if (CartUtilsManager.cartUtilsInstance?.shopDetailsModel?.id !=
          shopDetailsModel?.id) {
        // show error
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: AppLocalizations.of(context)
                .shopdetailspage
                .error
                .addProductofSameshop);
        return;
      }
    }

    bool _isAddressAdded =
        await CartUtilsManager.cartUtilsInstance.checkAddressIsAdded();

    if (_isAddressAdded == false) {
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context,
          scaffoldState: scaffoldState,
          message: AppLocalizations.of(context)
              .shopdetailspage
              .error
              .addAddressFirst);

      AuthResponseModel _responseModel =
          await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();

      AuthResponseModel _authResponseModel =
          AuthResponseModel.fromMap(_responseModel?.toMap());
      _authResponseModel?.popScreen = true;
      var _addressResult = await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(context, AddressRoutes.ADDRESS_LIST,
              dataToBeSend: _authResponseModel);
      return;
    }
    CartUtilsManager.cartUtilsInstance.addOrRemoveQuantity(
        shopItem: shopItem,
        add: addOrRemove,
        quantity: 1,
        shopDetailsModel: shopDetailsModel);
    actionToUpdateUi();
  }

  // action to navigate to cart ;
  actionToViewCart() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) async {
      var result = await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(context, Routes.CART_PAGE);
      actionToUpdateUi();
    });
  }

  //this alert is used to show message to a guest user
  void showGuestUserAlert({BuildContext context}) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: 'Guest User',
        positiveButton: AppLocalizations.of(context).common.text.ok,
        negativeButton: 'Cancel',
        titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        buttonTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        subTitle:
            'You are logged in as a guest user.To make any order please login with your mobile number. ',
        onNegativeButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        onPositiveButtonTab: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
              context, AuthRoutes.SEND_OTP_SCREEN,
              dataToBeSend: true);
        });
  }
}

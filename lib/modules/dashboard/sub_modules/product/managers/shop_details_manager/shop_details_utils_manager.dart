import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

class ShopDetailsUtilManager{

  static ShopDetailsUtilManager _shopDetailsUtilManagerManager = ShopDetailsUtilManager();

  static ShopDetailsUtilManager get shopDetailsUtilManagerManager => _shopDetailsUtilManagerManager;
  // used to get product of category id
  List<ShopItem> getProductsOfParticularCategory({CategoriesData categoriesData,ProductsShopDetailsModel productsShopDetailsModel}){

    List<ShopItem> _shopItems =    List<ShopItem>();

    productsShopDetailsModel?.data?.items?.forEach((element) {

      if(categoriesData?.id == element?.categoryId){
        _shopItems.add(element);
      }

    });
    return _shopItems;

  }
}
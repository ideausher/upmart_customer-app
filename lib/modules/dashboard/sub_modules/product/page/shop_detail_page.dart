import 'package:clippy_flutter/label.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/coupon_listing/enums/coupon_enums.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/managers/categories_shop_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/shop_details_bloc/shop_details_state.dart';
import 'package:user/modules/dashboard/sub_modules/product/managers/shop_details_manager/shop_details_action_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/managers/shop_details_manager/shop_details_utils_manager.dart';
import 'package:intl/intl.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';

class ShopDetailPage extends StatefulWidget {
  BuildContext context;

  ShopDetailPage({this.context}) {
    this.context = context;
  }

  @override
  _ShopDetailPageState createState() => _ShopDetailPageState();
}

class _ShopDetailPageState extends State<ShopDetailPage>
    implements PushReceived {
  ShopDetailsActionManager _actionManager = ShopDetailsActionManager();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ShopDetailsModel _shopDetailsModel;

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _actionManager?.context = widget.context;
    _shopDetailsModel = ModalRoute.of(widget.context).settings.arguments;
    _actionManager?.authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _actionManager?.actionToGetProductsOfShops(
        shopDetailsModel: _shopDetailsModel);
  }

  @override
  void dispose() {
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _actionManager?.shopDetailsBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _actionManager?.context = context;
    return BlocEventStateBuilder<AuthState>(
      bloc: _actionManager?.authBloc,
      builder: (BuildContext context, AuthState authState) {
        _actionManager?.context = context;
        if (_actionManager?.authState != authState) {
          _actionManager?.authState = authState;
        }
        return ModalProgressHUD(
            inAsyncCall: _actionManager?.authState?.isLoading ?? false,
            child: BlocEventStateBuilder<ShopDetailsState>(
              bloc: _actionManager?.shopDetailsBloc,
              builder:
                  (BuildContext context, ShopDetailsState shopDetailsState) {
                _actionManager?.context = context;
                if (_actionManager?.shopDetailsState != shopDetailsState) {
                  _actionManager?.shopDetailsState = shopDetailsState;
                  _actionManager?.actionToUpdateLatestDataInCart(
                      shopDetailsModel: _shopDetailsModel);
                }
                return ModalProgressHUD(
                    inAsyncCall: shopDetailsState?.isLoading ?? false,
                    child: Scaffold(
                      key: _scaffoldKey,
                      backgroundColor: Colors.white,
                      body: SafeArea(
                          child: (shopDetailsState?.productsResponseModel?.data
                                      ?.categoryServed?.isNotEmpty ==
                                  true)
                              ? Column(
                                  children: [
                                    Expanded(
                                        child: DefaultTabController(
                                      length: shopDetailsState
                                          ?.productsResponseModel
                                          ?.data
                                          ?.categoryServed
                                          ?.length,
                                      // This is the number of tabs.
                                      child: NestedScrollView(
                                        headerSliverBuilder:
                                            (BuildContext context,
                                                bool innerBoxIsScrolled) {
                                          // These are the slivers that show up in the "outer" scroll view.
                                          return <Widget>[
                                            SliverOverlapAbsorber(
                                              // This widget takes the overlapping behavior of the SliverAppBar,
                                              // and redirects it to the SliverOverlapInjector below. If it is
                                              // missing, then it is possible for the nested "inner" scroll view
                                              // below to end up under the SliverAppBar even when the inner
                                              // scroll view thinks it has not been scrolled.
                                              // This is not necessary if the "headerSliverBuilder" only builds
                                              // widgets that do not overlap the next sliver.
                                              handle: NestedScrollView
                                                  .sliverOverlapAbsorberHandleFor(
                                                      context),

                                              sliver: SliverSafeArea(
                                                top: true,
                                                bottom: true,
                                                sliver: _sliverAppBar(
                                                    context: context,
                                                    innerBoxIsScrolled:
                                                        innerBoxIsScrolled),
                                              ),
                                            ),
                                            SliverPersistentHeader(
                                              delegate: _SliverAppBarDelegate(
                                                TabBar(
                                                  isScrollable: true,
                                                  labelColor: COLOR_PRIMARY,
                                                  unselectedLabelColor:
                                                      COLOR_GREY_LIGHT,
                                                  labelStyle:
                                                      textStyleSize14TabGreenColor,
                                                  unselectedLabelStyle:
                                                      textStyleSize14TabGreyColor,
                                                  indicatorColor: COLOR_PRIMARY,
                                                  indicatorSize:
                                                      TabBarIndicatorSize.label,
                                                  // These are the widgets to put in each tab in the tab bar.
                                                  tabs: shopDetailsState
                                                      ?.productsResponseModel
                                                      ?.data
                                                      ?.categoryServed
                                                      .map((CategoriesData categoryData) => Tab(
                                                          text: toBeginningOfSentenceCase(categoryData
                                                                  ?.categoryName ??
                                                              AppLocalizations.of(
                                                                      context)
                                                                  .common
                                                                  .text
                                                                  .notAllowed)))
                                                      .toList(),
                                                ),
                                              ),
                                              pinned: true,
                                            ),
                                          ];
                                        },
                                        body: TabBarView(
                                          // These are the contents of the tab views, below the tabs.
                                          children: shopDetailsState
                                              ?.productsResponseModel
                                              ?.data
                                              ?.categoryServed
                                              .map((CategoriesData
                                                  categoryData) {
                                            return _productTabView(
                                                context: context,
                                                categoryData: categoryData);
                                          }).toList(),
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                        ),
                                      ),
                                    )),
                                    (CartUtilsManager?.cartUtilsInstance
                                                ?.shopItems?.isNotEmpty ==
                                            true)
                                        ? _showCartButton(context: context)
                                        : const SizedBox()
                                  ],
                                )
                              : _ifNoProductFoundInShop(context: context)),
                    ));
              },
            ));
      },
    );
  }

  // used to show product listing of particualr tab
  Widget _productTabView({CategoriesData categoryData, BuildContext context}) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Builder(
        // This Builder is needed to provide a BuildContext that is "inside"
        // the NestedScrollView, so that sliverOverlapAbsorberHandleFor() can
        // find the NestedScrollView.
        builder: (BuildContext context) {
          final currentTabIndex = DefaultTabController.of(context).index;
          CategoriesData _categoryModel = _actionManager?.shopDetailsState
              ?.productsResponseModel?.data?.categoryServed[currentTabIndex];

          print('Current Index: $currentTabIndex');

          List<ShopItem> _shopItem = ShopDetailsUtilManager
              .shopDetailsUtilManagerManager
              .getProductsOfParticularCategory(
                  categoriesData: _categoryModel,
                  productsShopDetailsModel:
                      _actionManager?.shopDetailsState?.productsResponseModel);

          return CustomScrollView(
            // The "controller" and "primary" members should be left
            // unset, so that the NestedScrollView can control this
            // inner scroll view.
            // If the "controller" property is set, then this scroll
            // view will not be associated with the NestedScrollView.
            // The PageStorageKey should be unique to this ScrollView;
            // it allows the list to remember its scroll position when
            // the tab view is not on the screen.
            key: PageStorageKey<String>(_categoryModel?.id?.toString()),
            physics: NeverScrollableScrollPhysics(),
            slivers: <Widget>[
              ((_shopItem.isNotEmpty == true))
                  ? SliverPadding(
                      padding: const EdgeInsets.all(8.0),
                      // In this example, the inner scroll view has
                      // fixed-height list items, hence the use of
                      // SliverFixedExtentList. However, one could use any
                      // sliver widget here, e.g. SliverList or SliverGrid.
                      sliver: SliverList(
                        // The items in this example are fixed to 48 pixels
                        // high. This matches the Material Design spec for
                        // ListTile widgets.
                        // itemExtent: MediaQuery?.of(context)?.size?.height,
                        delegate: SliverChildBuilderDelegate(
                          (
                            BuildContext context,
                            int index,
                          ) {
                            print('hii $index');
                            // This builder is called for each child.
                            // In this example, we just number each list item.
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: SIZE_10,
                                ),
                                Text(
                                  toBeginningOfSentenceCase(
                                      _categoryModel?.categoryName ?? "N/A"),
                                  style: AppConfig?.of(context)
                                      ?.themeData
                                      ?.primaryTextTheme
                                      ?.headline5,
                                ),
                                _showProductListView(
                                    categoryModel: _categoryModel,
                                    shopItems: _shopItem,
                                    context: context),
                                SizedBox(
                                  height: SIZE_10,
                                ),
                                SizedBox(height: SIZE_10),
                              ],
                            );
                          },
                          // The childCount of the SliverChildBuilderDelegate
                          // specifies how many children this inner list
                          // has. In this example, each tab has a list of
                          // exactly 30 items, but this is arbitrary.
                          childCount: 1,
                        ),
                      ),
                    )
                  : Container(
                      child: Text(AppLocalizations.of(context)
                          .shopdetailspage
                          .text
                          .noProducts),
                    ),
            ],
          );
        },
      ),
    );
  }

  // if no product found
  Widget _ifNoProductFoundInShop({BuildContext context}) {
    return Container(
      height: MediaQuery?.of(context)?.size?.height,
      child: Padding(
        padding: const EdgeInsets.all(SIZE_12),
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    NavigatorUtils.navigatorUtilsInstance
                        .navigatorPopScreen(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    size: SIZE_20,
                  ),
                ),
                (_actionManager?.authState?.authResponseModel != null)
                    ? NotificationReadCountWidget(
                        context: context,
                        rounded: true,
                      )
                    : SizedBox()
              ],
            ),
            Expanded(
                child: Align(
                    alignment: Alignment.center,
                    child: _actionManager?.shopDetailsState?.isLoading == true
                        ? const SizedBox()
                        : CommonImageWithTextWidget(
                            context: _actionManager?.context,
                            localImagePath: PRODUCT_LOGO,
                            imageHeight: SIZE_7,
                            imageWidth: SIZE_7,
                            textToShow: AppLocalizations.of(context)
                                .shopdetailspage
                                .text
                                .noProducts) /*Text(AppLocalizations.of(context)
                            .shopDetails
                            .text
                            .noProductAddedInShop)*/
                    ))
          ],
        ),
      ),
    );
  }

  // app bar
  Widget _sliverAppBar({BuildContext context, bool innerBoxIsScrolled}) {
    return SliverAppBar(
      leading: InkWell(
        onTap: () {
          NavigatorUtils.navigatorUtilsInstance?.navigatorPopScreen(context);
        },
        child: Container(
          margin: EdgeInsets.only(left: SIZE_12),
          height: 30,
          width: 30,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
          ),
          child: Icon(
            Icons.arrow_back,
            size: SIZE_18,
            color: Colors.black,
          ),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(top: SIZE_0, right: SIZE_10),
          child: (_actionManager?.authState?.authResponseModel != null)
              ? NotificationReadCountWidget(
                  context: context,
                  rounded: true,
                )
              : const SizedBox(),
        )
      ],
      backgroundColor: (_shopDetailsModel?.shopAvailability ==
              ShopAvailability.available?.value)
          ? Colors.white
          : COLOR_GREY_DEC,
      expandedHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: context, percentage: SIZE_45, ofWidth: false),
      floating: true,
      pinned: true,
      snap: false,
      primary: true,
      forceElevated: innerBoxIsScrolled,
      leadingWidth: 42,
      automaticallyImplyLeading: false,
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          background: ColorFiltered(
            colorFilter: ColorFilter.mode(
              (_shopDetailsModel?.shopAvailability ==
                      ShopAvailability.available?.value)
                  ? Colors.transparent
                  : COLOR_GREY_DEC,
              BlendMode.saturation,
            ),
            child: Wrap(
              children: [
                _getShopImage(context: context),
                Padding(
                  padding: const EdgeInsets.only(left: SIZE_12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: SIZE_5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _shopName(context: context),
                          _shopDetailsModel?.primaryCoupon?.code?.isNotEmpty ==
                                  true
                              ? Label(
                                  triangleHeight: 10.0,
                                  edge: Edge.LEFT,
                                  child: Container(
                                    color: COLOR_PRIMARY,
                                    width: 120.0,
                                    height: 22.0,
                                    child: Center(
                                        child: Text(
                                            (_shopDetailsModel
                                                        ?.primaryCoupon?.type ==
                                                    CouponType?.Fixed?.value)
                                                ? NumberFormatUtils
                                                    .numberFormatUtilsInstance
                                                    .formatPriceWithSymbol(
                                                        price: _shopDetailsModel
                                                            ?.primaryCoupon
                                                            ?.discount)
                                                : "${_shopDetailsModel?.primaryCoupon?.discount}% ${AppLocalizations.of(context).shopdetailspage.text.off}" ??
                                                    "",
                                            style:
                                                textStyleSize12WithWhiteColor)),
                                  ),
                                )
                              : const SizedBox()
                        ],
                      ),
                      SizedBox(height: SIZE_10),
                      _shopDesc(context: context),
                      SizedBox(height: SIZE_10),
                      _ratingAndDistance(context: context),
                      SizedBox(
                        height: SIZE_5,
                      ),
                      Divider(
                        color: COLOR_GREY,
                        thickness: SIZE_2,
                      )
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  // shop rating and distance
  Widget _ratingAndDistance({BuildContext context}) {
    return Row(
      children: <Widget>[
        Row(
          children: [
            Icon(
              Icons.star,
              color: COLOR_PRIMARY,
              size: SIZE_16,
            ),
            SizedBox(
              width: SIZE_5,
            ),
            Text(
              _shopDetailsModel?.rating?.toStringAsFixed(1) ?? "",
              style: textStyleSize14WithGreenColor,
            )
          ],
        ),
        SizedBox(
          width: SIZE_16,
        ),
        Row(
          children: [
            Image.asset(
              CLOCK_IMAGE,
              color: COLOR_LIGHT_GREY,
              height: SIZE_16,
              width: SIZE_16,
            ),
            SizedBox(
              width: SIZE_5,
            ),
            Text(
              CategoriesShopUtilsManager.homeUtilsInstance?.getShopDistanceTime(
                      expTime: _shopDetailsModel?.expTime) ??
                  "",
              style: textStyleSize14WithGreyColor,
            ),
          ],
        ),
        SizedBox(
          width: SIZE_16,
        ),
        Row(
          children: [
            Image.asset(
              CAR_IMAGE,
              color: COLOR_LIGHT_GREY,
              height: SIZE_16,
              width: SIZE_16,
            ),
            SizedBox(
              width: SIZE_5,
            ),
            Text(
              '${_shopDetailsModel?.distance?.toString()} ${AppLocalizations.of(context).common.text.km.toLowerCase()}' ??
                  "" +
                      "${AppLocalizations.of(context).common.text.km.toLowerCase()}",
              style: textStyleSize14WithGreyColor,
            )
          ],
        )
      ],
    );
  }

  // shop name
  Widget _shopName({BuildContext context}) {
    return Text(
        toBeginningOfSentenceCase(_shopDetailsModel?.name ??
            AppLocalizations.of(context).common.text.notAllowed),
        style: AppConfig.of(context).themeData.primaryTextTheme.headline6,
        maxLines: 1,
        overflow: TextOverflow.ellipsis);
  }

  // shop desc
  Widget _shopDesc({BuildContext context}) {
    return Text(
      toBeginningOfSentenceCase(_shopDetailsModel?.description ??
          AppLocalizations.of(context).common.text.notAllowed),
      style: textStyleSize12WithBLACK,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      textAlign: TextAlign.justify,
    );
  }

  // used to show product list view
  Widget _showProductListView(
      {CategoriesData categoryModel,
      List<ShopItem> shopItems,
      BuildContext context}) {
    return Container(
      child: ListView.separated(
          itemCount: shopItems?.length,
          separatorBuilder: (BuildContext context, int index) {
            return Divider(
              color: COLOR_BORDER_GREY,
            );
          },
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            ShopItem _item = shopItems[index];
            return Container(
              margin: EdgeInsets.only(left: SIZE_16, right: SIZE_16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 30,
                        child: (_item?.productImage?.fileName?.isNotEmpty == true)
                            ? ImageUtils.imageUtilsInstance
                                .showCacheNetworkImage(
                                    context: context,
                                    radius: SIZE_0,
                                    showProgressBarInPlaceHolder: true,
                                    shape: BoxShape.rectangle,
                                    url: _item.productImage?.fileName ?? "",
                                    height: CommonUtils?.commonUtilsInstance
                                        ?.getPercentageSize(
                                            context: context,
                                            percentage: SIZE_15,
                                            ofWidth: false),
                                    width: CommonUtils.commonUtilsInstance
                                        .getPercentageSize(
                                            context: context,
                                            percentage: SIZE_20,
                                            ofWidth: true))
                            : SvgPicture.asset(PRODUCT_LOGO,
                                height: CommonUtils?.commonUtilsInstance
                                    ?.getPercentageSize(
                                        context: context,
                                        percentage: SIZE_15,
                                        ofWidth: false),
                                width: CommonUtils.commonUtilsInstance
                                    .getPercentageSize(
                                        context: context,
                                        percentage: SIZE_20,
                                        ofWidth: true)),
                      ),
                      SizedBox(
                        width: SIZE_30,
                      ),
                      Expanded(
                        flex: 70,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: <Widget>[
                                Text(
                                    NumberFormatUtils.numberFormatUtilsInstance
                                        .formatPriceWithSymbol(
                                            price: _item?.discountedPrice),
                                    style: AppConfig.of(context)
                                        .themeData
                                        .primaryTextTheme
                                        .headline3),
                                Visibility(
                                  visible:
                                      _item?.discountedPrice != _item?.mrpPrice,
                                  child: SizedBox(
                                    width: SIZE_10,
                                  ),
                                ),
                                Visibility(
                                  visible:
                                      _item?.discountedPrice != _item?.mrpPrice,
                                  child: Text(
                                      NumberFormatUtils
                                          .numberFormatUtilsInstance
                                          .formatPriceWithSymbol(
                                              price: _item?.mrpPrice),
                                      style: TextStyle(
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: COLOR_GREY_LIGHT)),
                                )
                              ],
                            ),
                            SizedBox(
                              height: SIZE_5,
                            ),
                            Text(
                              toBeginningOfSentenceCase(
                                  _item?.productName ??
                                      AppLocalizations.of(context)
                                          .common
                                          .text
                                          .notAllowed),
                            ),
                            ((_item?.gstEnabled==1))? Padding(
                              padding: const EdgeInsets.only(top:SIZE_10),
                              child: Text(
                               '${AppLocalizations.of(_actionManager?.context).cartpage.text.gst}:${_item?.gstPrice}%',
                                style:textStyleSize12GREY,
                              ),
                            ):const SizedBox(),
                            SizedBox(
                              height: SIZE_5,
                            ),
                            Visibility(
                              visible: (_shopDetailsModel?.shopAvailability ==
                                  ShopAvailability?.available?.value),
                              child: _showIncrementDecrementWidget(
                                  context: context, shopItem: _item),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: SIZE_10,
                  ),
                  _showStockEmptyOrQuantityDecreased(
                      shopItem: _item, context: context),
                ],
              ),
            );
          }),
    );
  }

  // show increment and decrement widget
  Widget _showIncrementDecrementWidget(
      {BuildContext context, ShopItem shopItem}) {
    var _tupple = CartUtilsManager?.cartUtilsInstance
        ?.checkProductInACartOrNot(shopItem: shopItem);
    var _existInCart = _tupple?.item1;
    var _quantityAddedInCart = _tupple?.item2;
    var _canAddQuantity =
        CartUtilsManager?.cartUtilsInstance?.canAddQuantity(shopItem: shopItem);

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: _existInCart,
          child: InkWell(
            onTap: () {
              //this is for logged in user
              if (_actionManager?.authState?.authResponseModel != null) {
                _actionManager?.actionToAddOrRemoveQuantityInACart(
                    shopItem: shopItem,
                    context: context,
                    addOrRemove: false,
                    shopDetailsModel: _shopDetailsModel,
                    scaffoldState: _scaffoldKey?.currentState);
              } else
                _actionManager?.showGuestUserAlert(context: context);
              /* _actionManager?.actionToAddOrRemoveQuantityInACart(
                  shopItem: shopItem,
                  context: context,
                  addOrRemove: false,
                  shopDetailsModel: _shopDetailsModel,
                  scaffoldState: _scaffoldKey?.currentState);*/
            },
            child: Container(
              decoration: BoxDecoration(
                color: COLOR_GREY_DEC,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SIZE_5),
                    bottomLeft: Radius.circular(SIZE_5)),
              ),
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_4, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_7, ofWidth: true),
              child: Icon(
                Icons.remove,
                color: Colors.black,
                size: SIZE_16,
              ),
            ),
          ),
        ),
        Visibility(
          visible: _existInCart,
          child: Container(
            alignment: Alignment.center,
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, percentage: SIZE_4, ofWidth: false),
            width: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, percentage: SIZE_7, ofWidth: true),
            color: Colors.white,
            child: Text(_quantityAddedInCart?.toString() ?? "0",
                style: textStyleSize14WithGreyColor),
          ),
        ),
        Visibility(
          visible: shopItem?.stockQuantity != 0,
          child: InkWell(
            onTap: () {
              print(
                  "shopItem ${shopItem?.itemQuantity} ---- ${shopItem?.stockQuantity}  ${_canAddQuantity}");
              if (_canAddQuantity) if (_actionManager
                      ?.authState?.authResponseModel !=
                  null) {
                _actionManager?.actionToAddOrRemoveQuantityInACart(
                    shopItem: shopItem,
                    context: context,
                    addOrRemove: true,
                    shopDetailsModel: _shopDetailsModel,
                    scaffoldState: _scaffoldKey?.currentState);
              } else {
                _actionManager?.showGuestUserAlert(context: context);
              }
              /*       print(
                  "shopItem ${shopItem?.itemQuantity} ---- ${shopItem?.stockQuantity}  ${_canAddQuantity}");
              if (_canAddQuantity)
                _actionManager?.actionToAddOrRemoveQuantityInACart(
                    shopItem: shopItem,
                    context: context,
                    addOrRemove: true,
                    shopDetailsModel: _shopDetailsModel,
                    scaffoldState: _scaffoldKey?.currentState);*/
            },
            child: Container(
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_4, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_7, ofWidth: true),
              decoration: BoxDecoration(
                color: (_canAddQuantity ? Colors.black : COLOR_GREY_TEXT),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(SIZE_5),
                    bottomRight: Radius.circular(SIZE_5)),
              ),
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: SIZE_16,
              ),
            ),
          ),
        ),
      ],
    );
  }

//this method will show cart button when user selects any product
  Widget _showCartButton({BuildContext context}) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: InkWell(
        onTap: () async {
          _actionManager?.actionToViewCart();
        },
        child: Container(
          margin:
              EdgeInsets.only(left: SIZE_20, right: SIZE_20, bottom: SIZE_20),
          padding: EdgeInsets.only(left: SIZE_20, right: SIZE_20),
          alignment: Alignment.center,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: context, percentage: SIZE_7, ofWidth: false),
          width: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: context, percentage: SIZE_100, ofWidth: true),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [COLOR_DARK_PRIMARY, COLOR_PRIMARY],
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                children: [
                  Text(
                    NumberFormatUtils.numberFormatUtilsInstance
                        .formatPriceWithSymbol(
                            price: CartUtilsManager.cartUtilsInstance
                                .cartAmount()),
                    style: textStyleSize14WithWhiteColor,
                  ),
                  Text(
                    '  - ${CartUtilsManager.cartUtilsInstance?.shopItems?.length ?? 0} ${CartUtilsManager.cartUtilsInstance?.shopItems?.length > 1 ? AppLocalizations.of(context).shopdetailspage.text.items.toUpperCase() : AppLocalizations.of(context).shopdetailspage.text.shopItem.toUpperCase()} ',
                    style: textStyleSize14WithWhiteColor,
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    AppLocalizations.of(context)
                        .shopdetailspage
                        .button
                        .viewCart,
                    style: textStyleSize14WithWhiteColor,
                  ),
                  Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                    size: SIZE_20,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

//this method is used to show user if product quantity is empty or gets decreased
  Widget _showStockEmptyOrQuantityDecreased(
      {BuildContext context, ShopItem shopItem}) {
    var _tupple = CartUtilsManager?.cartUtilsInstance
        ?.checkProductInACartOrNot(shopItem: shopItem);
    var _existInCart = _tupple?.item1;
    var _quantityAddedInCart = _tupple?.item2;
    if (shopItem?.stockQuantity == 0) {
      return Container(
          child: Text(AppLocalizations.of(context).cartpage.error.outOfStock,
              style: textStyleSize12WithRed));
    } else if (_quantityAddedInCart != null &&
        _quantityAddedInCart > shopItem?.stockQuantity) {
      return Container(
          child: Text(
              '${AppLocalizations.of(context).cartpage.error.quantityDecreased(value: shopItem?.stockQuantity?.toString())}',
              style: textStyleSize12WithRed));
    } else {
      return SizedBox();
    }
  }


//this method is used to show shop image
  Widget _getShopImage({BuildContext context}) {
    List<String> _shopImages = CommonUtils.commonUtilsInstance.getShopImages(
        context: _actionManager?.context, shopDetailsModel: _shopDetailsModel);
    if (CommonUtils.commonUtilsInstance
                .getShopImages(
                    context: context, shopDetailsModel: _shopDetailsModel)
                ?.isNotEmpty ==
            true /*||
        _shopDetailsModel?.profileImage?.fileName?.isNotEmpty == true*/
        ) {
      return Container(
          alignment: Alignment.center,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _actionManager?.context,
              percentage: SIZE_30,
              ofWidth: false),
          child: Swiper(
            itemBuilder: (BuildContext context, int index) {
              String _imageToShow = _shopImages[index];
              return Container(
                margin: EdgeInsets.only(bottom: SIZE_30),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(SIZE_20)),
                  color: Colors.white,
                ),
                child: ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                    context: context,
                    showProgressBarInPlaceHolder: true,
                    shape: BoxShape.rectangle,
                    url: _imageToShow ?? "",
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context, percentage: SIZE_100, ofWidth: true)),
              );
            },
            autoplay: true,
            itemCount: _shopImages?.length,
            viewportFraction: SIZE_1,
            scale: 0.4,
            pagination: new SwiperPagination(
              alignment: Alignment.bottomCenter,
              builder: new DotSwiperPaginationBuilder(
                  color: COLOR_GREY_PICKER,
                  size: SIZE_6,
                  activeColor: COLOR_PRIMARY),
            ),
          ));
    } else {
      return Align(
        alignment: Alignment.center,
        child: SvgPicture.asset(SHOP_LOGO,
            fit: BoxFit.cover,
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, percentage: SIZE_20, ofWidth: false),
            width: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, percentage: SIZE_100, ofWidth: true)),
      );
    }
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _actionManager?.actionToGetProductsOfShops();
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}

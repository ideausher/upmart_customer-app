import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/bloc/product_search_bloc/product_search_state.dart';
import 'package:user/modules/dashboard/sub_modules/product/managers/product_search/product_search_action_manager.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import '../../../../../localizations.dart';
import 'package:intl/intl.dart';

class ProductSearchPage extends StatefulWidget {

  BuildContext context;
  ProductSearchPage({this.context}) {}

  @override
  _ProductSearchPageState createState() => _ProductSearchPageState();
}

class _ProductSearchPageState extends State<ProductSearchPage> {
  //Managers
  ProductSearchActionManager _actionManager = new ProductSearchActionManager();

  //class variables
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _searchController = new TextEditingController();
  int _page = 1;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _actionManager?.context = widget.context;
    _scrollController.addListener(_scrollListener);
    _searchController.text = ModalRoute.of(widget.context).settings.arguments;
    _actionManager?.authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _actionManager?.actionToGetSearchedData(
        page: _page, text: _searchController.text);
  }

  ///scroll listener
  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      _page = _page + 1;
      _actionManager?.actionToGetSearchedData(
          page: _page, text: _searchController?.text, showAlert: true);
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {}
  }

  @override
  void dispose() {
    super.dispose();
    _searchController.dispose();
    _scrollController.dispose();
    _actionManager?.productSearchBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _actionManager?.context = context;

    return BlocEventStateBuilder<AuthState>(
      bloc: _actionManager?.authBloc,
      builder: (BuildContext context, AuthState authState) {
        _actionManager?.context = context;
        if (_actionManager?.authState != authState) {
          _actionManager?.authState = authState;
        }
        return ModalProgressHUD(
            inAsyncCall: _actionManager?.authState?.isLoading ?? false,
            child: BlocEventStateBuilder<ProductSearchState>(
              bloc: _actionManager?.productSearchBloc,
              builder: (BuildContext context, ProductSearchState productsState) {
                _actionManager?.context = context;
                if (_actionManager?.productSearchState != productsState) {
                  _actionManager?.productSearchState = productsState;
                }
                return ModalProgressHUD(
                    inAsyncCall: productsState?.isLoading ?? false,
                    child: Scaffold(
                      appBar: _showAppBar(),
                      key: _scaffoldKey,
                      body: Padding(
                        padding: const EdgeInsets.only(
                            left: SIZE_10, right: SIZE_10, bottom: SIZE_8),
                        child: Stack(
                          alignment: Alignment.bottomCenter,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                _showShopSearch(),
                                SizedBox(
                                  height: SIZE_10,
                                ),
                                _searchResultTitle(),
                                SizedBox(
                                  height: SIZE_10,
                                ),
                                _showProductsListing(),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ));
              },
            ));
      },
    );

  }

  //textform field widget
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      FormFieldValidator<String> validator,
      int maxLines = 1,
      Widget suffixIcon,
      FocusNode currentFocusNode,
      FocusNode nextFocusNode,
      BuildContext context,
      bool enabled = true}) {
    return Padding(
      padding: EdgeInsets.only(left: SIZE_10),
      child: TextField(
        controller: controller,
        keyboardType: keyboardType,
        maxLines: maxLines,
        enabled: enabled,
        style: AppConfig.of(_actionManager?.context)
            .themeData
            .primaryTextTheme
            .subtitle2,
        focusNode: currentFocusNode,
        onSubmitted: (term) {
          _searchController?.text = term;
          if (term?.isNotEmpty == true) {
            _page = 1;
            _actionManager?.actionToGetSearchedData(
                page: _page, text: _searchController?.text, showAlert: true);
          }
        },
        decoration: new InputDecoration(
          hintText: hint,
          suffixIcon: suffixIcon,
          border: InputBorder.none,
          hintStyle: TextStyle(
            fontWeight: AppConfig.of(_actionManager?.context)
                .themeData
                .primaryTextTheme
                .subtitle2
                .fontWeight,
            fontSize: AppConfig.of(_actionManager?.context)
                .themeData
                .primaryTextTheme
                .subtitle2
                .fontSize,
          ),
        ),
      ),
    );
  }

  //this method will show search bar to search shops
  Widget _showShopSearch() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_10, right: SIZE_10),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _actionManager?.context, percentage: SIZE_7, ofWidth: false),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
        color: COLOR_GREY,
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: _textFieldForm(
                controller: _searchController,
                keyboardType: TextInputType.text,
                context: _actionManager?.context,
                hint: AppLocalizations.of(_actionManager?.context)
                    .productpage
                    .hint
                    .searchProducts),
          ),
          InkWell(
            onTap: () {
              _page = 1;
              _searchController?.clear();
              _actionManager?.actionToClearSearchData();
            },
            child: Icon(
              Icons.clear,
              size: SIZE_20,
            ),
          ),
        ],
      ),
    );
  }

  //method to show categories widget
  Widget _searchResultTitle() {
    return Text(
      '${AppLocalizations.of(_actionManager?.context).productpage.title.searchResultFor} ${_searchController?.text}',
      style: AppConfig.of(_actionManager?.context)
          .themeData
          .primaryTextTheme
          .headline6,
    );
  }

  //method to show shops listing
  Widget _showProductsListing() {
    return Expanded(
      child: (_actionManager?.productSearchState?.productsResponseModel?.data
                  ?.items?.isNotEmpty ==
              true)
          ? Padding(
              padding: const EdgeInsets.only(bottom: SIZE_30),
              child: GridView.count(
                controller: _scrollController,
                key: PageStorageKey("ProductListing"),
                crossAxisCount: 2,
                childAspectRatio: 0.8,
                children: List.generate(
                    _actionManager?.productSearchState?.productsResponseModel
                        ?.data?.items?.length, (index) {
                  ShopItem _item = _actionManager?.productSearchState
                      ?.productsResponseModel?.data?.items[index];
                  return InkWell(
                    onTap: () {
                      _actionManager?.actionToNavigateToDetailPage(
                          shopDetailsModel: _item?.shopDetails);
                    },
                    child: Container(
                      color: COLOR_GREY_PRODUCT,
                      margin: EdgeInsets.only(right: SIZE_10, bottom: SIZE_10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          _showImageAndDiscountPrice(_item),
                          Padding(
                            padding: const EdgeInsets.all(SIZE_5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                _showItemPrice(_item),
                                _showItemName(toBeginningOfSentenceCase(
                                    _item?.productName ?? AppLocalizations.of(context).common.text.notAllowed)),
                                _showProductDescription(
                                    toBeginningOfSentenceCase(
                                        _item?.productDescription ?? AppLocalizations.of(context).common.text.notAllowed)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
            )
          : (_actionManager?.productSearchState?.isLoading == false)
              ? Center(
                  child: CommonImageWithTextWidget(
                    context: _actionManager?.context,
                    localImagePath: PRODUCT_LOGO,
                    imageHeight: SIZE_20,
                    imageWidth: SIZE_20,
                    textToShow: AppLocalizations.of(context).productpage.text.noProductFound,
                  ),
                )
              : const SizedBox(),
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _actionManager?.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        popScreenOnTapOfLeadingIcon: true,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_16),
            child: (_actionManager?.authState?.authResponseModel != null)
                ? NotificationReadCountWidget(
              context: context,
            )
                : const SizedBox(),
          )
        ],
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_actionManager?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show image and discount price
  Widget _showImageAndDiscountPrice(ShopItem items) {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_10, top: SIZE_8),
      child: Stack(
        children: [
          Container(
              color: Colors.transparent,
              alignment: Alignment.center,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _actionManager?.context,
                  percentage: SIZE_10,
                  ofWidth: false),
              child: (items?.productImage?.fileName?.isNotEmpty == true)
                  ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                      context: context,
                      radius: SIZE_0,
                      showProgressBarInPlaceHolder: true,
                      shape: BoxShape.rectangle,
                      url: items?.productImage?.fileName ?? "",
                      height: CommonUtils?.commonUtilsInstance
                          ?.getPercentageSize(
                              context: _actionManager?.context,
                              percentage: SIZE_15,
                              ofWidth: false),
                      width: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _actionManager?.context,
                          percentage: SIZE_30,
                          ofWidth: true))
                  : SvgPicture.asset(
                      PRODUCT_LOGO,
                      width: CommonUtils?.commonUtilsInstance
                          ?.getPercentageSize(
                              context: _actionManager?.context,
                              percentage: SIZE_30,
                              ofWidth: false),
                      height: CommonUtils?.commonUtilsInstance
                          ?.getPercentageSize(
                              context: _actionManager?.context,
                              percentage: SIZE_15,
                              ofWidth: false),
                    )),
          Visibility(
            visible: items?.discountPercentage > 0,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(SIZE_5)),
                color: COLOR_PRIMARY,
              ),
              padding: EdgeInsets.only(
                  left: SIZE_5, right: SIZE_5, top: SIZE_3, bottom: SIZE_3),
              child: Text(
                '${items?.discountPercentage?.toString()}%',
                style: textStyleSize12WithWhiteColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  //this method will return item price
  Widget _showItemPrice(ShopItem item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Visibility(
              visible: item?.mrpPrice != item?.discountedPrice,
              child: Text(
                  NumberFormatUtils.numberFormatUtilsInstance
                      .formatPriceWithSymbol(price: item?.discountedPrice),
                  style: AppConfig.of(_actionManager?.context)
                      .themeData
                      .primaryTextTheme
                      .headline3),
            ),
            Visibility(
              visible: item?.gstEnabled == 1,
              child: Text('${AppLocalizations.of(_actionManager?.context).cartpage.text.gst}:${item?.gstPrice}%'
                  /*NumberFormatUtils.numberFormatUtilsInstance
                      .formatPriceWithSymbol(price: item?.discountedPrice)*/,
                  style: textStyleSize12GREY),
            ),
          ],
        ),
        Text(
            NumberFormatUtils.numberFormatUtilsInstance
                .formatPriceWithSymbol(price: item?.mrpPrice),
            style: TextStyle(
                decoration: TextDecoration.lineThrough,
                color: COLOR_GREY_LIGHT))
      ],
    );
  }

  //method to show item name
  Widget _showItemName(String itemName) {
    return Text(
      itemName ?? "",
      style: textStyleSize13WithBlackColor,
    );
  }

  //method to show product description
  Widget _showProductDescription(String productDescription) {
    return Text(
      productDescription,
      style: textStyleSize14GreyColorFont500,
      maxLines: 1,
    );
  }
}

enum SettingOption { MyOrders,MyAddresses, TermsAndPolicies, FAQ,HelpAndSupport,Favourites,InviteFriends, LogOut }

extension SettingOptionExtension on SettingOption {
  int get value {
    switch (this) {
      case SettingOption.MyOrders:
        return 0;
      case SettingOption.MyAddresses:
        return 1;
      case SettingOption.TermsAndPolicies:
        return 2;
      case SettingOption.FAQ:
        return 3;
      case SettingOption.HelpAndSupport:
        return 4;
        case SettingOption.Favourites:
        return 5;
      case SettingOption.InviteFriends:
        return 6;
      case SettingOption.LogOut:
        return 7;

      default:
        return null;
    }
  }
}

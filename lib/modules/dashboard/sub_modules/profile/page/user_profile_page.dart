import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/auth/auth_routes.dart';
import 'package:user/modules/auth/manager/auth_manager.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/launcher_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/sub_modules/cart/constants/image_constant.dart';
import 'package:user/modules/dashboard/sub_modules/profile/enums/setting_enums.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:user/modules/orders/order_routes.dart';
import '../../../../../routes.dart';

class UserProfilePage extends StatefulWidget {
  BuildContext context;
  AuthResponseModel authResponseModel;
  Function backCallBack;

  UserProfilePage(this.context, {this.authResponseModel, this.backCallBack});

  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AuthState _authState;
  AuthBloc _authBloc;
  AuthManager _authManager;

  //Build Context
  BuildContext _context;

  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _authManager = AuthManager();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      bottom: true,
      top: true,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: _showAppBar(),
        body: BlocEventStateBuilder<AuthState>(
          bloc: _authBloc,
          builder: (BuildContext context, AuthState authState) {
            _context = context;
            if (_authState != authState) {
              _authState = authState;

              _authManager.actionOnStateUpdate(
                  context: _context,
                  authBloc: _authBloc,
                  authState: _authState,
                  scaffoldState: _scaffoldKey.currentState);
            }
            return ModalProgressHUD(
                inAsyncCall: _authState?.isLoading ?? false,
                child: (_authState?.authResponseModel != null)
                    ? Column(
                        children: <Widget>[
                          _showDivider(),
                          _showUserProfileImage(),
                          _showUserName(),
                          _showUserPhoneNumber(),
                          _showEditProfileButton(),
                          SizedBox(
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: _context,
                                    percentage: SIZE_2,
                                    ofWidth: false),
                          ),
                          Expanded(
                            child: _showListView(),
                          )
                        ],
                      )
                    : const SizedBox());
          },
        ),
      ),
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        defaultLeadIconPressed: () {
          widget.backCallBack();
        },
        popScreenOnTapOfLeadingIcon: false,
        centerTitle: true,
        appBarTitle:
            AppLocalizations.of(context).profilepage.appbartitle.profile,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_16),
            child: NotificationReadCountWidget(
              context: _context,
            ),
          )
        ],
        defaultLeadingIcon: null,
        defaultLeadingIconColor: Colors.transparent,
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show listview
  Widget _showListView() {
    return ListView.builder(
        itemCount: TOTAL_MENU_COUNT,
        itemBuilder: (_context, index) {
          return Padding(
              padding: const EdgeInsets.only(
                  left: SIZE_14, right: SIZE_30, top: SIZE_6, bottom: SIZE_6),
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  _handleClick(index);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      _getIcons(index),
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _context,
                          percentage: SIZE_6,
                          ofWidth: false),
                      width: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _context, percentage: SIZE_6, ofWidth: true),
                    ),
                    SizedBox(
                      width: SIZE_10,
                    ),
                    Expanded(
                      child: ((index == TOTAL_MENU_COUNT - 1))
                          ? Text(
                              _getTitle(index),
                              style: textStyleSize16WithBlackColor,
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _getTitle(index),
                                  style: textStyleSize16WithBlackColor,
                                ),
                                Text(
                                  _getSubTitle(index),
                                  style: textStyleSize14WithGREY,
                                ),
                              ],
                            ),
                    ),
                    (index == TOTAL_MENU_COUNT - 1)
                        ? const SizedBox()
                        : SvgPicture.asset(
                            ARROW,
                            color: COLOR_PRIMARY,
                            height: SIZE_12,
                            width: SIZE_12,
                          )
                  ],
                ),
              ));
        });
  }

  //this method is used to return icons of setting list
  String _getIcons(int index) {
    if (index == SettingOption.MyOrders.value) {
      return ORDERS_ICON;
    } else if (index == SettingOption.MyAddresses.value) {
      return ADDRESS_ICON;
    } else if (index == SettingOption.TermsAndPolicies.value) {
      return POLICY_ICON;
    } else if (index == SettingOption.FAQ.value) {
      return CHAT_ICON;
    } else if (index == SettingOption.HelpAndSupport.value) {
      return USER_ICON;
    } else if (index == SettingOption.Favourites.value) {
      return FAVOURITE_ICON;
    } else if (index == SettingOption.InviteFriends.value) {
      return REFER_ICON;
    } else if (index == SettingOption.LogOut.value) {
      return LOGOUT_ICON;
    }
  }

  //this method is used to return title
  String _getTitle(int index) {
    if (index == SettingOption.MyOrders.value) {
      return AppLocalizations.of(_context).profilepage.text.myOrders;
    } else if (index == SettingOption.MyAddresses.value) {
      return AppLocalizations.of(_context).profilepage.text.myAddresses;
    } else if (index == SettingOption.FAQ.value) {
      return AppLocalizations.of(_context).profilepage.text.fAQs;
    } else if (index == SettingOption.TermsAndPolicies.value) {
      return AppLocalizations.of(_context).profilepage.text.termsAndPolicies;
    } else if (index == SettingOption.HelpAndSupport.value) {
      return AppLocalizations.of(_context).profilepage.text.helpAndSupport;
    } else if (index == SettingOption.Favourites.value) {
      return AppLocalizations.of(_context).favouritepage.text.appBarTitle;
    } else if (index == SettingOption.InviteFriends.value) {
      return AppLocalizations.of(_context).profilepage.text.inviteFriends;
    } else if (index == SettingOption.LogOut.value) {
      return AppLocalizations.of(_context).profilepage.text.logout;
    }
  }

  //this method is used to return title
  String _getSubTitle(int index) {
    if (index == SettingOption.MyOrders.value) {
      return AppLocalizations.of(context).profilepage.text.checkOrders;
    } else if (index == SettingOption.MyAddresses.value) {
      return AppLocalizations.of(context).profilepage.text.yourAddresses;
    } else if (index == SettingOption.FAQ.value) {
      return AppLocalizations.of(context).profilepage.text.termsAndPolicies;
    } else if (index == SettingOption.TermsAndPolicies.value) {
      return AppLocalizations.of(context).profilepage.text.thingsYoumayknow;
    } else if (index == SettingOption.HelpAndSupport.value) {
      return AppLocalizations.of(context).profilepage.text.helpAndSupport;
    } else if (index == SettingOption.Favourites.value) {
      return AppLocalizations.of(_context).profilepage.text.favouriteText;
    } else if (index == SettingOption.InviteFriends.value) {
      return AppLocalizations.of(_context).profilepage.text.inviteFriendsText;
    } else if (index == SettingOption.LogOut.value) {
      return "";
    }
  }

  //this method is used to return a user  profile image widget
  Widget _showUserProfileImage() {
    return InkWell(
      onTap: () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedName(_context, AuthRoutes.PROFILE_SCREEN_ROOT);
      },
      child: Container(
        margin: EdgeInsets.only(top: SIZE_10),
        height: SIZE_80,
        width: SIZE_80,
        child: (_authState?.authResponseModel?.userData?.profilePicture !=
                    null &&
                _authState?.authResponseModel?.userData?.profilePicture
                        ?.isNotEmpty ==
                    true)
            ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                context: _context,
                fit: BoxFit.contain,
                radius: 0.0,
                showProgressBarInPlaceHolder: true,
                placeHolderImage: DEFAULT_PROFILE_ICON,
                url: _authState?.authResponseModel?.userData?.profilePicture ??
                    "",
              )
            : CircleAvatar(
                backgroundColor: Colors.white,
                radius: SIZE_30,
                backgroundImage: AssetImage(DEFAULT_PROFILE_ICON)),
      ),
    );
  }

  //this method is used to show user name
  Widget _showUserName() {
    return InkWell(
      onTap: () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedName(_context, AuthRoutes.PROFILE_SCREEN_ROOT);
      },
      child: Text(_authState?.authResponseModel?.userData?.name ?? "",
          style: AppConfig.of(_context).themeData.primaryTextTheme.headline5),
    );
  }

  //this method will show the user mobile number
  Widget _showUserPhoneNumber() {
    return Text(' ${_authState?.authResponseModel?.userData?.email ?? ""}',
        style: textStyleSize14WithGREY);
  }

  //this method is used to handle list item clicks
  void _handleClick(int index) {
    if (index == SettingOption.MyOrders.value) {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedName(_context, OrderRoutes.MY_ORDERS);
    } else if (index == SettingOption.MyAddresses.value) {
      NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          _context, AddressRoutes.ADDRESS_LIST,
          dataToBeSend: _authState?.authResponseModel);
    } else if (index == SettingOption.FAQ.value) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPushedName(_context, Routes.FAQ_PAGE);
        }
      });
    } else if (index == SettingOption.TermsAndPolicies.value) {
      LauncherUtils.launcherUtilsInstance
          .launchInBrowser(url: AppConfig.of(context).termsUrl);
    } else if (index == SettingOption.HelpAndSupport.value) {
      NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
        context,
        Routes.QUERY_LISTING_PAGE,
      );
    } else if (index == SettingOption.Favourites.value) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPushedName(_context, Routes.FAV_SHOP_LIST);
        }
      });
    } else if (index == SettingOption.InviteFriends.value) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPushedName(_context, Routes.REFERRAL_PAGE);
        }
      });
    } else if (index == SettingOption.LogOut.value) {
      return _authManager?.signOutDialog(
          context: _context,
          authState: _authState,
          scaffoldState: _scaffoldKey.currentState,
          authBloc: _authBloc);
    }
  }

  //method to show horizontal divider
  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1,
    );
  }

  //method used to show edit profile button
  Widget _showEditProfileButton() {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedName(_context, AuthRoutes.PROFILE_SCREEN_ROOT);
      },
      child: Container(
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, percentage: SIZE_20, ofWidth: false),
        margin: EdgeInsets.only(top: SIZE_12),
        padding: EdgeInsets.all(SIZE_10),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_PRIMARY, width: SIZE_1_5),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.edit,
              size: SIZE_16,
              color: COLOR_PRIMARY,
            ),
            SizedBox(
              width: SIZE_2,
            ),
            Text(
              AppLocalizations.of(context).profilepage.button.editProfile,
              style:
                  AppConfig.of(_context).themeData.primaryTextTheme.headline2,
            ),
          ],
        ),
      ),
    );
  }
}

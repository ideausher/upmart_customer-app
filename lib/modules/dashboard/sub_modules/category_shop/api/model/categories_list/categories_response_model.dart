// To parse this JSON data, do
//
//     final categoriesResponseModel = categoriesResponseModelFromJson(jsonString);

import 'dart:convert';

CategoriesResponseModel categoriesResponseModelFromJson(String str) => CategoriesResponseModel.fromMap(json.decode(str));

String categoriesResponseModelToJson(CategoriesResponseModel data) => json.encode(data.toJson());

class CategoriesResponseModel {
  CategoriesResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<CategoriesData> data;
  String message;
  int statusCode;

  factory CategoriesResponseModel.fromMap(Map<String, dynamic> json) => CategoriesResponseModel(
    data: json["data"] == null ? null : List<CategoriesData>.from(json["data"].map((x) => CategoriesData.fromMap(x))),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class CategoriesData {
  CategoriesData({
    this.id,
    this.categoryName,
    this.categoryDescription,
    this.image,
  });

  int id;
  String categoryName;
  String categoryDescription;
  String image;

  factory CategoriesData.fromMap(Map<String, dynamic> json) => CategoriesData(
    id: json["id"] == null ? null : json["id"],
    categoryName: json["category_name"] == null ? null : json["category_name"],
    categoryDescription: json["category_description"] == null ? null : json["category_description"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "category_name": categoryName == null ? null : categoryName,
    "category_description": categoryDescription == null ? null : categoryDescription,
    "image": image == null ? null : image,
  };
}

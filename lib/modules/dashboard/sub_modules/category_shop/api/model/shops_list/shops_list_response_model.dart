// To parse this JSON data, do
//
//     final shopsListResponseModel = shopsListResponseModelFromJson(jsonString);

import 'dart:convert';

import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';


ShopsListResponseModel shopsListResponseModelFromJson(String str) => ShopsListResponseModel.fromJson(json.decode(str));

String shopsListResponseModelToJson(ShopsListResponseModel data) => json.encode(data.toJson());

class ShopsListResponseModel {
  ShopsListResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<ShopDetailsModel> data;
  String message;
  int statusCode;

  factory ShopsListResponseModel.fromJson(Map<String, dynamic> json) => ShopsListResponseModel(
    data: json["data"] == null ? null : List<ShopDetailsModel>.from(json["data"].map((x) => ShopDetailsModel.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class ShopDetailsModel {
  ShopDetailsModel({
    this.id,
    this.name,
    this.showOwner,
    this.description,
    this.profileImage,
    this.address,
    this.additionalAddress,
    this.latitude,
    this.longitude,
    this.rating,
    this.distance,
    this.expTime,
    this.shopType,
    this.media,
    this.primaryCoupon,
    this.shopAvailability,
    this.isFav,
    this.banners,
    this.hst,
    this.pst,
    this.gst
  });

  int id;
  String name;
  ShopOwner showOwner;
  String description;
  ProfileImageModel profileImage;
  String address;
  String additionalAddress;
  String latitude;
  String longitude;
  num rating;
  num distance;
  String expTime;
  int shopType;
  List<Media> media;
  List<Banners>banners;
  CouponData primaryCoupon;
  num shopAvailability;
  int isFav;
  num gst;
  num pst;
  num hst;

  factory ShopDetailsModel.fromJson(Map<String, dynamic> json) => ShopDetailsModel(
    id: json["id"] == null ? null : json["id"],
    isFav: json["isFavourite"] == null ? null : json["isFavourite"],
    name: json["name"] == null ? null : json["name"],
    showOwner: json["show_owner"] == null ? null : ShopOwner.fromJson(json["show_owner"]),
    description: json["description"] == null ? null : json["description"],
    profileImage: json["profile_image"] == null ? null : ProfileImageModel.fromMap(json["profile_image"]),
    address: json["address"] == null ? null : json["address"],
    additionalAddress: json["additional_address"] == null ? null : json["additional_address"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    rating: json["rating"] == null ? null : json["rating"],
    distance: json["distance"] == null ? null : json["distance"].toDouble(),
    expTime: json["exp_time"] == null ? null : json["exp_time"],
    shopType: json["shop_type"] == null ? null : json["shop_type"],
    banners: json["banners"] == null ? null : List<Banners>.from(json["banners"].map((x) => Banners.fromJson(x))),
    media: json["media"] == null ? null : List<Media>.from(json["media"].map((x) => Media.fromJson(x))),
    primaryCoupon: json["primary_coupon"] == null ? null : CouponData.fromJson(json["primary_coupon"]),
    shopAvailability: json["availability"] == null ? null : json["availability"],
    gst: json["gst"] == null ? null : json["gst"],
    pst: json["pst"] == null ? null : json["pst"],
    hst: json["hst"] == null ? null : json["hst"],

  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "isFavourite": isFav == null ? null : isFav,
    "name": name == null ? null : name,
    "show_owner": showOwner == null ? null : showOwner.toJson(),
    "description": description == null ? null : description,
    "profile_image": profileImage == null ? null : profileImage.toMap(),
    "address": address == null ? null : address,
    "additional_address": additionalAddress == null ? null : additionalAddress,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "rating": rating == null ? null: rating,
    "distance": distance == null ? null : distance,
    "exp_time": expTime == null ? null : expTime,
    "shop_type": shopType == null ? null : shopType,
    "media": media == null ? null : List<dynamic>.from(media.map((x) => x.toJson())),
    "banners": banners == null ? null : List<dynamic>.from(banners.map((x) => x.toJson())),
    "primary_coupon": primaryCoupon == null ? null : primaryCoupon.toJson(),
    "availability": shopAvailability == null ? null : shopAvailability,
    "gst": gst == null ? null : gst,
    "pst": pst == null ? null : pst,
    "hst": hst == null ? null : hst,

  };
}

class Banners {
  Banners({
    this.id,
    this.merchantId,
    this.bannerName,
    this.link,
  });

  int id;
  int merchantId;
  String bannerName;
  String link;

  factory Banners.fromJson(Map<String, dynamic> json) => Banners(
    id: json["id"] == null ? null : json["id"],
    merchantId: json["merchant_id"] == null ? null : json["merchant_id"],
    bannerName: json["bannerName"] == null ? null : json["bannerName"],
    link: json["link"] == null ? null : json["link"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "merchant_id": merchantId == null ? null : merchantId,
    "bannerName": bannerName == null ? null : bannerName,
    "link": link == null ? null : link,
  };
}

class Media {
  Media({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory Media.fromJson(Map<String, dynamic> json) => Media(
    id: json["id"] == null ? null : json["id"],
    modelId: json["model_id"] == null ? null : json["model_id"],
    modelType: json["model_type"] == null ? null : json["model_type"],
    fileName: json["file_name"] == null ? null : json["file_name"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "model_id": modelId == null ? null : modelId,
    "model_type": modelType == null ? null : modelType,
    "file_name": fileName == null ? null : fileName,
    "name": name == null ? null : name,
  };
}



class ShopOwner {
  ShopOwner({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
  });

  int id;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;

  factory ShopOwner.fromJson(Map<String, dynamic> json) => ShopOwner(
    id: json["id"] == null ? null : json["id"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    email: json["email"] == null ? null : json["email"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "email": email == null ? null : email,
    "phone_number": phoneNumber == null ? null : phoneNumber,
  };
}


ProfileImageModel profileImageModelFromMap(String str) => ProfileImageModel.fromMap(json.decode(str));

String profileImageModelToMap(ProfileImageModel data) => json.encode(data.toMap());

class ProfileImageModel {
  ProfileImageModel({
    this.id,
    this.modelId,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String fileName;
  String name;

  factory ProfileImageModel.fromMap(Map<String, dynamic> json) => ProfileImageModel(
    id: json["id"] == null ? null : json["id"],
    modelId: json["model_id"] == null ? null : json["model_id"],
    fileName: json["file_name"] == null ? null : json["file_name"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "model_id": modelId == null ? null : modelId,
    "file_name": fileName == null ? null : fileName,
    "name": name == null ? null : name,
  };
}


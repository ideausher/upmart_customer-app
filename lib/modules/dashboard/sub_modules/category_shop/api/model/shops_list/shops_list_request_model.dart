// To parse this JSON data, do
//
//     final shopsListRequestModel = shopsListRequestModelFromJson(jsonString);

/*import 'dart:convert';

ShopsListRequestModel shopsListRequestModelFromJson(String str) => ShopsListRequestModel.fromJson(json.decode(str));

String shopsListRequestModelToJson(ShopsListRequestModel data) => json.encode(data.toJson());

class ShopsListRequestModel {
  ShopsListRequestModel({this.addressId, this.categoryId, this.page, this.searchText});

  int addressId;
  List<int> categoryId;
  int page;
  String searchText;

  factory ShopsListRequestModel.fromJson(Map<String, dynamic> json) => ShopsListRequestModel(
        addressId: json["address_id"] == null ? null : json["address_id"],
        categoryId: json["category_id"] == null ? null : json["category_id"],
        page: json["page"] == null ? null : json["page"],
        searchText: json["searchText"] == null ? null : json["searchText"],
      );

  Map<String, dynamic> toJson() => {
        "address_id": addressId == null ? null : addressId,
        "category_id": categoryId == null ? null : categoryId,
        "page": page == null ? null : page,
        "searchText": searchText == null ? null : searchText,
      };
}*/


// To parse this JSON data, do
//
//     final shopListRequestModel = shopListRequestModelFromJson(jsonString);

// To parse this JSON data, do
//
//     final shopsListRequestModel = shopsListRequestModelFromJson(jsonString);

import 'dart:convert';

ShopsListRequestModel shopsListRequestModelFromJson(String str) => ShopsListRequestModel.fromJson(json.decode(str));

String shopsListRequestModelToJson(ShopsListRequestModel data) => json.encode(data.toJson());

class ShopsListRequestModel {
  ShopsListRequestModel({
    this.categoryIds,
    this.searchshopname,
    this.lat,
    this.lng,
    this.shopType,
    this.limit,
    this.page,
  });

  List<int> categoryIds;
  String searchshopname;
  String lat;
  String lng;
  int shopType;
  int limit;
  int page;

  factory ShopsListRequestModel.fromJson(Map<String, dynamic> json) => ShopsListRequestModel(
    categoryIds: json["category_ids"] == null ? null : List<int>.from(json["category_ids"].map((x) => x)),
    searchshopname: json["searchshopname"] == null ? null : json["searchshopname"],
    lat: json["lat"] == null ? null : json["lat"],
    lng: json["lng"] == null ? null : json["lng"],
    shopType: json["shop_type"] == null ? null : json["shop_type"],
    limit: json["limit"] == null ? null : json["limit"],
    page: json["page"] == null ? null : json["page"],
  );

  Map<String, dynamic> toJson() => {
    "category_ids": categoryIds == null ? null : List<dynamic>.from(categoryIds.map((x) => x)),
    "searchshopname": searchshopname == null ? null : searchshopname,
    "lat": lat == null ? null : lat,
    "lng": lng == null ? null : lng,
    "shop_type": shopType == null ? null : shopType,
    "limit": limit == null ? null : limit,
    "page": page == null ? null : page,
  };
}

// To parse this JSON data, do
//
//     final bannersRequestModel = bannersRequestModelFromJson(jsonString);

import 'dart:convert';

BannersRequestModel bannersRequestModelFromJson(String str) => BannersRequestModel.fromJson(json.decode(str));

String bannersRequestModelToJson(BannersRequestModel data) => json.encode(data.toJson());

class BannersRequestModel {
  BannersRequestModel({
    this.limit,
    this.page,
    this.latitude,
    this.longitude,
  });

  int limit;
  int page;
  String latitude;
  String longitude;

  factory BannersRequestModel.fromJson(Map<String, dynamic> json) => BannersRequestModel(
    limit: json["limit"] == null ? null : json["limit"],
    page: json["page"] == null ? null : json["page"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
  );

  Map<String, dynamic> toJson() => {
    "limit": limit == null ? null : limit,
    "page": page == null ? null : page,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
  };
}

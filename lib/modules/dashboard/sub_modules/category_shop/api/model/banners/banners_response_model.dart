// To parse this JSON data, do
//
//     final bannersResponseModel = bannersResponseModelFromJson(jsonString);

import 'dart:convert';

BannersResponseModel bannersResponseModelFromJson(String str) => BannersResponseModel.fromMap(json.decode(str));

String bannersResponseModelToJson(BannersResponseModel data) => json.encode(data.toJson());

class BannersResponseModel {
  BannersResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<Datum> data;
  String message;
  int statusCode;

  factory BannersResponseModel.fromMap(Map<String, dynamic> json) => BannersResponseModel(
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Datum {
  Datum({
    this.id,
    this.bannerName,
    this.address,
    this.latitude,
    this.longitude,
    this.distance,
    this.bannerImage,
  });

  int id;
  String bannerName;
  String address;
  String latitude;
  String longitude;
  double distance;
  BannerImage bannerImage;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    bannerName: json["banner_name"] == null ? null : json["banner_name"],
    address: json["address"] == null ? null : json["address"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    distance: json["distance"] == null ? null : json["distance"].toDouble(),
    bannerImage: json["banner_image"] == null ? null : BannerImage.fromJson(json["banner_image"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "banner_name": bannerName == null ? null : bannerName,
    "address": address == null ? null : address,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "distance": distance == null ? null : distance,
    "banner_image": bannerImage == null ? null : bannerImage.toJson(),
  };
}

class BannerImage {
  BannerImage({
    this.id,
    this.modelId,
    this.modelType,
    this.fileName,
    this.name,
  });

  int id;
  int modelId;
  String modelType;
  String fileName;
  String name;

  factory BannerImage.fromJson(Map<String, dynamic> json) => BannerImage(
    id: json["id"] == null ? null : json["id"],
    modelId: json["model_id"] == null ? null : json["model_id"],
    modelType: json["model_type"] == null ? null : json["model_type"],
    fileName: json["file_name"] == null ? null : json["file_name"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "model_id": modelId == null ? null : modelId,
    "model_type": modelType == null ? null : modelType,
    "file_name": fileName == null ? null : fileName,
    "name": name == null ? null : name,
  };
}

import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/banners/banners_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_request_model.dart';

class CategoryShopProvider {
  Future<dynamic> getCategoriesListing(
      {BuildContext context, String addressId}) async {
    var confirmOtp = "getcategory";

    var result = await AppConfig.of(context).baseApi.getRequest(
          confirmOtp,
          context,
        );

    return result;
  }

  Future<dynamic> getBanners(
      {BuildContext context, BannersRequestModel bannersRequestModel}) async {
    var confirmOtp = "banners";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(confirmOtp, context, queryParameters: {
      "limit": "10",
      "shop_type": 1,
      "latitude": bannersRequestModel?.latitude,
      "longitude": bannersRequestModel?.longitude,
      "page": 1,
    });

    return result;
  }

  Future<dynamic> getShopsListing(
      {BuildContext context,
      ShopsListRequestModel shopsListRequestModel}) async {
    print('the shop type is ${shopsListRequestModel?.shopType}');
    print('the category id is ${shopsListRequestModel?.categoryIds?.length}');
    var getShops = "getshop";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(getShops, context, queryParameters: {
      "category_ids": shopsListRequestModel?.categoryIds ?? [],
      "searchText": shopsListRequestModel?.searchshopname,
      "shop_type": shopsListRequestModel?.shopType,
      "lng": shopsListRequestModel?.lng,
      "lat": shopsListRequestModel?.lat,
      "page": shopsListRequestModel?.page,
      "limit": shopsListRequestModel?.limit
    });

    return result;
  }

  Future<dynamic> callFavUnFavApi(
      {BuildContext context, int isFav, int shopId}) async {
    var makeShopFavUnFav = "favourites/shops";

    var result = await AppConfig.of(context).baseApi.postRequest(
        makeShopFavUnFav, context,
        data: {"shop_id": shopId, "isFavourite": (isFav == 0) ? 1 : 0});
    print('Ths req is $isFav');

    return result;
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/dashboard/sub_modules/product/page/product_search_page.dart';
import 'package:user/modules/dashboard/sub_modules/product/page/shop_detail_page.dart';


class CategoryShopRoutes {
  static const String SHOPS_PRODUCTS_DETAIL = '/SHOPS_PRODUCTS_DETAIL';
  static const String PRODUCTS_SEARCh_PAGE = '/PRODUCTS_SEARCh_PAGE';

  static Map<String, WidgetBuilder> routes() {
    return {
      SHOPS_PRODUCTS_DETAIL: (context) => ShopDetailPage(context: context),
      PRODUCTS_SEARCh_PAGE: (context) => ProductSearchPage(context: context),
    };
  }
}


import 'package:flutter/material.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/banners/banners_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';

class CategoriesShopState extends BlocState {
  CategoriesShopState({
    this.isLoading: false,
    this.context,
    this.currentLocation,
    this.shopTypeEnum,
    this.scheduleDateTime,
    this.selectedCategoryList,
    this.shopsListResponseModel,
    this.bannersResponseModel,
    this.page,
    this.categoriesResponseModel,
  }) : super(isLoading);

  final bool isLoading;
  final BuildContext context;

  final CurrentLocation currentLocation;
  final ShopType shopTypeEnum;
  final DateTime scheduleDateTime;
  final List<int> selectedCategoryList;
  final ShopsListResponseModel shopsListResponseModel;
  final BannersResponseModel bannersResponseModel;
  final CategoriesResponseModel categoriesResponseModel;
  final int page;

  // used for update home page state
  factory CategoriesShopState.updateHome({
    bool isLoading,
    BuildContext context,
    CurrentLocation currentLocation,
    ShopType shopTypeEnum,
    DateTime scheduleDateTime,
    List<int> selectedCategoryList,
    ShopsListResponseModel shopsListResponseModel,
    BannersResponseModel bannersResponseModel,
    CategoriesResponseModel categoriesResponseModel,
    int page,
  }) {
    return CategoriesShopState(
      isLoading: isLoading,
      context: context,
      currentLocation: currentLocation,
      shopTypeEnum: shopTypeEnum,
      scheduleDateTime: scheduleDateTime,
      selectedCategoryList: selectedCategoryList,
      shopsListResponseModel: shopsListResponseModel,
      bannersResponseModel: bannersResponseModel,
      categoriesResponseModel: categoriesResponseModel,
      page: page,
    );
  }

  factory CategoriesShopState.initiating() {
    return CategoriesShopState(
      isLoading: false,
    );
  }
}

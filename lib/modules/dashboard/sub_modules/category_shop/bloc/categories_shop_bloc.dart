import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/common_response_model.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/banners/banners_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/banners/banners_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/provider/category_shop_provider.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_event.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_state.dart';

class CategoriesShopBloc
    extends BlocEventStateBase<CategoriesShopEvent, CategoriesShopState> {
  CategoriesShopBloc({bool initializing = true})
      : super(initialState: CategoriesShopState.initiating());

  @override
  Stream<CategoriesShopState> eventHandler(
      CategoriesShopEvent event, CategoriesShopState currentState) async* {
    // update home page UI
    if (event is UpdateHomePageUIEvent) {
      yield CategoriesShopState.updateHome(
          isLoading: false,
          context: event?.context,
          page: event?.page,

          currentLocation: event?.currentLocation,
          bannersResponseModel: event?.bannersResponseModel,
          categoriesResponseModel: event?.categoriesResponseModel,
          scheduleDateTime: event?.scheduleDateTime,
          selectedCategoryList: event?.selectedCategoryList,
          shopsListResponseModel: event?.shopsListResponseModel,
          shopTypeEnum: event?.shopTypeEnum);
    }

    //to get shops listing based on user current location or user's primary added location.
    if (event is GetShopsListingEvent) {
      yield CategoriesShopState.updateHome(
          currentLocation: event.currentLocation,
          isLoading: true,
          context: event?.context,
          page: event?.page,
          bannersResponseModel: event?.bannersResponseModel,
          categoriesResponseModel: event?.categoriesResponseModel,
          scheduleDateTime: event?.scheduleDateTime,
          selectedCategoryList: event?.selectedCategoryList,
          shopsListResponseModel: event?.shopsListResponseModel,
          shopTypeEnum: event?.shopTypeEnum);

      ShopsListResponseModel _shopListResponseModel =
          event?.shopsListResponseModel;

      var shopListResult = await CategoryShopProvider().getShopsListing(
        context: event.context,
        shopsListRequestModel: ShopsListRequestModel(
            limit: 10,
            page: event.page,
            categoryIds: event.selectedCategoryList,
            lat: event.currentLocation?.lat?.toString(),
            lng: event.currentLocation?.lng?.toString(),
            shopType: event.shopTypeEnum.value),
      );

      int _page = event.page;
      if (shopListResult != null) {
        // check result status
        if (shopListResult[ApiStatusParams.Status.value] != null &&
            shopListResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          var _shopListModel = ShopsListResponseModel.fromJson(shopListResult);

          if (_shopListModel?.data?.isNotEmpty == true) {
            if (_page > 1) {
              _shopListResponseModel?.data?.addAll(_shopListModel?.data);
            } else {
              _shopListResponseModel = _shopListModel;
            }
          } else {
            _page = _page - 1;
          }
        } else {
          if (_page > 1) {
            _page = _page - 1;
          }
        }
      } else {
        if (_page > 1) {
          _page = _page - 1;
        }
      }

      yield CategoriesShopState.updateHome(
          isLoading: false,
          context: event?.context,
          currentLocation: event.currentLocation,
          page: _page,
          shopTypeEnum: event.shopTypeEnum,
          shopsListResponseModel: _shopListResponseModel,
          selectedCategoryList: event.selectedCategoryList,
          scheduleDateTime: event.scheduleDateTime,
          categoriesResponseModel: event.categoriesResponseModel,
          bannersResponseModel: event.bannersResponseModel);
    }

    //event to get banners, categories and shop on home page
    if (event is GetBannersCategoryAndShopListingEvent) {
      BannersResponseModel _bannersResponseModel = new BannersResponseModel();
      // used to show loader
      yield CategoriesShopState.updateHome(
          currentLocation: event.currentLocation,
          isLoading: true,
          context: event?.context,
          page: event?.page,
          bannersResponseModel: event?.bannersResponseModel,
          categoriesResponseModel: event?.categoriesResponseModel,
          scheduleDateTime: event?.scheduleDateTime,
          selectedCategoryList: event?.selectedCategoryList,
          shopsListResponseModel: event?.shopsListResponseModel,
          shopTypeEnum: event?.shopTypeEnum);

      // used to get current location and update ui
      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();
      yield CategoriesShopState.updateHome(
          currentLocation: _currentLocation,
          isLoading: true,
          context: event?.context,
          page: event?.page,
          bannersResponseModel: event?.bannersResponseModel,
          categoriesResponseModel: event?.categoriesResponseModel,
          scheduleDateTime: event?.scheduleDateTime,
          selectedCategoryList: event?.selectedCategoryList,
          shopsListResponseModel: event?.shopsListResponseModel,
          shopTypeEnum: event?.shopTypeEnum);

      // used to get banners
      var result = await CategoryShopProvider().getBanners(
          context: event.context,
          bannersRequestModel: BannersRequestModel(
              page: 1,
              limit: 200,
              latitude: _currentLocation?.lat?.toString(),
              longitude: _currentLocation?.lng?.toString()));

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _bannersResponseModel = BannersResponseModel.fromMap(result);
        }
      }

      ///calling categories api
      CategoriesResponseModel _categoriesResponseModel =
          new CategoriesResponseModel();

      // used to get category listing
      var categoryResult = await CategoryShopProvider().getCategoriesListing(
        context: event.context,
      );

      if (categoryResult != null) {
        // check result status
        if (categoryResult[ApiStatusParams.Status.value] != null &&
            categoryResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          _categoriesResponseModel =
              CategoriesResponseModel.fromMap(categoryResult);
        }
      }

      ShopsListResponseModel _shopListResponseModel =
          event?.shopsListResponseModel;

      var shopListResult = await CategoryShopProvider().getShopsListing(
        context: event.context,
        shopsListRequestModel: ShopsListRequestModel(
            limit: 10,
            page: event.page,
            categoryIds: event.selectedCategoryList,
            lat: _currentLocation?.lat?.toString(),
            lng: _currentLocation?.lng?.toString(),
            shopType: event.shopTypeEnum.value),
      );

      int _page = event.page;
      if (shopListResult != null) {
        // check result status
        if (shopListResult[ApiStatusParams.Status.value] != null &&
            shopListResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          var _shopListModel = ShopsListResponseModel.fromJson(shopListResult);

          if (_shopListModel?.data?.isNotEmpty == true) {
            if (_page > 1) {
              _shopListResponseModel?.data?.addAll(_shopListModel?.data);
            } else {
              _shopListResponseModel = _shopListModel;
            }
          } else {
            _page = _page - 1;
          }
        } else {
          if (_page > 1) {
            _page = _page - 1;
          }
        }
      } else {
        if (_page > 1) {
          _page = _page - 1;
        }
      }

      yield CategoriesShopState.updateHome(
          isLoading: false,
          context: event?.context,
          currentLocation: _currentLocation,
          page: _page,
          shopTypeEnum: event.shopTypeEnum,
          shopsListResponseModel: _shopListResponseModel,
          selectedCategoryList: event.selectedCategoryList,
          scheduleDateTime: event.scheduleDateTime,
          categoriesResponseModel: _categoriesResponseModel,
          bannersResponseModel: _bannersResponseModel);
    }

    if (event is MakeShopFavUnFavEvent) {
      String _message = "";
      ShopsListResponseModel _shopListResponseModel =
          event?.shopsListResponseModel;
      yield CategoriesShopState.updateHome(
          isLoading: true,
          context: event?.context,
          page: event?.page,
          currentLocation: event?.currentLocation,
          bannersResponseModel: event?.bannersResponseModel,
          categoriesResponseModel: event?.categoriesResponseModel,
          scheduleDateTime: event?.scheduleDateTime,
          selectedCategoryList: event?.selectedCategoryList,
          shopsListResponseModel: event?.shopsListResponseModel,
          shopTypeEnum: event?.shopTypeEnum);

      var result = await CategoryShopProvider().callFavUnFavApi(
          context: event.context, shopId: event.shopId, isFav: event?.isFav);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          ShopDetailsModel _shopListModel = ShopDetailsModel.fromJson(result["data"]);
          for (int index = 0;
              index < _shopListResponseModel?.data?.length;
              index++) {
            ShopDetailsModel shopDetailsModel =
                _shopListResponseModel?.data[index];

            if (shopDetailsModel?.id == event?.shopId) {
              shopDetailsModel?.isFav=_shopListModel?.isFav;
              event?.shopsListResponseModel?.data[index] = shopDetailsModel;
            }
          }

          yield CategoriesShopState.updateHome(
              isLoading: false,
              context: event?.context,
              page: event?.page,
              currentLocation: event?.currentLocation,
              bannersResponseModel: event?.bannersResponseModel,
              categoriesResponseModel: event?.categoriesResponseModel,
              scheduleDateTime: event?.scheduleDateTime,
              selectedCategoryList: event?.selectedCategoryList,
              shopsListResponseModel: event?.shopsListResponseModel,
              shopTypeEnum: event?.shopTypeEnum);
        }
      }
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_bloc.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_event.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_state.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/category_shop_routes.dart';

class CategoriesShopActionManager {
  // context
  BuildContext context;

  // bloc
  CategoriesShopBloc categoriesShopBloc = CategoriesShopBloc();
  CategoriesShopState categoriesShopState;

  AuthBloc authBloc;
  AuthState authState;

  //call shops list data
  callGetShopsListing(
      {ShopType shopTypeEnum,
      CategoriesData categoryData,
      ScaffoldState scaffoldState,
      int page}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        List<int> _selectedCategoryData =
            categoriesShopState?.selectedCategoryList ?? List<int>();

        // selected category case
        if (categoryData != null) {
          if (_selectedCategoryData?.contains(categoryData?.id) == true) {
            _selectedCategoryData?.remove(categoryData?.id);
          } else {
            _selectedCategoryData?.add(categoryData?.id);
          }
        }

        if (page != null) {
          page = (page != null) ? page + 1 : 1;
        }
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        categoriesShopBloc?.emitEvent(GetShopsListingEvent(
            context: context,
            isLoading: true,
            page: page ?? 1,
            shopTypeEnum: shopTypeEnum ?? categoriesShopState?.shopTypeEnum,
            bannersResponseModel: categoriesShopState?.bannersResponseModel,
            categoriesResponseModel:
                categoriesShopState?.categoriesResponseModel,
            scheduleDateTime: categoriesShopState?.scheduleDateTime,
            selectedCategoryList: _selectedCategoryData,
            currentLocation: categoriesShopState?.currentLocation));
      }
    });
  }

  //call shops list data
  OnShopTypeChange(
      {ShopType shopTypeEnum,
      CategoriesData categoryData,
      ScaffoldState scaffoldState,
      int page}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        CartUtilsManager.cartUtilsInstance.scheduleDateTime = null;
        List<int> _selectedCategoryData =
            categoriesShopState?.selectedCategoryList ?? List<int>();

        // selected category case
        if (categoryData != null) {
          if (_selectedCategoryData?.contains(categoryData?.id) == true) {
            _selectedCategoryData?.remove(categoryData?.id);
          } else {
            _selectedCategoryData?.add(categoryData?.id);
          }
        }

        if (page != null) {
          page = (page != null) ? page + 1 : 1;
        }
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        categoriesShopBloc?.emitEvent(GetShopsListingEvent(
            context: context,
            isLoading: true,
            page: page ?? 1,
            shopTypeEnum: shopTypeEnum ?? categoriesShopState?.shopTypeEnum,
            bannersResponseModel: categoriesShopState?.bannersResponseModel,
            categoriesResponseModel:
                categoriesShopState?.categoriesResponseModel,
            scheduleDateTime: categoriesShopState?.scheduleDateTime,
            selectedCategoryList: _selectedCategoryData,
            currentLocation: categoriesShopState?.currentLocation));
      }
    });
  }

  //get banners,categories and shops data on home opage
  callGetBannersCategoriesAndShopsEvent({
    CurrentLocation currentLocation,
    int page,
  }) {
    categoriesShopBloc?.emitEvent(GetBannersCategoryAndShopListingEvent(
        context: context,
        isLoading: true,
        currentLocation: currentLocation,
        shopTypeEnum:
            CartUtilsManager.cartUtilsInstance.shopType ?? ShopType.delivery,
        page: page,
        scheduleDateTime: CartUtilsManager.cartUtilsInstance.scheduleDateTime));
  }

  //make shop fav/unfav api calling
  callFavUnFavShopApiEvent({int shopId, int isFav}) {
    categoriesShopBloc?.emitEvent(MakeShopFavUnFavEvent(
        context: context,
        isLoading: true,
        page: categoriesShopState?.page,
        shopsListResponseModel: categoriesShopState?.shopsListResponseModel,
        shopTypeEnum: categoriesShopState?.shopTypeEnum,
        bannersResponseModel: categoriesShopState?.bannersResponseModel,
        categoriesResponseModel: categoriesShopState?.categoriesResponseModel,
        selectedCategoryList: categoriesShopState?.selectedCategoryList,
        currentLocation: categoriesShopState?.currentLocation,
        isFav: isFav,
        shopId: shopId));
  }

  // action to update ui
  actionToUpdateUI({DateTime scheduledDateTime}) {
    // hide keyboard
    CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
    categoriesShopBloc?.emitEvent(UpdateHomePageUIEvent(
        context: context,
        isLoading: true,
        page: categoriesShopState?.page,
        shopsListResponseModel: categoriesShopState?.shopsListResponseModel,
        shopTypeEnum: categoriesShopState?.shopTypeEnum,
        bannersResponseModel: categoriesShopState?.bannersResponseModel,
        categoriesResponseModel: categoriesShopState?.categoriesResponseModel,
        scheduleDateTime: scheduledDateTime,
        selectedCategoryList: categoriesShopState?.selectedCategoryList,
        currentLocation: categoriesShopState?.currentLocation));
  }

  // action to open detail page
  actionToOpenDetailPage({ShopDetailsModel shopDetails}) async {
    NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
        context, CategoryShopRoutes.SHOPS_PRODUCTS_DETAIL,
        dataToBeSend: shopDetails);
  }

  // action to navigate on product search
  actionToSearchData({String searchText}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) {
      NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          context, CategoryShopRoutes.PRODUCTS_SEARCh_PAGE,
          dataToBeSend: searchText);
    });
  }
}

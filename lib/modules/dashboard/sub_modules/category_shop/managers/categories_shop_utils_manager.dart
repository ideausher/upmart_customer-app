import 'package:user/modules/address/enum/address_enum.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/utils/shared_prefs_utils.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/banners/banners_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_state.dart';
import 'package:user/modules/slider_image_video/enums/slider_image_video_enum.dart';
import 'package:user/modules/slider_image_video/model/slider_model.dart';

class CategoriesShopUtilsManager {
  // main state
  static CategoriesShopUtilsManager _homeUtilsManager = CategoriesShopUtilsManager();

  static CategoriesShopUtilsManager get homeUtilsInstance => _homeUtilsManager;

  //method to set Images to the image List
  SliderImageVideoModel getImagesList(
      BannersResponseModel bannersResponseModel) {
    List<SliderModel> slider = List();
    if (bannersResponseModel?.data?.isNotEmpty == true) {

      bannersResponseModel?.data?.forEach((element) {
        SliderModel sliderModel = new SliderModel();
        sliderModel.mediaType = MediaType.Image;
        sliderModel.url =element?.bannerImage?.fileName;

        slider.add(sliderModel);
      });

    }
    return SliderImageVideoModel(
        autoPlay: false, comingFromHome: true, sliderModelList: slider);
  }


  //here showing time take to cover the distance at an average speed on 40Km/hr
  String getShopDistanceTime({String expTime}) {
    var arr = expTime?.split(":");
    if (arr[0] == "00") {
      if (arr[1] == "00") {
        return "${arr[2]} sec";
      } else {
        return "${arr[1]} min ${arr[2]} sec";
      }
    } else {
      return '${arr[0]} hr ${arr[1]} min ${arr[2]} sec ';
    }
  }
}

import 'package:clippy_flutter/label.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:intl/intl.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/coupon_listing/enums/coupon_enums.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/categories_list/categories_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/bloc/categories_shop_state.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/constants/images_constants.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/managers/categories_shop_action_managers.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/managers/categories_shop_utils_manager.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:user/modules/slider_image_video/model/slider_model.dart';

class CategoriesShopPage extends StatefulWidget {
  BuildContext context;
  CurrentLocation currentLocation;

  CategoriesShopPage(this.context, {this.currentLocation}) {
    if (currentLocation == null) {
      currentLocation = ModalRoute.of(context).settings.arguments;
    }
  }

  @override
  _CategoriesShopPageState createState() => _CategoriesShopPageState();
}

class _CategoriesShopPageState extends State<CategoriesShopPage>
    implements PushReceived {
  //Managers
  CategoriesShopActionManager _actionManager = CategoriesShopActionManager();

  CurrentLocation _currentLocation;
  num _page = 1;
  String _scheduledDateTime;

  //initialise controllers
  ScrollController _scrollController = ScrollController();
  TextEditingController _searchController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  SliderImageVideoModel _sliderImageVideoModel;

  /// Controller to scroll or jump to a particular item.
  final ItemScrollController itemScrollController = ItemScrollController();

  /// Listener that reports the position of items when the list is scrolled.
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();

  int min;
  int max;

  @override
  void initState() {
    super.initState();

    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _actionManager?.context = widget.context;
    _currentLocation = widget.currentLocation;

    _scrollController.addListener(_scrollListener);
    _actionManager?.authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _actionManager?.callGetBannersCategoriesAndShopsEvent(
        currentLocation: _currentLocation, page: 1);
  }

  ///scroll listener
  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange &&
        (_actionManager
                ?.categoriesShopState?.shopsListResponseModel?.data?.length ==
            (_page * 10))) {
      _actionManager?.callGetShopsListing(
          scaffoldState: _scaffoldKey?.currentState, page: _page);
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {}
  }

  @override
  void dispose() {
    super.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _searchController.dispose();
    _actionManager?.categoriesShopBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _actionManager?.context = context;
    return BlocEventStateBuilder<AuthState>(
      bloc: _actionManager?.authBloc,
      builder: (BuildContext context, AuthState authState) {
        _actionManager?.context = context;
        if (_actionManager?.authState != authState) {
          _actionManager?.authState = authState;
        }
        return ModalProgressHUD(
            inAsyncCall: _actionManager?.authState?.isLoading ?? false,
            child: BlocEventStateBuilder<CategoriesShopState>(
              bloc: _actionManager?.categoriesShopBloc,
              builder: (BuildContext context, CategoriesShopState homeState) {
                _actionManager?.context = context;

                //_setUserLocationData(_authState?.authResponseModel?.userData?.address, authState);
                if (_actionManager?.categoriesShopState != homeState) {
                  _actionManager?.categoriesShopState = homeState;
                  // allocate current location
                  if (homeState?.currentLocation != null)
                    _currentLocation = homeState?.currentLocation;
                  // allocate page
                  if (homeState?.page != null) _page = homeState?.page;
                  // manage banners
                  _sliderImageVideoModel = CategoriesShopUtilsManager
                      .homeUtilsInstance
                      .getImagesList(homeState?.bannersResponseModel);
                  // allocate shop type
                  CartUtilsManager.cartUtilsInstance.shopType =
                      homeState?.shopTypeEnum ?? ShopType.delivery;
                  // allocate schedule time
                  CartUtilsManager.cartUtilsInstance.scheduleDateTime =
                      homeState?.scheduleDateTime;
                }
                return ModalProgressHUD(
                    inAsyncCall: homeState?.isLoading ?? false,
                    child: Scaffold(
                      backgroundColor: Colors.white,
                      key: _scaffoldKey,
                      body: SafeArea(
                        child: SingleChildScrollView(
                          child: Container(
                            margin: EdgeInsets.only(
                                left: SIZE_14,
                                right: SIZE_14,
                                top: SIZE_10,
                                bottom: SIZE_10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                _showUserLocation(),
                                _showSpace(SIZE_16),
                                _showProductSearch(),
                                _showSpace(SIZE_16),
                                _showBannerView(),
                                _showRadioButtonView(),
                                //_showScheduling(),
                                _showSpace(SIZE_16),
                                _showCategoriesTitle(),
                                _showCategoriesList(),
                                _showShopsTitle(),
                                _showShopsListing()
                              ],
                            ),
                          ),
                        ),
                      ),
                    ));
              },
            ));
      },
    );
  }

  //this method will show location search widget and notification widget
  Widget _showUserLocation() {
    return Padding(
      padding: const EdgeInsets.only(
          top: SIZE_10, bottom: SIZE_10, left: SIZE_2, right: SIZE_2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: SIZE_5),
            child: SvgPicture.asset(
              HOME_ICON,
              height: SIZE_16,
              width: SIZE_16,
              color: COLOR_BLACK,
            ),
          ),
          SizedBox(
            width: SIZE_5,
          ),
          Expanded(
            flex: 3,
            child: InkWell(
              onTap: () async {
                AuthResponseModel _authResponseModel =
                    AuthResponseModel.fromMap(
                        _actionManager?.authState?.authResponseModel?.toMap());
                _authResponseModel?.popScreen = true;
                var _addressResult = await NavigatorUtils.navigatorUtilsInstance
                    .navigatorPushedNameResult(
                        context, AddressRoutes.ADDRESS_LIST,
                        dataToBeSend: _authResponseModel);
                _currentLocation = _addressResult;
                _actionManager?.callGetBannersCategoriesAndShopsEvent(
                    currentLocation: _currentLocation, page: 1);
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(_actionManager?.context)
                        .categoriesshoppage
                        .text
                        .home,
                    overflow: TextOverflow.ellipsis,
                    style: AppConfig.of(_actionManager?.context)
                        .themeData
                        .primaryTextTheme
                        .headline6,
                  ),
                  Text(
                    _currentLocation?.currentAddress ??
                        AppLocalizations.of(context).common.text.notAllowed,
                    overflow: TextOverflow.ellipsis,
                    style: textStyleSize12WithGrey,
                  )
                ],
              ),
            ),
          ),
          (_actionManager?.authState?.authResponseModel != null)
              ? NotificationReadCountWidget(
                  context: context,
                )
              : SizedBox()
        ],
      ),
    );
  }

  //textform  field widget
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      FormFieldValidator<String> validator,
      int maxLines = 1,
      Widget suffixIcon,
      FocusNode currentFocusNode,
      FocusNode nextFocusNode,
      BuildContext context,
      bool enabled = true}) {
    return TextField(
      controller: controller,
      keyboardType: keyboardType,
      maxLines: maxLines,
      enabled: enabled,
      style: textStyleSize12WithGrey,
      focusNode: currentFocusNode,
      onSubmitted: (term) {
        if (_searchController.text.isNotEmpty) {
          _actionManager?.actionToSearchData(
              searchText: _searchController.text);
          _searchController.clear();
        }
      },
      decoration: new InputDecoration(
        hintText: hint,
        suffixIcon: suffixIcon,
        border: InputBorder.none,
        hintStyle: textStyleSize12WithGrey,
      ),
    );
  }

  //this method will show search bar to search products
  Widget _showProductSearch() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_10, right: SIZE_10),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _actionManager?.context, percentage: SIZE_7, ofWidth: false),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
        color: COLOR_GREY,
      ),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: SIZE_10,
          ),
          Padding(
            padding: const EdgeInsets.only(top: SIZE_2),
            child: SvgPicture.asset(
              SEARCH_SHOP_ICON,
              color: COLOR_BLACK,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _actionManager?.context,
                  percentage: SIZE_2_2,
                  ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _actionManager?.context,
                  percentage: SIZE_3,
                  ofWidth: true),
            ),
          ),
          SizedBox(
            width: SIZE_10,
          ),
          Expanded(
            flex: 1,
            child: _textFieldForm(
                controller: _searchController,
                keyboardType: TextInputType.text,
                context: _actionManager?.context,
                hint: AppLocalizations.of(_actionManager?.context)
                    .categoriesshoppage
                    .hint
                    .searchProduct),
          ),
        ],
      ),
    );
  }

  //method to show categories widget
  Widget _showScheduling() {
    _scheduledDateTime = _actionManager
                ?.categoriesShopState?.scheduleDateTime !=
            null
        ? '${AppLocalizations?.of(_actionManager?.context)?.categoriesshoppage?.text?.schedulingAt} ${DateUtils.dateUtilsInstance.getExpectedDateTime(dateTime: _actionManager?.categoriesShopState?.scheduleDateTime)}'
        : AppLocalizations?.of(widget?.context)
            ?.categoriesshoppage
            ?.text
            ?.noScheduling;

    return InkWell(
      onTap: () {
        if (_actionManager?.categoriesShopState?.scheduleDateTime != null) {
          _showScheduleDateTimePicker();
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            _scheduledDateTime,
            style: textStyleSize14BlackColor,
          ),
          Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              value:
                  _actionManager?.categoriesShopState?.scheduleDateTime != null,
              activeColor: COLOR_PRIMARY,
              onChanged: (bool value) {
                if (value == true) {
                  _showScheduleDateTimePicker();
                } else {
                  _actionManager?.actionToUpdateUI();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  ///this method will return categories list
  Widget _showCategoriesList() {
    return (_actionManager?.categoriesShopState?.categoriesResponseModel?.data
                ?.isNotEmpty ==
            true)
        ? Stack(
            alignment: Alignment.center,
            children: [
              Container(
                  height: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _actionManager?.context,
                      percentage: SIZE_17,
                      ofWidth: false),
                  margin: EdgeInsets.only(top: SIZE_10),
                  width: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _actionManager?.context,
                      percentage: SIZE_100,
                      ofWidth: true),
                  child: ScrollablePositionedList.builder(
                      itemScrollController: itemScrollController,
                      scrollDirection: Axis.horizontal,
                      itemPositionsListener: itemPositionsListener,
                      itemCount: _actionManager?.categoriesShopState
                          ?.categoriesResponseModel?.data?.length,
                      itemBuilder: (
                        BuildContext context,
                        int index,
                      ) {
                        CategoriesData _categoriesData = _actionManager
                            ?.categoriesShopState
                            ?.categoriesResponseModel
                            ?.data[index];
                        return InkWell(
                          onTap: () {
                            _page = 1;
                            _actionManager?.callGetShopsListing(
                                scaffoldState: _scaffoldKey?.currentState,
                                categoryData: _categoriesData);
                          },
                          child: Column(
                            children: [
                              positionsView,
                              Wrap(
                                alignment: WrapAlignment.center,
                                runAlignment: WrapAlignment.center,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                direction: Axis.vertical,
                                spacing: SIZE_8,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.all(SIZE_5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(SIZE_10)),
                                        border: Border.all(
                                            color: _getBorderColor(
                                                _categoriesData),
                                            width: SIZE_1_5)),
                                    alignment: Alignment.center,
                                    width: CommonUtils.commonUtilsInstance
                                        .getPercentageSize(
                                            context: _actionManager?.context,
                                            percentage: SIZE_24,
                                            ofWidth: true),
                                    height: CommonUtils.commonUtilsInstance
                                        .getPercentageSize(
                                            context: _actionManager?.context,
                                            percentage: SIZE_12,
                                            ofWidth: false),
                                    child: (_categoriesData
                                                ?.image?.isNotEmpty ==
                                            true)
                                        ? ImageUtils.imageUtilsInstance
                                            .showCacheNetworkImage(
                                                context: context,
                                                showProgressBarInPlaceHolder:
                                                    true,
                                                radius: SIZE_0,
                                                shape: BoxShape.circle,
                                                url: _categoriesData?.image ??
                                                    "",
                                                height: CommonUtils
                                                    .commonUtilsInstance
                                                    .getPercentageSize(
                                                        context: _actionManager
                                                            ?.context,
                                                        percentage: SIZE_7,
                                                        ofWidth: false),
                                                width: CommonUtils
                                                    .commonUtilsInstance
                                                    .getPercentageSize(
                                                        context: _actionManager
                                                            ?.context,
                                                        percentage: SIZE_10,
                                                        ofWidth: false))
                                        : SvgPicture.asset(
                                            SHOP_LOGO,
                                            height: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                    context:
                                                        _actionManager?.context,
                                                    percentage: SIZE_5,
                                                    ofWidth: false),
                                            width: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                    context:
                                                        _actionManager?.context,
                                                    percentage: SIZE_5,
                                                    ofWidth: true),
                                          ),
                                  )
                                ],
                              ),
                              Text(
                                _categoriesData?.categoryName ?? "",
                                style: textStyleSize14WithGreyColor,
                              )
                            ],
                          ),
                        );
                      })),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      //here scrolling the list in reverse order
                      if (min > 0) {
                        scrollTo(min - 1);
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.all(SIZE_7),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: COLOR_PRIMARY,
                          boxShadow: [
                            BoxShadow(
                              color: COLOR_PRIMARY,
                              blurRadius: SIZE_8,
                            ),
                          ]),
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                        size: SIZE_12,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      //scroll list in forward order
                      if (min <=
                          _actionManager
                              ?.categoriesShopState
                              ?.categoriesResponseModel
                              ?.data
                              ?.length) scrollTo(min + 2);
                    },
                    child: Container(
                      padding: EdgeInsets.all(SIZE_7),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: COLOR_PRIMARY,
                          boxShadow: [
                            BoxShadow(
                              color: COLOR_PRIMARY,
                              blurRadius: SIZE_8,
                            ),
                          ]),
                      child: Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: Colors.white,
                        size: SIZE_12,
                      ),
                    ),
                  )
                ],
              )
            ],
          )
        : const SizedBox();
  }

  //method to show shops listing
  Widget _showShopsListing() {
    return (_actionManager?.categoriesShopState?.shopsListResponseModel?.data
                ?.isNotEmpty ==
            true)
        ? Container(
            height: CommonUtils?.commonUtilsInstance?.getPercentageSize(
                context: _actionManager?.context,
                percentage: SIZE_40,
                ofWidth: false),
            child: ListView.builder(
                itemCount: _actionManager
                    ?.categoriesShopState?.shopsListResponseModel?.data?.length,
                shrinkWrap: true,
                controller: _scrollController,
                key: PageStorageKey("ShopListing"),
                padding: EdgeInsets.only(top: SIZE_5),
                itemBuilder: (BuildContext context, int index) {
                  ShopDetailsModel _shopData = _actionManager
                      ?.categoriesShopState
                      ?.shopsListResponseModel
                      ?.data[index];

                  List<String> _shopImages = CommonUtils.commonUtilsInstance
                      .getShopImages(
                          context: _actionManager?.context,
                          shopDetailsModel: _shopData);
                  return InkWell(
                    onTap: () {
                      NetworkConnectionUtils.networkConnectionUtilsInstance
                          .getConnectivityStatus(context,
                              showNetworkDialog: true)
                          .then((onValue) {
                        if (onValue) {
                          _actionManager?.actionToOpenDetailPage(
                              shopDetails: _shopData);
                        }
                      });
                    },
                    child: Card(
                      margin: EdgeInsets.only(bottom: SIZE_10, top: SIZE_10),
                      clipBehavior: Clip.antiAlias,
                      elevation: ELEVATION_03,
                      color: (_shopData?.shopAvailability ==
                              ShopAvailability.available.value)
                          ? Colors.white
                          : COLOR_GREY_DEC,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(SIZE_10)),
                          side: BorderSide(
                              width: SIZE_1, color: COLOR_BORDER_GREY)),
                      child: ColorFiltered(
                        colorFilter: ColorFilter.mode(
                          (_shopData?.shopAvailability ==
                                  ShopAvailability.available.value)
                              ? Colors.transparent
                              : COLOR_GREY_DEC,
                          BlendMode.saturation,
                        ),
                        child: Column(
                          children: <Widget>[
                            (CommonUtils.commonUtilsInstance
                                        .getShopImages(
                                            context: _actionManager?.context,
                                            shopDetailsModel: _shopData)
                                        ?.isNotEmpty ==
                                    true)
                                ? Container(
                                    alignment: Alignment.center,
                                    height: CommonUtils.commonUtilsInstance
                                        .getPercentageSize(
                                            context: _actionManager?.context,
                                            percentage: SIZE_30,
                                            ofWidth: false),
                                    child: Swiper(
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        String _imageToShow =
                                            _shopImages[index];
                                        return Container(
                                          margin:
                                              EdgeInsets.only(bottom: SIZE_30),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(SIZE_20)),
                                            color: COLOR_LIGHT_WHITE,
                                          ),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(SIZE_10),
                                            child: Stack(
                                              alignment: Alignment.topRight,
                                              children: [
                                                ImageUtils.imageUtilsInstance
                                                    .showCacheNetworkImage(
                                                        context: context,
                                                        showProgressBarInPlaceHolder:
                                                            true,
                                                        shape:
                                                            BoxShape.rectangle,
                                                        url: _imageToShow ?? "",
                                                        width: CommonUtils
                                                            .commonUtilsInstance
                                                            .getPercentageSize(
                                                                context:
                                                                    context,
                                                                percentage:
                                                                    SIZE_100,
                                                                ofWidth: true)),
                                                InkWell(
                                                  onTap: () {
                                                    _makeShopFavourite(
                                                        _shopData);
                                                  },
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: SIZE_10,
                                                            right: SIZE_10),
                                                    child: Container(
                                                      padding: EdgeInsets.all(
                                                          SIZE_2),
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: Colors.white),
                                                      child: Icon(
                                                        (_shopData?.isFav == 0)
                                                            ? Icons
                                                                .favorite_border
                                                            : Icons.favorite,
                                                        size: SIZE_24,
                                                        color: Colors.red,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                      itemCount: _shopImages?.length,
                                      viewportFraction: SIZE_1,
                                      scale: 0.4,
                                      autoplay: true,
                                      pagination: new SwiperPagination(
                                        alignment: Alignment.bottomCenter,
                                        builder: new DotSwiperPaginationBuilder(
                                            color: COLOR_GREY_PICKER,
                                            size: SIZE_6,
                                            activeColor: COLOR_PRIMARY),
                                      ),
                                    ),
                                  )
                                : Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        width: CommonUtils.commonUtilsInstance
                                            .getPercentageSize(
                                                context:
                                                    _actionManager?.context,
                                                percentage: SIZE_100,
                                                ofWidth: true),
                                        color: Colors.transparent,
                                        child: Container(
                                          child: SvgPicture.asset(
                                            SHOP_LOGO,
                                            height: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                    context:
                                                        _actionManager?.context,
                                                    percentage: SIZE_20,
                                                    ofWidth: false),
                                            width: CommonUtils
                                                .commonUtilsInstance
                                                .getPercentageSize(
                                                    context:
                                                        _actionManager?.context,
                                                    percentage: SIZE_100,
                                                    ofWidth: true),
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          _makeShopFavourite(_shopData);
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: SIZE_10, right: SIZE_10),
                                          child: Container(
                                            padding: EdgeInsets.all(SIZE_2),
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: COLOR_LIGHT_WHITE),
                                            child: Icon(
                                              (_shopData?.isFav == 0)
                                                  ? Icons.favorite_border
                                                  : Icons.favorite,
                                              size: SIZE_24,
                                              color: Colors.red,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                            Padding(
                              padding: const EdgeInsets.only(left: SIZE_20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(height: SIZE_10),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          toBeginningOfSentenceCase(
                                              _shopData?.name ??
                                                  AppLocalizations.of(context)
                                                      .common
                                                      .text
                                                      .notAllowed),
                                          style: AppConfig.of(
                                                  _actionManager?.context)
                                              .themeData
                                              .primaryTextTheme
                                              .headline6,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                        ),
                                      ),
                                      (_shopData?.primaryCoupon?.code
                                                  ?.isNotEmpty ==
                                              true)
                                          ? Label(
                                              triangleHeight: 10.0,
                                              edge: Edge.LEFT,
                                              child: Container(
                                                color: COLOR_PRIMARY,
                                                height: SIZE_22,
                                                width: CommonUtils
                                                    .commonUtilsInstance
                                                    .getPercentageSize(
                                                        context: context,
                                                        ofWidth: true,
                                                        percentage: SIZE_40),
                                                child: Center(
                                                    child: Text(
                                                        (_shopData?.primaryCoupon
                                                                    ?.type ==
                                                                CouponType
                                                                    ?.Fixed
                                                                    ?.value)
                                                            ? NumberFormatUtils
                                                                .numberFormatUtilsInstance
                                                                .formatPriceWithSymbol(
                                                                    price: _shopData
                                                                        ?.primaryCoupon
                                                                        ?.discount)
                                                            : "${_shopData?.primaryCoupon?.discount}% ${AppLocalizations.of(context).shopdetailspage.text.off}" ??
                                                                "",
                                                        style:
                                                            textStyleSize12WithWhiteColor)),
                                              ),
                                            )
                                          : const SizedBox()
                                    ],
                                  ),
                                  SizedBox(height: SIZE_10),
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(right: SIZE_20),
                                    child: Text(
                                      toBeginningOfSentenceCase(
                                          _shopData?.description ??
                                              AppLocalizations.of(context)
                                                  .common
                                                  .text
                                                  .notAllowed),
                                      style: textStyleSize12WithBLACK,
                                      textAlign: TextAlign.justify,
                                    ),
                                  ),
                                  SizedBox(height: SIZE_10),
                                  Row(
                                    children: <Widget>[
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color: COLOR_PRIMARY,
                                            size: SIZE_16,
                                          ),
                                          SizedBox(
                                            width: SIZE_5,
                                          ),
                                          Text(
                                            _shopData?.rating
                                                    ?.toStringAsFixed(1) ??
                                                "",
                                            style:
                                                textStyleSize14WithGreenColor,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: SIZE_16,
                                      ),
                                      Row(
                                        children: [
                                          Image.asset(
                                            CLOCK_IMAGE,
                                            color: COLOR_LIGHT_GREY,
                                            height: SIZE_14,
                                            width: SIZE_14,
                                          ),
                                          SizedBox(
                                            width: SIZE_5,
                                          ),
                                          Text(
                                            CategoriesShopUtilsManager
                                                    .homeUtilsInstance
                                                    ?.getShopDistanceTime(
                                                        expTime: _shopData
                                                            ?.expTime) ??
                                                "",
                                            style: textStyleSize12WithBLACK,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: SIZE_16,
                                      ),
                                      Row(
                                        children: [
                                          Image.asset(
                                            CAR_IMAGE,
                                            color: COLOR_LIGHT_GREY,
                                            height: SIZE_14,
                                            width: SIZE_14,
                                          ),
                                          SizedBox(
                                            width: SIZE_5,
                                          ),
                                          Text(
                                            '${_shopData?.distance?.toString()} ${AppLocalizations.of(_actionManager?.context).common.text.km.toLowerCase()}' ??
                                                "" +
                                                    "${AppLocalizations.of(_actionManager?.context).common.text.km.toLowerCase()}",
                                            style: textStyleSize12WithBLACK,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(height: SIZE_10),
                                  _showShopType(shopType: _shopData?.shopType),
                                  SizedBox(height: SIZE_10),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          )
        : (_actionManager?.categoriesShopState?.isLoading == false)
            ? Container(
                alignment: Alignment.center,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _actionManager?.context,
                    percentage: SIZE_50,
                    ofWidth: false),
                child: CommonImageWithTextWidget(
                  context: _actionManager?.context,
                  localImagePath: SHOP_LOGO,
                  imageHeight: SIZE_7,
                  imageWidth: SIZE_7,
                  textToShow: AppLocalizations.of(context)
                      .categoriesshoppage
                      .text
                      .noShopFound,
                ),
              )
            : const SizedBox();
  }

  //show shop title text
  Widget _showShopsTitle() {
    return Text(
      AppLocalizations.of(context).categoriesshoppage.title.shop,
      style: textStyleSize22WithBlackColor,
    );
  }

  //set border color to color primary when selected
  _getBorderColor(CategoriesData categoriesData) {
    if (_actionManager
            ?.categoriesShopState?.categoriesResponseModel?.data?.isNotEmpty ==
        true) {
      if (_actionManager?.categoriesShopState?.selectedCategoryList
              ?.contains(categoriesData?.id) ==
          true) {
        return COLOR_PRIMARY;
      } else {
        return COLOR_GREY;
      }
    } else {
      return COLOR_GREY;
    }
  }

  //this method is used to show banners of shops
  Widget _showBannerView() {
    return (_sliderImageVideoModel?.sliderModelList?.isNotEmpty == true)
        ? Container(
            margin: EdgeInsets.only(top: SIZE_10),
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: _actionManager?.context,
                percentage: SIZE_26,
                ofWidth: false),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(SIZE_8)),
            ),
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                SliderModel _sliderModel =
                    _sliderImageVideoModel?.sliderModelList[index];
                return Container(
                  margin: EdgeInsets.only(bottom: SIZE_30),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(SIZE_20)),
                    color: Colors.white,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(SIZE_10),
                    child: ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                        context: context,
                        showProgressBarInPlaceHolder: true,
                        shape: BoxShape.rectangle,
                        url: _sliderModel.url ?? "",
                        width: CommonUtils.commonUtilsInstance
                            .getPercentageSize(
                                context: context,
                                percentage: SIZE_100,
                                ofWidth: true)),
                  ),
                );
              },
              itemCount: _sliderImageVideoModel?.sliderModelList?.length,
              viewportFraction: SIZE_1,
              scale: 0.9,
              pagination: new SwiperPagination(
                alignment: Alignment.bottomCenter,
                builder: new DotSwiperPaginationBuilder(
                    color: COLOR_GREY_PICKER, activeColor: COLOR_PRIMARY),
              ),
            ))
        : SizedBox();
  }

  //method to show radio button view to select delivery and take out
  Widget _showRadioButtonView() {
    return Theme(
      data: Theme.of(_actionManager?.context).copyWith(
          unselectedWidgetColor: COLOR_PRIMARY, disabledColor: COLOR_PRIMARY),
      child: Row(
        children: [
          Transform.scale(
            scale: 0.9,
            child: new Radio(
                value: ShopType.delivery.value,
                groupValue:
                    _actionManager?.categoriesShopState?.shopTypeEnum?.value,
                onChanged: (value) {
                  _page = 1;
                  _actionManager?.OnShopTypeChange(
                      shopTypeEnum: ShopType.delivery,
                      scaffoldState: _scaffoldKey?.currentState);
                }),
          ),
          new Text(
            AppLocalizations?.of(_actionManager?.context)
                ?.categoriesshoppage
                ?.text
                ?.delivery,
            style: textStyleSize14BlackColor,
          ),
          Transform.scale(
            scale: 0.9,
            child: new Radio(
              value: ShopType.pickUp.value,
              groupValue:
                  _actionManager?.categoriesShopState?.shopTypeEnum?.value,
              onChanged: (value) {
                _page = 1;
                _actionManager?.OnShopTypeChange(
                    shopTypeEnum: ShopType.pickUp,
                    scaffoldState: _scaffoldKey?.currentState);
              },
            ),
          ),
          new Text(
            'Pick Up',
            style: textStyleSize14BlackColor,
          ),
        ],
      ),
    );
  }

  //method to show categories title
  Widget _showCategoriesTitle() {
    return (_actionManager?.categoriesShopState?.categoriesResponseModel?.data
                ?.isNotEmpty ==
            true)
        ? Text(
            AppLocalizations.of(_actionManager?.context)
                .categoriesshoppage
                .title
                .categories,
            style: textStyleSize22WithBlackColor)
        : const SizedBox();
  }

  //this method is used to show shop type in flutter
  Widget _showShopType({int shopType}) {
    if (shopType == ShopType?.both?.value) {
      return _showDeliveryAndTakeAway();
    } else {
      return _showDeliveryOrTakeAway(shopType);
    }
  }

  //this method will open cupertino date time picker
  void _showScheduleDateTimePicker() {
    var _minDate = DateTime.now();

    if (CartUtilsManager.cartUtilsInstance.shopType == ShopType.pickUp) {
      _minDate = _minDate.add(Duration(minutes: 15));
    } else
      _minDate = _minDate.add(Duration(hours: 3));

    showCupertinoModalPopup(
        context: _actionManager?.context,
        builder: (_) => Material(
              borderRadius: new BorderRadius.only(
                  topLeft: Radius.circular(SIZE_20),
                  topRight: Radius.circular(SIZE_20)),
              child: Container(
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    percentage: SIZE_38,
                    ofWidth: false,
                    context: _actionManager?.context),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: Radius.circular(SIZE_20),
                        topRight: Radius.circular(SIZE_20))),
                child: Column(
                  children: [
                    SizedBox(
                      height: SIZE_20,
                    ),
                    Container(
                      padding: EdgeInsets.all(SIZE_10),
                      color: COLOR_GREY_PICKER,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppLocalizations?.of(_actionManager?.context)
                                ?.categoriesshoppage
                                ?.text
                                ?.day,
                            style: textStyleSize14BlackColor,
                          ),
                          SizedBox(
                            width: SIZE_100,
                          ),
                          Text(
                            AppLocalizations?.of(_actionManager?.context)
                                ?.categoriesshoppage
                                ?.text
                                ?.time,
                            style: textStyleSize14BlackColor,
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          percentage: SIZE_20,
                          ofWidth: false,
                          context: _actionManager?.context),
                      child: CupertinoTheme(
                        data: CupertinoThemeData(
                          textTheme: CupertinoTextThemeData(
                            dateTimePickerTextStyle: TextStyle(
                              fontSize: SIZE_16,
                            ),
                          ),
                        ),
                        child: CupertinoDatePicker(
                            initialDateTime: (_actionManager
                                            ?.categoriesShopState
                                            ?.scheduleDateTime !=
                                        null &&
                                    _actionManager
                                        ?.categoriesShopState?.scheduleDateTime
                                        ?.isBefore(_minDate))
                                ? _minDate
                                : _actionManager?.categoriesShopState
                                        ?.scheduleDateTime ??
                                    _minDate,
                            minimumDate: _minDate,
                            onDateTimeChanged: (val) {
                              _actionManager?.actionToUpdateUI(
                                  scheduledDateTime: val);
                            }),
                      ),
                    ),

                    // Close the modal
                    _showScheduleButton(minTime: _minDate)
                  ],
                ),
              ),
            ));
  }

  //method to show schedule button on cupertino picker
  Widget _showScheduleButton({DateTime minTime}) {
    return Container(
      margin: EdgeInsets.only(top: SIZE_10),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _actionManager?.context, percentage: SIZE_5, ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _actionManager?.context, percentage: SIZE_42, ofWidth: true),
      child: RaisedButton(
        onPressed: () {
          _actionManager?.actionToUpdateUI(
              scheduledDateTime:
                  _actionManager?.categoriesShopState?.scheduleDateTime ??
                      minTime);
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(_actionManager?.context);
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SIZE_80)),
        padding: EdgeInsets.all(SIZE_0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [COLOR_PRIMARY, COLOR_PRIMARY],
              ),
              borderRadius: BorderRadius.circular(SIZE_30)),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              AppLocalizations.of(context).categoriesshoppage.button.schedule,
              textAlign: TextAlign.center,
              style: textStyleSize14WithWhiteColor,
            ),
          ),
        ),
      ),
    );
  }

  //this method is used to show space
  Widget _showSpace(double size) {
    return SizedBox(
      height: size,
    );
  }

  //this method will show both delivery and take away option on shop listing
  Widget _showDeliveryAndTakeAway() {
    return Row(
      children: [
        DottedBorder(
          strokeWidth: 1,
          color: COLOR_DARK_PRIMARY,
          dashPattern: [2, 4],
          strokeCap: StrokeCap.round,
          child: Container(
            padding: EdgeInsets.only(left: SIZE_2, right: SIZE_2),
            child: Text(
              AppLocalizations?.of(_actionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.delivery,
              style: textStyleSize12WithPrimary,
            ),
          ),
        ),
        SizedBox(
          width: SIZE_16,
        ),
        DottedBorder(
          strokeWidth: 1,
          color: COLOR_DARK_PRIMARY,
          dashPattern: [2, 4],
          strokeCap: StrokeCap.round,
          child: Container(
            padding: EdgeInsets.only(left: SIZE_2, right: SIZE_2),
            child: Text(
              AppLocalizations?.of(_actionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.takeAway,
              style: textStyleSize12WithPrimary,
            ),
          ),
        )
      ],
    );
  }

  //this method will return delivery or take away option based on its type
  Widget _showDeliveryOrTakeAway(int shopType) {
    return DottedBorder(
      strokeWidth: 1,
      color: COLOR_DARK_PRIMARY,
      child: Container(
        padding: EdgeInsets.only(left: SIZE_2, right: SIZE_2),
        child: Text(
          (shopType == ShopType?.pickUp?.value)
              ? /*AppLocalizations?.of(_actionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.takeAway*/
              AppLocalizations.of(_actionManager?.context)
                  .common
                  ?.text
                  ?.pickUp
                  ?.toUpperCase()
              : AppLocalizations?.of(_actionManager?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.delivery
                  ?.toUpperCase(),
          style: textStyleSize12WithPrimary,
        ),
      ),
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _page = 1;
    _actionManager?.callGetShopsListing(
      scaffoldState: _scaffoldKey?.currentState,
    );
  }

  void scrollTo(int index) => itemScrollController.scrollTo(
      index: index,
      duration: Duration(milliseconds: 600),
      curve: Curves.easeInOutCubic,
      alignment: 0);

  //method to get the position of each items of the list scrolled currently
  Widget get positionsView => ValueListenableBuilder<Iterable<ItemPosition>>(
        valueListenable: itemPositionsListener.itemPositions,
        builder: (context, positions, child) {
          if (positions.isNotEmpty) {
            // Determine the first visible item by finding the item with the
            // smallest trailing edge that is greater than 0.  i.e. the first
            // item whose trailing edge in visible in the viewport.
            min = positions
                .where((ItemPosition position) => position.itemTrailingEdge > 0)
                .reduce((ItemPosition min, ItemPosition position) =>
                    position.itemTrailingEdge < min.itemTrailingEdge
                        ? position
                        : min)
                .index;
            // Determine the last visible item by finding the item with the
            // greatest leading edge that is less than 1.  i.e. the last
            // item whose leading edge in visible in the viewport.
            max = positions
                .where((ItemPosition position) => position.itemLeadingEdge < 1)
                .reduce((ItemPosition max, ItemPosition position) =>
                    position.itemLeadingEdge > max.itemLeadingEdge
                        ? position
                        : max)
                .index;

            print('the minimum position is $min and max is $max');
          }
          return SizedBox();
        },
      );

  //this method is used to shop fav and unfav API
  void _makeShopFavourite(ShopDetailsModel shopData) {
    _actionManager?.callFavUnFavShopApiEvent(
        shopId: shopData?.id, isFav: shopData?.isFav);
  }
}

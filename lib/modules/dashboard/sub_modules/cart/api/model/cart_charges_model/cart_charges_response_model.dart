// To parse this JSON data, do
//
//     final getChargesResponseModel = getChargesResponseModelFromJson(jsonString);

import 'dart:convert';

GetChargesResponseModel getChargesResponseModelFromJson(String str) => GetChargesResponseModel.fromMap(json.decode(str));

String getChargesResponseModelToJson(GetChargesResponseModel data) => json.encode(data.toJson());

class GetChargesResponseModel {
  GetChargesResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory GetChargesResponseModel.fromMap(Map<String, dynamic> json) => GetChargesResponseModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.amountBeforeDiscount,
    this.discountAmount,
    this.amountAfterDiscount,
    this.deliveryChargeForCustomer,
    this.deliveryChargeToDeliveryBoy,
    this.platformCharge,
    this.commissionCharge,
    this.afterChargeAmount,
    this.stripeCharges,
    this.amountAfterStripeCharges,
    this.hst,
    this.pst,
    this.gstTax
  });

  num amountBeforeDiscount;
  num discountAmount;
  num amountAfterDiscount;
  num deliveryChargeForCustomer;
  num deliveryChargeToDeliveryBoy;
  num platformCharge;
  num commissionCharge;
  num afterChargeAmount;
  num stripeCharges;
  num amountAfterStripeCharges;
  num hst;
  num pst;
  num gstTax;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    amountBeforeDiscount: json["amount_before_discount"] == null ? null : json["amount_before_discount"],
    hst: json["hst_tax"] == null ? null : json["hst_tax"],
    pst: json["pst_tax"] == null ? null : json["pst_tax"],
    gstTax: json["gst_tax"] == null ? null : json["gst_tax"],
    discountAmount: json["discount_amount"] == null ? null : json["discount_amount"],
    amountAfterDiscount: json["amount_after_discount"] == null ? null : json["amount_after_discount"],
    deliveryChargeForCustomer: json["delivery_charge_for_customer"] == null ? 0.0 : json["delivery_charge_for_customer"],
    deliveryChargeToDeliveryBoy: json["delivery_charge_to_delivery_boy"] == null ? null : json["delivery_charge_to_delivery_boy"],
    platformCharge: json["platform_charge"] == null ? 0.0 : json["platform_charge"] ,
    commissionCharge: json["commission_charge"] == null ? null : json["commission_charge"].toDouble(),
    afterChargeAmount: json["after_charge_amount"] == null ? null : json["after_charge_amount"].toDouble(),
    stripeCharges: json["stripe_charges"] == null ? null : json["stripe_charges"].toDouble(),
    amountAfterStripeCharges: json["amount_after_stripe_charges"] == null ? null : json["amount_after_stripe_charges"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "amount_before_discount": amountBeforeDiscount == null ? null : amountBeforeDiscount,
    "hst_tax": hst == null ? null : hst,
    "pst_tax": pst == null ? null : pst,
    "gst_tax": gstTax == null ? null : gstTax,
    "discount_amount": discountAmount == null ? null : discountAmount,
    "amount_after_discount": amountAfterDiscount == null ? null : amountAfterDiscount,
    "delivery_charge_for_customer": deliveryChargeForCustomer == null ? null : deliveryChargeForCustomer,
    "delivery_charge_to_delivery_boy": deliveryChargeToDeliveryBoy == null ? null : deliveryChargeToDeliveryBoy,
    "platform_charge": platformCharge == null ? null : platformCharge,
    "commission_charge": commissionCharge == null ? null : commissionCharge,
    "after_charge_amount": afterChargeAmount == null ? null : afterChargeAmount,
    "stripe_charges": stripeCharges == null ? null : stripeCharges,
    "amount_after_stripe_charges": amountAfterStripeCharges == null ? null : amountAfterStripeCharges,
  };
}

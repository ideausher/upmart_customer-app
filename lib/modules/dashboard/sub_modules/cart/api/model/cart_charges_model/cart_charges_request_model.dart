// To parse this JSON data, do
//
//     final getChargesRequestModel = getChargesRequestModelFromJson(jsonString);

import 'dart:convert';

GetChargesRequestModel getChargesRequestModelFromJson(String str) => GetChargesRequestModel.fromJson(json.decode(str));

String getChargesRequestModelToJson(GetChargesRequestModel data) => json.encode(data.toJson());

class GetChargesRequestModel {
  GetChargesRequestModel({
    this.addressId,
    this.couponId,
    this.shopId,
    this.products,
    this.bookingType,
  });

  int addressId;
  String couponId;
  int shopId;
  List<Product> products;
  int bookingType;

  factory GetChargesRequestModel.fromJson(Map<String, dynamic> json) => GetChargesRequestModel(
    addressId: json["address_id"] == null ? null : json["address_id"],
    couponId: json["coupon_id"] == null ? null : json["coupon_id"],
    shopId: json["shop_id"] == null ? null : json["shop_id"],
    products: json["products"] == null ? null : List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    bookingType: json["booking_type"] == null ? null : json["booking_type"],
  );

  Map<String, dynamic> toJson() => {
    "address_id": addressId == null ? null : addressId,
    "coupon_id": couponId == null ? null : couponId,
    "shop_id": shopId == null ? null : shopId,
    "products": products == null ? null : List<dynamic>.from(products.map((x) => x.toJson())),
    "booking_type": bookingType == null ? null : bookingType,
  };
}

class Product {
  Product({
    this.id,
    this.quantity,
  });

  int id;
  int quantity;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"] == null ? null : json["id"],
    quantity: json["quantity"] == null ? null : json["quantity"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "quantity": quantity == null ? null : quantity,
  };
}

// To parse this JSON data, do
//
//     final shopSlotsResponseModel = shopSlotsResponseModelFromJson(jsonString);

import 'dart:convert';

ShopSlotsResponseModel shopSlotsResponseModelFromJson(String str) => ShopSlotsResponseModel.fromMap(json.decode(str));

String shopSlotsResponseModelToJson(ShopSlotsResponseModel data) => json.encode(data.toJson());

class ShopSlotsResponseModel {
  ShopSlotsResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  List<SlotsData> data;
  String message;
  int statusCode;

  factory ShopSlotsResponseModel.fromMap(Map<String, dynamic> json) => ShopSlotsResponseModel(
    data: json["data"] == null ? null : List<SlotsData>.from(json["data"].map((x) => SlotsData.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class SlotsData {
  SlotsData({
    this.id,
    this.shopId,
    this.day,
    this.slotFrom,
    this.slotTo,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int shopId;
  int day;
  String slotFrom;
  String slotTo;
  DateTime createdAt;
  DateTime updatedAt;

  factory SlotsData.fromJson(Map<String, dynamic> json) => SlotsData(
    id: json["id"] == null ? null : json["id"],
    shopId: json["shop_id"] == null ? null : json["shop_id"],
    day: json["day"] == null ? null : json["day"],
    slotFrom: json["slot_from"] == null ? null : json["slot_from"],
    slotTo: json["slot_to"] == null ? null : json["slot_to"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "shop_id": shopId == null ? null : shopId,
    "day": day == null ? null : day,
    "slot_from": slotFrom == null ? null : slotFrom,
    "slot_to": slotTo == null ? null : slotTo,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}

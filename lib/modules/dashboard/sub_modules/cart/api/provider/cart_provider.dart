import 'package:flutter/material.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_request_model.dart';

class CartProvider {
  Future<dynamic> getCartCharges(
      {BuildContext context,
      GetChargesRequestModel getCartRequestModel}) async {
    var getCartCharges = "calculate/charge";

    var result = await AppConfig.of(context).baseApi.getRequest(
        getCartCharges, context,
        queryParameters: getCartRequestModel.toJson());
    return result;
  }


  //api to get shop slots day wise
  Future<dynamic> getShopSlots(
      {BuildContext context,
      int shopId}) async {
    var getShopSlots = "getslots";
    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(getShopSlots, context, queryParameters: {"shop_id": shopId});
    return result;
  }
}

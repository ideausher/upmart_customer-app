import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/common_image_with_text.dart';
import 'package:user/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/constants/dimens_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/theme/app_themes.dart';
import 'package:user/modules/common/utils/common_utils.dart';
import 'package:user/modules/common/utils/date_picker_utils.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/modules/common/utils/image_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/utils/number_format_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/dashboard/constants/image_constants.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/shop_slots/shop_slot_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/bloc/cart_state.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_action_managers.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:user/routes.dart';
import 'package:intl/intl.dart';

class CartPage extends StatefulWidget {
  BuildContext context;

  CartPage({this.context}) {
    this.context = context;
  }

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> implements PushReceived {
  TextEditingController _couponController = new TextEditingController();
  TextEditingController _instructionController = new TextEditingController();
  CartActionManagers _cartActionManagers = CartActionManagers();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isNativePaySupported = false;
  String _scheduledDateTime;
  List<SlotsData> slotsData;

  @override
  void initState() {
    super.initState();
    checkNativePaySupport();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _cartActionManagers?.context = widget.context;
    _cartActionManagers?.actionToCallApi();
  }

  @override
  void dispose() {
    super.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _couponController?.dispose();
    _instructionController?.dispose();
    _cartActionManagers?.cartBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _cartActionManagers?.context = context;
    return BlocEventStateBuilder<CartState>(
      bloc: _cartActionManagers?.cartBloc,
      builder: (BuildContext context, CartState cartState) {
        _cartActionManagers?.context = context;
        if (_cartActionManagers?.cartState != cartState) {
          _cartActionManagers?.cartState = cartState;
          // allocate schedule time
          CartUtilsManager.cartUtilsInstance.scheduleDateTime =
              cartState?.scheduleDateTime;
          slotsData = _cartActionManagers?.getDayWiseSlotsList();
        }
        return ModalProgressHUD(
            inAsyncCall: cartState?.isLoading ?? false,
            child: WillPopScope(
              onWillPop: () {
                return NavigatorUtils.navigatorUtilsInstance
                    .navigatorPopScreen(context, dataToBeSend: true);
              },
              child: Scaffold(
                key: _scaffoldKey,
                appBar: _showAppBar(),
                body:
                (CartUtilsManager.cartUtilsInstance.shopItems?.isNotEmpty ==
                    true)
                    ? SingleChildScrollView(
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          _showDivider(),
                          _showTitleText(),
                          _showDivider(),
                          _showCartView(),
                        ],
                      ),
                    ],
                  ),
                )
                    : Center(
                  child: CommonImageWithTextWidget(
                    context: _cartActionManagers?.context,
                    localImagePath: EMPTY_CART_LOGO,
                    imageHeight: SIZE_20,
                    imageWidth: SIZE_20,
                    textToShow: AppLocalizations
                        .of(context)
                        .cartpage
                        .text
                        .noItemsInCart,
                  ),
                ),
              ),
            ));
      },
    );
  }

  //this method will return a app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        elevation: ELEVATION_0,
        centerTitle: true,
        appBarTitle: AppLocalizations
            .of(_cartActionManagers?.context)
            .cartpage
            .appbartitle
            .cart,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadIconPressed: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _cartActionManagers?.context,
              dataToBeSend: true);
        },
        leadingWidget:
        (ModalRoute
            .of(context)
            .settings
            .name == Routes.DASH_BOARD)
            ? SizedBox()
            : null,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.all(SIZE_15),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitleStyle: AppConfig
            .of(_cartActionManagers?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        defaultLeadingIconColor: Colors.black,
        backGroundColor: Colors.transparent);
  }

  //My Cart title text
  Widget _showTitleText() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_12, right: SIZE_12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              (CartUtilsManager?.cartUtilsInstance?.shopDetailsModel?.media
                  ?.isNotEmpty ==
                  true ||
                  CartUtilsManager?.cartUtilsInstance?.shopDetailsModel
                      ?.profileImage?.fileName?.isNotEmpty ==
                      true)
                  ? _showCacheNetworkImage()
                  : _showDefaultImage(defaultImage: SHOP_LOGO),
              SizedBox(
                width: SIZE_10,
              ),
              Text(
                  toBeginningOfSentenceCase(CartUtilsManager
                      ?.cartUtilsInstance?.shopDetailsModel?.name ??
                      AppLocalizations
                          .of(context)
                          .common
                          .text
                          .notAllowed),
                  style: AppConfig
                      ?.of(_cartActionManagers?.context)
                      ?.themeData
                      ?.primaryTextTheme
                      ?.headline3)
            ],
          ),
          Text(
            NumberFormatUtils.numberFormatUtilsInstance.formatPriceWithSymbol(
                price: CartUtilsManager.cartUtilsInstance.cartAmount()),
            style: AppConfig
                ?.of(_cartActionManagers?.context)
                ?.themeData
                ?.primaryTextTheme
                ?.subtitle2,
          )
        ],
      ),
    );
  }

  //this method will show cart items list
  Widget _showCartItemsList() {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(
            top: SIZE_10, left: SIZE_12, right: SIZE_12, bottom: SIZE_10),
        itemCount: CartUtilsManager.cartUtilsInstance?.shopItems?.length,
        itemBuilder: (BuildContext context, int index) {
          ShopItem _cartItem =
          CartUtilsManager.cartUtilsInstance?.shopItems[index];
          return Padding(
            padding: const EdgeInsets.only(top: SIZE_10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _showRemoveOption(context: context, cartItem: _cartItem),
                SizedBox(
                  height: SIZE_10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        child: (_cartItem?.productImage?.fileName?.isNotEmpty ==
                            true)
                            ? _showImageComingFromApi(
                            image: _cartItem.productImage?.fileName)
                            : _showDefaultImage(defaultImage: PRODUCT_LOGO)),
                    SizedBox(
                      width: SIZE_20,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            toBeginningOfSentenceCase(_cartItem?.productName ??
                                AppLocalizations
                                    .of(context)
                                    .common
                                    .text
                                    .notAllowed),
                            style: textStyleSize15BlackColor,
                            textAlign: TextAlign.left,
                          ),
                          Visibility(
                            visible: (_cartItem?.gstEnabled == 1),
                            child: Text('GST:${_cartItem?.gstPrice}%',
                                style: textStyleSize12GREY),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: _showIncrementDecrementWidget(
                          shopItem: _cartItem, context: context),
                    ),
                    SizedBox(
                      width: SIZE_20,
                    ),
                    Column(
                      children: [
                        Text(
                          NumberFormatUtils.numberFormatUtilsInstance
                              .formatPriceWithSymbol(
                              price: _cartItem?.discountedPrice),
                          style: AppConfig
                              .of(_cartActionManagers?.context)
                              .themeData
                              .primaryTextTheme
                              .subtitle2,
                        ),
                        Visibility(
                          visible:
                          _cartItem?.discountedPrice != _cartItem?.mrpPrice,
                          child: Text(
                            NumberFormatUtils.numberFormatUtilsInstance
                                .formatPriceWithSymbol(
                                price: _cartItem?.mrpPrice),
                            style: TextStyle(
                                decoration: TextDecoration.lineThrough,
                                color: COLOR_GREY_LIGHT),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: SIZE_5,
                ),
                _showStockEmptyOrQuantityDecreased(
                    shopItem: _cartItem, context: context),
              ],
            ),
          );
        });
  }

  // show increment and decrement widget
  Widget _showIncrementDecrementWidget(
      {BuildContext context, ShopItem shopItem}) {
    var _tupple = CartUtilsManager?.cartUtilsInstance
        ?.checkProductInACartOrNot(shopItem: shopItem);
    var _existInCart = _tupple?.item1;
    var _quantityAddedInCart = _tupple?.item2;
    var _canAddQuantity =
    CartUtilsManager?.cartUtilsInstance?.canAddQuantity(shopItem: shopItem);

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: _existInCart,
          child: InkWell(
            onTap: () {
              NetworkConnectionUtils.networkConnectionUtilsInstance
                  .getConnectivityStatus(context, showNetworkDialog: true)
                  .then((value) {
                CartUtilsManager.cartUtilsInstance.addOrRemoveQuantity(
                    shopItem: shopItem,
                    quantity: 1,
                    add: false,
                    shopDetailsModel:
                    CartUtilsManager.cartUtilsInstance.shopDetailsModel);

                //here increasing product calculating get charges api
                _cartActionManagers?.actionOnIncrementDecrementOfProduct(
                    slotsData: _cartActionManagers?.cartState
                        ?.selectedSlotsData,
                    scheduledDateTim:
                    _cartActionManagers?.cartState?.scheduleDateTime);
              });


              /*   _cartActionManagers?.actionOnCartScreenStateChange(
                  slotsData: _cartActionManagers?.cartState?.selectedSlotsData,
                  scheduledDateTim:
                      _cartActionManagers?.cartState?.scheduleDateTime);*/
            },
            child: Container(
              decoration: BoxDecoration(
                color: COLOR_GREY_DEC,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(SIZE_5),
                    bottomLeft: Radius.circular(SIZE_5)),
              ),
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_4, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_7, ofWidth: true),
              child: Icon(
                Icons.remove,
                color: Colors.black,
                size: SIZE_16,
              ),
            ),
          ),
        ),
        Visibility(
          visible: _existInCart,
          child: Container(
            alignment: Alignment.center,
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, percentage: SIZE_4, ofWidth: false),
            width: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, percentage: SIZE_7, ofWidth: true),
            color: Colors.white,
            child: Text(_quantityAddedInCart?.toString() ?? "0",
                style: textStyleSize14WithGreyColor),
          ),
        ),
        Visibility(
          visible: shopItem?.stockQuantity != 0,
          child: InkWell(
            onTap: () {
              print(
                  "shopItem ${shopItem?.itemQuantity} ---- ${shopItem
                      ?.stockQuantity}  ${_canAddQuantity}");
              if (_canAddQuantity) {
                NetworkConnectionUtils.networkConnectionUtilsInstance
                    .getConnectivityStatus(context, showNetworkDialog: true)
                    .then((value) {
                  CartUtilsManager.cartUtilsInstance.addOrRemoveQuantity(
                      shopItem: shopItem,
                      quantity: 1,
                      add: true,
                      shopDetailsModel:
                      CartUtilsManager.cartUtilsInstance.shopDetailsModel);

                  //here increasing product calculating get charges api
                  _cartActionManagers?.actionOnIncrementDecrementOfProduct(
                      slotsData: _cartActionManagers?.cartState
                          ?.selectedSlotsData,
                      scheduledDateTim:
                      _cartActionManagers?.cartState?.scheduleDateTime);
                });


                /*   _cartActionManagers?.actionOnCartScreenStateChange(
                    slotsData:
                        _cartActionManagers?.cartState?.selectedSlotsData,
                    scheduledDateTim:
                        _cartActionManagers?.cartState?.scheduleDateTime);*/
              }
            },
            child: Container(
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_4, ofWidth: false),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_7, ofWidth: true),
              decoration: BoxDecoration(
                color: (_canAddQuantity ? Colors.black : COLOR_GREY_TEXT),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(SIZE_5),
                    bottomRight: Radius.circular(SIZE_5)),
              ),
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: SIZE_16,
              ),
            ),
          ),
        ),
      ],
    );
  }

  //this method will show cart view
  Widget _showCartView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _showCartItemsList(),
        _showDivider(thickness: SIZE_5),
        _showPromoCodeOption(),
        _showSpace(SIZE_15),
        _showScheduleAt(),
        _showSpace(SIZE_10),
        _showScheduling(),
        _showSpace(SIZE_16),
        _showBookingSlotsDateWise(),
        _showSpace(SIZE_16),
        _showAddressDesc(),
        _showSpace(SIZE_15),
        _showAddressView(),
        _showSpace(SIZE_15),
        _showInstruction(),
        _showSpace(SIZE_15),
        _showPaymentTitle(),
        _showSpace(SIZE_15),
        _showCartDetails(),
        _showSpace(SIZE_15),
        _showCartButton()
      ],
    );
  }

  //show promo code option
  Widget _showPromoCodeOption() {
    _couponController?.text =
        CartUtilsManager.cartUtilsInstance.couponData?.code ?? "";
    return InkWell(
      onTap: () {
        _cartActionManagers?.actionToChooseCoupon();
      },
      child: Padding(
        padding: const EdgeInsets.only(top: SIZE_15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 50,
              child: _textFieldForm(
                  radius: SIZE_10,
                  enabled: false,
                  controller: _couponController,
                  keyboardType: TextInputType.text,
                  context: _cartActionManagers?.context,
                  hint:
                  AppLocalizations
                      .of(context)
                      .cartpage
                      .hint
                      .offPercentage),
            ),
            Expanded(flex: 10, child: SizedBox()),
            Expanded(
              flex: 30,
              child: _showApplyButton(),
            )
          ],
        ),
      ),
    );
  }

  //method to return space
  Widget _showSpace(double size) {
    return SizedBox(
      height: size,
    );
  }

  //method to show horizontal divider
  Widget _showDivider({double thickness: SIZE_0}) {
    return Divider(
      color: COLOR_BORDER_GREY,
      thickness: thickness,
    );
  }

  //this method will show cart button
  Widget _showCartButton() {
    return (CartUtilsManager.cartUtilsInstance?.shopItems?.isNotEmpty == true)
        ? Align(
      alignment: Alignment.bottomCenter,
      child: InkWell(
        onTap: () {
          if (CartUtilsManager.cartUtilsInstance?.scheduleDateTime !=
              null &&
              CartUtilsManager.cartUtilsInstance?.slotsData != null) {
            _cartActionManagers?.actionToMoveStripe(
                note: _instructionController?.text ?? "",
                isNativePaySupported: isNativePaySupported ?? false);
          } else
            _showSnackBarToScheduleOrder(
                message: AppLocalizations
                    .of(_cartActionManagers?.context)
                    .cardlistpage
                    .error
                    .scheduletime);
        },
        child: Container(
          margin: EdgeInsets.only(
              bottom: SIZE_6, left: SIZE_12, right: SIZE_12),
          padding: EdgeInsets.only(left: SIZE_20, right: SIZE_20),
          alignment: Alignment.center,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: context, percentage: SIZE_7, ofWidth: false),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
            gradient: LinearGradient(
              colors: [
                CartUtilsManager.cartUtilsInstance
                    .anyProductOutOfSTock() ==
                    true
                    ? COLOR_GREY_TEXT
                    : COLOR_LIGHT_GREEN,
                CartUtilsManager.cartUtilsInstance
                    .anyProductOutOfSTock() ==
                    true
                    ? COLOR_GREY_TEXT
                    : COLOR_DARK_PRIMARY,
              ],
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                NumberFormatUtils.numberFormatUtilsInstance
                    .formatPriceWithSymbol(
                    price: CartUtilsManager.cartUtilsInstance
                        .getTotalAmountNeedToPayByCustomer()),
                style: textStyleSize14WithWhiteColor,
              ),
              Row(
                children: [
                  Text(
                    AppLocalizations
                        .of(_cartActionManagers?.context)
                        .cartpage
                        .button
                        .goToCheckOut,
                    style: textStyleSize14WithWhiteColor,
                  ),
                  SizedBox(
                    width: SIZE_5,
                  ),
                  Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                    size: SIZE_20,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    )
        : const SizedBox();
  }

//textform  field widget
  Widget _textFieldForm({TextEditingController controller,
    TextInputType keyboardType,
    String hint,
    FormFieldValidator<String> validator,
    int maxLines = 1,
    double radius,
    Widget suffixIcon,
    FocusNode currentFocusNode,
    FocusNode nextFocusNode,
    BuildContext context,
    bool enabled = true}) {
    return Padding(
      padding: EdgeInsets.only(left: SIZE_10),
      child: TextField(
        controller: controller,
        keyboardType: keyboardType,
        maxLines: maxLines,
        enabled: enabled,
        style: textStyleSize12WithGrey,
        focusNode: currentFocusNode,
        onSubmitted: (term) {},
        decoration: new InputDecoration(
          hintText: hint,
          suffixIcon: suffixIcon,
          contentPadding: EdgeInsets.only(
              right: SIZE_10, bottom: SIZE_5, left: SIZE_20, top: SIZE_20),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              borderSide: BorderSide(
                color: COLOR_GREY_PICKER,
              )),
          hintStyle: textStyleSize12WithGrey,
        ),
      ),
    );
  }

//method to show apply button
  Widget _showApplyButton() {
    return Container(
      margin: EdgeInsets.only(right: SIZE_10),
      alignment: Alignment.center,
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: context, percentage: SIZE_6, ofWidth: false),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(SIZE_30)),
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: [COLOR_DARK_PRIMARY, COLOR_PRIMARY],
        ),
      ),
      child: Text(
        AppLocalizations
            .of(_cartActionManagers?.context)
            .cartpage
            .button
            .apply,
        style: textStyleSize14WithWhiteColor,
      ),
    );
  }

  Widget _showAddressDesc() {
    return Container(
      width: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_100,
          ofWidth: true),
      padding: EdgeInsets.all(SIZE_12),
      color: COLOR_GREY_PRODUCT,
      child: Text(
        AppLocalizations
            .of(_cartActionManagers?.context)
            .cartpage
            .text
            .destination,
        style: textStyleSize12WithGrey,
      ),
    );
  }

//method to show address description view
  Widget _showAddressView() {
    return InkWell(
      onTap: () {
        _cartActionManagers?.actionToChangeAddress();
      },
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Container(
                margin: EdgeInsets.only(left: SIZE_20),
                child: Text(
                  _cartActionManagers
                      ?.cartState?.currentLocation?.currentAddress ??
                      AppLocalizations
                          .of(context)
                          .common
                          .text
                          .notAllowed,
                  style: textStyleSize12WithBlackColor,
                ),
              )),
          Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  AppLocalizations
                      .of(_cartActionManagers?.context)
                      .cartpage
                      .text
                      .change
                      .toUpperCase(),
                  style: textStyleSize14WithGreenColor,
                ),
              )),
        ],
      ),
    );
  }

//method to show instruction text form field
  Widget _showInstruction() {
    return Padding(
      padding: const EdgeInsets.only(right: SIZE_12),
      child: _textFieldForm(
          radius: SIZE_24,
          controller: _instructionController,
          keyboardType: TextInputType.text,
          maxLines: 4,
          context: _cartActionManagers?.context,
          hint: AppLocalizations
              .of(_cartActionManagers?.context)
              .cartpage
              .hint
              .addInstruction),
    );
  }

  Widget _showPaymentTitle() {
    return Container(
      width: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_100,
          ofWidth: true),
      padding: EdgeInsets.all(SIZE_12),
      color: COLOR_GREY_PRODUCT,
      child: Text(
        AppLocalizations
            .of(context)
            .cartpage
            .text
            .payments,
        style: textStyleSize12WithGrey,
      ),
    );
  }

//method to show cart details
  Widget _showCartDetails() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_12, right: SIZE_12),
      child: Card(
        elevation: ELEVATION_05,
        child: Wrap(
          children: [
            Padding(
              padding: const EdgeInsets.all(SIZE_12),
              child: Wrap(
                runSpacing: SIZE_5,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppLocalizations
                            .of(_cartActionManagers?.context)
                            .cartpage
                            .text
                            .cartValue,
                        style: textStyleSize16WithGreenColor,
                      ),
                      Text(
                        NumberFormatUtils.numberFormatUtilsInstance
                            .formatPriceWithSymbol(
                            price: CartUtilsManager.cartUtilsInstance
                                .cartAmount()),
                        style: textStyleSize16WithGreenColor,
                      ),
                    ],
                  ),
                  (CartUtilsManager.cartUtilsInstance.shopType ==
                      ShopType?.delivery)
                      ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppLocalizations
                            .of(_cartActionManagers?.context)
                            .cartpage
                            .text
                            .tipToDriver,
                        style: AppConfig
                            .of(_cartActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline4,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              _cartActionManagers?.actionToUpdateTip(
                                  add: false);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: COLOR_GREY_DEC,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(SIZE_5),
                                    bottomLeft: Radius.circular(SIZE_5)),
                              ),
                              height: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                  context:
                                  _cartActionManagers?.context,
                                  percentage: SIZE_4,
                                  ofWidth: false),
                              width: CommonUtils.commonUtilsInstance
                                  .getPercentageSize(
                                  context:
                                  _cartActionManagers?.context,
                                  percentage: SIZE_7,
                                  ofWidth: true),
                              child: Icon(
                                Icons.remove,
                                color: Colors.black,
                                size: SIZE_16,
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                context: _cartActionManagers?.context,
                                percentage: SIZE_4,
                                ofWidth: false),
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(SIZE_2),
                              child: Text(CartUtilsManager
                                  .cartUtilsInstance.tipOfDriver
                                  ?.toString() ??
                                  "0"),
                            ),
                          ),
                          InkWell(
                              onTap: () {
                                _cartActionManagers?.actionToUpdateTip(
                                    add: true);
                              },
                              child: Container(
                                height: CommonUtils.commonUtilsInstance
                                    .getPercentageSize(
                                    context:
                                    _cartActionManagers?.context,
                                    percentage: SIZE_4,
                                    ofWidth: false),
                                width: CommonUtils.commonUtilsInstance
                                    .getPercentageSize(
                                    context:
                                    _cartActionManagers?.context,
                                    percentage: SIZE_7,
                                    ofWidth: true),
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(SIZE_5),
                                      bottomRight:
                                      Radius.circular(SIZE_5)),
                                ),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: SIZE_16,
                                ),
                              ))
                        ],
                      ),
                    ],
                  )
                      : SizedBox(),
                  Visibility(
                    visible:
                    CartUtilsManager.cartUtilsInstance.couponData != null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations
                              .of(_cartActionManagers?.context)
                              .cartpage
                              .text
                              .couponDiscount,
                          style: textStyleSize16WithLightGreenColor,
                        ),
                        Text(
                          NumberFormatUtils.numberFormatUtilsInstance
                              .formatPriceWithSymbol(
                              price: (CartUtilsManager
                                  .cartUtilsInstance
                                  .getChargesResponseModel
                                  ?.data
                                  ?.discountAmount ??
                                  0.0) *
                                  -1),
                          style: textStyleSize16WithLightGreenColor,
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppLocalizations
                            .of(context)
                            .cartpage
                            .text
                            .platformCharges,
                        style: AppConfig
                            .of(_cartActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline4,
                      ),
                      Text(
                        NumberFormatUtils.numberFormatUtilsInstance
                            .formatPriceWithSymbol(
                            price: CartUtilsManager
                                .cartUtilsInstance
                                .getChargesResponseModel
                                ?.data
                                ?.platformCharge ??
                                0.0),
                        style: AppConfig
                            .of(_cartActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline4,
                      ),
                    ],
                  ),
                  Visibility(
                    visible: (CartUtilsManager.cartUtilsInstance
                        .getChargesResponseModel?.data?.gstTax!=null && CartUtilsManager?.cartUtilsInstance?.getChargesResponseModel?.data?.gstTax > 0 == true),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations
                              .of(_cartActionManagers?.context)
                              .cartpage
                              .text
                              .gst,
                          style: AppConfig
                              .of(_cartActionManagers?.context)
                              .themeData
                              .primaryTextTheme
                              .headline4,
                        ),
                        Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance
                                .formatPriceWithSymbol(price: CartUtilsManager
                                ?.cartUtilsInstance?.getChargesResponseModel
                                ?.data?.gstTax ?? 0.0)}',style: AppConfig
                            .of(_cartActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline4,),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: (
                        CartUtilsManager.cartUtilsInstance
                            .getChargesResponseModel?.data?.hst!=null &&
                        CartUtilsManager?.cartUtilsInstance?.getChargesResponseModel?.data?.hst >0 == true),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations
                              .of(_cartActionManagers?.context)
                              .cartpage
                              .text
                              .hst,
                          style: AppConfig
                              .of(_cartActionManagers?.context)
                              .themeData
                              .primaryTextTheme
                              .headline4,
                        ),
                        Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance
                                .formatPriceWithSymbol(price: (CartUtilsManager
                                .cartUtilsInstance.getChargesResponseModel?.data
                                ?.hst ?? 0.0))}',style: AppConfig
                            .of(_cartActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline4,),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: (
                        CartUtilsManager.cartUtilsInstance
                            .getChargesResponseModel?.data?.pst!=null &&
                        CartUtilsManager?.cartUtilsInstance
                        .getChargesResponseModel?.data?.pst>0 == true),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations
                              .of(_cartActionManagers?.context)
                              .cartpage
                              .text
                              .pst,
                          style: AppConfig
                              .of(_cartActionManagers?.context)
                              .themeData
                              .primaryTextTheme
                              .headline4,
                        ),
                        Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance
                                .formatPriceWithSymbol(price: (CartUtilsManager
                                .cartUtilsInstance.getChargesResponseModel?.data
                                ?.pst ?? 0.0))}',style: AppConfig
                            .of(_cartActionManagers?.context)
                            .themeData
                            .primaryTextTheme
                            .headline4,),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: (CartUtilsManager.cartUtilsInstance.shopType ==
                        ShopType?.delivery),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations
                              .of(_cartActionManagers?.context)
                              .cartpage
                              .text
                              .deliveryFee,
                          style: AppConfig
                              .of(_cartActionManagers?.context)
                              .themeData
                              .primaryTextTheme
                              .headline4,
                        ),
                        Text(
                          NumberFormatUtils.numberFormatUtilsInstance
                              .formatPriceWithSymbol(
                              price: CartUtilsManager
                                  .cartUtilsInstance
                                  .getChargesResponseModel
                                  ?.data
                                  ?.deliveryChargeForCustomer ??
                                  0.0),
                          style: AppConfig
                              .of(_cartActionManagers?.context)
                              .themeData
                              .primaryTextTheme
                              .headline4,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              color: COLOR_LIGHT_RED,
              padding: EdgeInsets.all(SIZE_10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocalizations
                        .of(_cartActionManagers?.context)
                        .cartpage
                        .text
                        .totalPayment,
                    style: AppConfig
                        .of(_cartActionManagers?.context)
                        .themeData
                        .textTheme
                        .bodyText2,
                  ),
                  Text(
                    NumberFormatUtils.numberFormatUtilsInstance
                        .formatPriceWithSymbol(
                        price: CartUtilsManager.cartUtilsInstance
                            .getTotalAmountNeedToPayByCustomer() ??
                            0.0),
                    style: AppConfig
                        .of(_cartActionManagers?.context)
                        .themeData
                        .textTheme
                        .bodyText2,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//this method is used to show user if product quantity is empty or gets decreased
  Widget _showStockEmptyOrQuantityDecreased(
      {BuildContext context, ShopItem shopItem}) {
    var _tupple = CartUtilsManager?.cartUtilsInstance
        ?.checkProductInACartOrNot(shopItem: shopItem);
    var _existInCart = _tupple?.item1;
    var _quantityAddedInCart = _tupple?.item2;
    if (shopItem?.stockQuantity == 0) {
      return Container(
          child: Text(AppLocalizations
              .of(context)
              .cartpage
              .error
              .outOfStock,
              style: textStyleSize12WithRed));
    } else if (_quantityAddedInCart != null &&
        _quantityAddedInCart > shopItem?.stockQuantity) {
      return Container(
          child: Text(
              '${AppLocalizations
                  .of(context)
                  .cartpage
                  .error
                  .quantityDecreased(
                  value: shopItem?.stockQuantity?.toString())}',
              style: textStyleSize12WithRed));
    } else {
      return SizedBox();
    }
  }

//method to show cache network image
  Widget _showCacheNetworkImage() {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      radius: SIZE_0,
      placeHolderImage: SHOP_LOGO,
      height: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_16,
          ofWidth: false),
      width: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_16,
          ofWidth: true),
      url: (CartUtilsManager
          ?.cartUtilsInstance?.shopDetailsModel?.media?.isNotEmpty ==
          true)
          ? CartUtilsManager
          ?.cartUtilsInstance?.shopDetailsModel?.media[0]?.fileName ??
          ""
          : CartUtilsManager?.cartUtilsInstance?.shopDetailsModel?.profileImage
          ?.fileName ??
          "",
    );
  }

//method to default shop logo
  Widget _showDefaultImage({String defaultImage}) {
    return SvgPicture.asset(
      defaultImage,
      width: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_6,
          ofWidth: false),
      height: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_6,
          ofWidth: false),
    );
  }

//show cache network image coming from api
  Widget _showImageComingFromApi({String image}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
        context: context,
        radius: SIZE_0,
        showProgressBarInPlaceHolder: true,
        shape: BoxShape.rectangle,
        url: image ?? "",
        height: CommonUtils?.commonUtilsInstance?.getPercentageSize(
            context: _cartActionManagers?.context,
            percentage: SIZE_8,
            ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _cartActionManagers?.context,
            percentage: SIZE_8,
            ofWidth: true));
  }

//this method is used to show remove button on cart list item
  Widget _showRemoveOption({ShopItem cartItem, BuildContext context}) {
    return InkWell(
        onTap: () {
          CartUtilsManager.cartUtilsInstance
              .removeItemFromCart(shopItem: cartItem);
          _cartActionManagers?.actionOnCartScreenStateChange(
              slotsData: _cartActionManagers?.cartState?.selectedSlotsData,
              scheduledDateTim:
              _cartActionManagers?.cartState?.scheduleDateTime);
        },
        child: Padding(
          padding: const EdgeInsets.all(SIZE_8),
          child: Text(
            AppLocalizations
                .of(context)
                .common
                .text
                .remove,
            style: textStyleSize12WithRed,
            textAlign: TextAlign.end,
          ),
        ));
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _cartActionManagers?.actionToCallApi();
  }

  checkNativePaySupport() async {
    isNativePaySupported = await StripePayment.deviceSupportsNativePay();
  }

//method to show schedule booking option
  Widget _showScheduling() {
    _scheduledDateTime = _cartActionManagers?.cartState?.scheduleDateTime !=
        null
        ? '${DateUtils.dateUtilsInstance.convertDateTimeToString(
        dateTime: _cartActionManagers?.cartState
            ?.scheduleDateTime)} ${getStartingSlots()}'
        : '${DateUtils?.dateUtilsInstance?.convertDateTimeToString(
        dateTime: DateTime.now())} ${getStartingSlots()}';

    return InkWell(
      onTap: () {
        _showScheduleDateTimePicker();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: SIZE_12),
            child: Text(
              AppLocalizations
                  ?.of(_cartActionManagers?.context)
                  ?.categoriesshoppage
                  ?.text
                  ?.schedulingAt,
              style: textStyleSize14BlackColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: SIZE_12),
            child: Text(
              _scheduledDateTime,
              style: textStyleSize14BlackColor,
            ),
          ),
        ],
      ),
    );
  }

  //method to show date tim picker
  Future<void> _showScheduleDateTimePicker() async {
    var picked = await DatePickerUtils.datePickerUtilsInstance.getDatePicker(
        context: context, selectedDate: DateTime.now().toString());
    _cartActionManagers?.actionOnCartScreenStateChange(
        scheduledDateTim:
        DateUtils.dateUtilsInstance.convertStringToDate(dateTime: picked),
        slotsData: null);
  }

  Widget _showScheduleAt() {
    return Container(
      width: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_100,
          ofWidth: true),
      padding: EdgeInsets.all(SIZE_12),
      color: COLOR_GREY_PRODUCT,
      child: Text(
        AppLocalizations
            .of(context)
            .categoriesshoppage
            .text
            .schedulingAt,
        style: textStyleSize12WithGrey,
      ),
    );
  }

  void _showSnackBarToScheduleOrder({String message}) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
        context: _cartActionManagers?.context,
        scaffoldState: _scaffoldKey?.currentState,
        message: message);
  }

  //this method is used to show slots date wise according to the date user has selected
  Widget _showBookingSlotsDateWise() {
    //List<SlotsData> slotsData = _cartActionManagers?.getDayWiseSlotsList();
    return Container(
      alignment: Alignment.topLeft,
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _cartActionManagers?.context,
          percentage: SIZE_6,
          ofWidth: false),
      child: (slotsData?.isNotEmpty == true)
          ? ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: slotsData?.length,
          itemBuilder: (BuildContext context, int index) {
            SlotsData dayWiseSlots = slotsData[index];
            return InkWell(
              onTap: () {
                //changing slots
                if (_cartActionManagers?.checkCurrentTimeIsPassedOrNot(
                    slotsData: dayWiseSlots) ==
                    true) {
                  _cartActionManagers?.actionOnCartScreenStateChange(
                      slotsData: dayWiseSlots,
                      scheduledDateTim:
                      _cartActionManagers?.cartState?.scheduleDateTime);
                } else {
                  _showSnackBarToScheduleOrder(
                      message:
                      AppLocalizations
                          .of(_cartActionManagers?.context)
                          .cartpage
                          .text
                          .slotsNotAvailable);
                }
              },
              child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(SIZE_5),
                  padding: EdgeInsets.all(SIZE_5),
                  decoration: BoxDecoration(
                      color: (_cartActionManagers
                          ?.checkCurrentTimeIsPassedOrNot(
                          slotsData: dayWiseSlots) ==
                          true)
                          ? (CartUtilsManager
                          .cartUtilsInstance.slotsData?.id ==
                          dayWiseSlots?.id)
                          ? COLOR_PRIMARY
                          : COLOR_LIGHT_RED
                          : COLOR_BORDER_GREY,
                      border: Border.all(
                          color: (CartUtilsManager
                              .cartUtilsInstance.slotsData?.id ==
                              dayWiseSlots?.id)
                              ? COLOR_BORDER_GREY
                              : COLOR_BORDER_GREY),
                      borderRadius:
                      BorderRadius.all(Radius.circular(SIZE_5))),
                  child: Text(
                    '${DateUtils.dateUtilsInstance.getTimeFormat(
                        dateTime: '${DateUtils.dateUtilsInstance
                            .convertDateTimeToString(
                            dateTime: _cartActionManagers?.cartState
                                ?.scheduleDateTime)} ${dayWiseSlots?.slotFrom}',
                        toLocal: false)}- ${DateUtils.dateUtilsInstance
                        .getTimeFormat(dateTime: '${DateUtils.dateUtilsInstance
                        .convertDateTimeToString(
                        dateTime: _cartActionManagers?.cartState
                            ?.scheduleDateTime)} ${dayWiseSlots?.slotTo}',
                        toLocal: false)}',
                    style:
                    (_cartActionManagers?.checkCurrentTimeIsPassedOrNot(
                        slotsData: dayWiseSlots) ==
                        true)
                        ? (CartUtilsManager
                        .cartUtilsInstance.slotsData?.id ==
                        dayWiseSlots?.id)
                        ? textStyleSize12WithWhite
                        : textStyleSize12WithPrimary
                        : textStyleSize12GREY,
                  )),
            );
          })
          : Container(
        alignment: Alignment.center,
        child: Text(AppLocalizations
            .of(_cartActionManagers?.context)
            .cartpage
            .text
            .noSlotsAvailableForTheDay),
      ),
    );
  }

  //method to get the default start time of first starting slot
  getStartingSlots() {
    if (CartUtilsManager.cartUtilsInstance.slotsData?.slotFrom != null) {
      return '${DateUtils.dateUtilsInstance.getTimeFormat(
          dateTime: '${DateUtils.dateUtilsInstance.convertDateTimeToString(
              dateTime: _cartActionManagers?.cartState
                  ?.scheduleDateTime)} ${CartUtilsManager.cartUtilsInstance
              .slotsData?.slotFrom}', toLocal: false)}';
    } else {
      return "";
    }
  }
}

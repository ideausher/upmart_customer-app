import 'package:user/localizations.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/shop_slots/shop_slot_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/provider/cart_provider.dart';
import 'package:user/modules/dashboard/sub_modules/cart/bloc/cart_event.dart';
import 'package:user/modules/dashboard/sub_modules/cart/bloc/cart_state.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/provider/products_provider.dart';

class CartBloc extends BlocEventStateBase<CartEvent, CartState> {
  CartBloc({bool initializing = true})
      : super(initialState: CartState.initiating());

  @override
  Stream<CartState> eventHandler(
      CartEvent event, CartState currentState) async* {
    // update home page UI
    if (event is UpdateCartPageUIEvent) {
      yield CartState.updateCart(
          currentLocation: event?.currentLocation,
          isLoading: event?.isLoading,
          scheduleDateTime: event?.scheduleDateTime,
          selectedSlotsData: event?.selectedSlotsData,
          shopSlotsResponseModel: event?.shopSlotsResponseModel,
          productsShopDetailsModel: event?.productsShopDetailsModel);
    }

    //method to get products category wise
    if (event is GetProductDetailAndChargeApiEvent) {
      ProductsShopDetailsModel _productsResponseModel;
      GetChargesResponseModel _getChargesResponseModel;
      ShopSlotsResponseModel _shopSlotsResponseModel;

      yield CartState.updateCart(
          isLoading: true,
          scheduleDateTime: event?.scheduleDateTime,
          selectedSlotsData: event?.selectedSlotsData,
          shopSlotsResponseModel: event?.shopSlotsResponseModel,
          productsShopDetailsModel: event?.productsShopDetailsModel);

      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();

      //products data on cart
      var result = await ProductPageProvider().getShopProducts(
          context: event.context,
          productsRequestModel: ProductDetailsRequestModel(
            categoryId: [],
            shopId: CartUtilsManager.cartUtilsInstance.shopDetailsModel?.id,
            lat: _currentLocation?.lat?.toString(),
            lng: _currentLocation?.lng?.toString(),
          ));

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          _productsResponseModel = ProductsShopDetailsModel.fromMap(result);
          if (_productsResponseModel?.data?.items?.isNotEmpty == true) {
            CartUtilsManager?.cartUtilsInstance?.updateLatestDataInProducts(
                latestShopItems: _productsResponseModel?.data?.items);
          }
        }
      }

      //get cart charges api calling
      var chargesResult = await CartProvider().getCartCharges(
        getCartRequestModel: GetChargesRequestModel(
            shopId: CartUtilsManager.cartUtilsInstance.shopDetailsModel?.id,
            bookingType: CartUtilsManager.cartUtilsInstance?.shopType?.value,
            addressId: int.parse(_currentLocation?.id),
            couponId:
                CartUtilsManager.cartUtilsInstance?.couponData?.id?.toString(),
            products:
                CartUtilsManager.cartUtilsInstance.convertShopToChargeModel()),
        context: event.context,
      );

      if (chargesResult != null) {
        // check result status
        if (chargesResult[ApiStatusParams.Status.value] != null &&
            chargesResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          _getChargesResponseModel =
              GetChargesResponseModel.fromMap(chargesResult);
          if (_getChargesResponseModel != null &&
              _getChargesResponseModel?.data != null) {
            CartUtilsManager.cartUtilsInstance?.getChargesResponseModel =
                _getChargesResponseModel;
          }
        }
      }

      //call get slots api
      var slotsResult = await CartProvider().getShopSlots(
        shopId: CartUtilsManager.cartUtilsInstance.shopDetailsModel?.id,
        context: event.context,
      );

      if (slotsResult != null) {
        // check result status
        if (slotsResult[ApiStatusParams.Status.value] != null &&
            slotsResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          _shopSlotsResponseModel = ShopSlotsResponseModel.fromMap(slotsResult);
          if (_shopSlotsResponseModel != null &&
              _shopSlotsResponseModel?.data != null) {
            CartUtilsManager.cartUtilsInstance?.getChargesResponseModel =
                _getChargesResponseModel;

          }
        }
      }

      yield CartState.updateCart(
          isLoading: false,
          productsShopDetailsModel: _productsResponseModel,
          scheduleDateTime: event?.scheduleDateTime,
          shopSlotsResponseModel: _shopSlotsResponseModel,
          selectedSlotsData: event?.selectedSlotsData,
          currentLocation: _currentLocation);
    }



    //calculate charges api event
    if(event is CalculateChargesEvent){
      GetChargesResponseModel _getChargesResponseModel;
      yield CartState.updateCart(
          isLoading: true,
          scheduleDateTime: event?.scheduleDateTime,
          selectedSlotsData: event?.selectedSlotsData,
          shopSlotsResponseModel: event?.shopSlotsResponseModel,
          productsShopDetailsModel: event?.productsShopDetailsModel);

      CurrentLocation _currentLocation = await FetchPrefsUtils
          .fetchPrefsUtilsInstance
          .getCurrentLocationModel();


      //get cart charges api calling
      var chargesResult = await CartProvider().getCartCharges(
        getCartRequestModel: GetChargesRequestModel(
            shopId: CartUtilsManager.cartUtilsInstance.shopDetailsModel?.id,
            bookingType: CartUtilsManager.cartUtilsInstance?.shopType?.value,
            addressId: int.parse(_currentLocation?.id),
            couponId:
            CartUtilsManager.cartUtilsInstance?.couponData?.id?.toString(),
            products:
            CartUtilsManager.cartUtilsInstance.convertShopToChargeModel()),
        context: event.context,
      );

      if (chargesResult != null) {
        // check result status
        if (chargesResult[ApiStatusParams.Status.value] != null &&
            chargesResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          _getChargesResponseModel =
              GetChargesResponseModel.fromMap(chargesResult);
          if (_getChargesResponseModel != null &&
              _getChargesResponseModel?.data != null) {
            CartUtilsManager.cartUtilsInstance?.getChargesResponseModel =
                _getChargesResponseModel;
          }
        }
      }
      yield CartState.updateCart(
          isLoading: false,
          productsShopDetailsModel: event?.productsShopDetailsModel,
          scheduleDateTime: event?.scheduleDateTime,
          shopSlotsResponseModel: event?.shopSlotsResponseModel,
          selectedSlotsData: event?.selectedSlotsData,
          currentLocation: _currentLocation);
    }

    // update home page UI
    if (event is GetShopSlotsEvent) {
      ShopSlotsResponseModel _shopSlotsResponseModel;
      GetChargesResponseModel _getChargesResponseModel;

      yield CartState.updateCart(
          currentLocation: event?.currentLocation,
          isLoading: event?.isLoading,
          scheduleDateTime: event?.scheduleDateTime,
          shopSlotsResponseModel: event?.shopSlotsResponseModel,
          productsShopDetailsModel: event?.productsShopDetailsModel);
      //call get slots api
      var slotsResult = await CartProvider().getShopSlots(
        shopId: CartUtilsManager.cartUtilsInstance.shopDetailsModel?.id,
        context: event.context,
      );

      if (slotsResult != null) {
        // check result status
        if (slotsResult[ApiStatusParams.Status.value] != null &&
            slotsResult[ApiStatusParams.Status.value] ==
                ApiStatus.Success.value) {
          _shopSlotsResponseModel = ShopSlotsResponseModel.fromMap(slotsResult);
          if (_shopSlotsResponseModel != null &&
              _shopSlotsResponseModel?.data != null) {
            CartUtilsManager.cartUtilsInstance?.getChargesResponseModel =
                _getChargesResponseModel;
          }
        }
      }

      yield CartState.updateCart(
          isLoading: false,
          productsShopDetailsModel: event?.productsShopDetailsModel,
          scheduleDateTime: event?.scheduleDateTime,
          shopSlotsResponseModel: _shopSlotsResponseModel,
          currentLocation: event?.currentLocation);
    }
  }
}

import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/shop_slots/shop_slot_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

abstract class CartEvent extends BlocEvent {
  final bool isLoading;
  final ProductsShopDetailsModel productsShopDetailsModel;
  final BuildContext context;
  final CurrentLocation currentLocation;
  final DateTime scheduleDateTime;
  final ShopSlotsResponseModel shopSlotsResponseModel;
  final SlotsData selectedSlotsData;

  CartEvent(
      {this.isLoading: false,
      this.productsShopDetailsModel,
      this.currentLocation,
      this.scheduleDateTime,
      this.shopSlotsResponseModel,
      this.selectedSlotsData,
      this.context});
}

//this event is used for updating the home page UI data
class UpdateCartPageUIEvent extends CartEvent {
  UpdateCartPageUIEvent(
      {bool isLoading,
      ProductsShopDetailsModel productsShopDetailsModel,
      BuildContext context,
      CurrentLocation currentLocation,
      ShopSlotsResponseModel shopSlotsResponseModel,
        SlotsData selectedSlotsData,
      DateTime scheduleDateTime})
      : super(
            isLoading: isLoading,
            context: context,
            productsShopDetailsModel: productsShopDetailsModel,
            currentLocation: currentLocation,
            selectedSlotsData: selectedSlotsData,
            shopSlotsResponseModel: shopSlotsResponseModel,
            scheduleDateTime: scheduleDateTime);
}

//this event is used for updating the home page UI data
class GetProductDetailAndChargeApiEvent extends CartEvent {
  GetProductDetailAndChargeApiEvent(
      {bool isLoading,
      ProductsShopDetailsModel productsShopDetailsModel,
      BuildContext context,
      CurrentLocation currentLocation,
        SlotsData selectedSlotsData,
      ShopSlotsResponseModel shopSlotsResponseModel,
      DateTime scheduleDateTime})
      : super(
            currentLocation: currentLocation,
            isLoading: isLoading,
            context: context,
            selectedSlotsData: selectedSlotsData,
            shopSlotsResponseModel: shopSlotsResponseModel,
            productsShopDetailsModel: productsShopDetailsModel,
            scheduleDateTime: scheduleDateTime);
}

//this event is used for updating the home page UI data
class GetShopSlotsEvent extends CartEvent {
  GetShopSlotsEvent(
      {bool isLoading,
      ProductsShopDetailsModel productsShopDetailsModel,
      BuildContext context,
      CurrentLocation currentLocation,
        SlotsData selectedSlotsData,
      ShopSlotsResponseModel shopSlotsResponseModel,
      DateTime scheduleDateTime})
      : super(
            currentLocation: currentLocation,
            isLoading: isLoading,
            context: context,
            selectedSlotsData: selectedSlotsData,
            shopSlotsResponseModel: shopSlotsResponseModel,
            productsShopDetailsModel: productsShopDetailsModel,
            scheduleDateTime: scheduleDateTime);
}

//this event is used for calculate charges event
class CalculateChargesEvent extends CartEvent {
  CalculateChargesEvent(
      {bool isLoading,
        ProductsShopDetailsModel productsShopDetailsModel,
        BuildContext context,
        CurrentLocation currentLocation,
        SlotsData selectedSlotsData,
        ShopSlotsResponseModel shopSlotsResponseModel,
        DateTime scheduleDateTime})
      : super(
      currentLocation: currentLocation,
      isLoading: isLoading,
      context: context,
      selectedSlotsData: selectedSlotsData,
      shopSlotsResponseModel: shopSlotsResponseModel,
      productsShopDetailsModel: productsShopDetailsModel,
      scheduleDateTime: scheduleDateTime);
}



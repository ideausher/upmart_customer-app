import 'package:flutter/material.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/shop_slots/shop_slot_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

class CartState extends BlocState {
  CartState({
    this.isLoading: false,
    this.productsShopDetailsModel,
    this.currentLocation,
    this.scheduleDateTime,
    this.shopSlotsResponseModel,
    this.selectedSlotsData
  }) : super(isLoading);

  final bool isLoading;
  final ProductsShopDetailsModel productsShopDetailsModel;
  final ShopSlotsResponseModel shopSlotsResponseModel;
  final CurrentLocation currentLocation;
  final DateTime scheduleDateTime;
  final SlotsData selectedSlotsData;


  // used for update cart page state
  factory CartState.updateCart({
    bool isLoading,
    ProductsShopDetailsModel productsShopDetailsModel,
    CurrentLocation currentLocation,
    DateTime scheduleDateTime,
    ShopSlotsResponseModel shopSlotsResponseModel,
    SlotsData selectedSlotsData
  }) {
    return CartState(
      isLoading: isLoading,
      productsShopDetailsModel: productsShopDetailsModel,
      currentLocation: currentLocation,
      scheduleDateTime:scheduleDateTime,
      shopSlotsResponseModel: shopSlotsResponseModel,
        selectedSlotsData: selectedSlotsData
    );
  }

  factory CartState.initiating() {
    return CartState(
      isLoading: false,
    );
  }
}

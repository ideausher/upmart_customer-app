import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:user/modules/address/routes/address_routes.dart';
import 'package:user/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:user/modules/common/constants/color_constants.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/enum/enums.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/coupon_listing/coupon_routes.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/shop_slots/shop_slot_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/bloc/cart_bloc.dart';
import 'package:user/modules/dashboard/sub_modules/cart/bloc/cart_event.dart';
import 'package:user/modules/dashboard/sub_modules/cart/bloc/cart_state.dart';
import 'package:user/modules/dashboard/sub_modules/cart/manager/cart_utils_manager.dart';
import 'package:user/modules/stripe/customer/stripe_customer_routes.dart';

class CartActionManagers {
  BuildContext context;

  // bloc
  CartBloc cartBloc = CartBloc();
  CartState cartState;

  // action to call api
  actionToCallApi() {
    if (CartUtilsManager.cartUtilsInstance.shopItems?.isNotEmpty == true) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((value) {
        cartBloc.emitEvent(GetProductDetailAndChargeApiEvent(
            productsShopDetailsModel: cartState?.productsShopDetailsModel,
            context: context,
            scheduleDateTime: cartState?.scheduleDateTime ?? DateTime.now(),
            isLoading: true));
      });
    }
  }


  //call get charges api
  actionOnIncrementDecrementOfProduct(
      {DateTime scheduledDateTim, SlotsData slotsData}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) {
      cartBloc.emitEvent(CalculateChargesEvent(
          productsShopDetailsModel: cartState?.productsShopDetailsModel,
          context: context,
          selectedSlotsData: slotsData,
          scheduleDateTime: scheduledDateTim ?? cartState?.scheduleDateTime,
          shopSlotsResponseModel: cartState?.shopSlotsResponseModel,
          currentLocation: cartState?.currentLocation,
          isLoading: true));
    });



  }

  //action to update
  actionOnCartScreenStateChange(
      {DateTime scheduledDateTim, SlotsData slotsData}) {
    cartBloc.emitEvent(UpdateCartPageUIEvent(
        productsShopDetailsModel: cartState?.productsShopDetailsModel,
        context: context,
        selectedSlotsData: slotsData,
        scheduleDateTime: scheduledDateTim ?? cartState?.scheduleDateTime,
        shopSlotsResponseModel: cartState?.shopSlotsResponseModel,
        currentLocation: cartState?.currentLocation,
        isLoading: false));
  }

  // action to update
  actionToUpdateTip({bool add}) {
    if (add) {
      if (CartUtilsManager.cartUtilsInstance.tipOfDriver != null) {
        CartUtilsManager.cartUtilsInstance.tipOfDriver =
            CartUtilsManager.cartUtilsInstance.tipOfDriver + 1;
      } else {
        CartUtilsManager.cartUtilsInstance.tipOfDriver = 1;
      }
    } else {
      if (CartUtilsManager.cartUtilsInstance.tipOfDriver != null &&
          CartUtilsManager.cartUtilsInstance.tipOfDriver > 0) {
        CartUtilsManager.cartUtilsInstance.tipOfDriver =
            CartUtilsManager.cartUtilsInstance.tipOfDriver - 1;
      }
    }
    actionOnCartScreenStateChange(scheduledDateTim: cartState?.scheduleDateTime,
        slotsData: cartState?.selectedSlotsData);
  }

  // action on tap of address
  actionToChangeAddress() async {
    AuthResponseModel _authResponseModel =
    await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();
    _authResponseModel = AuthResponseModel.fromMap(_authResponseModel?.toMap());
    _authResponseModel?.popScreen = true;
    var _addressResult = await NavigatorUtils.navigatorUtilsInstance
        .navigatorPushedNameResult(context, AddressRoutes.ADDRESS_LIST,
        dataToBeSend: _authResponseModel);

    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) {
      actionToCallApi();
    });
  }

  // action to update coupon
  actionToChooseCoupon() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((value) async {
      var result = await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(
          context, CouponRoutes.COUPON_LISTING_SCREEN);
      if (CartUtilsManager.cartUtilsInstance.couponData != null)
        actionToCallApi();
    });
  }

  // action to navigate to stripe
  actionToMoveStripe({String note, bool isNativePaySupported}) async {
    if (CartUtilsManager.cartUtilsInstance.anyProductOutOfSTock() == false) {
      CartUtilsManager.cartUtilsInstance.notes = note;
      var result = await NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedNameResult(context,
          StripeCustomerRoutes.STRIPE_CUSTOMER_ORDER_LISTING_SCREEN,
          dataToBeSend: isNativePaySupported);
      actionToCallApi();
    }
  }

  //method to get slots list according to the day to show in listview
  List<SlotsData> getDayWiseSlotsList() {
    List<SlotsData> slotsData = new List();
    if (cartState?.shopSlotsResponseModel?.data?.isNotEmpty == true) {
      slotsData = CartUtilsManager.cartUtilsInstance.getDayWiseSlots(
          slotsData: cartState?.shopSlotsResponseModel?.data,
          dateTime: DateUtils.dateUtilsInstance.convertStringToDate(
              dateTime: CartUtilsManager.cartUtilsInstance.scheduleDateTime
                  ?.toString() ??
                  DateTime.now()?.toString()));

      if (slotsData?.isNotEmpty == true) {
        if (cartState?.selectedSlotsData != null) {
          CartUtilsManager.cartUtilsInstance.slotsData =
              cartState?.selectedSlotsData;
        } else {
          CartUtilsManager.cartUtilsInstance.slotsData =
              getAvailableSlots(slotsData);
        }
      }
      //in case of empty slots
      else {
        CartUtilsManager.cartUtilsInstance.slotsData =
            cartState?.selectedSlotsData;
      }
    }
    return slotsData;
  }

  //get current time condition
  bool checkCurrentTimeIsPassedOrNot({SlotsData slotsData}) {
    DateTime dateTime = DateUtils.dateUtilsInstance.convertStringToDateTime(
        dateTime:
        '${DateUtils.dateUtilsInstance.convertDateTimeToString(
            dateTime: cartState?.scheduleDateTime)} ${slotsData?.slotTo}');
    if (dateTime?.isAfter(DateTime.now()) == true)
      return true;
    else
      return false;
  }

  //method to get available slot
  SlotsData getAvailableSlots(List<SlotsData> slotsData) {
    for (var data in slotsData) {
      if (checkCurrentTimeIsPassedOrNot(slotsData: data) == true) {
        return data;
      }
    }
  }
}

import 'package:tuple/tuple.dart';
import 'package:user/modules/common/date/date_utils.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/fetch_prefs_utils.dart';
import 'package:user/modules/coupon_listing/api/coupon_list/model/coupon_list/coupon_listing_response_model.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_request_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/cart_charges_model/cart_charges_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/cart/api/model/shop_slots/shop_slot_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/api/model/shops_list/shops_list_response_model.dart';
import 'package:user/modules/dashboard/sub_modules/product/api/model/product_details_response_model.dart';

class CartUtilsManager {
  static CartUtilsManager _cartUtilsManager = CartUtilsManager();

  static CartUtilsManager get cartUtilsInstance => _cartUtilsManager;
  ShopDetailsModel shopDetailsModel;
  DateTime scheduleDateTime;
  SlotsData slotsData;
  num totalProductGst = 0.0;
  ShopType shopType;
  List<ShopItem> shopItems = List<ShopItem>();
  String notes = "";
  CouponData couponData;
  num tipOfDriver = 0.0;
  GetChargesResponseModel getChargesResponseModel;

  // check product exist of not in a cart and return quantity
  //to get total number of quantity in the cart
  Tuple2<bool, int> checkProductInACartOrNot({ShopItem shopItem}) {
    if (shopItems?.isNotEmpty == true) {
      ShopItem _cartItem;
      for (int index = 0; index < shopItems?.length; index++) {
        ShopItem _shopItem = shopItems[index];
        if (_shopItem?.id == shopItem.id) {
          _cartItem = _shopItem;
          break;
        }
      }
      if (_cartItem != null) {
        return Tuple2<bool, int>(true, _cartItem?.itemQuantity);
      } else {
        return Tuple2<bool, int>(false, 0);
      }
    } else {
      return Tuple2<bool, int>(false, 0);
    }
  }

  // add or remove quantity
  addOrRemoveQuantity(
      {ShopItem shopItem,
      num quantity,
      bool add,
      ShopDetailsModel shopDetailsModel}) {
    if (shopItems?.isNotEmpty == true) {
      bool _productExist = false;
      ShopItem _selectedShopItem;
      for (int index = 0; index < shopItems?.length; index++) {
        ShopItem _shopItem = shopItems[index];
        if (_shopItem?.id == shopItem.id) {
          _productExist = true;
          if (add) {
            // add
            _shopItem?.itemQuantity = _shopItem?.itemQuantity + 1;
          } else {
            // remove
            _shopItem?.itemQuantity = _shopItem?.itemQuantity - 1;
          }
          this.shopDetailsModel = shopDetailsModel;
          _selectedShopItem = _shopItem;
          break;
        }
      }

      if (_productExist == false) {
        // newly  product added
        ShopItem _item = ShopItem.fromJson(shopItem?.toJson());
        _item.itemQuantity = 1;
        shopItems.add(_item);
        this.shopDetailsModel = shopDetailsModel;
      } else if (add == false &&
          _selectedShopItem != null &&
          _selectedShopItem?.itemQuantity == 0) {
        // if quanity become 0 ;
        removeItemFromCart(shopItem: _selectedShopItem);
      }
    } else if (add) {
      // newly  product added and not any value in cart yet
      ShopItem _item = ShopItem.fromJson(shopItem?.toJson());
      _item.itemQuantity = 1;
      shopItems.add(_item);
      this.shopDetailsModel = shopDetailsModel;
    }
  }

  // can add quantity
  bool canAddQuantity({ShopItem shopItem}) {
    bool _canAdd = true;
    for (int index = 0; index < shopItems?.length; index++) {
      ShopItem _shopItem = shopItems[index];
      if (_shopItem?.id == shopItem.id) {
        if (_shopItem?.itemQuantity >= shopItem?.stockQuantity) {
          _canAdd = false;
        }
      }
    }
    return _canAdd;
  }

  //method to check whether user address is added proceed him to add quantity in cart else show message  to add address
  Future<bool> checkAddressIsAdded() async {
    CurrentLocation _currentLocation =
        await FetchPrefsUtils.fetchPrefsUtilsInstance.getCurrentLocationModel();
    if (_currentLocation?.id?.isNotEmpty == true)
      return true;
    else
      return false;
  }

// delete
  removeItemFromCart({ShopItem shopItem}) {
    List<ShopItem> _shopItems = List<ShopItem>();
    for (int index = 0; index < shopItems?.length; index++) {
      ShopItem _shopItem = shopItems[index];
      if (_shopItem?.id != shopItem.id) {
        _shopItems?.add(_shopItem);
      }
    }
    shopItems?.clear();
    shopItems?.addAll(_shopItems);

    // if shop list become empty then clear data
    if (shopItems?.isNotEmpty == true) {
    } else {
      clearData(makeScheduleTimeEmpty: false);
    }
  }

// update product data
  updateLatestDataInProducts({List<ShopItem> latestShopItems}) {
    if (latestShopItems?.isNotEmpty == true && shopItems?.isNotEmpty == true) {
      for (int index = 0; index < shopItems?.length; index++) {
        ShopItem _shopItem = shopItems[index];

        for (int indexlatestShopItems = 0;
            indexlatestShopItems < latestShopItems?.length;
            indexlatestShopItems++) {
          ShopItem _shopLatestItem = latestShopItems[indexlatestShopItems];

          if (_shopItem?.id == _shopLatestItem.id) {
            ShopItem _item = ShopItem.fromJson(_shopLatestItem?.toJson());
            _item?.itemQuantity = _shopItem?.itemQuantity;

            shopItems[index] = _item;
            break;
          }
        }
      }
    }
  }

  // used to get cart amount
  num cartAmount() {
    num _price;
    for (int index = 0; index < shopItems?.length; index++) {
      ShopItem _shopItem = shopItems[index];
      if (_price == null) {
        _price = _shopItem?.itemQuantity * _shopItem?.discountedPrice;
      } else {
        _price = _price + _shopItem?.itemQuantity * _shopItem?.discountedPrice;
      }
    }
    return _price;
  }

// total amount
  num getTotalAmountNeedToPayByCustomer() {
    num _cartAmount = cartAmount();

    return _cartAmount +
        (tipOfDriver ?? 0.0) +
        (getChargesResponseModel?.data?.deliveryChargeForCustomer ?? 0.0) +
        (getChargesResponseModel?.data?.platformCharge ?? 0.0) +
        (getChargesResponseModel?.data?.hst ?? 0.0) +
        (getChargesResponseModel?.data?.gstTax ?? 0.0)+
        (getChargesResponseModel?.data?.pst ?? 0.0) -
        (getChargesResponseModel?.data?.discountAmount ?? 0.0);
  }

  clearData({bool makeScheduleTimeEmpty = true}) {
    shopItems?.clear();
    shopDetailsModel = null;
    couponData = null;
    tipOfDriver = 0.0;
    getChargesResponseModel = null;
    notes = "";
    totalProductGst = 0.0;
    slotsData = null;
    if (makeScheduleTimeEmpty) scheduleDateTime = null;
  }

  // used to convert model for charge api
  List<Product> convertShopToChargeModel() {
    List<Product> _products = List<Product>();
    shopItems?.forEach((element) {
      _products.add(Product(id: element?.id, quantity: element?.itemQuantity));
    });
    return _products;
  }

  // any product out of stock
  bool anyProductOutOfSTock() {
    bool _anyProductOutOfStock = false;
    shopItems?.forEach((element) {
      if (element?.itemQuantity > element?.stockQuantity) {
        _anyProductOutOfStock = true;
      }
    });
    return _anyProductOutOfStock;
  }

  //show slots day wise
  List<SlotsData> getDayWiseSlots(
      {List<SlotsData> slotsData, DateTime dateTime}) {
    List<SlotsData> slotsDataDayWise = new List();

    //insert data according to the day
    for (var slots in slotsData) {
      if (slots.day == dateTime?.weekday) {
        slotsDataDayWise.add(slots);
      }
    }
    return slotsDataDayWise;
  }

  //calculate total gst applied to a prodcut
  num calculateTotalGst({List<ShopItem> shopItems}) {
    num totalGST = 0.0;
    for (var gst in shopItems) {
      if (gst?.gstEnabled == 1) {
        totalGST = totalGST + gst.gstPrice;
      }
    }
    totalProductGst = totalGST;
    return totalGST;
  }
}

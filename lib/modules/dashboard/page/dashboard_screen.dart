import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:user/localizations.dart';
import 'package:user/modules/auth/page/send_otp/page/send_otp_page.dart';
import 'package:user/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/common_widget/common_bottom_appbar.dart';
import 'package:user/modules/common/model/user_current_location_model.dart';
import 'package:user/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/common/utils/network_connectivity_utils.dart';
import 'package:user/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:user/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:user/modules/dashboard/bloc/dashboard_state.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/dashboard/manager/dashboard_action_manager.dart';
import 'package:user/modules/dashboard/sub_modules/cart/page/cart_page.dart';
import 'package:user/modules/dashboard/sub_modules/category_shop/page/categories_shop_page.dart';
import 'package:user/modules/dashboard/sub_modules/profile/page/user_profile_page.dart';

class DashboardScreen extends StatefulWidget {
  BuildContext context;
  CurrentLocation currentLocation;

  DashboardScreen(this.context) {
    currentLocation = ModalRoute.of(context).settings.arguments;
  }

  @override
  DashboardScreenState createState() => DashboardScreenState();
}

class DashboardScreenState extends State<DashboardScreen>
    with WidgetsBindingObserver {
  // initialise scaffold key
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // initialise dashboard manager
  DashboardActionManager _dashboardActionManager = DashboardActionManager();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    checkLocationPermissionEnabledOrNot(widget?.context);
    _dashboardActionManager.context = widget.context;
    _dashboardActionManager?.actionToGetAuthenticationData(
        bottomBarEnum: BottomBarEnum.Home);
  }

  @override
  void dispose() {
    super.dispose();
    _dashboardActionManager?.dashboardBloc?.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.resumed) {
      checkLocationPermissionEnabledOrNot(widget?.context);
    }
  }

  @override
  Widget build(BuildContext context) {
    _dashboardActionManager.context = context;
    return BlocEventStateBuilder<DashboardState>(
      bloc: _dashboardActionManager?.dashboardBloc,
      builder: (BuildContext buildContext, DashboardState dashboardState) {
        _dashboardActionManager.context = context;
        if (dashboardState != null &&
            _dashboardActionManager?.dashboardState != dashboardState) {
          _dashboardActionManager?.dashboardState = dashboardState;
          _dashboardActionManager?.actionOnDashBoardStateChanged();
        }
        // main ui started
        return WillPopScope(
          onWillPop: () {
            _dashboardActionManager?.actionOnBackPressed();
          },
          child: ModalProgressHUD(
            inAsyncCall:
            _dashboardActionManager?.dashboardState?.isLoading ?? false,
            child: Scaffold(
              bottomNavigationBar: _getBottomAppBar(),
              key: _scaffoldKey,
              backgroundColor:
              AppConfig.of(buildContext).themeData.backgroundColor,

              body: ModalProgressHUD(
                //TODO done changes for guest user later uncomment
                inAsyncCall: false
                /* dashboardState?.authResponseModel?.userData == null*/,
                child: showView(
                    context:
                    context) /*dashboardState?.authResponseModel?.userData != null
                    ? showView(context: context)
                    : const SizedBox()*/
                ,
              ),
            ),
          ),
        );
      },
    );
  }

  // used for the bottom appbar
  Widget _getBottomAppBar() {
    return CommonBottomAppBar(
      onTab: (enumType) {
        NetworkConnectionUtils.networkConnectionUtilsInstance
            .getConnectivityStatus(_dashboardActionManager?.context,
            showNetworkDialog: true)
            .then(
              (onValue) {
            if (onValue) {
              _dashboardActionManager?.actionToChangeTabs(
                  bottomBarEnum: enumType);
            }
          },
        );
      },
      bottomEnumType: _dashboardActionManager?.dashboardState?.bottomEnumType,
    );
  }

  // used to show screen view
  Widget showView({BuildContext context}) {
    if (_dashboardActionManager?.dashboardState?.bottomEnumType ==
        BottomBarEnum.Home) {
      return CategoriesShopPage(
        context,
        currentLocation: widget?.currentLocation,
      );
    } else if (_dashboardActionManager?.dashboardState?.bottomEnumType ==
        BottomBarEnum.Cart) {
      return (_dashboardActionManager
          ?.dashboardState?.authResponseModel?.userData !=
          null)
          ? CartPage(
        context: context,
      )
          : SendOtpPage(
        context: context,
        isGuestUser: true,
        showBackButton: false,
      );
    } else if (_dashboardActionManager?.dashboardState?.bottomEnumType ==
        BottomBarEnum.Profile) {
      return (_dashboardActionManager
          ?.dashboardState?.authResponseModel?.userData !=
          null)
          ? UserProfilePage(context,
          authResponseModel: _dashboardActionManager
              ?.dashboardState?.authResponseModel, backCallBack: () {
            _dashboardActionManager?.actionOnBackPressed();
          })
          : SendOtpPage(
        context: context,
        isGuestUser: true,
        showBackButton: false,
      );
    } else {
      return const SizedBox();
    }
  }

  Future<void> checkLocationPermissionEnabledOrNot(BuildContext context) async {
    LocationPermission permission = await CurrentLocationManger
        .locationMangerInstance
        .permissionEnabledOrNot();
    if (permission == LocationPermission.deniedForever ||
        permission == LocationPermission.denied) {
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
          context: context,
          barrierDismissible: false,
          dismissOnHardwareButtonClick: true,
          title: AppLocalizations.of(context).locationAlert.title.permissionAlert,
          // negativeButton: AppLocalizations.of(context).common.text.ok,
          positiveButton:AppLocalizations.of(context).locationAlert.text.setting,
          subTitle:AppLocalizations.of(context).locationAlert.subtitle.locationPermission,
          // onNegativeButtonTab: () {
          //   NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          // },
          onPositiveButtonTab: () async {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            if (Platform.isIOS)
              AppSettings.openLocationSettings();
            else
              AppSettings.openAppSettings();
          });
    }
  }
}

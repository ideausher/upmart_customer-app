import 'package:flutter/cupertino.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/auth/auth_bloc/auth_state.dart';
import 'package:user/modules/common/utils/navigator_utils.dart';
import 'package:user/modules/dashboard/bloc/dashboard_bloc.dart';
import 'package:user/modules/dashboard/bloc/dashboard_event.dart';
import 'package:user/modules/dashboard/bloc/dashboard_state.dart';
import 'package:user/modules/dashboard/enums/dashboard_enums.dart';
import 'package:user/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';

class DashboardActionManager {
  BuildContext context;

  // current screen bloc
  DashboardBloc dashboardBloc = DashboardBloc();
  DashboardState dashboardState;

  // action to get authentication data
  actionToGetAuthenticationData({BottomBarEnum bottomBarEnum}) {
    dashboardBloc.emitEvent(
      GetAuthenticationDataEvent(
        bottomEnumType: bottomBarEnum ?? BottomBarEnum.Home,
        context: context,
      ),
    );
  }

  // action to change tabs
  actionToChangeTabs({BottomBarEnum bottomBarEnum}) {
    dashboardBloc.emitEvent(
      SelectBottomTabEvent(
          bottomEnumType: bottomBarEnum,
          context: context,
          authResponseModel: dashboardState?.authResponseModel),
    );
  }

  // action on back pressed
  actionOnBackPressed() {
    if (dashboardState?.bottomEnumType != BottomBarEnum.Home) {
      actionToChangeTabs(bottomBarEnum: BottomBarEnum.Home);
    } else {
      NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
    }
    //we need to return a future
    return Future.value(false);
  }

  void actionOnDashBoardStateChanged() {
    if (dashboardState?.isLoading == false) {
      if (dashboardState?.forceUpdateResponseModel != null &&
          dashboardState?.forceUpdateResponseModel?.data != null &&
          dashboardState?.needToUpdate != null &&
          dashboardState?.needToUpdate == true) {
        WidgetsBinding.instance.addPostFrameCallback(
          (_) {
            ForceUpdateActionManager _forceUpdateActionManager =
                ForceUpdateActionManager();
            _forceUpdateActionManager.showForceUpdateDialog(
              context: context,
              forcefullyUpdate: (dashboardState
                          ?.forceUpdateResponseModel?.data?.forceUpdate ==
                      1)
                  ? true
                  : false,
              message: dashboardState?.forceUpdateResponseModel?.data?.message,
            );
          },
        );
      }
    }
  }
}

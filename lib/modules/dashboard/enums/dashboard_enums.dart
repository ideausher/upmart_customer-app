enum BottomBarEnum { Home, Cart, Profile }

enum Selection { selected, unselected }
enum ShopType { delivery, pickUp, both }
enum ShopAvailability { available, notAvailable }

extension ShopAvailabilityExtension on ShopAvailability {
  int get value {
    switch (this) {
      case ShopAvailability.available:
        return 1;
      case ShopAvailability.notAvailable:
        return 0;
        default:
        return null;
    }
  }
}

extension SelectionExtension on Selection {
  int get value {
    switch (this) {
      case Selection.selected:
        return 1;
      case Selection.unselected:
        return 0;

      default:
        return null;
    }
  }
}

extension ShopTypeExtension on ShopType {
  int get value {
    switch (this) {
      case ShopType.delivery:
        return 2;
      case ShopType.pickUp:
        return 1;
      case ShopType.both:
        return 3;
      default:
        return null;
    }
  }
}

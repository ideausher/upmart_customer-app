// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localizations.dart';

// **************************************************************************
// SheetLocalizationGenerator
// **************************************************************************

class AppLocalizations {
  AppLocalizations(this.locale) : this.labels = languages[locale];

  final Locale locale;

  static final Map<Locale, AppLocalizations_Labels> languages = {
    Locale.fromSubtags(languageCode: "en"): AppLocalizations_Labels(
      common: AppLocalizations_Labels_Common(
        title: AppLocalizations_Labels_Common_Title(
          appName: "Upmart Customer",
          appSubtitle: "Fast . Simple . Ready",
        ),
        text: AppLocalizations_Labels_Common_Text(
          noDataFound: "No Data Found",
          no: "No",
          yes: "Yes",
          km: "km",
          save: "Save",
          done: "Done",
          ok: "Ok",
          noFaq: "No FAQs",
          pickUp: "Pick Up",
          checkInternetConnection: "Please check your internet connection",
          oderPlaced: "Order Placed",
          error: "Error",
          success: "Success",
          orderRejected: "Order Rejected",
          orderConfirmed: "Order Confirmed",
          driverAssigned: "Driver Assigned",
          dispatched: "Dispatched",
          delivered: "Delivered",
          remove: "Remove",
          notAllowed: "N/A",
          orderCancelled: "Order Cancelled",
        ),
        error: AppLocalizations_Labels_Common_Error(
          somethingWentWrong: "Something went wrong",
        ),
        button: AppLocalizations_Labels_Common_Button(
          submit: "Submit",
          continueText: "Continue",
        ),
      ),
      intropage: AppLocalizations_Labels_Intropage(
        title: AppLocalizations_Labels_Intropage_Title(
          searchItemOnline: "Search for any\nitem online",
          placeOrder: "Place the order\nonline instantly",
          trackYourOrder: "Live track your\nOrder",
        ),
        subtitle: AppLocalizations_Labels_Intropage_Subtitle(
          quickSearch:
              "Quick search and add filters for\nyour comfort of search.",
          searchAndPlaceOrder:
              "Search and place the order\ninstant in few steps.",
          realTimeTracking:
              "See a real time tracking of your\ncourier on the map during\ndelivery.",
        ),
      ),
      sendotppage: AppLocalizations_Labels_Sendotppage(
        text: AppLocalizations_Labels_Sendotppage_Text(
          iAccept: "I accept the",
          termsAndPolicies: "I agree to the Terms and Policies",
          ofLimitLess: "of\nLimitLess",
          signIn: "Sign In / Sign Up",
          guestLogin: " Log In As Guest",
        ),
        button: AppLocalizations_Labels_Sendotppage_Button(
          proceed: "Proceed",
        ),
        error: AppLocalizations_Labels_Sendotppage_Error(
          validPhoneNumber: "Please enter a valid phone number",
          acceptTermsAndServices: "Please accept terms and services",
          numberAlreadyExist: "Number already registered on driver app",
          youAreBlocked: "You are blocked by admin",
        ),
        hint: AppLocalizations_Labels_Sendotppage_Hint(
          phoneNumber: "Enter Mobile Number",
        ),
      ),
      verifyotppage: AppLocalizations_Labels_Verifyotppage(
        title: AppLocalizations_Labels_Verifyotppage_Title(
          verifyMobileNumber: "Verify mobile number",
          verifyEmail: "Enter email to verify",
        ),
        subtitle: AppLocalizations_Labels_Verifyotppage_Subtitle(
          enterOtp: "Enter the OTP sent to",
        ),
        text: AppLocalizations_Labels_Verifyotppage_Text(
          changeNumber: "Change number",
          resend: "Resend",
        ),
        button: AppLocalizations_Labels_Verifyotppage_Button(
          verify: "Verify",
        ),
        error: AppLocalizations_Labels_Verifyotppage_Error(
          emptyOtp: "Please fill the OTP",
        ),
      ),
      enterdetailspage: AppLocalizations_Labels_Enterdetailspage(
        title: AppLocalizations_Labels_Enterdetailspage_Title(
          enterDetails: "Enter Details",
        ),
        subtitle: AppLocalizations_Labels_Enterdetailspage_Subtitle(
          enterdetailstoProceed: "Enter the details to proceed further.",
        ),
        hint: AppLocalizations_Labels_Enterdetailspage_Hint(
          enterName: "Enter Name",
        ),
        error: AppLocalizations_Labels_Enterdetailspage_Error(
          emptyName: "Please enter your name",
          minNameLength: "Name must have atleast 3 charecters",
        ),
      ),
      loginSuccessfulPage: AppLocalizations_Labels_LoginSuccessfulPage(
        title: AppLocalizations_Labels_LoginSuccessfulPage_Title(
          loginSuccess: "Login Successful!",
        ),
      ),
      enablelocationpage: AppLocalizations_Labels_Enablelocationpage(
        title: AppLocalizations_Labels_Enablelocationpage_Title(
          enableLocation: "Enable Location",
        ),
        subtitle: AppLocalizations_Labels_Enablelocationpage_Subtitle(
          giveLocationAccess: "Give access to your location",
        ),
        button: AppLocalizations_Labels_Enablelocationpage_Button(
          allow: "Allow",
        ),
        error: AppLocalizations_Labels_Enablelocationpage_Error(
          pleaseGrantPermission: "Please grant permission",
          permissionDenied:
              "Permission denied.Please enable it from app settings",
          locationNotAllowed: "Please enable your location",
        ),
      ),
      profilepage: AppLocalizations_Labels_Profilepage(
        appbartitle: AppLocalizations_Labels_Profilepage_Appbartitle(
          profile: "Profile",
        ),
        text: AppLocalizations_Labels_Profilepage_Text(
          myOrders: "My Orders",
          favouriteText: "See your favourite shops",
          inviteFriendsText: "Invite your friends with your referral code.",
          inviteFriends: "Invite Friends",
          checkOrders: "Check recent and previous Orders",
          myAddresses: "My Addresses",
          yourAddresses: "Your Addresses",
          termsAndPolicies: "Terms and Policies",
          thingsYoumayknow: "Things You may want to know",
          fAQs: "FAQs",
          freqAskedQue: "Frequently asked question",
          helpAndSupport: "Help & Support",
          getSupport: "Get support from us",
          logout: "Log out",
        ),
        button: AppLocalizations_Labels_Profilepage_Button(
          editProfile: "Edit Profile",
        ),
      ),
      editprofilepage: AppLocalizations_Labels_Editprofilepage(
        appbartitle: AppLocalizations_Labels_Editprofilepage_Appbartitle(
          editProfile: "Edit Profile",
        ),
        title: AppLocalizations_Labels_Editprofilepage_Title(
          personalInfo: "Personal Info",
        ),
        text: AppLocalizations_Labels_Editprofilepage_Text(
          name: "Name",
          phoneNumber: "Phone Number",
          email: "Email",
        ),
      ),
      editphoneoremailpage: AppLocalizations_Labels_Editphoneoremailpage(
        title: AppLocalizations_Labels_Editphoneoremailpage_Title(
          enterNumbertoVerify: "Enter number to verify",
          enterEmailtoVerify: "Enter email to verify",
        ),
        button: AppLocalizations_Labels_Editphoneoremailpage_Button(
          sendOtp: "Send Otp",
        ),
        hint: AppLocalizations_Labels_Editphoneoremailpage_Hint(
          enterEmail: "Enter Email ",
        ),
        error: AppLocalizations_Labels_Editphoneoremailpage_Error(
          enterEmail: "Please enter your email",
          validEmail: "Enter valid email",
        ),
      ),
      addresslistingpage: AppLocalizations_Labels_Addresslistingpage(
        appbartitle: AppLocalizations_Labels_Addresslistingpage_Appbartitle(
          myAddress: "My Address",
        ),
        text: AppLocalizations_Labels_Addresslistingpage_Text(
          edit: "Edit",
          addressType: "1",
          setPrimary: "Set Primary",
          noAddressAdded: "No Address Added yet",
          makePrimary: "Make Primary",
          primary: "PRIMARY",
          addAddres: "Add Address",
        ),
      ),
      deliveryaddresspage: AppLocalizations_Labels_Deliveryaddresspage(
        title: AppLocalizations_Labels_Deliveryaddresspage_Title(
          selectAddress: "Select Address",
          yourAddress: "Your Address",
        ),
      ),
      setaddresspage: AppLocalizations_Labels_Setaddresspage(
        error: AppLocalizations_Labels_Setaddresspage_Error(
          emptyAddress: "Please select a address first.",
        ),
        hint: AppLocalizations_Labels_Setaddresspage_Hint(
          emptyAddress: "Search for location",
        ),
        button: AppLocalizations_Labels_Setaddresspage_Button(
          save: "Save",
        ),
      ),
      categoriesshoppage: AppLocalizations_Labels_Categoriesshoppage(
        title: AppLocalizations_Labels_Categoriesshoppage_Title(
          categories: "Categories",
          shop: "Shops",
        ),
        text: AppLocalizations_Labels_Categoriesshoppage_Text(
          home: "Home",
          delivery: "Delivery",
          takeAway: "Take Away",
          schedulingAt: "Schedule At",
          noScheduling: "No Scheduling",
          day: "Day",
          noShopFound: "No Shop Found",
          time: "Time",
        ),
        button: AppLocalizations_Labels_Categoriesshoppage_Button(
          schedule: "Schedule",
        ),
        hint: AppLocalizations_Labels_Categoriesshoppage_Hint(
          searchProduct: "Search Products",
        ),
      ),
      shopdetailspage: AppLocalizations_Labels_Shopdetailspage(
        button: AppLocalizations_Labels_Shopdetailspage_Button(
          viewCart: "View Cart ",
        ),
        text: AppLocalizations_Labels_Shopdetailspage_Text(
          items: "Items",
          shopItem: "Item",
          noProducts: "No products are added in this shop.",
          off: "OFF",
        ),
        error: AppLocalizations_Labels_Shopdetailspage_Error(
          addAddressFirst: "Please add your address",
          addProductofSameshop:
              "Please add product of same shop added in the cart",
        ),
      ),
      productpage: AppLocalizations_Labels_Productpage(
        hint: AppLocalizations_Labels_Productpage_Hint(
          searchProducts: "Search Products",
        ),
        text: AppLocalizations_Labels_Productpage_Text(
          noProductFound: "No Products found",
        ),
        title: AppLocalizations_Labels_Productpage_Title(
          searchResultFor: "Search Result for",
        ),
      ),
      cartpage: AppLocalizations_Labels_Cartpage(
        appbartitle: AppLocalizations_Labels_Cartpage_Appbartitle(
          cart: "Cart",
        ),
        text: AppLocalizations_Labels_Cartpage_Text(
          change: "Change",
          destination: "Destination",
          payments: "Payments",
          tipToDriver: "Tip for Driver",
          platformCharges: "Platform Charges",
          deliveryFee: "Delivery Fee",
          couponDiscount: "Coupon Discount",
          totalPayment: "Total Payment",
          cartValue: "Cart Value",
          noItemsInCart: "No items in the Cart",
          remove: "Remove",
          gst: "GST",
          pst: "PST",
          hst: "HST",
          slotsNotAvailable: "Slot not available",
          noSlotsAvailableForTheDay: "No slots available for the day",
        ),
        hint: AppLocalizations_Labels_Cartpage_Hint(
          offPercentage: "20% off",
          addInstruction: "Add Instruction",
        ),
        error: AppLocalizations_Labels_Cartpage_Error(
          outOfStock: "This product is out of stock",
          quantityDecreased: ({value}) =>
              "Please decrease product quantity from cart left only ${value}!!! ",
        ),
        button: AppLocalizations_Labels_Cartpage_Button(
          apply: "Apply",
          goToCheckOut: "Go to Checkout",
        ),
      ),
      couponpage: AppLocalizations_Labels_Couponpage(
        appbartitle: AppLocalizations_Labels_Couponpage_Appbartitle(
          promosAvailable: "Promos Available",
        ),
        text: AppLocalizations_Labels_Couponpage_Text(
          use: "USE",
        ),
      ),
      cardlistpage: AppLocalizations_Labels_Cardlistpage(
        appbartitle: AppLocalizations_Labels_Cardlistpage_Appbartitle(
          paymentMode: "Payment Mode",
        ),
        title: AppLocalizations_Labels_Cardlistpage_Title(
          paymentPreference: "How would you prefer to pay?",
        ),
        text: AppLocalizations_Labels_Cardlistpage_Text(
          addMoreCards: "Add more Cards",
          noCardAdded: "No Card Attached Yet",
          delete: "Delete",
          expires: "Expires",
        ),
        error: AppLocalizations_Labels_Cardlistpage_Error(
          selectCard: "Please select a card for payment.",
          scheduletime: "Please schedule your order to make a booking.",
          pleaseSelectCard: "Please select a card for payment",
        ),
      ),
      addcardpage: AppLocalizations_Labels_Addcardpage(
        title: AppLocalizations_Labels_Addcardpage_Title(
          addCard: "Enter Card Details",
        ),
        error: AppLocalizations_Labels_Addcardpage_Error(
          requiredField: "Required Field",
          invalidCvv: "Invalid cvv",
          invalidMonth: "Invalid Month",
          expiredYear: "Invalid Expired Year",
          cardExpired: "Card Expired",
          invalidCard: "Invalid Card",
          enterCardDetails: "Please Enter Your Card Details",
        ),
        appbartitle: AppLocalizations_Labels_Addcardpage_Appbartitle(
          cardListing: "Card Listing",
        ),
      ),
      paymentsuccesspage: AppLocalizations_Labels_Paymentsuccesspage(
        title: AppLocalizations_Labels_Paymentsuccesspage_Title(
          paymentSuccessfull: "Payment Successfull",
        ),
      ),
      myorderpage: AppLocalizations_Labels_Myorderpage(
        appbartitle: AppLocalizations_Labels_Myorderpage_Appbartitle(
          myOrder: "My Order",
        ),
        text: AppLocalizations_Labels_Myorderpage_Text(
          ongoing: "Ongoing",
          history: "History",
          noOrderFound: "No Order Found",
          orderId: "Order ID",
          totalPayment: "Total Payment",
          orderAgain: "Order again",
          dated: "Dated",
          orderPlace: "Order Placed",
          orderAccepted: "Order Accepted By",
          orderCancelled: "Order Cancelled By",
          onProcess: "On Process",
          deliveryBoy: "Delivery Boy Assigned",
          onTheWay: "Delivery Boy on the way",
          reached: "Delivery Boy reached at",
          orderPicked: "Order Picked by Delivery Boy",
          reachedAtPlace: "Delivery Boy reached at your place",
          delivered: "Delivered",
          cancelledByYou: "Canceled by You",
          cancelledByDeliveryBoy: "Canceled by Delivery boy",
          otherReason: "Any other reason",
        ),
      ),
      orderdetailpage: AppLocalizations_Labels_Orderdetailpage(
        appbartitle: AppLocalizations_Labels_Orderdetailpage_Appbartitle(
          orderDetail: "Order Details",
        ),
        text: AppLocalizations_Labels_Orderdetailpage_Text(
          orderStatus: "Order Status",
          destination: "Destination",
          itemDescription: "Item Description",
          payments: "Payments",
          track: "TRACK",
          contact: "Contact",
          cancelOrder: "Cancel Order",
          totalItem: "Total Items",
          orderAcceptedBy: "Order Accepted By ",
          orderCancelledBy: "Order Cancelled By",
          onProcess: "On Process",
          deliveryBoyAssigned: "Delivery Boy Assigned",
          deliveryBoyOnTheWay: "Delivery Boy on the way",
          deliveryBoyReachedAt: "Delivery Boy reached at",
          orderPickedDeliveryBoy: "Order Picked by Delivery Boy",
          deliveryBoyReached: "Delivery Boy reached at your place",
          cancelledByYou: "Canceled by You",
          cancelledByDeliveryBoy: "Canceled by Delivery boy",
          otherReason: "Any other reason",
        ),
        button: AppLocalizations_Labels_Orderdetailpage_Button(
          raiseDispute: "Raise Dispute",
          shareFeedBack: "Share Feedback",
        ),
      ),
      feedbackalert: AppLocalizations_Labels_Feedbackalert(
        hint: AppLocalizations_Labels_Feedbackalert_Hint(
          comment: "Comment",
        ),
        text: AppLocalizations_Labels_Feedbackalert_Text(
          shop: "Shop",
          driver: "Driver",
          giveRating: "Please give your Rating!",
        ),
        button: AppLocalizations_Labels_Feedbackalert_Button(
          skip: "Skip",
          submit: "Submit",
        ),
      ),
      logoutalert: AppLocalizations_Labels_Logoutalert(
        title: AppLocalizations_Labels_Logoutalert_Title(
          logOutTitle: "Log out",
          logOutSubTitle: "Are you sure you want to log out?",
        ),
      ),
      addressalert: AppLocalizations_Labels_Addressalert(
        title: AppLocalizations_Labels_Addressalert_Title(
          removeAddress: "Remove Address",
        ),
        subtitle: AppLocalizations_Labels_Addressalert_Subtitle(
          sureToRemoveAddress: "Are you sure you want to remove this address?",
        ),
      ),
      bookingalert: AppLocalizations_Labels_Bookingalert(
        tittle: AppLocalizations_Labels_Bookingalert_Tittle(
          oops: "Oops!!!",
        ),
        subtitle: AppLocalizations_Labels_Bookingalert_Subtitle(
          pleaseCheckCart:
              "Please check your cart your product is out of stock or product quantity has been decreased.",
        ),
      ),
      sendQueryalert: AppLocalizations_Labels_SendQueryalert(
        title: AppLocalizations_Labels_SendQueryalert_Title(
          removeImage: "Remove Image",
        ),
        subtitle: AppLocalizations_Labels_SendQueryalert_Subtitle(
          sureToRemoveImage: "Are you sure you want to remove this image?",
        ),
      ),
      couponalert: AppLocalizations_Labels_Couponalert(
        title: AppLocalizations_Labels_Couponalert_Title(
          hurray: " Hurray!!!",
        ),
        subtitle: AppLocalizations_Labels_Couponalert_Subtitle(
          discountApplied: "Discount Applied",
        ),
      ),
      ratingalert: AppLocalizations_Labels_Ratingalert(
        title: AppLocalizations_Labels_Ratingalert_Title(
          giveRating: "Please give your Rating!",
          shop: "Shop",
        ),
        hint: AppLocalizations_Labels_Ratingalert_Hint(
          comment: " Comment",
        ),
      ),
      orderAlert: AppLocalizations_Labels_OrderAlert(
        subtile: AppLocalizations_Labels_OrderAlert_Subtile(
          clearCart: "Please clear your cart first",
        ),
      ),
      locationAlert: AppLocalizations_Labels_LocationAlert(
        title: AppLocalizations_Labels_LocationAlert_Title(
          permissionAlert: "Permission Alert!!",
        ),
        subtitle: AppLocalizations_Labels_LocationAlert_Subtitle(
          locationPermission:
              "Location permission is required to see nearby shops.\nIf you are not allowing the location permission you will not be able to make any order.\nSo please give permission to use all features of the Application.",
        ),
        text: AppLocalizations_Labels_LocationAlert_Text(
          setting: "Setting",
        ),
      ),
      trackingpage: AppLocalizations_Labels_Trackingpage(
        appbartitle: AppLocalizations_Labels_Trackingpage_Appbartitle(
          trackOrder: "Track Order",
        ),
      ),
      polylinepage: AppLocalizations_Labels_Polylinepage(
        markertitle: AppLocalizations_Labels_Polylinepage_Markertitle(
          shopAddress: "Shop Address",
          deliveryBoyAddress: "Delivery Boy",
          deliveryAddress: "Delivery Address",
          shopMarkerId: "sourcePin",
          desMarkerId: "destPin",
        ),
      ),
      helpandSupport: AppLocalizations_Labels_HelpandSupport(
        text: AppLocalizations_Labels_HelpandSupport_Text(
          ticketId: "Ticket ID:",
          issueRaisedOn: "Issue Raised On:",
          orderId: "Order Id:",
          orderDate: "Order Date:",
          ticketStatus: "Ticket Status:",
          pending: "Pending",
          closed: "Closed",
          query: "Query:",
          response: "Response:",
          noQueryFound: "No Queries Found",
          raiseNewTicket: "Raise New Ticket",
          previousTickets: "Previous Tickets",
          getInTouch: "Get in Touch!",
          fillDetails: "Fill Details",
          description: "Description",
          uploadPicture: "Upload picture",
          addImage: "Add Image",
        ),
        appbarTitle: AppLocalizations_Labels_HelpandSupport_AppbarTitle(
          helpAndSupport: "Help & Support",
        ),
        title: AppLocalizations_Labels_HelpandSupport_Title(
          whyNeedHelp: "Tell us why do you need help.",
        ),
        error: AppLocalizations_Labels_HelpandSupport_Error(
          enterQuery: "Please enter the query",
          minCharecters: "Minimum 50 char required.",
        ),
      ),
      notificationpage: AppLocalizations_Labels_Notificationpage(
        text: AppLocalizations_Labels_Notificationpage_Text(
          noNotificationFound: "No Notifications found",
        ),
        appbartitle: AppLocalizations_Labels_Notificationpage_Appbartitle(
          notification: "Notifications",
        ),
      ),
      filepickerpage: AppLocalizations_Labels_Filepickerpage(
        button: AppLocalizations_Labels_Filepickerpage_Button(
          camera: "Camera",
          gallery: "Gallery",
        ),
      ),
      faqpage: AppLocalizations_Labels_Faqpage(
        text: AppLocalizations_Labels_Faqpage_Text(
          question: ({value}) => "Q${value}",
          answer: ({value}) => "Ans${value}",
        ),
      ),
      bottomappbar: AppLocalizations_Labels_Bottomappbar(
        text: AppLocalizations_Labels_Bottomappbar_Text(
          home: "Home",
          cart: "Cart",
          profile: "Profile",
        ),
      ),
      favouritepage: AppLocalizations_Labels_Favouritepage(
        text: AppLocalizations_Labels_Favouritepage_Text(
          appBarTitle: "Favourites",
        ),
      ),
      invitefriendspage: AppLocalizations_Labels_Invitefriendspage(
        text: AppLocalizations_Labels_Invitefriendspage_Text(
          appBarTitle: "Invite Friends",
          refferalCode: "Referral Code",
          code: "No Refferal Code",
          codeCopied: "Referral Code Copied",
          share: "Share",
        ),
      ),
    ),
  };

  final AppLocalizations_Labels labels;

  static AppLocalizations_Labels of(BuildContext context) =>
      Localizations.of<AppLocalizations>(context, AppLocalizations)?.labels;
}

class AppLocalizations_Labels_Common_Title {
  const AppLocalizations_Labels_Common_Title({this.appName, this.appSubtitle});

  final String appName;

  final String appSubtitle;
}

class AppLocalizations_Labels_Common_Text {
  const AppLocalizations_Labels_Common_Text(
      {this.noDataFound,
      this.no,
      this.yes,
      this.km,
      this.save,
      this.done,
      this.ok,
      this.noFaq,
      this.pickUp,
      this.checkInternetConnection,
      this.oderPlaced,
      this.error,
      this.success,
      this.orderRejected,
      this.orderConfirmed,
      this.driverAssigned,
      this.dispatched,
      this.delivered,
      this.remove,
      this.notAllowed,
      this.orderCancelled});

  final String noDataFound;

  final String no;

  final String yes;

  final String km;

  final String save;

  final String done;

  final String ok;

  final String noFaq;

  final String pickUp;

  final String checkInternetConnection;

  final String oderPlaced;

  final String error;

  final String success;

  final String orderRejected;

  final String orderConfirmed;

  final String driverAssigned;

  final String dispatched;

  final String delivered;

  final String remove;

  final String notAllowed;

  final String orderCancelled;
}

class AppLocalizations_Labels_Common_Error {
  const AppLocalizations_Labels_Common_Error({this.somethingWentWrong});

  final String somethingWentWrong;
}

class AppLocalizations_Labels_Common_Button {
  const AppLocalizations_Labels_Common_Button({this.submit, this.continueText});

  final String submit;

  final String continueText;
}

class AppLocalizations_Labels_Common {
  const AppLocalizations_Labels_Common(
      {this.title, this.text, this.error, this.button});

  final AppLocalizations_Labels_Common_Title title;

  final AppLocalizations_Labels_Common_Text text;

  final AppLocalizations_Labels_Common_Error error;

  final AppLocalizations_Labels_Common_Button button;
}

class AppLocalizations_Labels_Intropage_Title {
  const AppLocalizations_Labels_Intropage_Title(
      {this.searchItemOnline, this.placeOrder, this.trackYourOrder});

  final String searchItemOnline;

  final String placeOrder;

  final String trackYourOrder;
}

class AppLocalizations_Labels_Intropage_Subtitle {
  const AppLocalizations_Labels_Intropage_Subtitle(
      {this.quickSearch, this.searchAndPlaceOrder, this.realTimeTracking});

  final String quickSearch;

  final String searchAndPlaceOrder;

  final String realTimeTracking;
}

class AppLocalizations_Labels_Intropage {
  const AppLocalizations_Labels_Intropage({this.title, this.subtitle});

  final AppLocalizations_Labels_Intropage_Title title;

  final AppLocalizations_Labels_Intropage_Subtitle subtitle;
}

class AppLocalizations_Labels_Sendotppage_Text {
  const AppLocalizations_Labels_Sendotppage_Text(
      {this.iAccept,
      this.termsAndPolicies,
      this.ofLimitLess,
      this.signIn,
      this.guestLogin});

  final String iAccept;

  final String termsAndPolicies;

  final String ofLimitLess;

  final String signIn;

  final String guestLogin;
}

class AppLocalizations_Labels_Sendotppage_Button {
  const AppLocalizations_Labels_Sendotppage_Button({this.proceed});

  final String proceed;
}

class AppLocalizations_Labels_Sendotppage_Error {
  const AppLocalizations_Labels_Sendotppage_Error(
      {this.validPhoneNumber,
      this.acceptTermsAndServices,
      this.numberAlreadyExist,
      this.youAreBlocked});

  final String validPhoneNumber;

  final String acceptTermsAndServices;

  final String numberAlreadyExist;

  final String youAreBlocked;
}

class AppLocalizations_Labels_Sendotppage_Hint {
  const AppLocalizations_Labels_Sendotppage_Hint({this.phoneNumber});

  final String phoneNumber;
}

class AppLocalizations_Labels_Sendotppage {
  const AppLocalizations_Labels_Sendotppage(
      {this.text, this.button, this.error, this.hint});

  final AppLocalizations_Labels_Sendotppage_Text text;

  final AppLocalizations_Labels_Sendotppage_Button button;

  final AppLocalizations_Labels_Sendotppage_Error error;

  final AppLocalizations_Labels_Sendotppage_Hint hint;
}

class AppLocalizations_Labels_Verifyotppage_Title {
  const AppLocalizations_Labels_Verifyotppage_Title(
      {this.verifyMobileNumber, this.verifyEmail});

  final String verifyMobileNumber;

  final String verifyEmail;
}

class AppLocalizations_Labels_Verifyotppage_Subtitle {
  const AppLocalizations_Labels_Verifyotppage_Subtitle({this.enterOtp});

  final String enterOtp;
}

class AppLocalizations_Labels_Verifyotppage_Text {
  const AppLocalizations_Labels_Verifyotppage_Text(
      {this.changeNumber, this.resend});

  final String changeNumber;

  final String resend;
}

class AppLocalizations_Labels_Verifyotppage_Button {
  const AppLocalizations_Labels_Verifyotppage_Button({this.verify});

  final String verify;
}

class AppLocalizations_Labels_Verifyotppage_Error {
  const AppLocalizations_Labels_Verifyotppage_Error({this.emptyOtp});

  final String emptyOtp;
}

class AppLocalizations_Labels_Verifyotppage {
  const AppLocalizations_Labels_Verifyotppage(
      {this.title, this.subtitle, this.text, this.button, this.error});

  final AppLocalizations_Labels_Verifyotppage_Title title;

  final AppLocalizations_Labels_Verifyotppage_Subtitle subtitle;

  final AppLocalizations_Labels_Verifyotppage_Text text;

  final AppLocalizations_Labels_Verifyotppage_Button button;

  final AppLocalizations_Labels_Verifyotppage_Error error;
}

class AppLocalizations_Labels_Enterdetailspage_Title {
  const AppLocalizations_Labels_Enterdetailspage_Title({this.enterDetails});

  final String enterDetails;
}

class AppLocalizations_Labels_Enterdetailspage_Subtitle {
  const AppLocalizations_Labels_Enterdetailspage_Subtitle(
      {this.enterdetailstoProceed});

  final String enterdetailstoProceed;
}

class AppLocalizations_Labels_Enterdetailspage_Hint {
  const AppLocalizations_Labels_Enterdetailspage_Hint({this.enterName});

  final String enterName;
}

class AppLocalizations_Labels_Enterdetailspage_Error {
  const AppLocalizations_Labels_Enterdetailspage_Error(
      {this.emptyName, this.minNameLength});

  final String emptyName;

  final String minNameLength;
}

class AppLocalizations_Labels_Enterdetailspage {
  const AppLocalizations_Labels_Enterdetailspage(
      {this.title, this.subtitle, this.hint, this.error});

  final AppLocalizations_Labels_Enterdetailspage_Title title;

  final AppLocalizations_Labels_Enterdetailspage_Subtitle subtitle;

  final AppLocalizations_Labels_Enterdetailspage_Hint hint;

  final AppLocalizations_Labels_Enterdetailspage_Error error;
}

class AppLocalizations_Labels_LoginSuccessfulPage_Title {
  const AppLocalizations_Labels_LoginSuccessfulPage_Title({this.loginSuccess});

  final String loginSuccess;
}

class AppLocalizations_Labels_LoginSuccessfulPage {
  const AppLocalizations_Labels_LoginSuccessfulPage({this.title});

  final AppLocalizations_Labels_LoginSuccessfulPage_Title title;
}

class AppLocalizations_Labels_Enablelocationpage_Title {
  const AppLocalizations_Labels_Enablelocationpage_Title({this.enableLocation});

  final String enableLocation;
}

class AppLocalizations_Labels_Enablelocationpage_Subtitle {
  const AppLocalizations_Labels_Enablelocationpage_Subtitle(
      {this.giveLocationAccess});

  final String giveLocationAccess;
}

class AppLocalizations_Labels_Enablelocationpage_Button {
  const AppLocalizations_Labels_Enablelocationpage_Button({this.allow});

  final String allow;
}

class AppLocalizations_Labels_Enablelocationpage_Error {
  const AppLocalizations_Labels_Enablelocationpage_Error(
      {this.pleaseGrantPermission,
      this.permissionDenied,
      this.locationNotAllowed});

  final String pleaseGrantPermission;

  final String permissionDenied;

  final String locationNotAllowed;
}

class AppLocalizations_Labels_Enablelocationpage {
  const AppLocalizations_Labels_Enablelocationpage(
      {this.title, this.subtitle, this.button, this.error});

  final AppLocalizations_Labels_Enablelocationpage_Title title;

  final AppLocalizations_Labels_Enablelocationpage_Subtitle subtitle;

  final AppLocalizations_Labels_Enablelocationpage_Button button;

  final AppLocalizations_Labels_Enablelocationpage_Error error;
}

class AppLocalizations_Labels_Profilepage_Appbartitle {
  const AppLocalizations_Labels_Profilepage_Appbartitle({this.profile});

  final String profile;
}

class AppLocalizations_Labels_Profilepage_Text {
  const AppLocalizations_Labels_Profilepage_Text(
      {this.myOrders,
      this.favouriteText,
      this.inviteFriendsText,
      this.inviteFriends,
      this.checkOrders,
      this.myAddresses,
      this.yourAddresses,
      this.termsAndPolicies,
      this.thingsYoumayknow,
      this.fAQs,
      this.freqAskedQue,
      this.helpAndSupport,
      this.getSupport,
      this.logout});

  final String myOrders;

  final String favouriteText;

  final String inviteFriendsText;

  final String inviteFriends;

  final String checkOrders;

  final String myAddresses;

  final String yourAddresses;

  final String termsAndPolicies;

  final String thingsYoumayknow;

  final String fAQs;

  final String freqAskedQue;

  final String helpAndSupport;

  final String getSupport;

  final String logout;
}

class AppLocalizations_Labels_Profilepage_Button {
  const AppLocalizations_Labels_Profilepage_Button({this.editProfile});

  final String editProfile;
}

class AppLocalizations_Labels_Profilepage {
  const AppLocalizations_Labels_Profilepage(
      {this.appbartitle, this.text, this.button});

  final AppLocalizations_Labels_Profilepage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Profilepage_Text text;

  final AppLocalizations_Labels_Profilepage_Button button;
}

class AppLocalizations_Labels_Editprofilepage_Appbartitle {
  const AppLocalizations_Labels_Editprofilepage_Appbartitle({this.editProfile});

  final String editProfile;
}

class AppLocalizations_Labels_Editprofilepage_Title {
  const AppLocalizations_Labels_Editprofilepage_Title({this.personalInfo});

  final String personalInfo;
}

class AppLocalizations_Labels_Editprofilepage_Text {
  const AppLocalizations_Labels_Editprofilepage_Text(
      {this.name, this.phoneNumber, this.email});

  final String name;

  final String phoneNumber;

  final String email;
}

class AppLocalizations_Labels_Editprofilepage {
  const AppLocalizations_Labels_Editprofilepage(
      {this.appbartitle, this.title, this.text});

  final AppLocalizations_Labels_Editprofilepage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Editprofilepage_Title title;

  final AppLocalizations_Labels_Editprofilepage_Text text;
}

class AppLocalizations_Labels_Editphoneoremailpage_Title {
  const AppLocalizations_Labels_Editphoneoremailpage_Title(
      {this.enterNumbertoVerify, this.enterEmailtoVerify});

  final String enterNumbertoVerify;

  final String enterEmailtoVerify;
}

class AppLocalizations_Labels_Editphoneoremailpage_Button {
  const AppLocalizations_Labels_Editphoneoremailpage_Button({this.sendOtp});

  final String sendOtp;
}

class AppLocalizations_Labels_Editphoneoremailpage_Hint {
  const AppLocalizations_Labels_Editphoneoremailpage_Hint({this.enterEmail});

  final String enterEmail;
}

class AppLocalizations_Labels_Editphoneoremailpage_Error {
  const AppLocalizations_Labels_Editphoneoremailpage_Error(
      {this.enterEmail, this.validEmail});

  final String enterEmail;

  final String validEmail;
}

class AppLocalizations_Labels_Editphoneoremailpage {
  const AppLocalizations_Labels_Editphoneoremailpage(
      {this.title, this.button, this.hint, this.error});

  final AppLocalizations_Labels_Editphoneoremailpage_Title title;

  final AppLocalizations_Labels_Editphoneoremailpage_Button button;

  final AppLocalizations_Labels_Editphoneoremailpage_Hint hint;

  final AppLocalizations_Labels_Editphoneoremailpage_Error error;
}

class AppLocalizations_Labels_Addresslistingpage_Appbartitle {
  const AppLocalizations_Labels_Addresslistingpage_Appbartitle(
      {this.myAddress});

  final String myAddress;
}

class AppLocalizations_Labels_Addresslistingpage_Text {
  const AppLocalizations_Labels_Addresslistingpage_Text(
      {this.edit,
      this.addressType,
      this.setPrimary,
      this.noAddressAdded,
      this.makePrimary,
      this.primary,
      this.addAddres});

  final String edit;

  final String addressType;

  final String setPrimary;

  final String noAddressAdded;

  final String makePrimary;

  final String primary;

  final String addAddres;
}

class AppLocalizations_Labels_Addresslistingpage {
  const AppLocalizations_Labels_Addresslistingpage(
      {this.appbartitle, this.text});

  final AppLocalizations_Labels_Addresslistingpage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Addresslistingpage_Text text;
}

class AppLocalizations_Labels_Deliveryaddresspage_Title {
  const AppLocalizations_Labels_Deliveryaddresspage_Title(
      {this.selectAddress, this.yourAddress});

  final String selectAddress;

  final String yourAddress;
}

class AppLocalizations_Labels_Deliveryaddresspage {
  const AppLocalizations_Labels_Deliveryaddresspage({this.title});

  final AppLocalizations_Labels_Deliveryaddresspage_Title title;
}

class AppLocalizations_Labels_Setaddresspage_Error {
  const AppLocalizations_Labels_Setaddresspage_Error({this.emptyAddress});

  final String emptyAddress;
}

class AppLocalizations_Labels_Setaddresspage_Hint {
  const AppLocalizations_Labels_Setaddresspage_Hint({this.emptyAddress});

  final String emptyAddress;
}

class AppLocalizations_Labels_Setaddresspage_Button {
  const AppLocalizations_Labels_Setaddresspage_Button({this.save});

  final String save;
}

class AppLocalizations_Labels_Setaddresspage {
  const AppLocalizations_Labels_Setaddresspage(
      {this.error, this.hint, this.button});

  final AppLocalizations_Labels_Setaddresspage_Error error;

  final AppLocalizations_Labels_Setaddresspage_Hint hint;

  final AppLocalizations_Labels_Setaddresspage_Button button;
}

class AppLocalizations_Labels_Categoriesshoppage_Title {
  const AppLocalizations_Labels_Categoriesshoppage_Title(
      {this.categories, this.shop});

  final String categories;

  final String shop;
}

class AppLocalizations_Labels_Categoriesshoppage_Text {
  const AppLocalizations_Labels_Categoriesshoppage_Text(
      {this.home,
      this.delivery,
      this.takeAway,
      this.schedulingAt,
      this.noScheduling,
      this.day,
      this.noShopFound,
      this.time});

  final String home;

  final String delivery;

  final String takeAway;

  final String schedulingAt;

  final String noScheduling;

  final String day;

  final String noShopFound;

  final String time;
}

class AppLocalizations_Labels_Categoriesshoppage_Button {
  const AppLocalizations_Labels_Categoriesshoppage_Button({this.schedule});

  final String schedule;
}

class AppLocalizations_Labels_Categoriesshoppage_Hint {
  const AppLocalizations_Labels_Categoriesshoppage_Hint({this.searchProduct});

  final String searchProduct;
}

class AppLocalizations_Labels_Categoriesshoppage {
  const AppLocalizations_Labels_Categoriesshoppage(
      {this.title, this.text, this.button, this.hint});

  final AppLocalizations_Labels_Categoriesshoppage_Title title;

  final AppLocalizations_Labels_Categoriesshoppage_Text text;

  final AppLocalizations_Labels_Categoriesshoppage_Button button;

  final AppLocalizations_Labels_Categoriesshoppage_Hint hint;
}

class AppLocalizations_Labels_Shopdetailspage_Button {
  const AppLocalizations_Labels_Shopdetailspage_Button({this.viewCart});

  final String viewCart;
}

class AppLocalizations_Labels_Shopdetailspage_Text {
  const AppLocalizations_Labels_Shopdetailspage_Text(
      {this.items, this.shopItem, this.noProducts, this.off});

  final String items;

  final String shopItem;

  final String noProducts;

  final String off;
}

class AppLocalizations_Labels_Shopdetailspage_Error {
  const AppLocalizations_Labels_Shopdetailspage_Error(
      {this.addAddressFirst, this.addProductofSameshop});

  final String addAddressFirst;

  final String addProductofSameshop;
}

class AppLocalizations_Labels_Shopdetailspage {
  const AppLocalizations_Labels_Shopdetailspage(
      {this.button, this.text, this.error});

  final AppLocalizations_Labels_Shopdetailspage_Button button;

  final AppLocalizations_Labels_Shopdetailspage_Text text;

  final AppLocalizations_Labels_Shopdetailspage_Error error;
}

class AppLocalizations_Labels_Productpage_Hint {
  const AppLocalizations_Labels_Productpage_Hint({this.searchProducts});

  final String searchProducts;
}

class AppLocalizations_Labels_Productpage_Text {
  const AppLocalizations_Labels_Productpage_Text({this.noProductFound});

  final String noProductFound;
}

class AppLocalizations_Labels_Productpage_Title {
  const AppLocalizations_Labels_Productpage_Title({this.searchResultFor});

  final String searchResultFor;
}

class AppLocalizations_Labels_Productpage {
  const AppLocalizations_Labels_Productpage({this.hint, this.text, this.title});

  final AppLocalizations_Labels_Productpage_Hint hint;

  final AppLocalizations_Labels_Productpage_Text text;

  final AppLocalizations_Labels_Productpage_Title title;
}

class AppLocalizations_Labels_Cartpage_Appbartitle {
  const AppLocalizations_Labels_Cartpage_Appbartitle({this.cart});

  final String cart;
}

class AppLocalizations_Labels_Cartpage_Text {
  const AppLocalizations_Labels_Cartpage_Text(
      {this.change,
      this.destination,
      this.payments,
      this.tipToDriver,
      this.platformCharges,
      this.deliveryFee,
      this.couponDiscount,
      this.totalPayment,
      this.cartValue,
      this.noItemsInCart,
      this.remove,
      this.gst,
      this.pst,
      this.hst,
      this.slotsNotAvailable,
      this.noSlotsAvailableForTheDay});

  final String change;

  final String destination;

  final String payments;

  final String tipToDriver;

  final String platformCharges;

  final String deliveryFee;

  final String couponDiscount;

  final String totalPayment;

  final String cartValue;

  final String noItemsInCart;

  final String remove;

  final String gst;

  final String pst;

  final String hst;

  final String slotsNotAvailable;

  final String noSlotsAvailableForTheDay;
}

class AppLocalizations_Labels_Cartpage_Hint {
  const AppLocalizations_Labels_Cartpage_Hint(
      {this.offPercentage, this.addInstruction});

  final String offPercentage;

  final String addInstruction;
}

typedef String AppLocalizations_Labels_Cartpage_Error_quantityDecreased(
    {@required String value});

class AppLocalizations_Labels_Cartpage_Error {
  const AppLocalizations_Labels_Cartpage_Error(
      {this.outOfStock,
      AppLocalizations_Labels_Cartpage_Error_quantityDecreased
          quantityDecreased})
      : this._quantityDecreased = quantityDecreased;

  final String outOfStock;

  final AppLocalizations_Labels_Cartpage_Error_quantityDecreased
      _quantityDecreased;

  String quantityDecreased({@required String value}) => this._quantityDecreased(
        value: value,
      );
}

class AppLocalizations_Labels_Cartpage_Button {
  const AppLocalizations_Labels_Cartpage_Button(
      {this.apply, this.goToCheckOut});

  final String apply;

  final String goToCheckOut;
}

class AppLocalizations_Labels_Cartpage {
  const AppLocalizations_Labels_Cartpage(
      {this.appbartitle, this.text, this.hint, this.error, this.button});

  final AppLocalizations_Labels_Cartpage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Cartpage_Text text;

  final AppLocalizations_Labels_Cartpage_Hint hint;

  final AppLocalizations_Labels_Cartpage_Error error;

  final AppLocalizations_Labels_Cartpage_Button button;
}

class AppLocalizations_Labels_Couponpage_Appbartitle {
  const AppLocalizations_Labels_Couponpage_Appbartitle({this.promosAvailable});

  final String promosAvailable;
}

class AppLocalizations_Labels_Couponpage_Text {
  const AppLocalizations_Labels_Couponpage_Text({this.use});

  final String use;
}

class AppLocalizations_Labels_Couponpage {
  const AppLocalizations_Labels_Couponpage({this.appbartitle, this.text});

  final AppLocalizations_Labels_Couponpage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Couponpage_Text text;
}

class AppLocalizations_Labels_Cardlistpage_Appbartitle {
  const AppLocalizations_Labels_Cardlistpage_Appbartitle({this.paymentMode});

  final String paymentMode;
}

class AppLocalizations_Labels_Cardlistpage_Title {
  const AppLocalizations_Labels_Cardlistpage_Title({this.paymentPreference});

  final String paymentPreference;
}

class AppLocalizations_Labels_Cardlistpage_Text {
  const AppLocalizations_Labels_Cardlistpage_Text(
      {this.addMoreCards, this.noCardAdded, this.delete, this.expires});

  final String addMoreCards;

  final String noCardAdded;

  final String delete;

  final String expires;
}

class AppLocalizations_Labels_Cardlistpage_Error {
  const AppLocalizations_Labels_Cardlistpage_Error(
      {this.selectCard, this.scheduletime, this.pleaseSelectCard});

  final String selectCard;

  final String scheduletime;

  final String pleaseSelectCard;
}

class AppLocalizations_Labels_Cardlistpage {
  const AppLocalizations_Labels_Cardlistpage(
      {this.appbartitle, this.title, this.text, this.error});

  final AppLocalizations_Labels_Cardlistpage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Cardlistpage_Title title;

  final AppLocalizations_Labels_Cardlistpage_Text text;

  final AppLocalizations_Labels_Cardlistpage_Error error;
}

class AppLocalizations_Labels_Addcardpage_Title {
  const AppLocalizations_Labels_Addcardpage_Title({this.addCard});

  final String addCard;
}

class AppLocalizations_Labels_Addcardpage_Error {
  const AppLocalizations_Labels_Addcardpage_Error(
      {this.requiredField,
      this.invalidCvv,
      this.invalidMonth,
      this.expiredYear,
      this.cardExpired,
      this.invalidCard,
      this.enterCardDetails});

  final String requiredField;

  final String invalidCvv;

  final String invalidMonth;

  final String expiredYear;

  final String cardExpired;

  final String invalidCard;

  final String enterCardDetails;
}

class AppLocalizations_Labels_Addcardpage_Appbartitle {
  const AppLocalizations_Labels_Addcardpage_Appbartitle({this.cardListing});

  final String cardListing;
}

class AppLocalizations_Labels_Addcardpage {
  const AppLocalizations_Labels_Addcardpage(
      {this.title, this.error, this.appbartitle});

  final AppLocalizations_Labels_Addcardpage_Title title;

  final AppLocalizations_Labels_Addcardpage_Error error;

  final AppLocalizations_Labels_Addcardpage_Appbartitle appbartitle;
}

class AppLocalizations_Labels_Paymentsuccesspage_Title {
  const AppLocalizations_Labels_Paymentsuccesspage_Title(
      {this.paymentSuccessfull});

  final String paymentSuccessfull;
}

class AppLocalizations_Labels_Paymentsuccesspage {
  const AppLocalizations_Labels_Paymentsuccesspage({this.title});

  final AppLocalizations_Labels_Paymentsuccesspage_Title title;
}

class AppLocalizations_Labels_Myorderpage_Appbartitle {
  const AppLocalizations_Labels_Myorderpage_Appbartitle({this.myOrder});

  final String myOrder;
}

class AppLocalizations_Labels_Myorderpage_Text {
  const AppLocalizations_Labels_Myorderpage_Text(
      {this.ongoing,
      this.history,
      this.noOrderFound,
      this.orderId,
      this.totalPayment,
      this.orderAgain,
      this.dated,
      this.orderPlace,
      this.orderAccepted,
      this.orderCancelled,
      this.onProcess,
      this.deliveryBoy,
      this.onTheWay,
      this.reached,
      this.orderPicked,
      this.reachedAtPlace,
      this.delivered,
      this.cancelledByYou,
      this.cancelledByDeliveryBoy,
      this.otherReason});

  final String ongoing;

  final String history;

  final String noOrderFound;

  final String orderId;

  final String totalPayment;

  final String orderAgain;

  final String dated;

  final String orderPlace;

  final String orderAccepted;

  final String orderCancelled;

  final String onProcess;

  final String deliveryBoy;

  final String onTheWay;

  final String reached;

  final String orderPicked;

  final String reachedAtPlace;

  final String delivered;

  final String cancelledByYou;

  final String cancelledByDeliveryBoy;

  final String otherReason;
}

class AppLocalizations_Labels_Myorderpage {
  const AppLocalizations_Labels_Myorderpage({this.appbartitle, this.text});

  final AppLocalizations_Labels_Myorderpage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Myorderpage_Text text;
}

class AppLocalizations_Labels_Orderdetailpage_Appbartitle {
  const AppLocalizations_Labels_Orderdetailpage_Appbartitle({this.orderDetail});

  final String orderDetail;
}

class AppLocalizations_Labels_Orderdetailpage_Text {
  const AppLocalizations_Labels_Orderdetailpage_Text(
      {this.orderStatus,
      this.destination,
      this.itemDescription,
      this.payments,
      this.track,
      this.contact,
      this.cancelOrder,
      this.totalItem,
      this.orderAcceptedBy,
      this.orderCancelledBy,
      this.onProcess,
      this.deliveryBoyAssigned,
      this.deliveryBoyOnTheWay,
      this.deliveryBoyReachedAt,
      this.orderPickedDeliveryBoy,
      this.deliveryBoyReached,
      this.cancelledByYou,
      this.cancelledByDeliveryBoy,
      this.otherReason});

  final String orderStatus;

  final String destination;

  final String itemDescription;

  final String payments;

  final String track;

  final String contact;

  final String cancelOrder;

  final String totalItem;

  final String orderAcceptedBy;

  final String orderCancelledBy;

  final String onProcess;

  final String deliveryBoyAssigned;

  final String deliveryBoyOnTheWay;

  final String deliveryBoyReachedAt;

  final String orderPickedDeliveryBoy;

  final String deliveryBoyReached;

  final String cancelledByYou;

  final String cancelledByDeliveryBoy;

  final String otherReason;
}

class AppLocalizations_Labels_Orderdetailpage_Button {
  const AppLocalizations_Labels_Orderdetailpage_Button(
      {this.raiseDispute, this.shareFeedBack});

  final String raiseDispute;

  final String shareFeedBack;
}

class AppLocalizations_Labels_Orderdetailpage {
  const AppLocalizations_Labels_Orderdetailpage(
      {this.appbartitle, this.text, this.button});

  final AppLocalizations_Labels_Orderdetailpage_Appbartitle appbartitle;

  final AppLocalizations_Labels_Orderdetailpage_Text text;

  final AppLocalizations_Labels_Orderdetailpage_Button button;
}

class AppLocalizations_Labels_Feedbackalert_Hint {
  const AppLocalizations_Labels_Feedbackalert_Hint({this.comment});

  final String comment;
}

class AppLocalizations_Labels_Feedbackalert_Text {
  const AppLocalizations_Labels_Feedbackalert_Text(
      {this.shop, this.driver, this.giveRating});

  final String shop;

  final String driver;

  final String giveRating;
}

class AppLocalizations_Labels_Feedbackalert_Button {
  const AppLocalizations_Labels_Feedbackalert_Button({this.skip, this.submit});

  final String skip;

  final String submit;
}

class AppLocalizations_Labels_Feedbackalert {
  const AppLocalizations_Labels_Feedbackalert(
      {this.hint, this.text, this.button});

  final AppLocalizations_Labels_Feedbackalert_Hint hint;

  final AppLocalizations_Labels_Feedbackalert_Text text;

  final AppLocalizations_Labels_Feedbackalert_Button button;
}

class AppLocalizations_Labels_Logoutalert_Title {
  const AppLocalizations_Labels_Logoutalert_Title(
      {this.logOutTitle, this.logOutSubTitle});

  final String logOutTitle;

  final String logOutSubTitle;
}

class AppLocalizations_Labels_Logoutalert {
  const AppLocalizations_Labels_Logoutalert({this.title});

  final AppLocalizations_Labels_Logoutalert_Title title;
}

class AppLocalizations_Labels_Addressalert_Title {
  const AppLocalizations_Labels_Addressalert_Title({this.removeAddress});

  final String removeAddress;
}

class AppLocalizations_Labels_Addressalert_Subtitle {
  const AppLocalizations_Labels_Addressalert_Subtitle(
      {this.sureToRemoveAddress});

  final String sureToRemoveAddress;
}

class AppLocalizations_Labels_Addressalert {
  const AppLocalizations_Labels_Addressalert({this.title, this.subtitle});

  final AppLocalizations_Labels_Addressalert_Title title;

  final AppLocalizations_Labels_Addressalert_Subtitle subtitle;
}

class AppLocalizations_Labels_Bookingalert_Tittle {
  const AppLocalizations_Labels_Bookingalert_Tittle({this.oops});

  final String oops;
}

class AppLocalizations_Labels_Bookingalert_Subtitle {
  const AppLocalizations_Labels_Bookingalert_Subtitle({this.pleaseCheckCart});

  final String pleaseCheckCart;
}

class AppLocalizations_Labels_Bookingalert {
  const AppLocalizations_Labels_Bookingalert({this.tittle, this.subtitle});

  final AppLocalizations_Labels_Bookingalert_Tittle tittle;

  final AppLocalizations_Labels_Bookingalert_Subtitle subtitle;
}

class AppLocalizations_Labels_SendQueryalert_Title {
  const AppLocalizations_Labels_SendQueryalert_Title({this.removeImage});

  final String removeImage;
}

class AppLocalizations_Labels_SendQueryalert_Subtitle {
  const AppLocalizations_Labels_SendQueryalert_Subtitle(
      {this.sureToRemoveImage});

  final String sureToRemoveImage;
}

class AppLocalizations_Labels_SendQueryalert {
  const AppLocalizations_Labels_SendQueryalert({this.title, this.subtitle});

  final AppLocalizations_Labels_SendQueryalert_Title title;

  final AppLocalizations_Labels_SendQueryalert_Subtitle subtitle;
}

class AppLocalizations_Labels_Couponalert_Title {
  const AppLocalizations_Labels_Couponalert_Title({this.hurray});

  final String hurray;
}

class AppLocalizations_Labels_Couponalert_Subtitle {
  const AppLocalizations_Labels_Couponalert_Subtitle({this.discountApplied});

  final String discountApplied;
}

class AppLocalizations_Labels_Couponalert {
  const AppLocalizations_Labels_Couponalert({this.title, this.subtitle});

  final AppLocalizations_Labels_Couponalert_Title title;

  final AppLocalizations_Labels_Couponalert_Subtitle subtitle;
}

class AppLocalizations_Labels_Ratingalert_Title {
  const AppLocalizations_Labels_Ratingalert_Title({this.giveRating, this.shop});

  final String giveRating;

  final String shop;
}

class AppLocalizations_Labels_Ratingalert_Hint {
  const AppLocalizations_Labels_Ratingalert_Hint({this.comment});

  final String comment;
}

class AppLocalizations_Labels_Ratingalert {
  const AppLocalizations_Labels_Ratingalert({this.title, this.hint});

  final AppLocalizations_Labels_Ratingalert_Title title;

  final AppLocalizations_Labels_Ratingalert_Hint hint;
}

class AppLocalizations_Labels_OrderAlert_Subtile {
  const AppLocalizations_Labels_OrderAlert_Subtile({this.clearCart});

  final String clearCart;
}

class AppLocalizations_Labels_OrderAlert {
  const AppLocalizations_Labels_OrderAlert({this.subtile});

  final AppLocalizations_Labels_OrderAlert_Subtile subtile;
}

class AppLocalizations_Labels_LocationAlert_Title {
  const AppLocalizations_Labels_LocationAlert_Title({this.permissionAlert});

  final String permissionAlert;
}

class AppLocalizations_Labels_LocationAlert_Subtitle {
  const AppLocalizations_Labels_LocationAlert_Subtitle(
      {this.locationPermission});

  final String locationPermission;
}

class AppLocalizations_Labels_LocationAlert_Text {
  const AppLocalizations_Labels_LocationAlert_Text({this.setting});

  final String setting;
}

class AppLocalizations_Labels_LocationAlert {
  const AppLocalizations_Labels_LocationAlert(
      {this.title, this.subtitle, this.text});

  final AppLocalizations_Labels_LocationAlert_Title title;

  final AppLocalizations_Labels_LocationAlert_Subtitle subtitle;

  final AppLocalizations_Labels_LocationAlert_Text text;
}

class AppLocalizations_Labels_Trackingpage_Appbartitle {
  const AppLocalizations_Labels_Trackingpage_Appbartitle({this.trackOrder});

  final String trackOrder;
}

class AppLocalizations_Labels_Trackingpage {
  const AppLocalizations_Labels_Trackingpage({this.appbartitle});

  final AppLocalizations_Labels_Trackingpage_Appbartitle appbartitle;
}

class AppLocalizations_Labels_Polylinepage_Markertitle {
  const AppLocalizations_Labels_Polylinepage_Markertitle(
      {this.shopAddress,
      this.deliveryBoyAddress,
      this.deliveryAddress,
      this.shopMarkerId,
      this.desMarkerId});

  final String shopAddress;

  final String deliveryBoyAddress;

  final String deliveryAddress;

  final String shopMarkerId;

  final String desMarkerId;
}

class AppLocalizations_Labels_Polylinepage {
  const AppLocalizations_Labels_Polylinepage({this.markertitle});

  final AppLocalizations_Labels_Polylinepage_Markertitle markertitle;
}

class AppLocalizations_Labels_HelpandSupport_Text {
  const AppLocalizations_Labels_HelpandSupport_Text(
      {this.ticketId,
      this.issueRaisedOn,
      this.orderId,
      this.orderDate,
      this.ticketStatus,
      this.pending,
      this.closed,
      this.query,
      this.response,
      this.noQueryFound,
      this.raiseNewTicket,
      this.previousTickets,
      this.getInTouch,
      this.fillDetails,
      this.description,
      this.uploadPicture,
      this.addImage});

  final String ticketId;

  final String issueRaisedOn;

  final String orderId;

  final String orderDate;

  final String ticketStatus;

  final String pending;

  final String closed;

  final String query;

  final String response;

  final String noQueryFound;

  final String raiseNewTicket;

  final String previousTickets;

  final String getInTouch;

  final String fillDetails;

  final String description;

  final String uploadPicture;

  final String addImage;
}

class AppLocalizations_Labels_HelpandSupport_AppbarTitle {
  const AppLocalizations_Labels_HelpandSupport_AppbarTitle(
      {this.helpAndSupport});

  final String helpAndSupport;
}

class AppLocalizations_Labels_HelpandSupport_Title {
  const AppLocalizations_Labels_HelpandSupport_Title({this.whyNeedHelp});

  final String whyNeedHelp;
}

class AppLocalizations_Labels_HelpandSupport_Error {
  const AppLocalizations_Labels_HelpandSupport_Error(
      {this.enterQuery, this.minCharecters});

  final String enterQuery;

  final String minCharecters;
}

class AppLocalizations_Labels_HelpandSupport {
  const AppLocalizations_Labels_HelpandSupport(
      {this.text, this.appbarTitle, this.title, this.error});

  final AppLocalizations_Labels_HelpandSupport_Text text;

  final AppLocalizations_Labels_HelpandSupport_AppbarTitle appbarTitle;

  final AppLocalizations_Labels_HelpandSupport_Title title;

  final AppLocalizations_Labels_HelpandSupport_Error error;
}

class AppLocalizations_Labels_Notificationpage_Text {
  const AppLocalizations_Labels_Notificationpage_Text(
      {this.noNotificationFound});

  final String noNotificationFound;
}

class AppLocalizations_Labels_Notificationpage_Appbartitle {
  const AppLocalizations_Labels_Notificationpage_Appbartitle(
      {this.notification});

  final String notification;
}

class AppLocalizations_Labels_Notificationpage {
  const AppLocalizations_Labels_Notificationpage({this.text, this.appbartitle});

  final AppLocalizations_Labels_Notificationpage_Text text;

  final AppLocalizations_Labels_Notificationpage_Appbartitle appbartitle;
}

class AppLocalizations_Labels_Filepickerpage_Button {
  const AppLocalizations_Labels_Filepickerpage_Button(
      {this.camera, this.gallery});

  final String camera;

  final String gallery;
}

class AppLocalizations_Labels_Filepickerpage {
  const AppLocalizations_Labels_Filepickerpage({this.button});

  final AppLocalizations_Labels_Filepickerpage_Button button;
}

typedef String AppLocalizations_Labels_Faqpage_Text_question(
    {@required String value});
typedef String AppLocalizations_Labels_Faqpage_Text_answer(
    {@required String value});

class AppLocalizations_Labels_Faqpage_Text {
  const AppLocalizations_Labels_Faqpage_Text(
      {AppLocalizations_Labels_Faqpage_Text_question question,
      AppLocalizations_Labels_Faqpage_Text_answer answer})
      : this._question = question,
        this._answer = answer;

  final AppLocalizations_Labels_Faqpage_Text_question _question;

  final AppLocalizations_Labels_Faqpage_Text_answer _answer;

  String question({@required String value}) => this._question(
        value: value,
      );
  String answer({@required String value}) => this._answer(
        value: value,
      );
}

class AppLocalizations_Labels_Faqpage {
  const AppLocalizations_Labels_Faqpage({this.text});

  final AppLocalizations_Labels_Faqpage_Text text;
}

class AppLocalizations_Labels_Bottomappbar_Text {
  const AppLocalizations_Labels_Bottomappbar_Text(
      {this.home, this.cart, this.profile});

  final String home;

  final String cart;

  final String profile;
}

class AppLocalizations_Labels_Bottomappbar {
  const AppLocalizations_Labels_Bottomappbar({this.text});

  final AppLocalizations_Labels_Bottomappbar_Text text;
}

class AppLocalizations_Labels_Favouritepage_Text {
  const AppLocalizations_Labels_Favouritepage_Text({this.appBarTitle});

  final String appBarTitle;
}

class AppLocalizations_Labels_Favouritepage {
  const AppLocalizations_Labels_Favouritepage({this.text});

  final AppLocalizations_Labels_Favouritepage_Text text;
}

class AppLocalizations_Labels_Invitefriendspage_Text {
  const AppLocalizations_Labels_Invitefriendspage_Text(
      {this.appBarTitle,
      this.refferalCode,
      this.code,
      this.codeCopied,
      this.share});

  final String appBarTitle;

  final String refferalCode;

  final String code;

  final String codeCopied;

  final String share;
}

class AppLocalizations_Labels_Invitefriendspage {
  const AppLocalizations_Labels_Invitefriendspage({this.text});

  final AppLocalizations_Labels_Invitefriendspage_Text text;
}

class AppLocalizations_Labels {
  const AppLocalizations_Labels(
      {this.common,
      this.intropage,
      this.sendotppage,
      this.verifyotppage,
      this.enterdetailspage,
      this.loginSuccessfulPage,
      this.enablelocationpage,
      this.profilepage,
      this.editprofilepage,
      this.editphoneoremailpage,
      this.addresslistingpage,
      this.deliveryaddresspage,
      this.setaddresspage,
      this.categoriesshoppage,
      this.shopdetailspage,
      this.productpage,
      this.cartpage,
      this.couponpage,
      this.cardlistpage,
      this.addcardpage,
      this.paymentsuccesspage,
      this.myorderpage,
      this.orderdetailpage,
      this.feedbackalert,
      this.logoutalert,
      this.addressalert,
      this.bookingalert,
      this.sendQueryalert,
      this.couponalert,
      this.ratingalert,
      this.orderAlert,
      this.locationAlert,
      this.trackingpage,
      this.polylinepage,
      this.helpandSupport,
      this.notificationpage,
      this.filepickerpage,
      this.faqpage,
      this.bottomappbar,
      this.favouritepage,
      this.invitefriendspage});

  final AppLocalizations_Labels_Common common;

  final AppLocalizations_Labels_Intropage intropage;

  final AppLocalizations_Labels_Sendotppage sendotppage;

  final AppLocalizations_Labels_Verifyotppage verifyotppage;

  final AppLocalizations_Labels_Enterdetailspage enterdetailspage;

  final AppLocalizations_Labels_LoginSuccessfulPage loginSuccessfulPage;

  final AppLocalizations_Labels_Enablelocationpage enablelocationpage;

  final AppLocalizations_Labels_Profilepage profilepage;

  final AppLocalizations_Labels_Editprofilepage editprofilepage;

  final AppLocalizations_Labels_Editphoneoremailpage editphoneoremailpage;

  final AppLocalizations_Labels_Addresslistingpage addresslistingpage;

  final AppLocalizations_Labels_Deliveryaddresspage deliveryaddresspage;

  final AppLocalizations_Labels_Setaddresspage setaddresspage;

  final AppLocalizations_Labels_Categoriesshoppage categoriesshoppage;

  final AppLocalizations_Labels_Shopdetailspage shopdetailspage;

  final AppLocalizations_Labels_Productpage productpage;

  final AppLocalizations_Labels_Cartpage cartpage;

  final AppLocalizations_Labels_Couponpage couponpage;

  final AppLocalizations_Labels_Cardlistpage cardlistpage;

  final AppLocalizations_Labels_Addcardpage addcardpage;

  final AppLocalizations_Labels_Paymentsuccesspage paymentsuccesspage;

  final AppLocalizations_Labels_Myorderpage myorderpage;

  final AppLocalizations_Labels_Orderdetailpage orderdetailpage;

  final AppLocalizations_Labels_Feedbackalert feedbackalert;

  final AppLocalizations_Labels_Logoutalert logoutalert;

  final AppLocalizations_Labels_Addressalert addressalert;

  final AppLocalizations_Labels_Bookingalert bookingalert;

  final AppLocalizations_Labels_SendQueryalert sendQueryalert;

  final AppLocalizations_Labels_Couponalert couponalert;

  final AppLocalizations_Labels_Ratingalert ratingalert;

  final AppLocalizations_Labels_OrderAlert orderAlert;

  final AppLocalizations_Labels_LocationAlert locationAlert;

  final AppLocalizations_Labels_Trackingpage trackingpage;

  final AppLocalizations_Labels_Polylinepage polylinepage;

  final AppLocalizations_Labels_HelpandSupport helpandSupport;

  final AppLocalizations_Labels_Notificationpage notificationpage;

  final AppLocalizations_Labels_Filepickerpage filepickerpage;

  final AppLocalizations_Labels_Faqpage faqpage;

  final AppLocalizations_Labels_Bottomappbar bottomappbar;

  final AppLocalizations_Labels_Favouritepage favouritepage;

  final AppLocalizations_Labels_Invitefriendspage invitefriendspage;
}

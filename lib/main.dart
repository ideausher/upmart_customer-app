import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:user/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:user/modules/common/app_config/app_config.dart';
import 'package:user/modules/common/base_api/base_api.dart';
import 'package:user/modules/common/utils/firebase_messaging_utils.dart';
import 'package:user/routes.dart';
import 'localizations.dart';
import 'modules/auth/page/initialization/page/initialization_page.dart';
import 'modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';

Future<void> main() async {

  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  var _appBloc = AuthBloc(isLoading: true);
  var configuredApp = new AppConfig(
    appName: "Upmart Customer",
    apiBaseUrl:"http://iphoneapps.co.in:9074/upmart/public/api/",
    termsUrl: "http://iphoneapps.co.in:9074/upmart/public/terms-and-policy",
    baseApi: BaseApi(navigatorKey),
    child: BlocProvider<AuthBloc>(

      bloc: _appBloc,
      child: MyApp(
        defaultLocale: Locale("en"),
        appBloc: _appBloc,
        navigatorKey: navigatorKey,
      ),
    ),
  );

  runApp(configuredApp);
}

class MyApp extends StatefulWidget {
  var defaultLocale = Locale("en");
  var appBloc;
  final GlobalKey<NavigatorState> navigatorKey;

  @override
  _MyAppState createState() => _MyAppState();

  MyApp({this.defaultLocale, this.appBloc, this.navigatorKey});
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils?.firebaseMessagingUtils?.initFirebaseMessaging();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: Routes.routes(),
      navigatorKey: widget.navigatorKey,
      debugShowCheckedModeBanner: false,
      locale: widget.defaultLocale,
      // <- Current locale
      localizationsDelegates: [
        const AppLocalizationsDelegate(), // <- Your custom delegate
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.languages.keys.toList(),
      title: 'Upmart Customer',
      theme: AppConfig.of(context).themeData,
      home: InitializationPage(widget.appBloc),
    );
  }
}
